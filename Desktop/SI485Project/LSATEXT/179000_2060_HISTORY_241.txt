Department of history University of Michigan

History 241

Fall 2015

SYLLABUS
Course Title: America and Middle Eastern Wars
Class Schedule: Lec TTH 2:30-4:00 Office Hours: Tues. 4-5 at Center for Middle East, Suite 3603, 1080 South University Avenue & by Appt. Instructor: Juan Cole Telephone: 764-6305; e-mail: j r c o l e /at/ u m i c h /dot/ e d u
This course treats the impact and experience of warfare on the Middle East in the last third of the 20th Century and into the 21st. We will examine how the Cold War, Afghanistan, the Gulf War, the Iraq War and the War on Terror have shaped the modern MiddleEast. We will consider political and social history as well as the military aspect. We will also look at the causes for Washington policy-making toward the region. Grading is based upon a midterm and a final.
Required texts: (Available at Ulrich's and at Reserve Reading Room, 3rd Floor, Shapiro Undergraduate Library).

Optional coursepack at Dollar Bill's, 611 Church St.
Texts:
Michael Axworthy, Revolutionary Iran: A History of the Islamic Republic (New York: Oxford University Press, 2013). Toby Dodge, Iraq: From War to a New Authoritarianism. (London: International Institute for Strategic Studies, 2012) Seth G. Jones, In the Graveyard of Empires: America's War in Afghanistan (New York: W.W. Norton, 2010) Phebe Marr, The Modern history of Iraq (Boulder, Co.: Westview, 3rd edn. 2011).
Class Sessions
BACKGROUND

Sept. 8 Orientation
THE COLD WAR
Sept. 10 The Cold War and the Middle East
John C. Campbell, "The Soviet Union and the United States in the Middle East," Annals of the American Academy of Political and Social Science, Vol. 401, America and the Middle East (May, 1972), pp. 126-135 JSTOR Stable URL
Marr, Chapters 5, 6 Douglas Little, "The Making of a Special Relationship: The United States and Israel, 1957-68." International Journal of Middle East Studies, Vol. 25, No. 4. (Nov., 1993), pp. 563585. JSTOR Stable URL
Sept. 15 The Islamic Revolution in Iran and Regional Implications Axworthy, Revolutionary Iran, chapters 1-3 Judith Palmer Harik, "Between Islam and the System: Sources and
Implications of Popular Support for Lebanon's Hizballah," The Journal of Conflict Resolution, Vol. 40, No. 1 (Nov., 1996), pp. 41-67. JSTOR Stable URL Sept. 17 Afghanistan Wars and the Fall of the Soviet Union
Nick Cullather, "Damming Afghanistan: Modernization in a Buffer State," The Journal of American History, Vol. 89, No. 2, (Sep., 2002), pp. 512-537,JSTOR Stable URL Jones, Graveyard, chapters 1-2
Bruce W. Jentleson, "The Reagan Administration and Coercive Diplomacy." Political Science Quarterly, Vol. 106, No. 1 (Spring, 1991), pp. 57-82. JSTOR Stable URL Sept. 22 Reagan and The Saudi Alliance
Harvey Sicherman, "King Fahd's Saudi Arabia," Orbis Vol. 55, Issue 3. Date: 2011 Pages: 481-488.
Jonathan Marshall, "Saudi Arabia and the Reagan Doctrine," Middle East Report, No. 155, (Nov. - Dec., 1988), pp. 12- 17
William Ochsenwald , "Saudi Arabia and The Islamic Revival," International Journal of Middle East Studies, Vol. 13, No. 3. (Aug., 1981), pp. 271-286. [Or keyword search title at Jstor) Sept. 24 The United States and the Iran-Iraq War
Axworthy, Revolutionary Iran, ch. 4, ch. 5 to p. 295 Marr, Chapters 7-8

Jack Colhoun, "Washington Watch: How Bush Backed Iraq." Middle East Report, No. 176, Iraq in the Aftermath. (May - Jun., 1992), pp. 3537 JSTOR Stable URL HYPERPOWER AMERICA AND THE MIDDLE EAST CHALLENGE
Sept. 29 The Kuwait Crisis in a Post-Soviet Context
Marr, Chapter 9, first half
Oct. 1 The Gulf War, 1991 Marr, Iraq, Chapter 9, second half Axworthy, Revolutionary Iran, pp. 307-313 Grant T. Hammond, "Myths of the Gulf War," Airpower
Journal 12.3 (Fall 1998): 6-18. Proquest. Fred Halliday, "The Gulf War and Its Aftermath: First Reflections,"
International Affairs (Royal Institute of International Affairs 1944-) Vol. 67, Issue 2. Date: 04/01/1991 Pages: 223-234. Jstor
Oct. 6 The Rise of the Jihadis and the Beginnings of al-Qaeda
Ziad Munson, "Islamic Mobilization: Social Movement Theory and the Egyptian Muslim Brotherhood," The Sociological Quarterly, Vol. 42, No. 4 (Autumn, 2001), pp. 487-510 Stable JSTOR URL
William E. Shepard, "Sayyid Qutb's Doctrine of 'Jahiliyya'," International Journal of Middle East Studies, Vol. 35, No. 4 (Nov., 2003), pp. 521-545 Stable JSTOR URL
Saad Eddin Ibrahim. "Egypt's Islamic Activism in the 1980s." Third World Quarterly, Vol. 10, No. 2, Islam & Politics. (Dec., 1988), pp. 632-657.
Ann M Lesch "Osama Bin Laden: Embedded in the Middle East crises," Middle East Policy, Washington: Jun 2002. Vol. 9, Iss. 2; pg. 82, 10 pgs Stable Proquest URL
Oct. 8 Warlords and Taliban in Afghanistan
Jones, Graveyard, chapters 3-4 THE WAR ON TERROR
Oct. 13 Al-Qaeda and September 11

Jones, Graveyard, chapter 5 Robert S. Snyder, "Hating America: Bin Laden as a Civilizational Revolutionary," The Review of Politics, Vol. 65, No. 4 (Autumn, 2003), pp. 325-349JSTOR Stable URL
Oct. 15 Class Review
Oct. 20 Fall Break
Oct. 22 Midterm
Oct. 27 The Unseating of the Taliban in 2001-2003
Jones, Graveyard, chapters 6-8
Oct. 29 The Build-Up to the Iraq War Amy Gershkoff and Shana Kushner, "Shaping Public Opinion: The
9/11-Iraq Connection in the Bush Administration's Rhetoric," Perspectives on Politics, Vol. 3, No. 3 (Sep., 2005), pp. 525537 JSTOR Stable URL
Alex Callinicos, "Iraq: Fulcrum of World Politics," Third World Quarterly, Vol. 26, No. 4/5, (2005), pp. 593-608 JSTOR Stable URL
Sukumar Muralidharan, "Intelligence, Incompetence and Iraq: Or, Time to Talk of Democracy, Demography and Israel," Social Scientist, Vol. 32, No. 11/12 (Nov. - Dec., 2004), pp. 21-80 JSTOR Stable URL
John Mearsheimer and Stephen Walt, "The Israel Lobby," London Review of Books, 23 March 2006
Nov. 3 The US Invasion of Iraq Timothy Garden, "Iraq: The Military
Campaign," International Affairs Vol. 79, No. 4 (Jul., 2003), pp. 701-718 JSTOR Stable URL
Marr, pp. 281-308 Eugene J Palka, Francis A Galgano, Mark W Corson. "Operation Iraqi Freedom: A Military Geographical Perspective," Geographical Review. Jul 2005. Vol. 95, Iss. 3; pg. 373, 27 pgs Stable Proquest URL
Nov. 5 The Sunni Arab Guerrilla Insurgency Grows, 2003-2004

Toby Dodge, Chapter 1 Steve Negus, "The Insurgency Intensifies," Middle East Report, No. 232 (Autumn, 2004), pp. 22-27 Stable Jstor URL Marr, Iraq, Chapter 10, first half Nov. 10 The Rise of the Mahdi Army Dodge, Iraq, chapter 2 Juan Cole, "The Iraqi Shiites: On the history of Americas would-be allies," Boston Review, Fall, 2003 Marr, Iraq, Chapter 10, second half Nov. 12 Shiite-Ruled Iraq and Civil War Juan Cole, "The Ayatollahs and Democracy in Iraq" ISIM Paper no. 7 (Amsterdam: Amsterdam University Press, 2006). Marr, Iraq, Chapter 11, first half Nov. 17 The Kurdistan Question and Post-Baath Iraq Michiel Leezenberg, "Iraqi Kurdistan: Contours of a Post-Civil War Society," Third World Quarterly, Vol. 26, No. 4/5, (2005), pp. 631647 JSTOR Stable URL Gareth Stansfield, Liam Anderson, "Kurds in Iraq: The Struggle between Baghdad and Erbil," Middle East Policy, Washington: Spring 2009. Vol. 16, Iss. 1; pg. 134ff, 12 pgs. Stable Proquest URL International Crisis Group, Iraq and the Kurds: Trouble along the Trigger Line , 8 July 2009. Juan Cole, Are Leftist, Feminist Kurds About to Deliver the Coup de Grâce to ISIL in Syria? The Nation June 26, 2015. Nov. 19 Surge, al-Maliki, SOFA and the End of American Iraq Dodge, Iraq, chapters 3-5 Marr, Iraq, Chapter 11, second half
Nov. 24 The US vs. Iran: Nuclear Issue and Iranian Middle East Expansion
Axworthy, Revolutionary Iran, chapter 7 Thijs Van de Graaf, "The 'Oil Weapon' Reversed? Sanctions Against Iran and U.S.‐EU Structural Power," Middle East Policy Vol. 20, Issue 3. Date: 10/01/2013 Pages: 145-163. W. Andrew Terrill, Iran's Strategy for Saving Asad The Middle East Journal 69.2 (Spring 2015): 222-236.
Dec. 1 The Resurgence of the Taliban Jones, Graveyard, chapters 9-15
Dec. 3 Obama and Counter-Insurgency in Afghanistan and Pakistan

Jones, Graveyard, Chapters 16-18 Rathnam Indurthy, "The Obama Administration's Strategy in
Afghanistan and Pakistan," International Journal on World Peace 28. 3 (Sep 2011): 7-52.Proquest stable URL
Steve Niva, "Disappearing violence: JSOC and the Pentagon’s new cartography of networked warfare," Security Dialogue Vol. 44, Issue 3. Date: 06/2013 Pages: 185-202.
Richard W. Weitz, "Transition in Afghanistan," Parameters 43.3 (Autumn 2013): 29-41.
Muhammad Mazhar and Naheed Goraya, "An Analytical Study of Pak-Us Relations: Post Osama (2011-2012) South Asian Studies 27. 1 (Jan-Jun 2012): 77-87. Proquest Stable URL
Michael Spangler, Pakistan's Changing Counterterrorism Strategy: A Window of Opportunity? Parameters 44.1 (Spring 2014): 37-49
Dec. 8 The Rise of the "Islamic State" group and the Return of Troops to Iraq
Kristina Daugirdas and Julian Davis Mortenson, United States Deepens Its Engagement with ISIL Conflict The American Journal of International Law 109.1 (Jan 2015): 199-211.
Dec. 10 Review Toward Exam
Final Exam: Friday, December 18 4:00 pm - 6:00 pm
Academic Integrity Policy: History 241 follows the academic integrity guidelines set forth by the College of LSA and the History Department. Students should familiarize themselves with both of these documents, which explain the standards of academic integrity and clarify the prohibited forms of academic misconduct. Students in History 241 should utilize the Chicago Manual of Style Online for all issues of source citation, along with any specific guidelines provided in the course assignments. Clarifying the disciplinary standards of research ethics and source citation is part of the educational mission of this course, and students should consult the faculty instructor and/or GSI regarding any questions. The penalties for deliberate cases of plagiarism and/or other forms of academic misconduct are a failing grade on the

assignment. Cases that the instructor judges to be particularly serious, or those in which the student contests the charge of academic misconduct, will be handled by the office of the Assistant Dean for Undergraduate Education. All cases of deliberate academic misconduct that result in formal sanctions of any kind will be reported to the dean’s office, as required by LSA policy, which also ensures due process rights of appeal for students.
Return to Juan R.I. Cole Syllabi.
WebMaster: Juan R.I. Cole

