EARTH 115: THE EMERALD PLANET Instructor: Dr. Selena Y. Smith; email: sysmith@umich.edu; office: 3508 CC Little

Classroom: Nat Sci Aud

Time: T, Th 11am–12 pm, 8 Sept to 22 Oct 2015

Office hours: T 12–1 pm or by appointment. Emails will be answered during normal working hours (Monday to Friday, 8 am–6 pm).

Course description: This minicourse explores the major events in the co-evolution of plants and the Earth. Topics include: how plants moved onto land, the rise of the first
forests, the invention of flowers and their impact on animals, and how plants bring about and respond to environmental change. These events set the stage for the world we live in,
with impacts on how the planet and terrestrial life function.

Textbooks: There are no required textbooks. Lecture slides will be posted on Canvas, but this is not a substitute for attending class.

Group study: Post questions relating to course material on the discussion board in Canvas. I will check it regularly, and encourage students to contribute and post answers. In addition, there will be a class google doc for students to collaboratively create a glossary/study aid, which I will also check regularly.

Grading: Your course grade will be based on three study quizzes (40%) and three assessment quizzes (60%). Quizzes will be administered online in the Quizzes section of Canvas. See the schedule and check Canvas for dates that each quiz is open and due. No quizzes can be repeated.

Study Quizzes: There will be three study quizzes. These are open-note, and designed as
study aids. These are untimed but must be completed by the due date (the day before the next study quiz opens).

Assessment Quizzes: There will be three assessment quizzes. These will have a preset time limit and will be due the following Monday by noon. Quizzes are to be done
individually. You may use your notes from lecture but no other resources (e.g., Google or other web searches). **Note that once the quiz is started, it cannot be paused or stopped,
so make sure you have a good internet connection and are distraction-free before you start the quiz!**

Academic misconduct: Cheating or plagiarizing will result in a grade of 0 (zero) for the piece of work involved (the entire assignment or exam) and may be sent to the Assistant
Dean of Student Academic Affairs. Review LSA's definitions of academic misconduct: www.lsa.umich.edu/academicintegrity/examples.html.

Letter grades will be assigned using the grade scale below:

Percentage A+ A AB+ B B-

Letter Grade
≥96.6 ≥93.3 ≥90.0 ≥86.6 ≥83.3 ≥80.0

Percentage C+ C CD+
D DE

Letter grade
≥76.6 ≥73.3 ≥70.0 ≥66.6
≥63.3 ≥60.0 <60.0

Date

Tentative Schedule Topic

Part 1: Introduction to plants

8 Sept Lecture 0: Introduction

10 Sept Lecture 1: Diversity of plants

15 Sept Lecture 2: Leaves

17 Sept Lecture 3: Roots

22 Sept Lecture 4: Flowers & seeds

Part 2: Plants and our planet

24 Sept Lecture 5: The greening of the Earth

29 Sept Lecture 6: Forests

1 Oct

Lecture 7: Grasslands

6 Oct

Lecture 8: Extremophiles

Part 3: Plants and other organisms

8 Oct

Lecture 9: Plant communication

13 Oct Lecture 10: Plant coevolution with animals

15 Oct Lecture 11: Plants and human history

20 Oct no class - Fall break

22 Oct Lecture 12: Plants and the future

Quizzes Study Quiz 1 open Asses. Quiz 1 (lec 0-4) Study Quiz 2 open Asses. Quiz 2 (lec 5-8) Study Quiz 3 open
Asses. Quiz 3 (lec 9-12)

A one-time extension of 48 hours may be given for a valid and documented reason that the assessment quiz cannot be completed within the given time; request must be made before the quiz closes.
Students requiring special accommodations for taking exams should give me a copy of official documentation 48 hours before the first assessment quiz.
Students on university-approved travel will be accommodated if it affects any assessment quiz due dates; please provide the necessary documentation at least 48 hours prior to an affected quiz.

