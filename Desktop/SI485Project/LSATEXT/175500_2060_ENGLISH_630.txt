English 630/Education 621: Literacy as Cultural Practice
Fall 2015
Professor Anne Ruggles Gere
Office Hours: Th 2:30-4:00 Phone: SCW 734-936-3144; home 734-668-8352
Email: argere@umich.edu
This course considers literacy as a cultural practice in the United States, ranging from the Colonial period to the present. It begins from the assumption that literacy is a dynamic term, continually shifting as its sponsors and participants negotiate with and contribute to its multiple meanings. We will consider how various constructions of literacy have contributed to and complicated nation building, and how these variations have impacted various populations and institutions within the nation. We will explore historical dimensions of literacy, its connection with an alphabetic approach to learning and its multiple representations.. We will look at how various technologies associated with literacy create, distribute and preserve information. And throughout the course we will consider the representations of our own literacies and those of others.
Course Texts:
Schmandt-Besserat, Denise. How Writing Came About. Texas, 1997
Ong, Walter. Literacy and Orality. Routledge, 2012.
Street, Brian. Literacy in Theory and Practice. Cambridge, 1985.
Heath, Shirley Brice. Ways with Words: Language, Life and Work in Communities and Classrooms. Cambridge, 1983.
Crain, Patricia. The Story of A: The Alphabetization of America. Stanford, 2000
Prendergast, Catherine. Literacy and Racial Justice: The Politics of Learning after Brown v. Board of Education. Southern Illinois, 2003.
Cushman, Ellen. Cherokee Syllabary: Writing the People’s Perserverance. Oklahoma, 2013.
Feigenbaum, Paul. Collaborative Imagination: Earning Activism through Literacy Education. Southern Illinois, 2015.
Brandt, Deborah. The Rise of Writing: Redefining Mass Literacy. Cambridge, 2015.

Scenters-Zapico, John. Generaciones’ Narratives: The Pursuit and Practice of Traditional and Electronic Literacies on the US-Mexico Borderlands. Utah State, 2010 http://ccdigitalpress.org/ebooks-and-projects/generaciones
Goldblatt, Eli. Writing Home: A Literacy Autobiography Southern Illinois, 2012

Articles are available on Canvas

Calendar of Class Readings

Date 9/17
9/24

Topic Origins of writing

Reading/Writing/Presenting Read: Schmandt-Besserat, Denise. How Writing Came About Texas, 1997 Book Discussion Leader:

Due Dates

Pilcher, “Earliest Handwriting Found?”
Martinez, “Oldest Writing in the New World”

Midnight 9/16: 3 questions to Anne and BDL

Schmandt-Beserat, “The Earliest Percurser of Writing”

Millard, “Infancy of the Alphabet”

Culture Watch:

Literacy and orality

Read: Ong, Walter. Literacy and Orality. Routledge, 2012. Book Discussion Leader:
Goody, “The Consequences of Literacy”

Midnight 9/23: 3 questions to Anne and BDL

10/1 10/8 10/15

Olson, “From Utterance to Text”

Ong “Writing is a Technology that Structures Thought”

Culture Watch:

Responses to the “great divide”

Read: Street, Brian. Literacy in Theory and Practice. Cambridge 1985. Book Discussion Leader:

Scribner, “Literacy in Three Metaphors”

Midnight 9/30: 3 questions to Anne and BDL

Stevens, “A Critical Discussion of the ‘New Literacy Studies’”

Street, “What’s New in New Literacies”

Culture Watch:

New literacies in action

Read: Heath, Shirley Brice. Ways with Words.: Language, Life and Work in Communities and Classrooms. Cambridge, 1983. Book Discussion Leader:
Heath, “What No Bedtime Story Means”

Midnight 10/7: 3 questions to Anne and BDL

Purcell-Gates, “Literacy Worlds of Children”

Vieira, “Undocumented in a Documentary Society”

Culture Watch:

Read:

10/22 10/29

Emerging concepts of literacy

Brandt, Deborah. The Rise of Writing: Redefining Mass Literacy. Cambridge, 2015. Book Discussion Leader:

Brandt, “Literacy Learning and Economic Change”

Brandt, “Sponsors of Literacy”

Brandt & Clinton, “Limits of the Local”

Culture Watch:

Access to literacy

Read: Prendergast, Catherine. Literacy and Racial Justice: The Politics of Learning after Brown v. Board of Education. Southern Illinois, 2003. Book Discussion Leader:

Miller, “Plymouth Rock Landed on Us”

Midnight 10/21: 3 questions to Anne and BDL

Prendergast, “The Economy of Literacy”

MacDonald, “Emissaries of Literacy”

Access and definitions

Culture Watch:

Read: Cushman, Ellen. Cherokee Syllabary: Writing the People’s Perserverance. Oklahoma, 2013. Book Discussion Leader:

Midnight 10/28: 3 questions to Anne and BDL

Cushman, “Wampum, Sequoyan, and Story”

11/5 11/12

Responses to Access Issues
Literacy and alphabets

Candeleria, “Misperspectives on Literacy”

Culture Watch:

Read: Feigenbaum, Paul. Collaborative Imagination: Earning Activism through Literacy Education. Southern Illinois, 2015. Book Discussion Leader:
Feigenbaum, “Rhetoric, Mathematics and the Pedagogies We Want”
Goldblatt, “Alinsky’s Revielle”

Midnight 11/4: 3 questions to Anne and BDL
Nov 5 (or earlier):
Two “Why I Added this Book to My Library”

Culture Watch:

Read: Crain, Patricia. The Story of A: The Alphabetization of America. Stanford, 2000. Book Discussion Leader:

11/14 (Saturday)

Crain, “Introduction” to Reading Children: Literacy, Property, and the Dilemmas of Childhood in NineteenthCentury America.
Monoghan, “Literacy Instruction and Gender”
Culture Watch:

Send proposal for final paper Nov 14:

to Anne by midnight

Proposal due

11/19 12/3 12/10

Digital literacies

Read: Scenters-ZapicoGeneraciones’ Narratives: The Pursuit and Practice of Traditional and Electronic Literacies on the USMexico Borderlands. Utah State, 2010 http://ccdigitalpress.org/ebooksand-projects/generaciones Book Discussion Leader:

Vee, “Understanding Computer Programming”

Comer, “Pedagogy of the Digital Archive”

Culture Watch:

Literacy narratives

Read: Goldblatt, Eli. Writing Home: A Literacy Autobiography Southern Illinois, 2012 Book Discussion Leader:

Dec 3 10 page draft of final paper due

Pennell, (Re)Placing the Literacy Narrative: Composing on Google Maps”

Culture Watch:
Bring Draft of Final Paper to Class

Perspectives on literacy

Ignite-style presentations of individual semester projects

Letter to paper author due

Final Paper Due: December 17

Assignments for Writing/Presenting
Three Questions: Prior to each class (by Wednesday at midnight) send to Anne and the Book Discussion Leader of the day three questions that arise from the readings. These should be questions that will lead to probing conversations about the issues addressed in the readings. As the semester goes on, your questions may well loop back to issues discussed earlier.
Culture Watch: Because this course is concerned with literacy practices in our nation, you will have an opportunity to make a brief (10 minute) presentation on a current representation of literacy. You may do this anytime during the semester. In addition to selecting and presenting the representation, you should be prepared to explain connections between this representation and class readings/discussions.
Book Discussion Leader: Each week one member of the class will be responsible for reading the scheduled book-length text and preparing to lead a discussion of it that makes connections with the other readings for the day at the same time that it makes clear the “rest of the story” offered by the book-length text. The designated leader will meet with Anne, prepare a lesson plan, incorporate classmates’ questions into the discussion, and, after the class, give Anne a copy of the plan along with a brief reflection on the enactment of the plan.
Why I Added This Book To My Library: As you build your professional library you make choices about what to include. Write a brief (500 word) explanation of why you have added a particular literacy-focused book to your collection. In your explanation explain a) what argument(s) it helps you make and/or what it contributes to your thinking about literacy; b) how it compares with or connects to other literacy-focused reading you have been doing; c) what new ideas/ways of thinking it offers. Two of these are due anytime on or before November 5.
Final Paper: This major project will give you an opportunity to explore an aspect of literacy of particular interest to you. It should be 20-25 pages long and should be developed throughout the term. Ideally this paper will be one you can revise to submit for publication. To facilitate this, you will produce successive versions to which the instructor and a peer will respond. A proposal for the final paper should be posted to me by midnight Saturday, November 14, and I will meet with you to discuss it with you during a conference that week. The proposal should include: an explanation of your general area of interest; a statement of the specific question you want to consider; a rationale for the paper; and a brief bibliography. The proposal should be about five pages long.
A draft of the final paper is due December 3. At this stage you should have at least 15 pages written and the overall shape of the paper should be clear. At this stage in

the process, it will be fine, however, to insert statements like WILL FILL IN LATER . Bring two copies of this paper to class, one for Anne, and one for the peer who will also read and respond to your draft. On December 10 bring to class two copies of a letter of advice for the author of the paper you have read, one for your peer and one for Anne. You should also bring your peer’s paper with your marginal notes, comments and suggestions.
The final version will be due Thursday, December 17. Earlier is fine.
Grades: Because I assume that you will come to class every day prepared by careful reading to contribute to substantive conversations, that you will do an excellent job on short writing and presentations, and that you will write a smashingly good final paper, it seems beside the point to talk about grades in this course. But in case they are helpful, here are some percentages that suggest the relative worth of various assignments: Culture Watch=5%; Why I Added This Book to My Library papers=20%; Three Questions and class participation=20%; and final paper (including proposal, draft, response to another’s draft, and oral presentation)=55%.
Future Conferences:
Your final paper may well provide the basis for a conference presentation, and so that you can start thinking ahead to conferences to which you might submit proposals, here is a list of some of the conferences you might want to consider. Please add to this list as you learn about additional conferences.
October 10, 2015: Writing in a Digital Environment (WIDE) Conference, East Lansing
https://sites.google.com/site/wideemu15/
October 30, 2015: Michigan Council of Teachers of English, East Lansing
http://mcte.info/item/996014
November 19-22, 2015: National Council of Teachers of English, Mineapolis
http://www.ncte.org/annual
December 2-5, 2015: Literacy Research Association, Carlsbad, CA
http://www.literacyresearchassociation.org/index.php?option=com_content&view =article&id=115

January 7-10, 2016: Modern Language Association, Austin https://www.mla.org/convention_services February 5-7, 2016: National Council of Teachers of English Assembly for Research (NCTEAR), Ypsilanti, http://www.nctear.org
March
April 6-9, 2016: College Composition and Communication, Houston http://www.ncte.org/cccc April 8-12, 2016: American Educational Research Association, Washington DC http://www.aera.net/EventsMeetings/AnnualMeeting/2016AnnualMeeting/tabid/ 15862/Default.aspx
May 19-23, 2016: Computers and Writing, Rochester, NY https://www.facebook.com/CandWCon
May 27-29, 2016: Rhetoric Society of America, Atlanta http://rhetoricsociety.org/aws/RSA/pt/sp/conferences June 23-26, 2016: International Writing across the Curriculum Conference, Ann Arbor https://iwac2016.org/cfp/ July 10-17: Council of Writing Program Administators, Raleigh NC http://wpacouncil.org/taxonomy/term/1551

