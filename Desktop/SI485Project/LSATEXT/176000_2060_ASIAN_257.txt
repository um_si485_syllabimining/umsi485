ASIAN257	  Great	  Cities	  in	  Asia	  
	   Syllabus	  	  
	  
Day/Time:	  MW11:30am-­‐1:00pm	  
Location:	  2306MH	   Instructor:	  Prof.	  Juhn	  Ahn	  
Office:	  202	  S.	  Thayer	  Building,	  Suite	  5145	  
Office	  hours:	  T	  4:00-­‐5:30pm	  
Email:	  jahn@umich.edu	   	  
Prerequisites:	  none	  
	  
Description:	  	   	  
This	  course	  will	  serve	  as	  an	  introduction	  to	  the	  history,	  sociology,	  and	  culture	  of	  a	  number	  
of	  great	  cities	  in	  Asia:	  Tokyo,	  Seoul,	  Pyongyang,	  Beijing,	  Shanghai,	  and	  Hong	  Kong.	  In	   addition	  to	  being	  the	  most	  populous	  cities	  in	  the	  world	  these	  great	  cities	  are	  also	  the	  
world’s	  most	  creative	  producers	  of	  new	  music,	  art,	  films,	  food,	  and	  dazzling	  consumer	  goods	  
whose	  impact	  is	  felt	  all	  over	  the	  world.	  This	  course	  will	  focus	  on	  a	  number	  of	  key	  issues	  that	  
are	  critical	  to	  making	  sense	  of	  these	  cities:	  industrialization,	  modernization,	   cosmopolitanism,	  poverty,	  class,	  gender,	  suburbanization,	  alienation,	  urban	  culture,	  
pollution	  etc.	  Students	  will	  be	  asked	  to	  explore	  these	  and	  other	  issues	  by	  applying	  a	  context-­‐
sensitive	  reading	  and	  critical	  analysis	  of	  various	  materials	  such	  as	  movies,	  music,	  art,	  maps,	  
street	  art,	  architecture,	  anime,	  comic	  books,	  novels,	  photos,	  and	  academic	  literature.	  There	   are	  no	  prerequisites,	  but	  some	  background	  in	  the	  history	  and	  culture	  of	  Asia	  is	  
recommended.	  	  
	  
Grading	   	  
Requirements	  include	  three	  quizzes,	  active	  participation	  in	  Friday	  discussion	  sections,	  a	  
mock	  fellowship	  application,	  and	  a	  final	  creative	  project.	  Late	  or	  missed	  assignments	  and	   quizzes	  may	  not	  be	  made	  up	  without	  a	  documented,	  official	  medical	  excuse.	  No	  exceptions.	  
As	  for	  attendance,	  no	  more	  than	  five	  unexcused	  absences	  are	  allowed.	  For	  all	  inquiries	  
concerning	  grading,	  the	  student	  must	  schedule	  an	  appointment	  and	  consult	  the	  instructor	  
during	  office	  hours	  and	  not	  before,	  during,	  or	  after	  class.	   	  
Final	  creative	  project:	   	   	   	   	   	   	   25%	  
3	  Quizzes	  (10%	  each):	  	   	   	   	   	   	   30%	  
Fellowship	  application:	   	   	   	   	   	   20%	   Peer-­‐review	  paper:	   	   	   	   	   	   	   5%	  
Attendance	  and	  participation:	   	   	   	   	   	   20%	  
	  
For	  the	  fellowship	  application,	  please	  prepare	  the	  following:	  (1)	  an	  outline	  of	  the	  research	   project	  –	  who,	  what,	  when,	  where,	  why,	  and	  how	  of	  what	  you	  propose	  to	  study	  and	  research	  
in	  one	  of	  the	  great	  cities	  in	  Asia,	  (2)	  a	  3-­‐page	  statement	  of	  grant	  purpose	  that	  explains	  the	  
research	  project	  and	  its	  goal(s),	  and	  (3)	  a	  2-­‐page	  personal	  statement	  –	  for	  the	  purposes	  of	   this	  assignment,	  please	  create	  a	  “fictional	  you”	  (“you”	  are	  a	  senior	  at	  the	  University	  of	  
Michigan,	  majoring	  in	  Asian	  Studies,	  with	  a	  4.0	  GPA,	  who	  has	  taken	  at	  least	  30	  credits-­‐worth	  
of	  courses	  related	  to	  either	  China,	  Korea,	  or	  Japan).	  Please	  do	  not	  use	  your	  actual	  personal	  

information	  for	  the	  personal	  statement.	  In	  discussion	  section,	  you	  will	  be	  asked	  to	  perform	   a	  peer-­‐review	  of	  another	  student’s	  statement	  of	  grant	  purpose	  and	  personal	  statement.	   These	  peer-­‐reviews	  must	  be	  2	  pages	  (max.)	  and	  submitted	  by	  Oct.	  26.	  Please	  prepare	  a	  first	   draft	  of	  the	  peer-­‐review	  paper	  by	  Oct.	  23	  and	  bring	  it	  to	  discussion	  section.	  	   	   First	  drafts	  of	  the	  statement	  of	  grant	  purpose	  and	  personal	  statement	  must	  be	  submitted	  by	   Nov	  2.	  Grading	  of	  the	  fellowship	  application	  will	  be	  based	  on	  creativity	  (25	  points),	   plausibility	  of	  the	  research	  project	  (25	  points),	  clarity	  and	  style	  of	  writing	  (25	  points),	  and	   persuasiveness	  (25	  points).	  Both	  creativity	  and	  plausibility	  of	  the	  project	  will	  be	  measured	   on	  the	  basis	  of	  your	  grasp	  of	  the	  lectures	  and	  assigned	  readings.	   	   All	  written	  assignments	  must	  be	  submitted	  in	  the	  following	  format	  online	  (via	  the	  course	   website	  on	  Canvas):	  12	  point	  font,	  Times	  New	  Roman,	  with	  1	  inch	  margin	  on	  all	  sides.	  No	   exceptions.	   	   For	  the	  final	  creative	  project	  (this	  is	  a	  group	  assignment),	  you	  must	  prepare	  a	  15-­‐minute	   powerpoint	  presentation	  and	  a	  formal	  proposal.	  Please	  identify	  an	  existing	  building	  or	   landmark	  in	  one	  of	  the	  great	  cities	  in	  Asia	  that	  is	  in	  need	  of	  preservation	  or	  reinvention.	  In	   your	  formal	  proposal	  (min.	  2	  pages	  and	  max.	  3	  pages)	  please	  explain	  why	  this	  building	  or	   landmark	  is	  in	  need	  of	  preservation	  or	  reinvention	  and	  how	  you	  propose	  to	  preserve	  or	   reinvent	  it.	  For	  the	  presentation,	  please	  collect	  relevant	  images	  (photos,	  posters,	  postcards,	   stamps,	  drawings,	  maps,	  videos,	  blueprints	  etc.)	  of	  the	  building	  or	  landmark	  and	  perform	  a	   “visual	  reading”	  of	  it:	  how	  does	  its	  shape,	  size,	  height,	  texture,	  weight,	  color,	  materiality,	   spatiality	  and	  so	  on	  affect	  the	  way	  one	  interacts	  with	  its	  history,	  meaning,	  function,	  and	   potentiality?	  After	  the	  visual	  reading,	  please	  use	  powerpoint	  to	  explain	  how	  you	  propose	  to	   preserve	  or	  reinvent	  it.	  You	  are	  encouraged	  to	  use	  multimedia	  for	  these	  presentations.	   Grading	  of	  the	  creative	  project	  will	  be	  based	  on	  creativity	  (25	  points),	  plausibility	  of	  the	   project	  (25	  points),	  depth	  of	  research	  (25	  points),	  and	  persuasiveness	  (25	  points).	   Creativity,	  plausibility,	  and	  persuasiveness	  will	  be	  measured	  on	  the	  basis	  of	  your	  grasp	  of	   the	  lectures	  and	  assigned	  readings:	  only	  context	  can	  explain	  “why”	  a	  building	  or	  landmark	  is	   in	  need	  of	  preservation	  or	  reinvention	  and	  this	  context	  will	  be	  explained	  in	  the	  lectures	  and	   assigned	  readings.	  The	  formal	  proposals	  are	  due	  Dec	  11.	  	   	   Attendance	  and	  participation	  in	  discussion	  sections	  account	  for	  20%	  of	  the	  final	  grade	  for	   this	  course.	  Participation	  will	  be	  graded	  on	  the	  student’s	  contribution	  to	  and	  performance	  in	   short	  thought	  exercises,	  discussions,	  and	  creative	  in-­‐class	  assignments.	   	   Academic	  Integrity	   	   In	  keeping	  with	  the	  LSA	  Community	  Standards	  of	  Academic	  Integrity,	  academic	  misconduct	   in	  this	  course	  will	  be	  taken	  very	  seriously	  and	  will	  result	  in	  disciplinary	  action	  (see	  the	   Office	  of	  the	  Assistant	  Dean	  for	  Student	  Academic	  Affairs’	  Procedures	  for	  Resolving	   Academic	  Misconduct	  in	  LSA).1	  In	  essence,	  taking	  credit	  in	  any	  way	  for	  work	  that	  is	  not	  his	   or	  her	  own	  without	  acknowledging	  the	  source(s)	  counts	  as	  plagiarism	  and	  cheating.	  If	  you	   are	  still	  unsure	  as	  to	  what	  counts	  as	  plagiarism	  or	  cheating	  and	  would	  like	  some	  specific	   advice	  on	  how	  to	  avoid	  it,	  please	  consult	  the	  following	  webpage:	   http://www.lib.umich.edu/academic-­‐integrity/understanding-­‐plagiarism-­‐and-­‐academic-­‐ integrity	  
1 http://www.lsa.umich.edu/academicintegrity/procedures/index.html

	  
	   Required	  Readings	  

	  

All	  required	  readings	  will	  be	  made	  available	  to	  students	  for	  no	  cost	  on	  Canvas	  or	  the	  web.	  

	  

	   Timetable	  

	  
Sep	  9	   	  

	  

Why	  are	  we	  here?	  

	   	  

Sep	  14	   	  

Swamps,	  Shoguns,	  and	  Tour	  of	  Duty	  (sankin	  kōtai)	  

	   	  

	   	   Readings	  

Marshall,	  Richard.	  “Asian	  Megacities.”	  In	  Shaping	  the	  City:	  Studies	  in	  History,	   Theory,	  and	  Urban	  Design,	  ed.	  Edward	  Robbins	  and	  Rodophe	  El-­‐Khoury,	  194-­‐

211.	  New	  York:	  Routledge,	  2004.	  	  

	  
Sep	  16	   	   	  

	  
Sep	  21	   	   	  

	  
Sep	  23	   	  
	   	   	   	  

	   	  	   Sep	  28	   	  

	   	  

Centering	  and	  De-­‐centering	  the	  Great	  City	  Edo	  
Readings	   Yonemoto,	  Marcia.	  “Nihonbashi:	  Edo’s	  Contested	  Center.”	  East	  Asian	  History	   17/18	  (1999):	  49-­‐70.	   	  
	  
Tokyo	  as	  an	  Idea	  
Readings	   Smith,	  Henry	  D.	  II.	  “Tokyo	  as	  an	  Idea:	  An	  Exploration	  of	  Japanese	  Urban	   Thought	  until	  1945.”	  Journal	  of	  Japanese	  Studies	  4,	  no.	  1	  (1978):	  45-­‐53	  
From	  Edo	  to	  Tokyo:	  Meiji	  Restoration,	  War,	  and	  the	  City	  
Readings	   Smith,	  “Tokyo	  as	  an	  Idea,”	  pp.	  53-­‐74	  
Modern	  Tokyo	  
Readings	   Bestor,	  Theodore	  C.	  “Wholesale	  sushi:	  culture	  and	  commodity	  in	  Tokyo’s	   Tsukiji	  market.”	  In	  Theorizing	  the	  city:	  the	  new	  urban	  anthropology	  reader,	   ed.	  Setha	  Low,	  201-­‐242.	  New	  Brunswick:	  Rutgers	  University	  Press,	  1999.	  

	   	  

Sep	  30	   	   	  
	   	   	   	  
	   	  
	   Oct	  5	   	   	   Oct	  7	   	   	   	   	  
	  
	   Oct	  12	   	  
	   Oct	  14	   	   	   	   	  
	  
	   Oct	  19	   	   	   Oct	  21	   	   	  
	   Oct	  26	   	   	   	   	   	  
	  

From	  Godzilla	  to	  Akira:	  Dreaming	  of	  the	  Future	  of	  Tokyo	  
Screening	   “Akira”	  (1988)	  
Readings	   Murakami,	  Haruki.	  "Superfrog	  Saves	  Tokyo"	  In	  After	  the	  Quake:	  Stories.	  New	   York:	  Vintage	  Books,	  2002.	   	  
	  
Quiz	  
The	  New	  Chosŏn	  Capital	  
Readings	   Henry,	  Todd.	  “Respatializing	  Chosŏn's	  Royal	  Capital.”	  In	  Sitings:	  Critical	   Approaches	  to	  Korean	  Geography,	  ed.	  Tangherlini	  Timothy	  R.	  and	  Sallie	  Yea,	   15-­‐38.	  Honolulu:	  University	  of	  Hawaii	  Press,	  2008.	  

	  
From	  Hansŏng	  to	  Keijō	   	   Readings	   Lonely	  Planet,	  Seoul	  (Selections)	  
Colonial	  City	  Keijō	  
Readings	   Pai,	  Hyung-­‐il.	  “Navigating	  Modern	  Keijō:	  The	  Typology	  of	  Reference	  Guides	   and	  City	  Landmarks,”	  Sŏulhak	  yŏn’gu	  44	  (2011):	  1-­‐40	  

No	  class	   No	  class	  

	  

Deconstructing	  Keijō/Constructing	  Seoul	  

	  

Readings	   Jin,	  Jong-­‐Heon.	  “Demolishing	  Colony:	  The	  Demolition	  of	  the	  Old	  Government	  
General	  Building	  of	  Chosŏn.”	  In	  Sitings:	  Critical	  Approaches	  to	  Korean	  
Geography,	  ed.	  Timothy	  R.	  Tangherlini	  and	  Sallie	  Yea,	  39-­‐58.	  Honolulu:	   University	  of	  Hawaii	  Press,	  2008.	  

Oct	  28	   	   	  
	   	   	   Nov	  2	   	   	   	   	  
Nov	  4	   	   	  
	   Nov	  9	   	   	   Nov	  11	  	   	   	   	   	   	   	   	  
	   	  

Gangnam	  Style	  
Readings	   Hong,	  Euny.	  “Growing	  Up	  Gangnam-­‐Style:	  What	  the	  Seoul	  Neighborhood	   Was	  Really	  Like.”	  The	  Atlantic,	  Sep	  24,	  2012.	  
First	  draft	  of	  peer	  review	  paper	  —hard	  copy	  —	  due	  in	  discussion	   section,	  Dec.	  30	  

The	  Seoul	  Train	  Station	  and	  the	  Urban	  Poor	  

	  

Readings	   Song,	  Jesook.	  "	  The	  Seoul	  Train	  Station	  Square	  and	  Homeless	  Shelters:	  
Thoughts	  on	  Geographical	  History	  Regarding	  Welfare	  Citizenship.”	  In	  
Sitings:	  Critical	  Approaches	  to	  Korean	  Geography,	  ed.	  Timothy	  R.	  Tangherlini	   and	  Sallie	  Yea,	  159-­‐172.	  Honolulu:	  University	  of	  Hawai‘i	  Press,	  2008.	  

Final	  draft	  of	  peer	  review	  papers	  due	  

	  

Pyongyang	  

Readings	  
Kim	  Suk-­‐Young.	  “Springtime	  for	  Kim	  Il-­‐sung	  in	  Pyongyang:	  City	  on	  Stage,	  
City	  as	  Stage”	  The	  Drama	  Review	  51,	  2	  (2007):	  24-­‐40.	   	  
Optional	  
Kim,	  Jong-­‐il,	  “On	  Architecture.”	  In	  Pyongyang	  Architectural	  and	  Cultural	  
Guide,	  ed.	  Ahn	  Chang-­‐mo	  and	  Christian	  Posthofen.	  Berlin:	  DOM	  publishers,	   2012.	  
	  

	  

Quiz	  

Chinese	  Political	  Capitals	   	  
Readings	  
Johnson,	  “China	  Molds	  a	  Supercity	  Around	  Beijing,”	  New	  York	  Times	  

Logan,	  John	  and	  Susan	  Fainstein,	  “Introduction:	  Urban	  China	  in	  Comparative	   Perspective.”	  Urban	  China	  in	  Transition,	  pp.	  1-­‐23.	  Blackwell	  Publishing,	   2008.

	  

Nov	  16	  	   	  
	  
	  	  
	  	  
	  	   	   	   Nov	  18	  	  
	  
	   	   Nov	  23	  	   	   	   	  
	   Nov	  25	  	   	   	   	   	   	   	  

Beijing	  in	  Time	  and	  Memory	  
Readings	   Selections	  from	  Marco	  Polo	  &	  Rustichello	  of	  Pisa,	  The	  Travels	  of	  Marco	   Polo,	  Volume	  1	  (n.p.:	  n.p.,	  1903).	  
Read	  the	  main	  text	  (no	  notes	  unless	  you	  want)	  to	  the	  following	  very	  short	   chapters:	  
Chapter	  X:	  Concerning	  the	  Palace	  of	  the	  Great	  Kaan	   Chapter	  XI:	  Concerning	  the	  City	  of	  Cambaluc	   Chapter	  XXII:	  Concerning	  the	  City	  of	  Cambaluc,	  and	  its	  Great	  Traffic	   and	  Population	  
The	  reading	  is	  available	  is	  as	  a	  free	  download	  in	  a	  variety	  of	  formats	   (including	  Kindle)	  through	  Gutenberg	  Press.	  Go	  to:	   http://www.gutenberg.org/ebooks/10636	  
Viewings	  from:	  	  Bernardo	  Bertolucci,	  The	  Last	  Emperor	  (1987)	  

Modern	  Beijing	   	   Readings	   Joseph	  Esherick,	  1991.	  “Xi’an	  Spring.”	  In	  Pro-­‐Democracy	  Protests	  in	  China:	   Reports	  from	  the	  Provinces,	  ed.	  Jonathan	  Unger	  (Armonk,	  NY:	  M.E.	  Sharpe,	   Inc.).	  
To	  access	  these,	  go	  to:	   http://www.tsquare.tv/links/ESHERICK.html	   	   Viewings	  from:	  Carla	  Hinton,	  The	  Gates	  of	  Heavenly	  Peace	  (1995)	  

Contemporary	  Beijing	  
Readings	   Gaetano,	  Arianne	  and	  Tamara	  Jacka.	  On	  the	  Move:	  Women	  in	  Rural-­‐-­‐to-­‐-­‐ Urban	  Migration	  in	  Contemporary	  China,	  286-­‐306.	  New	  York:	  Columbia	   University	  Press,	  2004.	  

	  

Thanksgiving:	  no	  class	  

	  

Nov	  30	  	   	   Dec	  2	   	  
	   	   Dec	  7	   	   	   Dec	  9	   	  
	   Dec	  14	   	   	   	   	   	   	  

Quiz	   	  
Colonial	  Chinese	  Cities:	  Shanghai	  and	  Hong	  Kong	   	  	   Assignment:	   	  	   Watch	  the	  interview,	  “Former	  Governor	  Patten	  Assess	  Hong	  Kong	  Post	   1997”:	  	  http://www.youtube.com/watch?v=3knS64M4YU0	   	  	   Read:	  Christopher	  Patten,	  “Comment:	  The	  City	  Will	  Not	  Sleep,”	  Guardian	   (July	  29,	  2007).	  Go	  to:	   http://www.guardian.co.uk/commentisfree/2007/jun/30/comment.china	   	   Readings	   	   So,	  Billy	  K.	  L.	  “British	  Concessions	  and	  Chinese	  Cities,	  1910s-­‐1930s.”	  New	   Narratives	  of	  Urban	  Space	  In	  Republican	  Chinese	  Cities:	  Emerging	  Social,	   Legal,	  And	  Governance	  Orders,	  157-­‐196.	  Leiden:	  Brill,	  2013.	   	  

Presentations	  
Presentations	   	   Formal	  proposals	  for	  creative	  project	  due	  Dec	  11	   	  
Presentations	  

	   	  

