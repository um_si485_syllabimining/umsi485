AMCULT 301.002/LATINOAM 301.001/ENG 407.003 CODESWITCH
A CRITICAL CREATIVE WRITING SEMINAR

Meeting Time: Wednesdays and Fridays, 10:00-11:30 am Meeting Place: The University of Michigan, Ann Arbor, 2401 Mason Hall
Professor: Amy Sara Carroll (amysara@umich.edu) Office Hours: Wednesdays and Fridays, 12-1:00 pm and by appointment, Haven Hall
3527B

In Chicano Poetics: Heterotexts and Hybridities, Alfred Arteaga suggests that for Octavio Paz, “the border is a thin one and across it ‘Even the birds speak English’… What is difficult for Paz is to consider a thick border.” In this course—something in between an academic and creative writing seminar—we will explore thick descriptions of thickening borders. We will address the perils and pleasures of mobilizing a multilingual poetics—laterally—examining written, visual, digital, and cinematic representations of bi- or multicultural subjectivities. Central to our discussion will be the conceit of codeswitching, a linguistic term that designates a speaker or writer’s ability to move between languages. Acknowledging the term’s literal definition, we will acquaint ourselves with work that shuttles between English and Spanish, between Englishes and Spanishes in the plural. In turn, we will dehyphenate “codeswitching” to mix and extend the term’s meaning. We will explore the ideal of difference-as-community, taking into account the merging of the personal and the political, fiction and memoir, prose and poetry, the visual and the verbal, the spoken and the written word in key examples of twentieth and twentyfirst century political and artistic representation. Finally, all of our investigations will lead us back to your work: we’ll invest a portion of this seminar’s energies in the yet-to-beimagined—your interpretations of codeswitching as practice.

COURSE INFORMATION Grade distribution
Class participation Hybrid texts
Peer critiques Final portfolio

20% 25%
25% 30%

Required & recommended texts
Gloria Anzaldúa, Borderlands/La Frontera: The New Mestiza Junot Díaz, The Brief Wondrous Life of Oscar Wao Guillermo Gómez-Peña, Enrique Chagoya, and Felicia Rice, Codex
Espangliensis: From Columbus to the Border Patrol All other course materials will be on CTools or the web.

Class participation For this seminar, I expect you to develop a readerly↔writerly codeswitching practice. To this end, you must attend class, prepared to discuss the week’s assigned texts, your own work, and/or the work of your classmates. Our Wednesday meetings will be devoted to

2
the week’s assigned texts. On Fridays we’ll use your work to remix our seminar conversations. If you miss more than three classes, your grade will be lowered one third of a letter grade for each day missed, i.e., a fourth unexcused absence will result in an “A” student receiving an “A-.” Please advise me at the beginning of the semester of any concerns you might have about our syllabus, course requirements, your learning styles, and/or interests. I consider our seminar to be a work-in-progress and am open to suggestions for revising it. I also would be happy to collaborate with you and the Services for Students with Disabilities Office (http://ssd.umich.edu/) to honor the statute of the Americans with Disabilities Act (ADA). Be advised, however, you are responsible for initiating that conversation.
Hybrid texts (3-4 pages, 5 minutes, or some agreed upon equivalent) Due on or before noon each Tuesday You will submit incrementally a collection of responses to the course’s assigned texts. These responses could and should take many forms: prose, poetry, performance monologues, fragments of a graphic novel, architectural models, hypertext, video, or some combination of the aforementioned... hence, my reference to them as “hybrid texts.” Please distribute these texts via CTools in the dated folders that I created under “Forums” for this purpose. Title your submission as follows: [Your last name], [Your first name] WORKSHOP [#] I will divide you into three groups. At least three times during the semester we will discuss your submissions in class. It is imperative that you submit these texts when they are due so that your peers and I will have adequate time to respond to them.
Peer critiques (1-2 pages, 2 minutes, or some agreed upon equivalent) Due on or before noon each Thursday During the weeks that you are not up for workshop, you will submit brief responses to the work of your classmates who are. These responses, like the hybrid texts themselves, could take many forms, but must remain constructively critical. Ideally, you would point out to your peer the strengths of his/her text, but also the places where you think that the work could be improved. Please post these critiques via CTools under the submissions to which they respond. Title your peer critique as follows: [Your last name], [The last name of your classmate to whose work you are responding] WORKSHOP [#] On Thursday nights, make sure to review the other posted responses to your classmates’ work, too. It is imperative that you submit these critiques when they are due so that we will have time to review them before our Friday workshops.
Final portfolio (≥20 pages, 20 minutes, or some agreed upon equivalent) Due December 11th at our final class meeting At the semester’s close, you will submit a final portfolio that will include both substantial revisions of the hybrid texts that you workshopped previously and new material that you’ve produced. You also will present your portfolio to your classmates at one of our final meetings. I will assign you a portfolio grade for both your portfolio’s contents and your presentation of them. A word to the wise: I will not accept your portfolio as an email attachment.

3
PROPOSED SCHEDULE
WEEK 1
So there I was Soaring like poets are wont To soar Above and beyond th’ trees And the palms Going ZOOM Straight al Sol.
(Que suave ser poeta, jefita!)
September 9 INTRODUCTIONS September 11 Selections from the Nuyorican Poets’ Café (CTools)
WEEK 2
1493: An Arawak brought back from the Caribbean by Columbus is left on display in the Spanish Court for two years until he dies of sadness.
September 16 Coco Fusco, “The Other History of Intercultural Performance” (CTools) and “Still in the Cage: Thoughts on ‘Two Undiscovered Amerindians,’ 20 Years Later” http://www.blouinartinfo.com/news/story/760842/still-in-the-cage-thoughts-on-twoundiscovered-amerindians-20-years-later (We’ll view Couple in a Cage in class.) September 18 WORKSHOP 1
WEEK 3
The U.S.-Mexico border es una herida abierta where the Third World grates against the first and bleeds. And before a scab forms it hemorrhages again, the lifeblood of two worlds merging to form a third country—a border culture… Los atravesados live here: the squint-eyed, the perverse, the queer, the troublesome, the mongrel, the mulato, the half-breed, the half dead; in short, those who cross over, pass over, or go through the confines of the “normal.”
September 23 Gloria Anzaldúa, Borderlands/La Frontera: The New Mestiza (Part I “Atravesando Fronteras/Crossing Borders,” also the “Preface to the First Edition” and the “Introduction to the Fourth Edition”) September 25 WORKSHOP 2
WEEK 4
Como cuero viejo caerá la esclavitud de obedecer, de callar, de aceptar. Como víbora relampagueando nos moveremos, mujercita. ¡Ya verás!
September 30 Gloria Anzaldúa, Borderlands/La Frontera: The New Mestiza continued (Part II “La Pérdida”) October 2 NO CLASS Please do your best to see Diego Rivera’s Detroit Industry murals at the Detroit Institute of Arts.

WEEK 5

4

October 7 Guillermo Gómez-Peña, Enrique Chagoya, and Felicia Rice, Codex Espangliensis: From Columbus to the Border Patrol October 9 UMMA Visit (Meet in the Forum, next to the Museum Store)
WEEK 6

October 14 Cecilia Vicuña, QUIPOem (CTools) Please wear walking shoes and bring cameras (phones are fine) to class. October 16 WORKSHOP 3
WEEK 7
Let me be clear about one thing: disidentification is about cultural, material, and psychic survival. It is a response to state and global apparatuses that employ systems of racial, sexual, and national subjugation.
Tuesday, October 21 José Esteban Muñoz, Disidentifications: Queers of Color and the Performance of Politics and Cruising Utopia: The Then and There of Queer Futurity, selections (CTools) Thursday, October 23 WORKSHOP 4
WEEK 8 October 28 CONFERENCES October 30 CONFERENCES
WEEK 9
They say it came first from Africa, carried in the screams of the enslaved; that it was the death bane of the Tainos, uttered just as one world perished and another began; that it was a demon drawn into Creation through the nightmare door that was cracked open in the Antilles. Fukú americanus, or more colloquially, fukú—generally a curse or a doom of some kind; specifically the Curse and the Doom of the New World.
November 4 Junot Díaz, The Brief Wondrous Life of Oscar Wao (Part I) November 6 WORKSHOP 5

5

WEEK 10
He wrote that he couldn’t believe he’d had to wait for this so goddamn long. (Ybón was the one who suggested calling the wait something else. Yeah, like what? Maybe, she said, you could call it life.) He wrote: So this is what everybody’s always talking about! Diablo! If only I’d known. The beauty! The beauty!
November 11 Junot Díaz, The Brief Wondrous Life of Oscar Wao (Parts II & III) November 13 WORKSHOP 6

WEEK 11
Race matters to a young man’s view of society when he spends his teenage years watching others tense up as he passes, no matter the neighborhood where he grew up. Race matters to a young woman’s sense of self when she states her hometown, and then is pressed, “No, where are you really from?”, regardless of how many generations her family has been in the country. Race matters to a young person addressed by a stranger in a foreign language, which he does not understand because only English was spoken at home. Race matters because of the slights, the snickers, the silent judgments that reinforce that most crippling of thoughts: “I do not belong here.”
November 18 Sotomayor, J., dissenting, Supreme Court of the United States, No. 12682, “Bill Schuette, Attorney General of Michigan, Petitioner v. Coalition to Defend Affirmative Action, Integration, and Immigration Rights and Fight for Equality By Any Means Necessary (BAMN), Et Al.” (CTools) November 20 WORKSHOP 7

WEEK 12

public TBCoordinates(double latitude, double longitude, float altitude, String name, String type) {

super(latitude, longitude, altitude);
this.name = name;
this.type = type; }
November 25 Electronic Disturbance Theater 2.0/b.a.n.g. lab, [({ Survival Series/La serie de sobrevivencia del desierto (CTools) NOVEMBER 27 THANKSGIVING BREAK

})] The Desert

WEEK 13

December 2 Selections from the Mongrel Coalition Against Gringpo (MCAG) Begin here http://gringpo.com/, then read around online. Please bring in one of your favorite articles/blogs/poems by or about MCAG. December 4 WORKSHOP 8
WEEK 14 December 9 PORTFOLIO PRESENTATIONS December 11 PORTFOLIO PRESENTATIONS

