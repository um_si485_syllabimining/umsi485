SOCIOLOGY 210 ELEMENTARY STATISTICS University of Michigan, Fall 2015
Professor: Rachel Best
Email: rkb@umich.edu Lecture: Tuesday / Thursday 10am-11am, 1230 Undergraduate Science Building Office hours: Tuesday 3:30-4:30pm and Friday 2:30-4pm, 4122 LSA
Graduate Student Instructors: Mike Fang
Email: mikefang@umich.edu Lab 6: Thursday 11am-1pm, G444C Mason Hall Lab 2: Thursday 1pm-3pm, G444A Mason Hall Office hours: Monday 10am-11am and Tuesday 12pm-1pm, 4249 LSA Katie Hauschildt Email: kehaus@umich.edu Lab 4: Friday 10am-12pm, G444A Mason Hall Lab 5: Friday 12pm-2pm, G444A Mason Hall Office hours: Thursday 9am-10am and 11:30am-1:30pm, 4253 LSA Richard Rodems Email: rodems@umich.edu Lab 3: Friday 11am-1pm, G444C Mason Hall Lab 7: Friday 1pm-3pm, G444C Mason Hall Office hours: Tuesday 2pm-3:30pm 4247 LSA, and by appointment
DESCRIPTION
Statistics is about systematically using the information at hand to understand the world. This course will teach you to analyze social science data and communicate your results. You will also become a more informed consumer of statistical claims in research, politics, and the media.
You do not need a math background beyond basic algebra, but you will need to think in unfamiliar ways and learn new words and symbols. The best way to do well in this class is to attend lecture and lab, work on problems, and ask questions frequently.
MATERIALS
Textbook: De Veaux, Velleman, and Bock, 2014. Intro Stats. 4th Edition. Course website: canvas.umich.edu Calculator: A calculator that can compute basic arithmetic (including square root)
	   1

REQUIREMENTS
Eight homework assignments Four exams Lab and lecture attendance and participation

40%
40% 20%

POLICIES
Technology: You will need to take notes by hand in order to jot down equations and sketch figures. To avoid distractions for yourself and others, laptops and cellphones are not allowed in lecture. Shortly after lecture is over, slides will be posted on the course website at canvas.umich.edu, so you don’t need to worry about copying down everything in the slides.
Homework assignments are due most Tuesdays at the beginning of lecture. Turn in a paper copy, either directly to your GSI or in your GSI’s mailbox in 3101 LSA. Most lab sessions will devote some time to working on homework with help from your GSI and classmates, so please plan to bring your textbook to lab. Work on each homework problem on your own first. When you get stuck, I encourage you to work with other students. If you work together, each student must write down his/her own answers— do not photocopy your work, and do not use one keyboard and print two copies. There are eight homework assignments, and your two lowest scores will be dropped from your grade. You must complete all homework assignments—no credit will be given for homework turned in more than 48 hours late, and incomplete or missed assignments will not be dropped.
Exams: There are three midterm exams and one cumulative final exam (October 1st, October 22nd, November 24th, and December 17th). No make-up exams will be given. The exams are closed-book, but you can bring one sheet of paper (8.5 by 11 inches) with notes on both sides. You must prepare these notes yourself. You can use a calculator on exams, but not your cellphone’s calculator functions. Your lowest exam score will be dropped from your grade. If you are satisfied with your grades on all three midterms, you can skip the final exam without penalty.
Grading: Your assignments and exams will be graded by your GSI. If you believe your work has been graded incorrectly, bring your graded work to your GSI’s office hours within one week of receiving it. If the issue is still unresolved, you may ask the professor to re-grade your assignment or exam. If you do so, your grade may go up, or it may go down.
Students with disabilities: If you need accommodation for a disability, please contact Services for Students with Disabilities (G664 Haven Hall, 734-763-3000) as soon as possible. I can provide accommodations if you give me a form from their office at least two weeks before a deadline or exam date.

	   2

OUTLINE
DESCRIPTIVE STATISTICS One variable
Lectures: September 8th and 10th Reading: Chapter 3 Two variables: Tables and distributions Lectures: September 15th and 17th Reading: Chapters 2 and 4 Homework: Assignment 1 (due September 22nd) Two variables: Correlation and regression Lectures: September 22nd and 24th Reading: Chapters 6 and 7 Homework: Assignment 2 (due September 29th) Review session and exam Lecture: September 29th (Review) Midterm exam 1: October 1st
PROBABILITY Randomness and probability
Lectures: October 6th and 8th Reading: Chapters 9 and 12 Homework: Assignment 3 (due October 13th) Sampling and review session Lectures: October 13th and 15th Reading: Chapter 10 Exam No class October 20th (fall break) Midterm exam 2: October 22nd
	  

3

INFERENTIAL STATISTICS Sampling distributions and confidence intervals
Lectures: October 27th and 29th Reading: Chapters 15 and 16 Homework: Assignment 4 (due November 3rd) Hypothesis tests Lectures: November 3rd and 5th Reading: Chapters 17 and 18 Homework: Assignment 5 (due November 10th) Comparing groups Lectures: November 10th and 12th Reading: Chapters 20 and 21 Homework: Assignment 6 (due November 17th) Regression and review session Lectures: November 17th and 19th Reading: Chapter 23 Homework: Assignment 7 (due November 24th) Exam Midterm exam 3: November 24th No class November 26th (Thanksgiving) Multiple regression Lectures: December 1st and 3rd Reading: Chapter 25 Homework: Assignment 8 (due December 8th) Overview and review session Lectures: December 8th and 10th Final exam December 17th, 4pm-6pm
	  

4

