Political Science 399 Political Science Internship Program Fall 2015

Dr. Debra Horner 5309 Weill Hall
Office Hours Mondays, by appt.
dhorner@umich.edu

Course Overview:

The Political Science Internship Program offers participating students an opportunity to apply theoretical knowledge from the classroom to public and non-profit agencies,
organizations, campaigns, media outlets and courts. The Internship Program also allows public and non-profit organizations the opportunity to work with University of Michigan
students and benefit from their service and enthusiasm. The goal of the course is to foster positive relations between the University and local community organizations and to assist
students in obtaining skills, experiences, and contacts that enhance employment prospects and options after graduation.

We will meet as a class four times during the course of the semester. However, I will
available throughout the semester via email, phone, or in office hours by appointment. Students are my highest priority. I want every student to do well and maximize the value
of each internship experience, and I am always eager to meet with students who need help or have questions.

Course Requirements:
Working at your Internship. You are required to work at least 12 hours each week at your internship (not including your time for commute). You may work any combination of days as long as this time commitment is fulfilled. If you miss a day through illness or other emergencies, you are obligated to make it up. Keep in mind that you are subject to the rules of your office. At the end of the semester, your supervisor will submit a performance evaluation of your work in their office. This is worth 55% of your grade.
Participation in CTools Class Forum. After each day you work at your placement (or two times per week) you will submit a 300-500 word commentary on some aspect of your organization, office, or internship experience. These entries should be thoughtful, thoughtprovoking reflections on what you are learning as a result of the internship process, not merely narratives recounting the events of the day. Each week I will provide several prompts you could choose to write about, for example,
• Describe the mission and goals of the organization. Why is or why isn’t it successful in accomplishing its mission?
• Describe and evaluate the decision-making process in your office, including both formal and informal processes. Who has authority?
• What are you doing well in your internship? How does that contribute to the goals of the organization or office?
or you could discuss some other interesting and relevant experience that week. These reflections do not need to take the tone of formal essays (although they can if that’s what you’re comfortable with), however they should be treated like typical written course

assignments (grammatically correct, spell-checked, well-organized, etc.). You will post these in the Forums section of our Course CTools Website (https://ctools.umich.edu/portal).
In addition, you will be required to comment on other students’ posts a minimum of four comments per week, although I’d like to encourage everyone to post and comment more frequently. I will evaluate these on a weekly basis, but I’ll only contact you if I find your submissions need improvement. Your participation in these reflections is worth 25% of your grade.
Internship Interview Reports. You will conduct three interviews-- one with a member of the office staff (preferably the head of your organization), one with your immediate supervisor, and one with someone of interest outside of your placement office—and write up your impressions and insights gained from them in a series of short reports. We will discuss in more detail the requirements for these interview reports at our February class meeting, and I will post the assignment details on CTools. You may turn in these reports at any time, but all three must be submitted by December 7. Each one constitutes 5% of your grade.
Final Presentation. At our last class meeting, you will be responsible for making a short 5-minute oral presentation about your internship experience. In this presentation you may discuss a variety of topics: what you have learned about the organization/agency/official you worked for, what you have learned about your own work habits and preferences, how the internship affects your future career decisions. You are welcome to draw upon your Forum reflections in composing your presentation, but the presentation itself should be more of an overview of your entire semester’s work. Again, I will provide more details about the assignment requirements toward the end of the semester. This presentation is worth 5% of your grade.
Grade Calculation:
• 55% ~ Internship Supervisor’s Evaluation
• 25% ~ Participation in CTools Class Forum
• 15% ~ Three Internship Interview Reports
• 5% ~ Final Presentation on Internship Experience
Other Course Policies and Notes:
Should you incur expenses for parking, you may be reimbursed up to $100 at the end of the semester, if you submit for reimbursement within 30 days of the end of term. Keep all parking receipts.
Check out the safety and security of your placement, and the parking arrangements. Never leave alone after dusk.

