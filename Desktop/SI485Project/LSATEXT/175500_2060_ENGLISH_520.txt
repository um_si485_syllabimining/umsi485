ENGLISH	  520	  

Andrea	  Zemgulys	  

Introduction	  to	  Graduate	  Studies	  

Office	  for	  2015-­‐16:	  3020	  Tisch	  

TuTh	  8:30-­‐10:00am	  

Office	  Hours:	  Wednesdays	  

1303	  Mason	  Hall	  

11:30am-­‐1pm	  or	  by	  appointment	  

	   zemgulys@umich.edu

	  

	  

Introduction	  to	  English	  Studies	  has	  three	  primary	  components.	  1.)	  Students	  will	  be	  

introduced	  to	  research	  and	  bibliographic	  tools	  appropriate	  to	  graduate-­‐level	  research,	  

largely	  through	  library	  workshops	  and	  practical	  research	  assignments.	  	  2.)	  Students	  will	  

gain	  an	  overview	  of	  a	  current	  debate	  in	  literary	  criticism,	  one	  between	  so-­‐called	  suspicious	  

and	  surface	  reading,	  and	  that	  includes	  a	  look	  back	  at	  the	  “textual	  moment”	  of	  post-­‐

structuralism.	  	  3.)	  We	  will	  engage	  in	  a	  discussion	  on	  responsible	  conduct	  in	  professional	  

literary	  research.	  	  	  

	  

Required	  Texts:	  

MLA	  Guide	  to	  Research	  

Virginia	  Woolf,	  To	  The	  Lighthouse.	  	  Please	  acquire	  a	  paper	  copy	  for	  this	  old-­‐timey	  professor.	  

Various	  essays	  posted	  online	  (either	  CTools	  or	  Canvas,	  link	  to	  be	  sent	  within	  next	  few	  

hours!),	  organized	  by	  meeting	  date.	  	  	  

	  

	  

Books	  and	  Texts	  

Tu	  9/8	  Course	  Introduction	  

Th	  9/10	  D.F.	  McKenzie,	  Bibliography	  and	  the	  Sociology	  of	  Texts	  (1985):	  “The	  Book	  as	  an	  

Expressive	  Form”;	  Gérard	  Genette,	  Paratexts:	  Thresholds	  of	  Interpretation	  (1997):	  

“Introduction”	  

	  

Tu	  9/15	  Jerome	  McGann,	  The	  Textual	  Condition	  (1991):	  “Introduction:	  Texts	  and	  

Textualities”;	  Leah	  Price	  (9/17/2015	  HEBERLE	  LECTURER,	  NB),	  Introduction	  and	   Chapter	  1	  “Reader’s	  Block,”	  How	  to	  Do	  Things	  with	  Books	  (2012)	  

Suspicious	  and	  Symptomatic	  Reading	  

Th	  9/17	  Paul	  Ricoeur,	  Freud	  and	  Philosophy:	  An	  Essay	  on	  Interpretation	  (1970):	  “The	  

Conflict	  of	  Interpretations”;	  Fredric	  Jameson,	  “Metacommentary”	  (PMLA	  86:1,	  1971)	  

	  

Tu	  9/22	  Library	  Workshop	  I.	  	  	  Research	  workshop	  led	  by	  Sigrid	  Cordell,	  PhD,	  English	  

specialist	  librarian.	  	  Meet	  in	  Gallery	  Lab	  Room	  100	  of	  Hatcher	  Graduate	  Library.	   The	  Very	  Model	  of	  a	  Major	  Modern	  Novel	  

Th	  9/24	  Virginia	  Woolf,	  To	  The	  Lighthouse,	  “The	  Window”	  chapters	  1-­‐9	  

	  

Tu	  9/29	  Virginia	  Woolf,	  To	  The	  Lighthouse,	  finish	  “The	  Window”	  and	  “Time	  Passes”	  

Th	  10/1	  Virginia	  Woolf,	  To	  The	  Lighthouse,	  “The	  Lighthouse”	  

	  

Reading	  a	  Major	  Reader	  in	  Place	  and	  Time	  

Tu	  10/6	  Erich	  Auerbach,	  “The	  Brown	  Stocking”	  from	  Mimesis	  (1946);	  Edward	  Said	  

“Secular	  Criticism”	  from	  The	  World,	  the	  Text,	  and	  the	  Critic	  (1983)	  	  

W	  10/7	  First	  Bibliographic	  Assignment	  due	  by	  noon.	  	  	  

Th	  10/8	  Presentations	  	   	   Tu	  10/13	  Michel	  Foucault,	  The	  History	  of	  Sexuality,	  volume	  1	  (1978):	  ‘‘We	  ‘Other	   Victorians’”;	  D.A.	  Miller,	  The	  Novel	  and	  the	  Police	  (1988):	  “The	  Novel	  and	  the	  Police.”	  	   Th	  10/15	  Leo	  Bersani,	  “Against	  Ulysses”	  (1988)	  	   	   Tu	  10/20	  Fall	  Break	  –	  no	  class	  meeting.	   Th	  10/22	  Software	  for	  Graduate	  Study	  	  	  Guest:	  John	  Whittier-­‐Ferguson	  
DUE	  ONLINE	  by	  NOON:	  please	  post	  your	  intended	  topic	  of	  research	  for	  Sigrid	   Cordell’s	  information.	   	   Tu	  10/27	  Library	  Workshop	  II.	  	  Research	  workshop	  led	  by	  Sigrid	  Cordell,	  PhD,	  English	   specialist	  librarian.	  	  Meet	  in	  Gallery	  Lab	  Room	  100	  of	  Hatcher	  Graduate	  Library.	   Library.	   Post	  Suspicion,	  Post	  Discipline,	  and	  an	  Ethical	  Turn	   Th	  10/29	  Eve	  K.	  Sedgwick,	  Touching	  Feeling:	  Affect,	  Pedagogy,	  Performativity	  (2003):	   “Introduction”	  (pp.	  1-­‐13);	  “Paranoid	  Reading,	  or,	  You’re	  so	  Paranoid,	  You	  Probably	  Think	   This	  Essay	  is	  About	  You”	  (1997)	   	   Tu	  11/3	  Sharon	  Marcus	  and	  Stephen	  Best,	  “Surface	  Reading:	  An	  Introduction”	   (Representations	  108,	  Fall	  2009);	  Sharon	  Marcus,	  Between	  Women	  (2007):	  “Just	  Reading:	   Female	  Friendship	  and	  the	  Marriage	  Plot.”	   W	  11/4	  Second	  Bibliographic	  Assignment	  due	  by	  noon	   Th	  11/5	  Presentations	   	   Tu	  11/10	  Franco	  Moretti,	  “Conjectures	  on	  World	  Literature”	  (New	  Left	  Review,	  2000);	  	   “Style,	  Inc.:	  Reflections	  on	  Seven	  Thousand	  Titles	  (British	  Novels	  1740-­‐1850)	  (Critical	   Inquiry	  36.1,	  Autumn	  2009).	   Th	  11/12	  Heather	  Love,	  “Close	  but	  Not	  Deep:	  Literary	  Ethics	  and	  the	  Descriptivist	  Turn”	   (New	  Literary	  History	  42:1,	  2010);	  Jane	  Bennett,	  Vibrant	  Matter:	  A	  Political	  Ecology	  of	   Things	  (2010):	  Introduction.	  	  	   	   Tu	  11/17	  Edward	  Said,	  excerpt	  from	  “Criticism	  between	  Culture	  and	  System”	  from	  The	   World,	  the	  Text,	  and	  the	  Critic	  (1983);	  Bruno	  Latour,	  “Why	  has	  Critique	  Run	  Out	  of	  Steam?:	   From	  Matters	  of	  Fact	  to	  Matters	  of	  Concern”	  (Critical	  Inquiry	  30:2,	  2004).	   Th	  11/19	  [One	  class	  between	  10/29	  and	  11/12	  will	  be	  canceled	  to	  make	  it	  easier	  for	  you	   to	  attend	  the	  plagiarism	  workshop,	  a	  component	  of	  the	  Responsible	  Conduct	  of	  Research	   and	  Scholarship	  training	  required	  by	  Rackham	  Graduate	  School.	  	  Syllabus	  will	  be	  re-­‐ distributed	  once	  date	  is	  set.]	   	   Tu	  11/24	  Special	  Collections/Clements	  Library	  presentation.	  	  Meet	  on	  8th	  floor	  of	  Hatcher	   Library.	   Th	  11/26	  Thanksgiving	  Break	  –	  no	  class	  meeting.	   	   Tu	  12/1	  Responsible	  Conduct	  of	  Research	  and	  Scholarship	  (readings	  online)	   Th	  12/3	  Responsible	  Conduct	  of	  Research	  and	  Scholarship	  (readings	  online)	   	   Tu	  12/8	  Elected	  Text	  

W	  12/9	  Third	  Bibliographic	  Assignment	  Due	  by	  noon	   Th	  12/10	  Presentations	  	   	   	  
	   Workload:	   •	  Participation:	  I	  expect	  each	  and	  every	  one	  of	  you	  to	  participate	  meaningfully	  in	  every	   single	  class	  discussion	  for	  the	  entire	  semester,	  September	  8th	  through	  December	  10th.	  	   Come	  to	  class	  prepared	  with	  developed	  questions	  or	  significant	  quotations	  you	  wish	  to	   discuss.	  	  I	  will	  often	  ask	  you	  to	  share	  your	  passage	  or	  set	  up	  a	  conversation	  based	  on	   questions	  you	  have.	  	  Every	  class	  meeting	  (with	  the	  exception	  of	  workshops	  and	   presentations)	  will	  begin	  with	  30	  minutes	  of	  discussion	  generated	  entirely	  by	  students,	   during	  which	  I	  will	  play	  the	  role	  of	  silent	  observer.	   •	  Three	  bibliographic	  assignments.	  	  These	  have	  elaborate	  guidelines	  that	  will	  be	  distributed	   in	  a	  timely	  manner.	  	  Each	  is	  linked	  to	  a	  library	  workshop	  held	  either	  at	  Hatcher	  or	  Special	   Collections.	  	  Each	  requires	  research,	  a	  bibliography,	  a	  brief	  written	  portion,	  and	  a	   presentation.	   •	  One	  meeting	  with	  the	  instructor.	  	  Make	  an	  appointment	  to	  meet	  with	  me	  in	  my	  office,	  or	   just	  stop	  by	  during	  office	  hours,	  before	  October.	  	  Please	  also	  stop	  by	  or	  make	  an	   appointment	  at	  any	  other	  time	  for	  any	  other	  reason!	   	  

