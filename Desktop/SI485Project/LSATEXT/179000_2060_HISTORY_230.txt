1
HISTORY 230: The Roaring Twenties and the Rough Thirties: Interwar America Fall 2015
Tues-Thurs 10:00-11:30a Angell Hall G228
Instructor: Tyran Steward Office: 2641 Haven Hall Office Hours: Thursday 11:30-12:30 and by appointment Email: tyrankai@umich.edu
Course materials will be available on CTools.
Course Website: There is a CTools website for this course available at https://ctools.umich.edu/gateway/ To learn more about CTools visit https://ctools.umich.edu/portal/site/!gateway/page/1091327577219-1420518 If you have trouble accessing material on CTools please call or email 4help (764-HELP or 4HELP@umich.edu).
COURSE DESCRIPTION
This course will probe the domestic history of the U.S. from 1919 to 1939 and the cultural, economic, political, and social changes accompanying America’s evolution into a modern society. Themes include: developments in work, leisure, and consumption; impact of depression on the organization of the public and private sectors; persistence of traditional values such as individualism and the success ethos in shaping responses to change; and the evolving diversity of America and the American experience.
Course Requirements and Texts:
Required Texts:  Kevin Boyle, Arc of Justice: A Saga of Race, Civil Rights, and Murder in the Jazz Age  Alan Brinkley, The End of Reform: New Deal Liberalism in Recession and War  Lizabeth Cohen, Making a New Deal: Industrial Workers in Chicago, 1919-1939  Chap Heap, Slumming: Sexual and Racial Encounters in American Nightlife, 1885-1940  Robin D.G. Kelley, Hammer and Hoe: Alabama Communists during the Great Depression  Moshik Temkin, The Sacco-Vanzetti Affair: America on Trial  Articles and book chapters are on CTools
1

2
Exam: There will be a Midterm and a Final Examination for this course.
Major Assignment: Research paper analyzing art work from the 20th Century American interwar period in relation to a major event or person chronicled during the era. Students will choose a topic from the attached list. Please see handout for more details.
The writing assignments for this course include:
 a declaration of topic/thesis statement  two primary sources  annotated bibliography of ten secondary sources  three response papers  a ten page final paper
COURSE POLICIES
Essay format (non-negotiable!)
 Use Times New Roman 12 Font. Do not use Times’ more ornate cousin, Garamond. And do not think of using Courier.
 All margins need to be 1 inch. In addition, please do not add any extra spaces between paragraphs of the same style. (Hint: MS Word allows you to designate this option)
 Grammar and punctuation are essential to the writing of a coherent essay. Please proofread all of your work carefully!
 All essays are to be documented (formatting for all your footnotes and bibliographic entries) in Chicago Style. For more information on the Chicago Style of Writing, please see: http://www.lib.umich.edu/database/chicago-manual-style-online
Breakdown of Final Grades
The goal of the course is for you to express original ideas in interesting, engaging ways. Your essay should enlighten your readers and provide a sense of how you conceptualize the historical treatment of certain events pertaining to the emergence of modern America. Likewise, your examinations and in-class participation should reflect a command of the material we have covered during the semester.
The majority of your final grade (50%) comes from your essay and final examination. The assignments are weighted more significantly as the quarter goes along in order to reward your improvement and acknowledge the assignments’ increasing complexity. Here is the grade breakdown:
05% Writing Assignments (Annotated Bibliography and Topic Thesis) 15% Response Papers 15% Participation (active participation in this course is a MUST) 15% Midterm (Take home)
2

3

20% Research Paper 30% Final Examination
Please note that I expect your essays to be free of grammatical, spelling, and formatting errors. (I am happy to explain any technical issues that seem confusing or obscure). Failure to meet these expectations will result in a lowered final grade.
Attendance and Participation (15% of final grade): Regular attendance is mandatory. A portion of a student’s attendance grade will be reduced for each unexcused absence. Any student who has more than 3 unexcused absences will receive a “zero” for the attendance grade. In addition, you will be expected to respond to the questions I will prepare each week and to offer your opinions on the questions/topics in a reasonable and respectful manner on the CTools forum. To earn the full participation credit, you will need to have contributed to the forum at least 5 times during the semester.
Essay Grading Standards
When grading, I evaluate the words on the page before me. I do not analyze your essay based on whether or not I agree with your opinion; instead, your grade will be determined by how effectively you frame your arguments as well as your deference to historical context for this course. Your improvement on each essay draft is expected. Further, the effort you put into an assignment will most certainly be evident in the essays you submit. These are the general standards to which I hold essays; point variations within these broad ranges represent shades of difference.
• An “A” range essay is both ambitious and successful. It presents a strong, interesting argument with grace and confidence.
• A “B” range essay is one that is ambitious but only partially successful, or one that achieves modest aims well.
• A “C” range essay has significant problems in articulating and presenting its argument, or seems to lack a central argument entirely.
• A “D” range essay does not grapple seriously with either ideas or texts, or fails to address the expectations of the assignment.
• An “E” essay is like a “D” essay but is significantly shorter than the assigned length, or doesn’t actually exist.
A more specific rubric is provided on CTools.
Special grading information
Grades will be distributed as follows:

A: 92.6 and above A-: 89.6-92.5 B+: 87.6-89.5 B: 82.6-87.5 B-: 79.6-82.5

C+: 77.6-79.5 C: 72.6-77.5 C-: 69.6-72.5 D+: 67.6-69.5 D: 62-67.5

E: below 62

3

4

Since the University does not record “F” grades, a student earning a course average below 62 will receive an “E” in this course.

I reserve the right to consider student progress and improvement as a salient factor when determining final grades.

CLASSROOM PHILOSOPHY
Your instructor holds the perspective that all classes are essentially intercultural encounters—among individuals in the class, between the readers and any given author, and among the authors, the students, and the professor. We are all learning how to effectively learn from one another. Such a classroom requires particular capacities and commitments on our part. It also requires a mutual effort in helping each other both understand the course material and the differing interpretative positions we may bring to a more complex understanding of the material. While each of us seeks to advance our own knowledge, we are also a community in which we are each responsible to help the other members of the community learn effectively.

In effort to enhance our learning experience, we expect that students and instructors will commit to
do the following: • acquire and utilize intellectual skills and capacities that will enable us to work effectively with the
complexities of the course material; • develop increased self-knowledge and knowledge of others; • understand how the material we are studying relates to our own previous learning, backgrounds,
and experiences, and how we can use and apply our new knowledge effectively; • develop the ability to critique material in a mature manner using our own previous learning and
experiences as part of the critique when appropriate; • develop the communication skills that facilitate our learning and our ability to listen, read, reflect,
and study to understand; • remain engaged and in communication even when the course material or discussion is confusing or
upsetting, by recognizing that understanding does not imply agreement • respect everyone’s ideas and values even when we disagree

Schedule of Assignments, Classes, and Readings ***Note: The reading listed for each class meeting corresponds to that specific day and should be completed in advance of the lecture***

Schedule of Classes and Readings

Week 1 The World The War Made Tuesday, September 8: Lecture—A Return to Normalcy? Reading: Cohen, Making a New Deal, introduction-chapter 2
Thursday, September 10: History in Focus—Bruce Barton, The Man Nobody Knows: A Discovery of the Real Jesus (CTools)

Assignment: Major assignment handed out.

Week 2 Purging Anti-Capitalist Agitation Tuesday, September 15: Lecture—This Business of America and the Age of Prosperity

4

5

Reading: Tempkin, The Sacco-Vanzetti Affair, Chapters 1-2 Thursday, September 17: Lecture—This Business of America Part 2 Assignment: Response Paper—Sacco and Vanzetti—Innocent or Guilty?

Week 3 Research Week Tuesday, September 22: Trip to the University Of Michigan Museum Of Art (we will meet at the museum and then walk together to the Paper Study Room)

Assignment Due: Response Paper on Sacco and Vanzetti (Due 9/22 by 10 am via Drop Box)
Thursday, September 24: Library Visit—Hatcher Library—class meets in Hatcher Gallery Lab Room 100 with Alexa Pearce, Librarian, for an overview of search engines and research tools.

Week 4 The Most Conservative Nation on Earth Tuesday, September 29: The Age of Anarchy Film: Sacco and Vanzetti Reading: Tempkin, The Sacco-Vanzetti Affair, Chapters 3-4
Thursday, October 1: History in Focus— Excerpts from Sacco-Vanzetti Trial (CTools)

Week 5 The Lost Generation Tuesday, October 6: Lecture—The Birth of Civil Liberties and Cultural Wars
Reading: Cohen, Making a New Deal, Ch. 3; Heap, Slumming, Ch. 2

Thursday, October 8: Trip to the University Of Michigan Museum Of Art (we will meet at the museum and then walk together to the Paper Study Room)

Week 6 Feminists and Flappers Tuesday, October 13: Lecture—Alice Paul, Women’s Freedom and the Equal Rights
Amendment
Reading: Heap, Slumming, Chapters 3-4

Assignment Due: Annotated bibliography AND primary sources list (Due 10/13 by 10 am via Drop Box)

Assignment: Midterm handed out

Thursday, October 15: History in Focus—Conversations with Alice Paul (on the ERA); “Lesbianism in the 1920s and 1930s: A Newfound Study” in Signs vol 2, no. 4 (1977) p 895-
904 (CTools)

Week 7 A New Category—“Illegal Alien” Tuesday, October 20: No Class (Fall Break)

Thursday, October 22: Lecture—Pluralism, Liberty and the Nation of Immigrants Reading: Cohen, Making a New Deal, ch.4; Kelley, Hammer and Hoe, ch.3

5

6

History in Focus—Images from Angel and Ellis Islands (CTools)

Assignment Due: MIDTERM! (Due 10/22 by 10 am via Drop Box)
Week 8 Monkey Business and the Consumer’s Republic Tuesday, October 27: The Scopes Monkey Trial Film: Scopes Monkey Trial (documentary) Reading: Heap, Slumming, Ch. 5; Kelley, Hammer and Hoe, Ch. 4
Thursday, October 29: History in Focus—William Jennings Bryan speech excerpt: http://www.pbs.org/wgbh/amex/monkeytrial/filmmore/ps_bryan.html

H.L. Mencken’s Trial Account: http://law2.umkc.edu/faculty/projects/ftrials/scopes/menk.htm

Week 9 The Consumer’s Republic Tuesday, November 3: Lecture—Becoming Modern: Art, Dress, Jazz, and the American Radio Reading: Boyle, Arc of Justice, introduction-ch.2; Kelley, Hammer and Hoe, ch.1
Thursday, November 5: Lecture—Race, Law, and the Coming of the Second Ku Klan Reading: Boyle, Arc of Justice Chapters 6, 9-10
Assignment: Response Paper—Should the push for racial equality/integration violate Americans’ “freedom of choice?”

Week 10 A Nation of Drunkards Tuesday, November 10/Thursday, November 12 Ken Burns’ Prohibition
Assignment Due: Response Paper on “freedom of choice” (Due 11/10 by 10 am via Drop Box)

Week 11 A Nation in Despair Tuesday, December 17: Lecture—America’s Great Depression
Reading: Cohen, Making a New Deal ch.5; Heap, Slumming, ch.6; Kelley, Hammer
and Hoe, ch.2

Thursday, November 19: The Great Depression Film: The Great Depression: The Road to Rock Bottom Reading: Cohen, Making a New Deal ch.6; Brinkley, End of Reform, introductionChapter 1 History in Focus—Images of the Depression (CTools)
Assignment: Response Paper—Should Herbert Hoover be blamed for the Great Depression?

Week 12 Thanksgiving Break!

Tuesday, November 24: No Class! Safe Travels!

6

7
Assignment Due: Response Paper on Herbert Hoover (Due 11/24 by 10 am via Drop Box) Thursday, November 26: Gobble Gobble! Football!
Saturday, November 28: University of Michigan Wolverines v. The Ohio State Buckeyes
Week 13 Willing and New Deal[ing] Tuesday, December 1: Lecture—Making a New Deal Reading: Kelley, Hammer and Hoe, Chapters 5-10
ASSIGNMENT: RESEARCH PAPER DUE (Due 12/1 by 1 p.m. via Drop Box)
Thursday, December 3: Lecture—Reckoning with Liberty: The Second New Deal History in Focus—“The New Deal was a Failure”—Dr. Santos
http://www.loc.gov/teachers/classroommaterials/presentationsandactivities/pres entations/timeline/depwwii/newdeal/failure.html
History in Focus— Herbert Hoover on the Great Depression and the New Deal (CTools)
Week 14 The Limits of Change Tuesday, December 8: Lecture— Lecture—A New Conception of America Reading: Brinkley, End of Reform, Chapters 2-6
ASSIGNMENT: Final exam handed out
Thursday, December 10: History in Focus—Hallie Flanagan and the Federal Theatre Project (excerpt on investigation by the Special House Committee on Un-American Activities—CTools)
Week 15 FINAL Examination Period Thursday, December 17: FINAL EXAMINATION DUE! NO EXCEPTIONS!
Other Important Information
Students are welcome to talk with me about any aspect of the course or the Department of History. My office hours and location are listed above. If you cannot meet with me during office hours, I can also be reached by email to set up an appointment.
Facebook Policy: Although I am available to meet with students regarding the class, it is my policy not to befriend students on Facebook, Twitter, or other social networking sites during the semester. I have adopted this policy in order to ensure that all students in the course are treated fairly in the grading process.
Addendum: The use of laptops and tablets are permitted for the purposes of note-taking; under no circumstances are students allowed to browse the web or make use of social media. Cell phone use is completely prohibited. Students found in violation of these policies will be asked to put devices away. Repeated violations will result in having your use of technology revoked and/or a deduction in your participation grade for the course.
7

8

Enrollment Policy: In accordance with departmental policy, all students should be officially enrolled in the course by the end of the second full week of the semester. Enrolling officially and on time is solely the responsibility of the student.

Services for Students with Disabilities (SSD): Students with disabilities that have been certified by the Services for Students with Disabilities will be appropriately accommodated, and they should inform the instructor as soon as possible of their needs. We rely on Services for Students with Disabilities for assistance in verifying the need for accommodations and developing accommodation strategies. If you have not previously contacted them, you may do so at: G-664 Haven Hall, 505 South State Street; telephone 763-3000 (V), 615-4461 (TDD); http://ssd.umich.edu

Submission of Assignments

All assignments are mandatory. If you do not submit an assignment, the instructors will reduce your final grade by one full letter grade in addition to giving you zero for that assignment. If you do not submit two or more assignments, you will automatically receive a failing grade for the course. Further, all assignments should be written in a clear, concise manner, and should make direct reference to course readings and lectures. They should be typed, double-spaced, using 1 inch margins and Times New Roman 12-point font. Please be sure to pay careful attention to spelling and grammar. ALL ASSINGMENTS SHOULD BE SUBMITTED VIA DROPBOX!

Late Assignments

Extensions of written work are granted at the discretion of the instructors to those presenting valid and verifiable excuses. Students who are unable to fulfill assignments as scheduled for family, religious, or medical reasons must contact the instructors before the due date of the assignment. The pressures of other course work, employment, and extra-curricular activities do not constitute valid excuses for late assignments. Take note of the due dates on the syllabus and plan ahead.

Make-up Exam Policy

If for any family or medical reason you find it absolutely necessary to miss an examination, you must contact me before the test and gain my consent if you wish to take a make-up exam. To make up any exam, you will have to take it during one of the regularly scheduled exam sessions offered by the Department of History.

Student Conduct

Students are expected to abide by the guidelines listed in the University of Michigan Statement of Student Rights and Responsibilities. In addition students are not allowed to sleep, read newspapers, leave class early without permission, use cell phones (to text message or talk), or conduct extracurricular conversations during the class session. The instructor will ask disruptive students to leave the class, and reserves the right to withdraw students from the course for inappropriate classroom behavior.

Academic Misconduct

Plagiarism, cheating, or other forms of academic misconduct will not be tolerated. The term “academic misconduct” includes all forms of student academic misconduct wherever committed,

8

9
illustrated by, but not limited to, cases of plagiarism and dishonest practices in connection with examinations. I will report any suspected misconduct to the Office of the Assistant Dean who handles matters related to academic integrity. The following is a statement from the Office of the Assistant Dean concerning academic integrity:
LSA COMMUNITY STANDARDS OF ACADEMIC INTEGRITY
The LSA undergraduate academic community, like all communities, functions best when its members treat one another with honesty, fairness, respect, and trust. The College holds all members of its community to high standards of scholarship and integrity. To accomplish its mission of providing an optimal educational environment and developing leaders of society, the College promotes the assumption of personal responsibility and integrity and prohibits all forms of academic dishonesty and misconduct. Academic dishonesty may be understood as any action or attempted action that may result in creating an unfair academic advantage for oneself or an unfair academic advantage or disadvantage for any other member or members of the academic community. Conduct, without regard to motive, that violates the academic integrity and ethical standards of the College community cannot be tolerated. The College seeks vigorously to achieve compliance with its community standards of academic integrity. Violations of the standards will not be tolerated and will result in serious consequences and disciplinary action.
For additional information, please see http://www.lsa.umich.edu/academicintegrity/
Please take particular note of the examples of academic misconduct provided by the university policy on plagiarism: http://www.lsa.umich.edu/academicintegrity/examples.html
I ask that you also take note of the following departmental guidelines and policies: http://www.lsa.umich.edu/history/undergraduate/courses/guidelinesandpolicies
And:
http://www.lsa.umich.edu/UMICH/history/Home/Undergraduate/Courses/Guidelines%20and%20Pol icies/History%20Department%20Academic%20Integrity%20Fall%202014.pdf
Should you have any questions concerning these policies, please consult me.
For strategies on how to avoid academic misconduct, please adhere to the following tips:
Academic integrity is essential to maintaining an environment that fosters excellence in teaching, research, and other educational and scholarly activities. Thus, students are expected to complete all academic and scholarly assignments with fairness and honesty. The following suggestions will help you preserve academic integrity by avoiding situations where you might be tempted to cheat or you might be perceived to be cheating.
1. ACKNOWLEDGE THE SOURCES THAT YOU USE WHEN COMPLETING ASSIGNMENTS: If you use another person's thoughts, ideas, or words in your work, you must acknowledge this fact. This applies regardless of whose thoughts, ideas, or words you
9

10
use as well as the source of the information. If you do not acknowledge the work of others, you are implying that another person's work is your own, and such actions constitute plagiarism. Plagiarism is the theft of another's intellectual property, and plagiarism is a serious form of academic misconduct. If you are ever in doubt about whether or not you should acknowledge a source, err on the side of caution and acknowledge it.
2. AVOID SUSPICIOUS BEHAVIOR: Do not put yourself in a position where an instructor might suspect that you are cheating or that you have cheated. Even if you have not cheated, the mere suspicion of dishonesty might undermine an instructor's confidence in your work. Avoiding some of the most common types of suspicious behavior is simple. Before an examination, check your surroundings carefully and make sure that all of your notes are put away and your books are closed. An errant page of notes on the floor or an open book could be construed as a "cheat sheet." Keep your eyes on your own work. Unconscious habits, such as looking around the room aimlessly or talking with a classmate, could be misinterpreted as cheating.
3. DO NOT FABRICATE INFORMATION: Never make up data, literature citations, experimental results, or any other type of information that is used in an academic or scholarly assignment.
4. DO NOT FALSIFY ANY TYPE OF RECORD: Do not alter, misuse, produce, or reproduce any University form or document or other type of form or document. Do not sign another person's name to any form or record (University or otherwise), and do not sign your name to any form or record that contains inaccurate or fraudulent information. Once an assignment has been graded and returned to you, do not alter it and ask that it be graded again. Many instructors routinely photocopy assignments and/or tests before returning them to students, thus making it easy to identify an altered document.
5. DO NOT GIVE IN TO PEER PRESSURE: Friends can be a tremendous help to one another when studying for exams or completing course assignments. However, don't let your friendships with others jeopardize your college career. Before lending or giving any type of information to a friend or acquaintance, consider carefully what you are lending (giving), what your friend might do with it, and what the consequences might be if your friend misuses it. Even something seemingly innocent, such as giving a friend an old term paper or last year's homework assignments, could result in an allegation of academic misconduct if the friend copies your work and turns it is as his/her own.
6. DO NOT SUBMIT THE SAME WORK FOR CREDIT IN TWO COURSES: Instructors do not give grades in a course, rather students earn their grades. Thus, instructors expect that students will earn their grades by completing all course requirements (assignments) while they are actually enrolled in the course. If a student uses his/her work from one course to satisfy the requirements of a different course, that student is not only violating the spirit of the assignment, but he/she is also putting other students in the course at a disadvantage. Even though it might be your own work, you are not permitted to turn in the same work to meet the requirements of more than one course. You should note that this applies even if you have to take the same course twice, and you are given the same or similar assignments the second time you take the course; all assignments for the second taking of the course must be started from scratch.
10

11
7. DO YOUR OWN WORK: When you turn in an assignment with only your name on it, then the work on that assignment should be yours and yours alone. This means that you should not copy any work done by or work together with another student (or other person). For some assignments, you might be expected to "work in groups" for part of the assignment and then turn in some type of independent report. In such cases, make sure that you know and understand where authorized collaboration (working in a group) ends and collusion (working together in an unauthorized manner) begins.
8. MANAGE YOUR TIME: Do not put off your assignments until the last minute. If you do, you might put yourself in a position where your only options are to turn in an incomplete (or no) assignment or to cheat. Should you find yourself in this situation and turn in an incomplete (or no) assignment, you might get a failing grade (or even a zero) on the assignment. However, if you cheat, the consequences could be much worse, such as a disciplinary record, failure of the course, and/or dismissal from the University.
9. PROTECT YOUR WORK AND THE WORK OF OTHERS: The assignments that you complete as a student are your "intellectual property," and you should protect your intellectual property just as you would any of your other property. Never give another student access to your intellectual property unless you are certain why the student wants it and what he/she will do with it. Similarly, you should protect the work of other students by reporting any suspicious conduct to the course instructor.
10. READ THE COURSE SYLLABUS AND ASK QUESTIONS: Many instructors prepare and distribute (or make available on a web site) a course syllabus. Read the course syllabus for every course you take! Students often do not realize that different courses have different requirements and/or guidelines, and that what is permissible in one course might not be permissible in another. "I didn't read the course syllabus" is never an excuse for academic misconduct. If after reading the course syllabus you have questions about what is or is not permissible, ask questions!
11

