Comparative Constitutional Law (Constitutional systems of civil law countries) Ekaterina Mishina
Course Description: The course will focus on constitutional systems of civil law countries featuring Germany, France, Spain, Italy, Japan, Russia, Ukraine, Georgia, Estonia, Latvia, Lithuania, Kazakhstan, Kyrgyzstan, Azerbaijan, Turkmenistan and Vietnam. Great Britain is the only common law country on the list of the countries we will study. The course will address
• the development of the civil law system; • main differences between common law and civil law systems; • main forms of government that exist today (including constitutional monarchy,
Westminster system, parliamentary republic, socialist republic, presidential and semipresidential constitutional systems. ); • specifics of contemporary federations and unitary states; • prerequisites for introducing a particular constitutional system in the countries studied; • democratic and non-democratic political regimes; • the role of parliaments in civil law countries, including the concept of Parliamentarism ; • the role of courts and judges in civil law countries; • analysis of constitutions of the countries of study; • how the principles of separation of powers, rule of law and supremacy of human and civil rights work in the civil law countries; • exercise and protection of human and civil rights in civil law countries; • the role of European Court for Human Rights
Reading Materials: C-Tools Resources (distributed via Course Tools website)
Grading system
Your grade for the course will be determined by class participation points (PP), the midterm paper score (MPS) and the final paper score (FPS). While certain forms of class participation are rewarded with points (see below), non-participation is not penalized. The weight that is put on each graded assignment is student-specific and depends on the extent of class participation chosen by the student.

Participation points – how to get them. At the instructor’s discretion, students may be awarded participation points for constructive and insightful contributions to class discussion. A student may get up to 20 participation points total per semester. Most valued comments would involve relevant insight and inferences drawn from facts rather than raw information. Students are also encouraged to earn participation points by making oral presentations (5-6 minutes) on topics relevant to the course syllabus. Presentation timing and topic details must be coordinated with the instructor ahead of time. A presentation may be awarded 1, 2 or 3 participation points.
Participation points – impact on grade Each participation point earned has two benefits: it credits you one point towards the perfect score, and it reduces the total weight that is put on your papers (TPW) by 1 percent. The more participation points you earn, the less is the TPW. Whatever your individual TPW is, your final paper has three times the weight of the midterm paper. Your grade for the course is based on your total score (TS) on a scale of 0 to 100. Your total score equals the sum of your participation points plus the appropriately weighted sum of your midterm and final paper scores. See formulas in the table below.

Grading system – variables and formulas

Grade-

relevant Description

Formula or range

variable

Participation PP
points

0 to 20

MPS

Midterm paper score

0 to 100

FPS Final paper score 0 to 100

TPW Total paper weight

������������������ = (100 − ������������)%

MPW

Midterm paper weight

������������������

=

1 4

������������������

FPW Final paper weight

������������������

=

3 4

������������������

TS Total score

������������ = ������������ + ������������������ × ������������������ + ������������������ × ������������������

Comments
Determined by the instructor Determined by the instructor Determined by the instructor Each participation point reduces TPW by 1 % MPW and FPW are student-specific and depend on student’s PP. Final paper has three times the weight of midterm paper Each participation point is a 1-point credit towards perfect score

Grading system – examples Due to its individualized TPW feature, the grading system offers the flexibility to accommodate students who prefer to speak up in class as well as those who prefer to just listen. The following example illustrates the flexibility of the grading system and the role of participation points. Suppose student A earned zero participation points. Accordingly, her midterm paper weight is set to 25%, and her final paper weight is set to 75%. Nevertheless, student A can still get a perfect TS by scoring 100 on each of the papers – non-participation is not penalized. By way of comparison, suppose student B earned 20 participation points credited towards her TS (and reducing her TPW to 80 %). For student

B, the midterm paper weight is 20%, and the final paper weight is 60%. Student B’s reduced TPW means that her paper scores will have a somewhat smaller impact on her TS than that of Student A. In other words, participation points act as insurance against less-than-perfect performance on papers.

Midterm and final paper requirements

Midterm paper

Weight: Length: 500-700 words

������������������

=

1 4

������������������

Due date: Friday, Oct 16, by 5:00 pm

Topics: students can choose one of 4 topics formulated by the instructor. The list of

topics will be posted on CTOOLs by the end of the first month of the semester

Final paper

Weight: Length: 3500-4500 words

������������������

=

3 4

������������������

Due date: Monday, Dec 21

Topics: students can choose one of at least 15 topics formulated by the instructor. The list

of topics will be posted on CTOOLs by the end of the first month of the semester.

Paper grading criteria include but are not limited to: • quality of understanding and operating with basic concepts discussed in the course
(e.g., main characteristics of three main legal systems (the common law system, the civil law system and the Soviet legal system), main sources of constitutional law, Parliamentarism, specific features of the British constitution, constitutional conventions (political customs), main characteristics of presidential constitutional system, semipresidential constitutional system, parliamentary constitutional system, the Soviet republic, the executive branch in parliamentary and semipresidential republics; • logical consistency • factual accuracy • adherence to topic chosen

• References to the original text of a Constitution or a legislative act of the country studied are required. Using secondary sources instead of original texts does not count as such reference.
• Bibliography of at least 2 distinct items for a midterm paper and at least 5 distinct items for a final paper outside of the course materials (might be a book, a scholarly article or a legislative act). Very sparse citing from lecture slides is permissible. Wikipedia entries are not considered bibliography items.
Paper formatting requirements Papers must be typed using word processing software and E-mailed in one of two file formats (*.docx, *.pdf) to emishina@umich.edu. Scans or photos of handwritten text are not acceptable.
Course outline
(with required reading in italics)
1. September 8. Importance of Comparative law. Anglo-Saxon (Common law), RomanoGermanic (Civil law) and Soviet Legal Systems.
2. September 10. Constitutions and other sources of law in civil law countries. Socialist Constitutions.
3. September 15. Citizenship. Basic rights and freedoms. Constitutional obligations. Readings –the Universal Declaration of Human Rights (1948), the EU Charter of Fundamental Rights (2000). Part 1.
4. September 17. Citizenship. Basic Rights and freedoms. Constitutional obligations. Readings –the Universal Declaration of Human Rights (1948), the EU Charter of Fundamental Rights (2000). Part 2.
5. September 22. Main constitutional systems. Political regimes. Part 1. 6. September 24. Main constitutional systems. Political regimes. Part 2. Readings –
“Divine Totalitarianism” (by Ekaterina Mishina). 7. September 29. Head of the State and the executive branch in civil law countries 8. October 01. Parliament and parliamentarism. 9. October 06. Constitutional status of courts and judges. Readings- “The Bangalore
Principles of Judicial Conduct” (2002).

10. October 08. The role of judges in different legal systems. Judicial independence. Constitutional review. Readings – “Kyiv Recommendations on Judicial Independence in Eastern Europe, South Caucasus and central Asia”.
11.October 13. European Court for Human Rights. Execution of the ECtHR judgments in Russia. Readings – the European Convention for Human Rights, 1950, “The Kremlin’s Scorn for Strasbourg” (by Ekaterina Mishina).
12. October 15. Evolution of constitutional system of Great Britain. Readings – Magna Carta (1215), the Instrument of Government (1653), the Humble Petition and Advice (1657),
13.October 22. Evolution of constitutional system of Germany. Readings – the Weimar Constitution (1919), the Basic law for the Federal Republic of Germany (1949). Constitution of Vietnam as a perfect example of a socialist Constitution in force. Readings – Constitution of Vietnam (2013). October 27 and October 29 No class meetings, take-home work. Students are expected to do take-home work on these dates. The purpose of this exercise is to develop analytic skills in reading scholarly articles on the topics relevant to the class material. No new material will be given to your this week to allow the class to focus exclusively on the writing assignment. Readings: K. Hendley. “Are Russian Judges Still Soviet?”, “The World Rule of Law Movement and Russian Legal reform”, pp. 96 – 132. Students are expected to complete one short writing assignment ( 250-300 words) in which they synthesize the readings. The suggested topics are: 1) “Importance of Judicial Independence”, 2) “Evidence of persisting Soviet judicial mentality within Russian judicial community”, 3) “Threats to Judicial Independence in the USA and civil law countries”. Assignments are due by November 01. Late assignments will receive zero participation points by default. Assignments received on time will be graded on the scale 0-3 participation points. Grading criteria and paper submission requirements listed above apply to this assignment.
14.November 03. Evolution of the constitutional system of France. Readings – Constitution of the V Republic of France (1958). Evolution of the constitutional system of Italy. Readings – Constitution of Italy (1947).

15.November 05. Evolution of the constitutional system of Spain. Readings – Constitution of Spain (1978). Constitutional system of Japan. Readings – Constitution of Japan (1947).
16.November 10. Constitutional transition of the Baltic States. Constitutional system of Lithuania. Readings – Constitution of Lithuania (1992). The Secret Protocol That Changed the World (by Ekaterina Mishina).
17.November 12. Constitutional system of Estonia. Readings – Constitution of Estonia (1992). Constitutional system of Latvia. Readings – Constitution of Latvia (1922).
18.November 17. Specifics of post-Soviet constitutional transition. Constitutional system of Georgia. Readings – Constitution of Georgia (1995). Constitutional system of Ukraine. Readings – Constitution of Ukraine (1996), the Venice Commission’s Opinions of October 8-9 of 2004 and of December 17-18 of 2010;
19.November 19. Constitutional system of Kyrgyzstan. Readings – Constitution of Kyrgyzstan (1993), Constitution of Kyrgyzstan (2010) Constitutional system of Kazakhstan. Readings – Constitution of Kazakhstan (1995).
20.November 24. Constitutional systems of Azerbaijan and Turkmenistan. Readings – Constitution of Azerbaijan (1995). Constitution of Turkmenistan (2008), Constitutional law on Khalk Maslakhaty (2003).
21.December 01. Discrimination and restriction of constitutional rights and freedoms in non-democratic regimes (freedom of press, freedom of speech, freedom of movement, freedom of assembly, gay rights, gender discrimination, absence of legislation on family violence and sexual harassment issues). Mikhail Khodorkovsky as the first political prisoner in the post-Soviet Russia. The Bolotnaya square case. Assassination of Boris Nemtzov. The ‘Foreign agents” legislation. Readings – “Russian activists in court as” Bolotnaya trial” begins” (By Alexander Winning), “Russia’s Political Prisoners: Mikhail Kosenko” (by Boris Bruk), “Realm of Darkness” (by Ekaterina Mishina)
22.December 03. Discrimination and restriction of constitutional rights and freedoms in non-democratic regimes. Gay rights. The cases of Svetlana Davydova and Pussy Riot. Readings – “Who is Troubled by Gay Propaganda?”, “When the State defends Itself Against Its Own Citizens”, “Whose Feelings Did Marylin Manson and Pussy Riot Hurt” (by Ekaterina Mishina).
23. December 08. Constitutional system of Russia. Readings – Constitution of Russia (1993).
24.December 10. Conclusions.

