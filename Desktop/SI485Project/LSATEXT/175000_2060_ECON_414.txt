ECONOMICS 414 Growth Theory
Dmitriy Stolyarov Fall 2015
Learning objectives This advanced undergraduate course is broadly about the modern approach to analyzing long-run macroeconomic phenomena. The course will focus on the major questions of economic growth and introduce some theoretical tools that are necessary to study them. The major questions include the following. What are the driving forces behind economic growth? Why are some nations richer than others? Will economic growth continue forever? What is the role of the government in shaping the long-run economic success of a nation? We will study key ideas and insights of modern growth theory and apply them to explain the facts from the history of population and technology, economic performance of the world over the long run and growth experience of different nations. The course requirement includes graded bi-weekly problem sets. Since many of the ideas will be developed in algebraic form, students are expected to be comfortable with above-average amount of math.
Class meetings: Mondays and Wednesdays, 2:30 pm – 4:00 pm, 1401 Mason Hall Discussion sections:
Fridays 12:00PM - 1:00PM 1033 DENT or Fridays 1:00PM - 2:00PM R2320 BUS
Textbook: Charles I. Jones, Dietrich Vollrath, Introduction to Economic Growth, 3d ed., W.W. Norton. ISBN 978-0-393-91917-2
Course requirements: midterm exam, final exam, problem sets, reading assignments.
Grading system: Your grade is determined by your homework points, midterms and final. Each homework point equals one percent of perfect grade and reduces the weight that is put on exam scores by 1%. There are 7 problem sets, worth 3-5 points each, for a total of 25 points. You can thus earn up to 25% towards perfect grade by accumulating homework points. The remaining weight is distributed evenly between your highest midterm score and your final score. The lower of your two midterm scores does not count for your grade. For example, if you accumulated 16 out of 25 homework points, you earned 16% towards perfect grade. Then your best midterm and the final will each have equal weights of (100%-16%)/2 = 42%. Accordingly, someone who does not do homework has earned 0 points, and for that student the best midterm and the final each carry 50% weight. Because only one midterm score is used in your evaluation, you are not required to take all three exams, only the final and one midterm of your choice.
Important dates to remember:
1st Midterm exam – Monday, Oct 12, 2:30 pm (sharp) – 4:00 pm, 1401 Mason Hall
2nd Midterm exam – Monday, Nov 16, 2:30 pm (sharp) – 4:00 pm, 1401 Mason Hall
Final Exam – Wednesday, Dec 16, 1:30 pm (sharp) – 3:30 pm, 1401 Mason Hall
Homework due dates: Sep 18, Oct 2, Oct 7, Oct 30, Nov 11, Dec 2, and Dec 11. Please note that homework due dates are unevenly spaced -- plan your work load accordingly.
Makeup exams will not be offered, because the grading system is flexible enough to accommodate some exam absences without grade penalty. If you decide to take this course, it is important that you are able to take the exams on the above dates and times. More generally, this course adheres to specific Economics Department policies on academic misconduct, graded assignments, grade grievances and religious holidays. You can find these policies at http://www.lsa.umich.edu/econ/undergraduatestudy/policiesandprocedures
1

Office hours: Dmitriy Stolyarov Huayu Xu (GSI)

Mondays 11:45 am – 12:45 pm, Fridays 2:00 pm – 3:00 pm, Lorch 313 TBA

Contact:

Dmitriy Stolyarov

Office:

313 Lorch Hall

E-mail

stolyar@umich.edu

Office phone 734-647-5609

Huayu Xu Office: E-mail

TBA xuhy@umich.edu

E-mail policy Questions about grading or other issues related to your performance assessment: E-mail Questions about special accommodations or excused absences: Email, with supporting documents attached as *pdf or *jpeg. Short questions about the material: E-mail or CTOOLs forum as appropriate. Long questions requiring much math: office hours in person or CTOOLs forum as appropriate.

2

Course outline

Reading

Introduction

slides

Neoclassical Growth Theory

Neoclassical production and capital accumulation. Financial

2.1, notes

markets and investment decisions made by firms

Neoclassical model and the national accounts (NIPA) data

notes

Basic Solow model and its evaluation

2.1.1-2.1.5

Solow model with technological progress

2.2-2.3

Solow model with investment-specific technological progress notes

Growth accounting and issues in productivity measurement

2.4, notes, Gordon

1-ST MIDTERM

Solow model application: cross-country income inequality

3.1

Understanding income inequality across countries: relative 3.2-3.3,

income decomposition

Hall-Jones, Lucas

Solow model extension: Human capital accumulation and notes, Mankiw-

income inequality

Romer-Weil

Modern Growth Theory

The peculiar economics of ideas

4.1-4.5

Growth model with accumulation of ideas

5.1

Economics of the endogenous growth model

5.2, notes

Efficiency in the endogenous growth model; policy implications 5.3

2-ND MIDTERM

Growth miracles: small-country model of technology transfer 6.1-6.4

Joint evolution of income, technology and population over very 8.1-8.3, notes, Kremer

long run

Talent allocation and growth

notes,

Murphy-

Schleifer-Vishny

Occupational choice and incentives to invest; History notes, Acemoglu

dependence, multiple equilibria

Corruption: industrial organization approach; implications of notes,

Schleifer-

corruption for growth

Vishny

Topic # 1
2
3 4 5 6 7 1-7 8 9
10
11 12 13 14 8-14 15 16
17
18
19

HW # NA
2
2 2 2 3 3 1-3 4 4
4
5 5 5 5 4-5 6 6
7
7
7

3

Reading List
Required text: Charles I. Jones, Dietrich Vollrath, Introduction to Economic Growth, 3d ed., W.W. Norton. ISBN 978-0-393-91917-2 Other required reading: Gordon, Robert J. "Does the "New Economy" Measure Up to the Great Inventions of the Past?" Journal of Economic Perspectives v14, n4 (Fall 2000): 49-74. Hall Robert E. and Charles I. Jones, "Why do Some Countries Produce so Much More Output per Worker than Others?", Quarterly Journal of Economics, February 1999, 83-116. Lucas, Robert E. Jr., "Why Doesn't Capital Flow from Rich to Poor Countries?", The American Economic Review, Vol. 80, No. 2, Papers and Proceedings of the Hundred and Second Annual Meeting of the American Economic Association. (May, 1990), pp. 92-96. N. Gregory Mankiw; David Romer; David N. Weil, "A Contribution to the Empirics of Economic Growth" The Quarterly Journal of Economics Vol. 107, No. 2 (May, 1992), pp. 407-437 Kremer, Michael, "Population Growth: 1 Million B.C. to the Present," Quarterly Journal of Economics, August 1993, 681-716. Murphy, Kevin, A. Shleifer and R. Vishny, "The allocation of talent: implications for growth" Quarterly Journal of Economics, May 1991, 503-530. Acemoglu, Daron, "Reward Structures and Allocation of Talent", January 1995, European Economic Review, volume 39, pp. 17-33. Schleifer Andrei, and Robert W. Vishny, "Corruption", Quarterly Journal of Economics, August 1993, 599-617.
4

