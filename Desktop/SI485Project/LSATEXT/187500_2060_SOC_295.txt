SOC. 295 TERRORISM, TORTURE AND VIOLENCE

Time: T Th 11:30 am – 1 pm Place: 1449 MH Instructor: Fatma Müge Göçek gocek@umich.edu Office: 4207 LS&A Phone: 647-4228 Office Hours: T 1 - 3 pm

In the twenty-first century, practices of violence in general, and terrorism and torture in particular occupy most media attention. Why have these phenomena risen to the forefront of public discussion and what can we as a society do about them? This course thus approaches sociologically the three contemporary phenomena of terrorism, torture and violence. It grounds each phenomenon in particular events, analyzing the emergence of the Islamic State (IS) for terrorism; the US Senate Torture Report for torture; and sexual assault in the US for violence. There will be three short examinations (each worth 30% of the total grade) at the end of the discussion of each phenomenon. Participation in class discussion will determine the remaining 10% of the course grade.

The course requirements comprise the following:

I. Three exams at the end of the discussion of each phenomenon (90%): The three examinations will take place on

15 October Thursday 2015 in class (30%); 12 November Thursday 2015 in class (30%); 10 December 2015 Thursday in class (30%).

II. Class participation (10%).

_____________________________________________________________________________

COURSE OUTLINE

September 8 T

Introduction

THEORETICAL FRAMEWORK: Triangulation of Violent Phenomena

9/10 Th 9/15 T 9/17 Th

1. Occurrence/Public Event (Experience) 2. Mediatization/Public Interpretation (Meaning Creation) 3. Legal Judgment/Public Sanction (Knowledge Production)

TERRORISM:

9/22 T

Film on IS terrorism

Context/History

9/24 Th

9/29 T

1

10/1 Th

NO CLASS

Instances/Events

10/6 T

10/8 Th

Impact/Consequences

10/13 T

10/15 Th Short Examination on Terrorism -- in class

10/20 T

FALL BREAK

TORTURE: 10/22 Th Film on US Torture 10/27 T 10/29 Th 11/3 T 11/5 Th 11/10 T 11/12 Th Short Examination on Torture -- in class

VIOLENCE:

11/17 T

Film on Sexual Assault

11/19 Th

11/24 T

11/26 Th THANKSGIVING BREAK

12/1 T

12/3 Th

12/8 T

12/10 Th Short Examination on Violence – in class

---------------------------------------------------------------------------------------------------------------------

COURSE READING OUTLINE (9/8) T INTRODUCTION ______________________________________________________________________________

THEORETICAL FRAMEWORK: TRIANGULATION

(9/10) Th 1. Occurrence/Public Event

De Swaan, Abraham. 2015. “Ordinary Perpetrators and Modernity: The Situationist Consensus.” Pp. 19-47 in The Killing Compartments: The Mentality of Mass Murder. New Haven: Yale University Press [28 pp.]

McDonald, Kevin. 2013. “The Return of Violence.” Pp. 1-14 in Our Violent World: Terrorism in Society. Houndmills: Palgrave Macmillan.

2

(9/15) T 2. Mediatization/Public Interpretation

McDonald, Kevin. 2013. “Mediatizing Violence: From Snapshots to the Internet.” Pp. 147-68 in Our Violent World: Terrorism in Society. Houndmills: Palgrave Macmillan [21 pp.]

Parham-Payne, Wanda. 2014. “The Role of the Media in the Disparate Response to Gun Violence in America.” Journal of Black Studies 45/8: 752-68.

(9/17) Th 3. Legal Judgment/Public Sanction

Justice, Benjamin and T.I. Meares. 2014. “How the Criminal Justice System Educates Citizens.” Annals of the American Academy of Political and Social Sciences 651: 159-77 [18 pp.]

Gurnham, David. 2011. “Legal Authority and savagery in judicial rhetoric: sexual violence and the criminal courts.” International Journal of Law in Context 7/2: 117-37 [20 pp.] ______________________________________________________________________________

TERRORISM:

(9/22) T
(9/24) Th Context/History

Film on Terrorism: IS in Syria and the Levant “Frontline – Rise of ISIS” (2014) 53 minutes

Vidino, Lorenzo. 2014. “The Evolution of the Post-9/11 Threat to the U.S. Homeland.” Pp. 3-28 in Hoffman, Bruce and F. Reinares, eds. The Evolution of the Global Terrorist Threat: From 9/11 to Osama bin Laden’s Death. New York: Columbia University Press [25 pp.]

Hafez, Mohammed. 2014. “The Origins of Sectarian Terrorism in Iraq.” Pp. 436-60 in Hoffman, Bruce and F. Reinares, eds. The Evolution of the Global Terrorist Threat: From 9/11 to Osama bin Laden’s Death. New York: Columbia University Press. [24 pp.]

(9/29) T McDonald, Kevin. 2013. “Terrorism.” Pp. 21-33 in Our Violent World: Terrorism in Society. Houndmills: Palgrave Macmillan [12 pp.]

Bonditti, Philippe. 2014. “Violence, ‘Terrorism,’ Otherness: Reshaping Enmity in Times of Terror.” Pp. 192-214 in Roderick Campbell, ed. Violence and Civilization: Studies of Social Violence in History and Pre-history. Oxford: Oxbow Book [22 pp.]

(10/1) Th Instances/Events

3

LaFree, Gary, L. Dugan and E. Miller. 2015. “The Creation of the Global Terrorism Database.” Pp. 12-26 in Putting Terrorism in Context: Lessons from the Global Terrorism Database. London: Routledge [14 pp.]

LaFree, Gary, L. Dugan and E. Miller. 2015. “Tracking Worldwide Terrorism Trends.” Pp. 2748 in Putting Terrorism in Context: Lessons from the Global Terrorism Database. London: Routledge [21 pp.]

(10/6) T Akhlaq, Ahmad. 2014. “The Role of Social Networks in the Recruitment of Youth in an Islamist Organization in Pakistan.” Sociological Spectrum 34/6: 469-88 [19 pp.]

Becker, Michael. 2014. “Explaining Lone Wolf Target Selection in the United States.” Studies in Conflict and Terrorism. 37/1: 959-78 [19 pp.]

(10/8) Th Impact/Consequences

Turk, Austin. 2004. “Sociology of Terrorism.” Annual Review of Sociology 30: 271-86 [15].

Deflem, Mathieu and S. McDonough. 2015. “The Fear of Counterterrorism: Surveillance and Civil Liberties since 9/11/” Society. 52: 70-9 [9 pp.]

(10/13) T Walklate, Sandra and G. Mythen. 2015. “Constructing New Terrorism: Discourse, Representation and Ideology.” Pp. 43-63 in Contradictions of Terrorism: Security, Risk and Resilience. London: Routledge [20 pp.]

McDonald, Kevin. 2013. “Beyond Terror?” Pp. 189-210 in Our Violent World: Terrorism in Society. Houndmills: Palgrave Macmillan [21 pp.]

(10/15) Th Short Examination on Terrorism – in class ______________________________________________________________________________

(10/20) T

FALL BREAK

______________________________________________________________________________

TORTURE:

(10/22) Th Film on Torture: US Torture Committed Overseas “Taxi to the Dark Side” (2007) 75 minutes [48698-D]

(10/27) T Context/History

4

Gordon, Rebecca. 2014. “Describing the Problem.” Pp. 14-35 in Mainstreaming Torture: Ethical Approaches in the Post-9/11 United States. New York: Oxford University Press [21 pp.]

Hajjar, Lisa. 2013. “Why Are We (Still) Talking about Torture?” Pp. 1-13 in Torture: A Sociology of Violence and Human Rights. New York: Routledge [12 pp.]

(10/29) Th Harlow, Barbara. 2011. “’Extraordinary Renditions:’ tales of Guantanamo, a review article.” Race and Class 52/4: 1-29 [28 pp.]

Brown, Michelle. 2005. “’Setting the Conditions’ for Abu Ghraib: The Prison Nation Abroad.” American Quarterly 57/3: 973-97 [24 pp.]

(11/3) T Instances/Events

Briggs, Laura. 2015. “Making Race, Making Sex: Perspectives on Torture.” International Feminist Journal of Politics 17/1: 20-39 [19 pp.]

Butler, Judith. 2008. “Sexual politics, torture and secular time.” British Journal of Sociology 59/1: 1-23 [22 pp.]

(11/5) Th Hron, Madeleine. 2008. “Torture Goes Pop!” Peace Review. 20: 22-30 [8 pp.].

Hutchings, Peter J. 2012. “Entertaining Torture, Embodying Law.” Cultural Studies 27/1: 49-71 [22 pp.]

(11/10) T Impact/Consequences

Del Rosso. 2014. “The Toxicity of Torture: The Cultural Structure of US Political Discourse on Waterboarding.” Social Forces 93/1: 383-404 [21 pp.]

Gordon, Rebecca. 2014. “What Is To Be Done?” Pp. 183-205 in Mainstreaming Torture: Ethical Approaches in the Post-9/11 United States. New York: Oxford University Press [22 pp.]

(11/12) Th Short Examination on Torture – in class ______________________________________________________________________________

VIOLENCE:

(11/17) T

Film on Violence: Sexual Assault in US Campuses “The Hunting Ground” (2013) 96 minutes [67458-D]

(11/19) Th

5

Context/History Presser, Lois. 2013. “Intimate Partner Violence: A Familiar Stranger.” Pp.69-87 in Why We Harm. New Brunswick: Rutgers University Press [18 pp.]
Tyner, James A. 2012. “Home.” Pp. 25-68 in Space, Place, and Violence: Violence and the Embodied Geographies of Race, Sex, and Gender. New York: Routledge [43 pp.]
(11/24) T
Hollander, Joceyln and K. Rodgers. 2014. “Constructing Victims: The Erasure of Women’s Resistance to Sexual Assault.” Sociological Forum 29/2: 342-64 [22 pp.]
Hlavka, Heather R. 2014. “Normalizing Sexual Violence: Young Women Account for Harassment and Abuse.” Gender and Society 28/3: 337-58 [21 pp.]
(11/26) Th THANKSGIVING RECESS
(12/1) T Instances/Events
Armstrong, Elizabeth, L. Hamilton and B. Sweeney. 2006. “Sexual Assault on Campus: A Multilevel, Integrative Approach.” Social Problems. 53/4: 483-99 [16 pp.]
Yung, Corey Rayburn. 2015. “Concealing Campus Sexual Assault: An Empirical Examination.” Psychology, Public Policy, Law 21/1: 1-9 [8 pp.]
(12/3) Th
Triplett, Matthew R. 2012. “Sexual Assault on College Campuses: Seeking the Appropriate Balance between Due Process and Victim Protection.” Duke Law Journal 62/2: 487-527 [40 pp.]
Franklin, Courtney, L.A. Bouffard and T.C. Pratt. 2012. “Sexual Assault on College Campus: Fraternity Affiliation, Male Peer Support, and Low Self Control.” Criminal Justice and Behavior. 39/11: 1457-80 [23 pp.].
Impact/Consequences (12/8) T
Cares, Alison et al. “Changing Attitudes about Being a Bystander to Violence: Translating an InPerson Sexual Violence Prevention Program to a New Campus.” Violence against Women 21/2: 165-87 [22 pp.]
Thornton, William, L. Voigt, and D.W Harper. 2013. “Is Peace Possible?” Pp. 343-61 in Why Violence? Leading Questions Regarding the Conceptualization and Reality of Violence in Society. Durham: Carolina Academic Press [18 pp.]
6

(12/10) Th Short Examination on Violence – in class 7

