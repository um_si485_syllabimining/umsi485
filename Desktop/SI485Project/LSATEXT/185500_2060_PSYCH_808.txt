10/14/2015
PSYCH 808 005 / SW 877 001 FA 2015
Jump to Today

PSYCH 808 005 / SW 877 001 FA 2015

Social Work 877/Psych 808: Special Seminar in Social Work and Psychology

INSTRUCTOR   Lorraine Gutierrez

OFFICES

 3267 EAST HALL/3828 SSWB

EMAIL

 lorraing@umich.edu

PHONE

 734­936­1450

OFFICE HOURS  Tuesdays, 10 ­ 12, 3267 EH
 
Overview
In this class we will focus on social issues of concern to social work as a field at various levels of analyses and ask what psychological theories and evidence can be brought to bear to understand which policies, practices and interventions are likely to work and which are unlikely to work because they fail to take into account what is known about memory, cognition, persuasion, motivation, procrastination, inter group relations and other topics well researched in psychology. The course is organized thematically with a focus on the different ways in which psychologists or social workers have conducted research have used psychological perspectives to address social work policy, practice and intervention. Current scholars in social work and psychology will spent time with us in most classes to discuss how their work reflects the themes of the course.
Course Readings
All readings will be posted to our course Canvas site in the Files section. Please read all articles before the assigned class.
Course Requirements
Research Paper
The purpose of the paper will be to apply psychological theory or research to a specific topic or issue of concern to social work. The paper should locate the specific topic or issue in the social work literature and identify how psychological theory or research can inform social work policy or practice. This paper should also describe how research could asses the effectiveness of this policy or practice.  I encourage students to meet with me outside class, in office hours if convenient, for consultation in developing their paper.
In the final class session each student will discuss their paper and what they learned. 
This paper will be turned in in three parts:
1.  A 1­2 page memo to me identifying the focus of your paper. (due October 23) 2.  The 15 ­ 18 page paper (due December 17) 3.  A 10­minute conference type, presentation of your paper for our final class meeting (December 10). 
Class Facilitation:
We will spend a part of each class discussing the readings for that day. We will use a structured process for our reading discussions that will ensure that each student is equally involved. This structure will give each students a chance to organize their thinking around social work topics of interest to them and grapple with the psychological research relevant to their topic. 
The roles for class discussions are specified in the Class Facilitation information on Canvas. Before each class each student will complete the reading and a reading preparation sheet for their reading role. The recorder will submit their assignment prior to the following class session.
Class Participation
Participation includes being present, on time, active, and prepared for class and group discussions. Reading the assigned materials prior to class is also required. Active discussion includes asking questions or providing critical perspectives on the readings. Participation will be assessed with an online self­assessment of class participation which covers these different dimensions.
Grading System
Each assignment has been given a specific number of points. Grades will be calculated based on the total number of points earned on assignments during the semester.

Assignment 

Points 

Class Facilitation

 30

Paper plan memo

 10

Research Paper

 30

Research Presentation    20

Class Participation

10

 

Grading Scale:

A+ = 100 A = 95 A­ = 90 B+ = 87  B = 83 B­ = 80

C+ = 77 C = 73  C– = 70  D+ = 67 D = 63  D ­ = 60

E = 50  F = 40  

  

 

https://umich.instructure.com/courses/37517

1/5

10/14/2015
Course Policies

PSYCH 808 005 / SW 877 001 FA 2015

Accommodations for students with disabilities

If you need an accommodation for a disability please let me know as soon as possible. Many aspects of this course, the assignments, the in­ class activities, and teaching methods can be modified to facilitate your participation and progress throughout the semester. I will make every effort to use the resources available to us, such as the services for Students with Disabilities, the Adaptive Technology Computing Site, and the like. If you disclose your disability, I will (to the extent permitted by law) treat that information as private and confidential. For more information and resources, please contact the Services for Students with Disabilities office at G664 Haven Hall, (734) 763­ 3000 or go to http://www.umich.edu/~sswd/ (http://www.umich.edu/~sswd/)

Health and Wellness Services

Health and wellness encompasses situations or circumstances that may impede your success within the program. The Office of Student Services  (http://ssw.umich.edu/student­life) offers health and wellness services that are directed to the MSW student body. Feel free to contact Health and Wellness Advocates Lauren Davis (laurdavi@umich.edu) or Nyshourn Price­R  eed (ndp@umich.edu); 734­9  36‐0961, regarding any health, mental health or wellness issue. This could include need for advocacy and referral to University or community resources, financial resources or counseling. Also contact Health and Wellness using ssw.wellness@umich.edu (mailto:ssw.wellness@umich.edu) . 

Dependent Care Resources

For students with child­ or parenting/elder care responsibilities, feel free to consult the Students with Children website (http://www.studentswithchildren.umich.edu). This site is dedicated to the needs of students at UM who juggle parenting/elder care, study, and work. Resources include child care, financial assistance, social support, housing, and health care information. The website was created by the former Committee on Student Parent Issues (COSPI). For additional information on work/life support please also visit the Work/Life Resource Center site  (http://hr.umich.edu/worklife/)  and the UM Child Care gateway  (http://hr.umich.edu/childcare/) .

Religious Observances

Please notify me if religious observances conflict with class attendance or due dates for assignments so that we can make appropriate arrangements. Any absences due to religious observances are excused.

Student Code of Academic and Professional Conduct

All students should be familiar with the Student Code for Academic and Professional Conduct  (http://ssw.umich.edu/msw­student­guide/section/1.12.00/student­code­of­academic­ and­professional­conduct)  which holds our students to the highest standards of academic and professional conduct.  unacceptable academic behavior refers to actions or behaviors that are contrary to maintaining the highest standards in course work and includes such actions as cheating, plagiarism, falsification of data, aiding and abetting dishonesty and impairment. Any suspected situations of academic misconduct will be discussed with the student and then reported to the Associate Dean for Academic Programs, Jorge Delva. 

Campus Commitment and "Expect Respect" campaign

A respectful, supportive, and welcoming environment are necessary for student learning. The University of Michigan has developed the Campus Commitment and Expect Respect campaign  (https://urespect.umich.edu/)  as a University­wide educational program that "provides clear definitions of discrimination and harassment prohibited by University policy, as well as contact information for University resources and ways to report concerns. Its goal is to contribute to an environment of civility and respect in which all members of the University community can work and learn." For more information on this program please click on the link in this paragraph. Please contact me if you have any concerns about these issues as they relate to our class or your experience at the UM in general.
Course Schedule

Week  Topic 

Readings 

Sept 10 

Introduction to the Course and Each Other

Activities  What do we mean by of "Social Work" and "Psychology"
Guest Speaker ­ Patricia Gurin  (https://igr.umich.edu/pro gurin­phd)

Sept 17

Lambert, S.J., Haley­Lock, A., & Henly, J.R.  (2012). Schedule flexibility in hourly jobs: Unanticipated

consequences and promising directions. Community, Work & Family. 15 (3), 293­315.

(https://umich.instructure.com/courses/37517/files/307264/download?wrap=1) 

Precarious scheduling

(https://umich.instructure.com/courses/37517/files/307264/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/307264/download?wrap=1)

practices:

Henly, J.R., & Lambert, S.J. (2014). Unpredictable work timing in retail jobs: Implications for employee

Conducting research to advance

work­life outcomes. Industrial and Labor Relations Review. 67 (3): 986­1016 (https://umich.instructure.com/courses/37517/files/307268/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/307268/download?wrap=1) 

Guest Speaker: Susan Lambert

knowledge,

(https://umich.instructure.com/courses/37517/files/307268/download?wrap=1) .

employer practice, and public policy

Lambert, S.J., Fugiel, P.J. & Henly, J.R.  (2014). Schedule Unpredictability among Early Career Adults in the US Labor Market: A National Snapshot. Research brief issued by EINet (Employment Instability,

Family Well­being, and Social Policy Network) at the University of Chicago:

(https://ssascholars.uchicago.edu/sites/default/files/einet/files/lambert.fugiel.henly_.executive_summary.b_0.pdf)

  http://ssascholars.uchicago.edu/einet  (http://ssascholars.uchicago.edu/einet)

 (https://ssascholars.uchicago.edu/s­lamb

Barth, R.P., Lee, B.R., Lindsey, M.A., Collins, K.S., Strieder, F., Chorpita, B.F., Becker, K.D., & Sparks, J.A. (2012). Evidence­based practice at a crossroads: The timely emergence of common elements and common factors. Research  on Social Work Practice, 22(1), 108­119. (https://umich.instructure.com/courses/37517/files/391044/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/391044/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/391044/download?wrap=1)

Sept 24

Integrating theory, Begun, A.L. (1999).  Intimate partner violence: An HBSE perspective. Journal of Social Work or “Why can’t the Education, 35(2), 239­252. (https://umich.instructure.com/courses/37517/files/391043/download?wrap=1)  baby ride a bike?” (https://umich.instructure.com/courses/37517/files/391043/download?wrap=1) 
(https://umich.instructure.com/courses/37517/files/391043/download?wrap=1)

Guest Speaker:
Audrey Begun  (http://csw.osu.edu/about/faculty­staff/facu directory/begun­audrey­ph­d/)

Matto, H.C., & Strolin­Goltzmsn, J. (2010). Integrating social neuroscience and social work:

https://umich.instructure.com/courses/37517

2/5

10/14/2015

PSYCH 808 005 / SW 877 001 FA 2015
Innovations for advancing practice­based research. Social Work, 55(2), 147­15 (https://umich.instructure.com/courses/37517/files/391042/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/391042/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/391042/download?wrap=1)

Bay­Cheng, L. Y. (2015). The Agency Line: A neoliberal metric for appraising young women’s sexuality. Sex Roles. 1­13. (https://umich.instructure.com/courses/37517/files/283182/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/283182/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/283182/download?wrap=1)

Katz, J., & Tirone, V. (2015). From the agency line to the picket line: Neoliberal ideals, sexual realities, and arguments about abortion in the U.S. Sex Roles. doi:10.1007/s11199­015­0475­z (https://umich.instructure.com/courses/37517/files/283291/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/283291/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/283291/download?wrap=1)

Oct 1

Appraising Young Women's Sexuality ­ Friendly Amendments and Productive Debates

Lamb, S. (2015). Revisiting choice and victimization: A commentary on Bay­Cheng’s agency matrix. Sex Roles. doi:10.1007/s11199­015­0508­7 (https://umich.instructure.com/courses/37517/files/283293/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/283293/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/283293/download?wrap=1)
Tolman, D. L., Anderson, S. M., & Belmonte, K. (2015). Mobilizing metaphor: Considering complexities, contradictions, and contexts in adolescent girls’ and young women’s sexual agency.Sex Roles. doi: 10.1007/s11199­015­0510­0 (https://umich.instructure.com/courses/37517/files/283294/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/283294/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/283294/download?wrap=1)

Guest Speaker:
Laina Bay­Cheng  (http://socialwork.buffalo.edu/faculty­re time­faculty/lb35.html)

Lerum, K., & Dworkin, S. L. (2015). Sexual agency is not a problem of neoliberalism: Feminism, sexual justice, & the carceral turn. Sex Roles. doi: 10.1007/s11199­015­0525­6 (https://umich.instructure.com/courses/37517/files/283292/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/283292/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/283292/download?wrap=1)

Bay­Cheng, 2015 Living in Metaphors, Trapped in Matrix.pdf (https://umich.instructure.com/courses/37517/files/391057/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/391057/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/391057/download?wrap=1)

(https://umich.instructure.com/courses/37517/files/202191/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/202191/download?wrap=1) (https://umich.instructure.com/courses/37517/files/235411/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/235411/download?wrap=1) Potenza, M.N., Grilo, C.M., & Gearhardt, A.N. (2015). Current Considerations Regarding Food Addiction. Current Addiction Reports, 7, 1­8 (https://umich.instructure.com/courses/37517/files/235411/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/235411/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/235411/download?wrap=1)

Oct 8

Schulte, E.M., Avena, N.M., & Gearhardt, A.N. (2015).  Which foods may be addictive? The roles of

processing, fat and glycemic load. PLOS ONE, 10(2): e0117959. doi:10.1371/journal. pone.0117959.

Disordered Eating (https://umich.instructure.com/courses/37517/files/235409/download?wrap=1) 

and Addiction

(https://umich.instructure.com/courses/37517/files/235409/download?wrap=1) 

(https://umich.instructure.com/courses/37517/files/235409/download?wrap=1)

Guest Speaker:
Ashley Gearhardt (https://www.lsa.umich.edu/psych/people/faculty/ci.gearhardta

(https://umich.instructure.com/courses/37517/files/235410/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/235410/download?wrap=1) Gearhardt, A.N., (https://umich.instructure.com/courses/37517/files/235410/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/235410/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/235410/download?wrap=1) White, M.A., & Potenza, M.N. (2011). Binge eating disorder and food addiction. Current Drug Abuse Reviews, 4, 201­207.

(https://umich.instructure.com/courses/37517/files/235411/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/235411/download?wrap=1)

Oct Research Paper I will be away from campus on Oct 15. Therefore, I would like to set up individual meetings with each student  

15 Planning

this week to discuss their possible research topics. 

Conway, A., McDonough, S.C, MacKenzie, M. J., Follett, C., & Sameroff, A. (2013). Stress­related

changes in toddlers and their mothers following the attack of September 11. American Journal of

Orthopsychiatry, 83(4), 536­44. (https://umich.instructure.com/courses/37517/files/318103/download?wrap=1) 

A contextual and

 (https://umich.instructure.com/courses/37517/files/318103/download?wrap=1) 

neurobehaviorally­ (https://umich.instructure.com/courses/37517/files/318103/download?wrap=1)

informed

Oct

approach to

Conway, A. (2005). Girls, aggression, and emotion regulation. American Journal of Orthopsychiatry, Guest Speaker:

understanding the 75(2), 334­339. (https://umich.instructure.com/courses/37517/files/318104/download?wrap=1) 

22 development of (https://umich.instructure.com/courses/37517/files/318104/download?wrap=1) 

Anne Conway  (http://socialwork.columbia.edu/faculty/ann

children’s self­

(https://umich.instructure.com/courses/37517/files/318104/download?wrap=1)

regulation and

Conway, A. & Stifter, C. A. (2012). Longitudinal antecedents of executive function. Child

https://umich.instructure.com/courses/37517

3/5

10/14/2015
mental health

PSYCH 808 005 / SW 877 001 FA 2015
Development, 83(3), 1022­36 (https://umich.instructure.com/courses/37517/files/318102/download?wrap=1)   (https://umich.instructure.com/courses/37517/files/318102/download?wrap=1) 
(https://umich.instructure.com/courses/37517/files/318102/download?wrap=1) .

Himle, J.A., Bybee, D., Steinberger, E., Laviolette, W.T., Golenberg, Z., Heimberg, R.G., Weaver, A., Vlnka, S., Levine, D.S., & O’Donnell, L.A.  (2014). Work­related CBT versus vocational services as usual for unemployed persons with social anxiety disorder: A randomized controlled pilot trial. Behaviour Research and Therapy, 63, 169­176. (https://umich.instructure.com/courses/37517/files/429226/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/429226/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/429226/download?wrap=1)

Himle, J.A., Weaver, A., Bybee, D., O'Donnell, L.A., Vlnka, S., Laviolette, W.T., Steinberger, E.,

Golenberg, Z., & Levine, D.S. (2014).  Employment barriers, skills and aspirations among unemployed

job­seekers with and without social anxiety. Psychiatric Services, 65, 924­930.

Oct Social Anxiety and (https://umich.instructure.com/courses/37517/files/429228/download?wrap=1)  29 Unemployment (https://umich.instructure.com/courses/37517/files/429228/download?wrap=1) 
(https://umich.instructure.com/courses/37517/files/429228/download?wrap=1)

Guest Speaker: Joseph Himle  (http://ssw.umich.edu/faculty/profiles/tenur

(https://umich.instructure.com/courses/37517/files/429238/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/429238/download?wrap=1) (https://umich.instructure.com/courses/37517/files/429238/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/429238/download?wrap=1)

Tolman, R., Himle, J., Bybee, D., Abelson, J., Hoffman, J., & Van Etten­Lee, M.  (2009). Impact of social anxiety disorder on employment among women receiving welfare benefits.  Psychiatric Services, 60, 61­66. (https://umich.instructure.com/courses/37517/files/429238/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/429238/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/429238/download?wrap=1)

The politics of Nov 5 racial
categorization

Ho­Roberts­Gelman­in­press.pdf (https://umich.instructure.com/courses/37517/files/217109/download? wrap=1)   (https://umich.instructure.com/courses/37517/files/217109/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/217109/download?wrap=1)
HoSidaniusCuddyBanaji_2013.pdf (https://umich.instructure.com/courses/37517/files/217111/download? wrap=1)   (https://umich.instructure.com/courses/37517/files/217111/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/217111/download?wrap=1)
HoSidaniusLevinBanaji_2011.pdf (https://umich.instructure.com/courses/37517/files/217110/download? wrap=1)   (https://umich.instructure.com/courses/37517/files/217110/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/217110/download?wrap=1)
 
 

Guest Speaker:
Arnold Ho (https://www.lsa.umich.edu/psych/people/fulldirectory/ci.hoar

Nov Critical 12 Awareness

Pitner, R. O. & Sakamoto, I. (In press). Cultural competence and critical consciousness in social work pedagogy. Encyclopedia of Social Work. Oxford University Press.  (https://umich.instructure.com/courses/37517/files/283182/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/283182/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/283182/download?wrap=1)

 Pitner, R. O. & Sakamoto, I. (2005). Examining the role of critical consciousness in multicultural practice: Examining how its strength becomes its limitation. American Journal of Orthopsychiatry, 75(4), 684­694. (https://umich.instructure.com/courses/37517/files/202191/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/202191/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/202191/download?wrap=1)  

guest speakers: Ronald Pitner  (http://www.cosw.sc.edu/f pitner) , University of South Carolina
Izumi Sakamoto  (http://www.ccqhr.utoronto.ca/izumi­saka University of Toronto

Sakamoto, I. & Pitner, R. O. (2005). Use of critical consciousness in anti­oppressive social work practice: Disentangling power dynamics at personal and structural levels. British Journal of Social Work, 35(4), 420­437.  (https://umich.instructure.com/courses/37517/files/202190/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/202190/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/202190/download?wrap=1)

Nurius, Hoy­Ellis ­ Stress_Effects_on_Health.pdf (https://umich.instructure.com/courses/37517/files/217105/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/217105/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/217105/download?wrap=1)

Nurius, Greena, Logan­Greene, Borjaa ­ CA_N ACEs Psych Well­being 2015 pub.pdf

(https://umich.instructure.com/courses/37517/files/217106/download?wrap=1) 

Psychological <­> (https://umich.instructure.com/courses/37517/files/217106/download?wrap=1) 

Biological

(https://umich.instructure.com/courses/37517/files/217106/download?wrap=1)

Pathways to Life

Nov Course Health, Nurius, Greena, Logan­Greene, Borjaa ­ Cum Disadv _ Youth Well­being CASW 2015.pdf

19 Learning, and

(https://umich.instructure.com/courses/37517/files/217107/download?wrap=1) 

Development:

(https://umich.instructure.com/courses/37517/files/217107/download?wrap=1) 

Guest Speaker: Paula Nurius  (http://socialwork.uw.edu/fa nurius)

Stress

(https://umich.instructure.com/courses/37517/files/217107/download?wrap=1)

Frameworks

Prince_Nurius_2014_AcademicID.pdf (https://umich.instructure.com/courses/37517/files/217108/download?

https://umich.instructure.com/courses/37517

4/5

10/14/2015

PSYCH 808 005 / SW 877 001 FA 2015
wrap=1)   (https://umich.instructure.com/courses/37517/files/217108/download?wrap=1)  (https://umich.instructure.com/courses/37517/files/217108/download?wrap=1)
optional: 
Prince, D. & Nurius, P. S. (2014). The role of positive academic self­concept in promoting school success. Children & Youth Services Review, 43, 145­152.

Plomin, R. (2011). Commentary: Why are children in the same family so different? Non­shared

environment three decades later. International Journal of Epidemiology, 40(3), 582­592. 

(https://umich.instructure.com/courses/37517/files/229911/download?wrap=1) 

Influences on

(https://umich.instructure.com/courses/37517/files/229911/download?wrap=1) 

12 ­ human behavior: (https://umich.instructure.com/courses/37517/files/229911/download?wrap=1)

Dec 3  The role of genes Kendler, K. S., & Baker, J. H. (2007). Genetic influences on measures of the environment: a

and environment systematic review.Psychological medicine, 37(05), 615­626.  

(https://umich.instructure.com/courses/37517/files/229912/download?wrap=1) 

(https://umich.instructure.com/courses/37517/files/229912/download?wrap=1) 

(https://umich.instructure.com/courses/37517/files/229912/download?wrap=1)

Guest Speaker: Cristina Bares  (http://ssw.umich.edu/faculty/profiles/tenu

13 ­ Dec 10

Putting it all together

Student research presentations and review of our learning

  Date Thu Sep 17, 2015 Thu Sep 24, 2015 Thu Oct 1, 2015 Thu Oct 8, 2015 Thu Oct 22, 2015 Fri Oct 23, 2015 Thu Oct 29, 2015 Thu Nov 5, 2015 Thu Nov 12, 2015 Thu Nov 19, 2015 Thu Dec 3, 2015 Thu Dec 10, 2015 Fri Dec 11, 2015 Thu Dec 17, 2015

Details Reading Discussion Roles 1 (https://umich.instructure.com/courses/37517/assignments/20293) Reading Discussion Roles 2 (https://umich.instructure.com/courses/37517/assignments/20294) Reading Discussion Roles 3 (https://umich.instructure.com/courses/37517/assignments/20295) Reading Discussion Roles 4 (https://umich.instructure.com/courses/37517/assignments/20297) Reading Discussion Roles 5 (https://umich.instructure.com/courses/37517/assignments/20298) Research Paper Memo (https://umich.instructure.com/courses/37517/assignments/20273) Reading Discussion Roles 6 (https://umich.instructure.com/courses/37517/assignments/20299) Reading Discussion Roles 7 (https://umich.instructure.com/courses/37517/assignments/20300) Reading Discussion Roles 8 (https://umich.instructure.com/courses/37517/assignments/20301) Reading Discussion Roles 9 (https://umich.instructure.com/courses/37517/assignments/20302) Reading Discussion Roles 10 (https://umich.instructure.com/courses/37517/assignments/20303) Research Paper Presentation (https://umich.instructure.com/courses/37517/assignments/20277) Class Participation (https://umich.instructure.com/courses/37517/assignments/13732) Research Paper (https://umich.instructure.com/courses/37517/assignments/20280)

11:59am 11:59am 11:59am 11:59am 11:59am 11:59pm 11:59pm 11:59pm 11:59am 11:59am 11:59am 11:59pm 11:59pm 11:59pm

https://umich.instructure.com/courses/37517

5/5

