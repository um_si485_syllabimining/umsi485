English 495: Writing the Honors Thesis MW 1-2:30pm Fall 2015

Section 001 (G228 Angell) Sean Silver srsilver@umich.edu Office: 3180 Angell Hall

Section 002 (G449 Mason) Valerie Traub traubv@umich.edu Office: 3064 Tisch Hall

COURSE DESCRIPTION AND POLICIES

The goal of this course is to support you in conceiving and writing an honors thesis. A thesis is a multi-part analytical project, which requires different skills from a shorter essay on a prescribed topic; we therefore will explore such topics as creating a project bibliography, reading scholarship critically and effectively, identifying a good question, drafting discrete sections with an eye to revision, piecing large segments of writing together, and summarizing for the purposes of abstracts, introductions, and conclusions. We will clarify the difference between description and analysis; we will evaluate what counts as good “evidence”; and we will spend a lot of time creating a strong argument. The work of the class will be staged so that each skill will build on the next.
Much of this seminar will be organized like a workshop. It will be one of the primary places that you will present your work in progress to the class, reading and responding to the work of your classmates. In a linked series of assignments, your writing for the fall semester will culminate in an approximately 40-page polished draft of your thesis. By the end of the semester, you will have a strongly focused sense of the changes and additions that are needed before you turn in the final version of your thesis in March. You will also have an appreciation of the challenges and rewards of advanced work in literary and cultural study.
Along with your peers, your seminar leader will respond to your work throughout the term. You will not receive grades for individual assignments, but you will receive a grade for the course that will reflect three things above all: the timely completion of assignments, the quality of the thought you have put into those assignments, and your generous and helpful participation in class discussions (including thesis draft workshops). Your grade for 495 is independent from the awarding of Honors at the end of the year, as that determination is based on faculty readers' assessment of your finished thesis. What counts most for this course is thoughtful, consistent involvement. Because the course is a collaborative workshop, your attendance is crucial. If you must miss a class due to illness, a personal emergency, or a religious holiday, please email your instructor beforehand.
In our discussions, we will have opportunities to explore matters of importance to all of us. (You wouldn’t be writing a thesis if you didn’t care about it, right?) Our conversations may touch on ideas and perspectives that are unfamiliar or challenging. Our aim will be to engage in mutual exploration of issues rather than in defending points of view. Throughout, our expectation with be one of mutual respect, evidenced by attentive listening and candid yet supportive questions.
REQUIRED TEXT
Wayne C. Booth, et al., The Craft of Research

GUIDELINES FOR MAJOR ASSIGNMENTS
Unless otherwise specified, please print all assignments double-spaced, stapled, with page numbers, and as close to standard format as you can. Observe all deadlines seriously: don’t let perfectionism sidetrack you!
Topic statement (due 9/16) This is a short document, of one page or less, that sets out your project as you currently understand it. A topic proposes a connection of elements—say, “Images of the Traveler in British Fantasy Fiction after World War II” or “Poetic Uses of Sappho’s Silences in American Women’s Poetry.” It’s likely that your own topic is somewhat underdeveloped at this point, but your statement should identify texts that you find compelling and begin to articulate the angle from which you will approach them. For the purposes of this assignment, we will ask you to compose your topic according to the threepart structure discussed in Chapter 3 of The Craft of Research.
BIBLIOGRAPHY Part one (due 9/9): This assignment is to be completed by the first day of class. It involves library work and critical commentary. Your job is to find, read, and then compile a list of books and articles (at least 8) that might help you develop your thesis. Each entry in the bibliography should list your source in a properly formatted bibliographic style (MLA or Chicago Manual of Style), followed by a sentence or two describing the important features of the source; you are asked to summarize each source’s key points and analyze how your argument could engage with them. This assignment is meant to help you begin isolating the key issues in your project. Having a sense of those issues is vital as you begin your reading and research.
Part two (due 9/28): A properly formatted list of 10 sources (including some or all from part one) that you think might be central to your thesis. Choose three sources that you currently think are the most central to your work. For each of these three, write a write a 150-200 word précis of the source. Summarize the author’s thesis argument objectively in your own words, without editorializing or commentary.
Part three (due 10/12): A list of 12 sources (including some or all from previous bibliographies), that are central to your current thinking. For each source you summarized in part two, write a 200300 position statement: your assessment of its relevance to your research. What aspects of it are compelling? Are there important connections with other work you’ve read? Where do your thoughts differ? What kinds of assumptions is the author working from? Is your engagement with the work in the form of appreciation or critique or both?
PROSPECTUS (3-4 pages; due 10/5; revised 10/12) The prospectus is a document that formally characterizes your thesis as a unified enterprise: a PROJECT. While your topic statement announced the subject of your thesis (along with the problem you will address and its interest and importance), your prospectus needs to propose an initial claim with respect to the topic, making concrete the agenda of the project. In order to develop a strong prospectus, you need to have identified your central, motivating question. Your prospectus will therefore build on the topic statement, which will help you articulate the framing statements included in your thesis Introduction. Typically, a prospectus will follow this format: it identifies a critical context, including making clear what primary and secondary materials are key to the project; it specifies your question/problem within that context; and it either tenders a claim regarding that question/problem, or develops a launching point into your argument. Finally, a strong prospectus will outline the major elements of your argument, describing its sections or chapters.

Along with your prospectus, you will propose an anticipated schedule of your work. This should comprise a completion schedule for the parts of your thesis. While most accomplished writers find that their schedules change while they are working, a realistic schedule sketched out at the start is part of most writers’ routines. Note that some deadlines are fixed, including deadlines for section drafts, so take this into account while putting your schedule together.

SECTION DRAFTS PARTS 1 and 2 (due 10/20, 11/1): Turn in a MINIMUM of 6 PAGES of continuous, well-crafted prose that you envision as part of a chapter of the thesis. You need not start at the beginning, either of the thesis introduction or of a chapter. Many successful theses are not written by starting at the beginning and proceeding in a straight line to the end; they may begin with an articulation of a problem; they may begin with a reading of a literary passage or image; they may begin with engagement with another critic. Turn in a different 6 pages for each deadline. You need not turn in a works cited page, but all citations should include author/title/page number in the appropriate format. Try to make this writing as polished as you can do on your own; in other words, it will not really be a “first” draft, but possibly a 3rd or 4th.

PARTS 3 and 4 (due 11/15, 11/29): Complete a draft of a MINIMUM of 8 PAGES of your thesis for each these dates. You may wish to focus on one particular section or chapter for this assignment. Turn in new prose for each deadline. You need not turn in a works cited page, but all citations should include author/title/page number in the appropriate format.

INTRODUCTION (due 12/7): Complete a draft of a MINIMUM of 6 PAGES of your introduction. Good introductions will introduce your topic in an engaging way, frame the problem you are addressing, and begin describing its importance. It will suggest the nature of the approach or solution you propose, and include a brief sketch of the structure of the thesis, including descriptions of the chapters to follow. Introductions generally draw some of their prose from the prospectus, though of course much will have changed since the prospectus was submitted.

WORKSHOP: Peer Review of Work in Progress (your dates will vary) During the second half of the term, we will workshop several sections of your thesis that you have turned in for review. This process lets us explore the “how” of writing a thesis: for instance, introducing questions and evidence, citing critics, etc. The writing you choose to share with the class should be as polished in style as possible: not an outline or free writing. By this time, you will have learned about appropriate styles and voice by examining honors theses from previous years. You need not turn in a works cited page, but all citations should include author/title/page number in the appropriate format.

THESIS DRAFT (due 12/16). Complete a draft of a MINIMUM of 35 PAGES. Think of your goal as submitting a draft of your thesis by this date. Include a bibliography, properly formatted, with this draft. Some of the prose in this document will be new (i.e., not workshopped), but much of it will be revised from previously submitted drafts. The more polished your work is at this point, the more helpful our responses will be. Do not submit outlines or notes in place of your prose, as all kinds of conceptual and analytic problems can be obscured in that format. Give copies to your advisor, your 495 instructor, and Prof. Silver if you are in Professor Traub’s section of 495.

Key to syllabus:

Normal type: what we're doing in class Italics: what to read before class Bold: written assignment due in class, or as otherwise indicated **: both sections meet together in Silver’s room, or elsewhere as indicated

WEEK ONE 9/9 **Introduction to course
BIBLIOGRAPHIES due (bring a copy to class and post on CTools)
WEEK TWO
9/14 Booth, prologue & chs. 1-5 Sample topic statements (CTools); developing your topic into a question and topic statement
9/16 TOPIC STATEMENTS due (bring a copy to class and post on CTools) Research presentations
WEEK THREE
9/21 **LIBRARY VISIT with subject specialist Sigrid Cordell (meet in Hatcher Gallery 100)** RESPOND ON CTOOLS to 2 peers’ topic statements.
9/23 ** Booth ch 6; creating an archive for your project, managing the research, strategies for gathering information and taking notes, marking up texts; IN CLASS: CREATE A READING LIST Research presentations
WEEK FOUR
9/28 establishing a critical position: Gaipa, “Breaking into the Conversation”; Booth ch. 7-8. BIBLIOGRAPHY PART TWO due
9/30 Prospectus: PRINT and read Brooks, intro to American Lazarus; UNDERLINE thesis and key claims; bring to class Booth ch. 12; Sample Prospectus Research presentations
WEEK FIVE
10/5 **Booth ch. 13 DRAFT PROSPECTUS due; CONCEPT MAPS 10/7 PLANNING AND DRAFT SCHEDULE: workshop prospectus
WEEK SIX 10/12 **English in the Archives: Guest speakers David Gold and Cathy Sanok
REVISED PROSPECTUS AND SCHEDULE; BIBLIOGRAPHY PART THREE due 10/14 Turchi, Workshop Tips; “Shitty First Drafts”
WEEK SEVEN 10/19 FALL BREAK – No class 10/20 DRAFT SECTION I DUE TUESDAY at 11PM 10/21 workshops
WEEK EIGHT 10/26 workshops 10/28 workshops
WEEK NINE 11/1 DRAFT SECTION II DUE SUNDAY AT 11PM

11/2 workshops 11/4 workshops
WEEK TEN 11/9 workshops 11/11 **“mixer”; Booth ch. 14
WEEK ELEVEN 11/15 DRAFT SECTION III DUE SUNDAY AT 11PM 11/16 workshops 11/18 workshops
WEEK TWELVE 11/23 workshops 11/25 Writing Day; NO CLASS
WEEK THIRTEEN 11/29 DRAFT SECTION IV DUE SUNDAY AT 11PM 11/30 Booth ch. 16; Sample Introductions 12/2 TBD
WEEK FOURTEEN 12/7 Sample Conclusions; DRAFT INTRODUCTIONS DUE 12/9 TBD
WEEK FIFTEEN 12/14 **general meeting: LAST DAY OF CLASS 12/16 THESIS DRAFT due at 4pm (35+pp)
BIBLIOGRAPHY:
Joanna Brooks, “Introduction,” American Lazarus: Religion and the Rise of African-American and Native American Literatures (Oxford: Oxford University Press, 2003), 1-18.
Mark Gaipa, “Breaking Into the Conversation: How Students Can Acquire Authority for Their Writing,” Pedagogy: Critical Approaches to Teaching Literature, Language, Composition, and Culture 4.3 (2004): 419-37.
Anne Lamott, “Shitty First Drafts,” from Bird By Bird: Some Instructions on Writing and Life (New York: Anchor, 1994), 21-27.
Peter Turchi, “Making the Most of a Writing Workshop; or, Out of the Workshop, Into the Laboratory.” http://www.peterturchi.com/resources-for-writers/. Used with Permission of the Author.

