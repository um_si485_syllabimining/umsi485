REEES 395 HISTORY 332, POLSCI 395, REEES 395, SLAVIC 395, SOC 392 Russia and the Soviet Union: Reform, Revolution, and the Socialist Experiment
4 credits Fall 2015

Instructor:

Professor Ronald Grigor Suny William H. Sewell, Jr. Distinguished University Professor of History Office: 1767 Haven Hall Office Hours: Tuesdays 4:15-5:15; Thursdays 1:15-2:15; and by appointment. Email: rgsuny@umich.edu

Graduate Student Instructors: William Lamping Office hours/Location: Thursday 11-1 PM, Espresso Royale (S. University) 1101 S. University Ave
Email: willvict@umich.edu

Alina Charniauskaya Office hours/Location: Mondays 4-5 PM, Espresso Royale (State St.) Wednesdays 12-1 PM, Espresso Royale (State St.) 324 S. State St. Email: chalina@umich.edu

Lectures: Section Meetings:

Tuesdays and Thursdays, 2:30-4:00, 260 Weiser Hall (formerly Dennison Hall) 002 Mondays, 11:00-12:00, 3347 MH 003 Mondays, 12:00-1:00, 3347 MH 004 Fridays, 12:00-1:00, 4096 EH 005 Fridays, 10:00-11:00, B834 EQ

Mid-term Exam: due to GSIs by 5:00pm, Friday, November 6, 2015 Final Exam: due to GSIs by 3:30pm, December 18, 2015

OVERVIEW This course is an introduction to the historical and geographic area that comprised the Russian Empire, later the Soviet Union, and now the former Soviet Union. While this region has often been referred to in a shorthand way as “Russia,” the term “Eurasia” has become more widely used since the fall of the USSR. The central theme of this course is “Empire and Nations.” This suggests that many different peoples have lived in the region, and the course will look at the dynamic between imperial rule, tsarist and Soviet, and the rise of ethnic and national identities and consciousness.
This is an area studies course, which is concerned with the history, economy, society, polity, and culture of this region in an integrated manner. The multidisciplinary nature of this course is indicated by the large number of departments with which it is cross-listed. The content of the course is varied: politics is explored along with culture; we read texts and look at films. Students need to keep an open mind and realize that an understanding of the region can be best gained from paying attention to different kinds of information and different intellectual approaches. We are interested not only in learning what happened but why it happened in the way it happened. Students should be able to analyze and compare major trends in academic thought about the region and be able to present their own views about these issues. The experiences in this course will hopefully motivate students to take additional courses about the region and in a variety of disciplines. The knowledge gained in the course should help each student decide whether to choose a minor, a major, or a career in Russian, East European, and Eurasian Studies.
There are additional opportunities besides formal coursework for students to increase their understanding of the region. Many activities related to the region are organized by the Center for Russian, East European, and Eurasian Studies (CREES). Information about CREES activities can be found at http://www.ii.umich.edu/crees/.

Last revised 9/7/11

COURSE REQUIREMENTS AND EVALUATION Attendance at Lectures, Discussion Sections, Films and Exams
• Students are required to attend all lectures and discussions in their sections and are responsible for all material from each lecture—not just the material in the accompanying readings. Students are responsible for obtaining copies of any materials that are handed out. Students should not expect to be able to obtain copies of such material from the professor or the GSIs after the lecture.
• Readings should be done before the lectures and discussions so that students are able to ask questions and participate in discussions. Note that the course requirements include the viewing of two required films and two additional Russian or Soviet films. We provide a list of ten films from which to choose. Students will be asked to write one short essay on their chosen films in either the midterm or the final examination.
• In the event that a student must miss a quiz or exam due to illness or a family emergency, the quiz or exam may be rescheduled, provided that the student presents sufficient documentation. Occasionally a student must be out of town due to a job interview or other occasion that cannot be rescheduled. These and other issues about attendance should be discussed with the relevant GSI as soon as possible.
Quizzes and Exams • Map Quiz – in section meetings • Midterm and Final will be take-home, typed examinations.
Content of Section Meetings • Attendance Each GSI will take attendance in section meetings. Students are responsible for the material discussed in the section meeting even if they are unable to attend. Absences must be explained and excused or will result in a lowering of the participation grade. Students must be on time for section meetings. • Participating in Discussion There are many ways for students to demonstrate that they have read the material and thought about it carefully. These include: contributing ideas or interpretations, asking questions of other students, respectfully disagreeing, or drawing parallels with other course material. • Leading a Discussion Instructors may ask students to pair off into groups of two students in their discussion sections. Once during the semester, each group will lead a discussion on the readings. Leading a discussion involves making a joint presentation on the readings and coming up with some questions for the class. In such cases students will be given an opportunity to choose which discussion they would like to lead. • Weekly Commentaries Another teaching technique that may be employed is written commentaries. In such cases students are required to submit 300-word commentaries responding to one or more of the week’s assigned readings or films. The objectives of these assignments are to: 1) encourage critical thinking about readings and how they complement lecture content; 2) prepare students for discussion; 3) help the GSI make decisions about which questions are of most interest and use for individual sections; and 4) give the GSI opportunities to offer feedback responding to individual student questions and concerns. The GSI will discuss due dates and guidelines for submission in section meetings during the first week of classes. Note: Although commentaries must focus on the readings or films, students (unless otherwise instructed) should choose their own approach in writing these assignments. Some possible approaches include: a discussion of the strengths and weakness of an author’s argument; comparison/contrast between two readings that in some way address similar questions; careful deconstruction of a reading that was not useful. • Other Each GSI may distribute short assignments at other times during the semester. These assignments will not be time consuming.
Academic Integrity Policy: This course follows the academic integrity guidelines set forth by the College of LSA [http://www.lsa.umich.edu/academicintegrity/] and the History Department [http://www.lsa.umich.edu/history/undergraduate/courses/guidelinesandpolicies/academicintegrity.pdf]. Students should familiarize themselves with both of these documents, which explain the standards of academic integrity and clarify the prohibited forms of academic misconduct, like plagiarism. Students in the course should utilize the Chicago
2

Manual of Style Online for all issues of source citation, along with any specific guidelines provided in the course assignments. Clarifying the disciplinary standards of research ethics and source citation is part of the educational mission of this course, and students should consult the faculty instructor and/or GSI regarding any questions. The penalties for deliberate cases of plagiarism and/or other forms of academic misconduct are zero on the assignment or failure for the course, within the parameters of LSA policy. Cases that the instructor judges to be particularly serious, or those in which the student contests the charge of academic misconduct, will be handled by the office of the Assistant Dean for Undergraduate Education. All cases of deliberate academic misconduct that result in formal sanctions of any kind will be reported to the dean’s office, as required by LSA policy, which also ensures due process rights of appeal for students.

NOTE ON PLAGIARISM: Plagiarism will not be tolerated. If a student has questions about what constitutes plagiarism, he or she should consult with his or her section GSI. Students can also refer to the LSA website (http://www.lsa.umich.edu/academicintegrity/) for LSA community standards of academic integrity.

CALCULATION OF GRADE The calculation of the grade is as follows:
• Map Quiz • Section participation and assignments • Mid-term Exam • Final Exam • Total

5% 50% 20% 25% 100%

The course will not be graded on a curve. If students understand the material and are able to express it orally and in written form, they will do very well in this class. Good grades are the result of doing the work!

Questions about grades should first be raised with the GSI for the student’s section.

REQUIRED TEXTS The following books are required for the course and should be bought by the students:

• Ronald Grigor Suny, The Soviet Experiment: Russia, The Soviet Union, and the Successor States, 2nd edition (New York: Oxford University Press, 2011).

Other required readings will be available on the CTools website for this course (www.ctools.umich.edu).

REQUIRED FILMS All films will be screened in the Shapiro Library Screening Room (Second floor of the Shapiro Library building, room 2160). Students unable to attend scheduled screenings can view the films in the Askwith Media Library (AML). Each film will be on reserve for two weeks after the screening date.

You are required to watch the following two films by Professor Eagle’s lecture on October 23rd .
• Strike directed by Sergei Eisenstein, 1924 • Circus directed by Grigori Aleksandrov, 1936

In addition to the above listed films you are required to view two films from the following list. You will be required to write a short essay about the four films you have viewed either on your midterm or final examination.
• October directed by Sergei Eisenstein, 1927 • Earth directed by Aleksandr Dovzhenko, 1930 • Chapayev directed by Georgi Vasilyev and Sergei Vasilyev, 1934 • Shadows of Forgotten Ancestors directed by Sergei Paradzhanov, 1964 • Burnt by the Sun directed by Nikita Mikhailkov, 1995 • Little Vera directed by Vasily Pichul and Maria Khmelic, 1988 • Taxi Blues directed by Pavel Lungin, 1991

3

• The Thief directed by Pavel Chukhrai, 1997 • Prisoner of the Caucasus (also called Prisoner of the Mountains) directed by Sergei Bodrov, 1996 • Brother directed by Alexei Balabanov, 1997
CTOOLS RESOURCES The Resources section of the course site on CTools includes a copy of this syllabus. If the syllabus is modified during the term, the revised syllabus will appear on the CTools website. Suggested readings are also included in the Resources section of that site. It is not required that students read the suggested readings. More suggested readings could be added as the course proceeds.

SCHEDULE OF LECTURES, READINGS, AND FILMS Required readings are listed for each lecture.

Date 9/8/2015 Tuesday 9/10/2015 Thursday
9/15/2015 Tuesday
9/17/2015 Thursday 9/18/2015 Friday 9/21/2015 Monday 9/22/2015 Tuesday
9/24/2015 Thursday
9/29/2015 Tuesday

Lecture Ron Suny
Valerie Kivelson
Ron Suny
Ron Suny Section Quiz Section Quiz
Ron Suny Douglas Northrop
Val Kivelson

Title Russia and Western Eyes Muscovy and Russian Identities
Empresses
Emperors

Required Readings
No reading
Nancy Kollmann, "Muscovite Russia, 1450-1598," in Gregory Freeze, ed., Russia: A History (2009), pp. 31-62. Isabel de Madariaga, Catherine the Great: A Short Biography (New Haven: Yale University Press; 2 edition, 2002), 131-175. Marc Raeff, "The Style of Russia’s Imperial Policy and Prince G. A. Potemkin,” in G. N. Grob (ed.), Statesmen and Statecraft of the Modern West: Essays in Honor of Dwight E. Lee and H. Donaldson Jordon (Barre, MA: Barre, 1967), pp. 151; reprinted in Raeff (ed.), Catherine the Great, A Profile (New York: Hill & Wang, 1972), pp. 197246. Ronald Grigor Suny, The Soviet Experiment, Chapter 1, “The Imperial Legacy,” pp. 3-46.

Map Quiz

Bluebooks not required

Map Quiz

Bluebooks not required

Peasants and Nobles Central Asia
Peoples of the Empire

Gary Marker and Rachel May, eds. and trans., Days of a Russian Noblewoman: The Memories of Anna Labzina. 1758-1821 (DeKalb, Ill.: Northern Illinois University Press, 2001).
Valerie A. Kivelson and Joan Neuberger (eds.), Picturing Russia: Explorations in Visual Culture (New Haven: Yale University Press, 2008), pp. 157217. Nikolai Gogol, “The Overcoat,” in The Collected Tales of Nikolai Gogol, trans. and annotated by Richard Pevear and Larissa Volokhonsky (NY: Vintage Books, 1998), pp. 394424. Leo Tolstoy, “The Wood Felling.” Leo Tolstoy, “Hadji Murat,” in The Cossacks and Other Stories (Penguin Classics, 2006), pp. 335-465, or any other edition.

4

10/1/2015 Thursday
10/6/2015 Tuesday
10 /8/2015 Thursday

Olga Maiorova Ron Suny Ron Suny

Russian Thinkers and the “Nation”
Intelligentsia and Revolutionary Movement
You Say You Want a Revolution

Nikolai Danilevsky, Russia and Europe
Karl Marx, “A Letter on Russia by Karl Marx,” 1879 and 1881, The New International, Vol.1 No.4, November 1934, pp.110-111. http://www.marxists.org/history/etol/newspape/ni/vo l01/no04/marx.htm Karl Marx, “First Draft of Letter to Vera Zasulich,” Marx and Engels Collected Works, Volume 24, p. 346; 1881; http://www.marxists.org/archive/marx/works/1881/0 3/zasulich1.htm Suny, The Soviet Experiment, Chapter 2, “The Double Revolution,” pp. 47-67.

10/13/2015 Krisztina Tuesday Fehervary

Planned Cities and Making the New Socialist Man

Suny, The Soviet Experiment, Chapter 3, “Socialism and Civil War,” pp. 68-109.

10/15/2015 Thursday
10/20/2015 Tuesday
10/22/2015 Thursday

Ron Suny Ron Suny

Civil War, Terror, and Making a New State
FALL STUDY BREAK
Aftermath of Revolution

10/27/2015 Tuesday

Herb Eagle

Seeing through the Lens of a Camera:
Soviet Cinema

10/29/2015 Thursday

Ron Suny

Stalin and Stalinism

10/30/2015 Friday

Mid-term Exam (take-home)

11/3/2015 Tuesday

Brian Porter-Szűcs

The Meanings of World War II

11/5/2015 Thursday

Ron Suny

Cold War

Suny, The Soviet Experiment, Chapter 4, “Nationalism and Revolution,” pp. 110-135. Suny, The Soviet Experiment, Chapter 5, “The Evolution of the Dictatorship,” pp. 139-156.
Suny, The Soviet Experiment, Chapter 6, “Socialism in One Country,” pp. 157-187; Chapter 7, “NEP Society,” pp. 188-212. No readings, but you should have viewed the following two films by this lecture:
● Strike directed by Sergei Eisenstein, 1924 (screening date: TBA)
● Circus directed by Grigorii Aleksandrov, 1936 (screening date: TBA)
Suny, The Soviet Experiment, Chapter 9, “The Stalin Revolution,” pp. 235-251; Chapter 10, “Stalin’s Industrial Revolution,” pp. 252-272; Chapter 11, “Building Stalinism,” pp. 273-290.
Due back by Friday, November 6, 2015 at 5:00 PM
Suny, The Soviet Experiment, Chapter 13, “Collective Security and the Coming of World War II,” pp. 316-335; Chapter 14, “The Great Fatherland War,” pp. 336-362. Suny, The Soviet Experiment, Chapter 15, “The Big Chill: The Cold War Begins,” pp. 363-388. Suny, The Soviet Experiment, Chapter 16, “Late Stalinism at Home and Abroad,” pp. 389-410.

5

11/10/2015 Tuesday
11/12/2015 Thursday
11/17/2015 Tuesday
11/19/2015 Thursday 11/24/2015 Tuesday 11/26/2015 Thursday 12/1/2015 Tuesday
12/3/2015 Thursday
12/8/2014 Tuesday
12/10/2014 Thursday

Ben Nathans
Lewis Siegelbaum William Lamping
Alina Charniauskaya Deborah Field
Ron Suny Ron Suny Yuri Zhukov
Ron Suny

Undoing Stalin
People Moving and Moving People
Nationalism and Soviet Identity
Coloured Revolutions: discussing the Orange and Rose Revolutions
in particular Khrushchev’s Thaw
Thanksgiving Russia and the Successor States The Gorbachev Revolution and the Yeltsin Counter-
Revolution Conceptualizing Putin's
Russia
Imperial Overreach
Final Exam distributed at 4:00
PM

Suny, The Soviet Experiment, Chapter 17, “From Autocracy to Oligarchy,” pp. 413-446. Benjamin Nathans, “Talking Fish: On Soviet Dissident Memoirs,” Journal of Modern History, 87 (September 2015), pp. 1-36.
“Introduction,” from Lewis H. Siegelbaum and Leslie Page Moch, Broad Is My Native Land: Repertoires and Regimes of Migration in Russia's Twentieth Century (Cornell University Press, 2014)
Suny, The Soviet Experiment, Chapter 8, “Culture Wars,” pp. 213-231. Suny, The Soviet Experiment, Chapter 12, “Culture and Society in the Socialist Motherland,” pp. 291315.
Suny, The Soviet Experiment, Chapter 18, “The Paradoxes of Brezhnev’s Long Reign,” pp. 447-475;
Suny, The Soviet Experiment, Chapter 19, “Reform and the Road to Revolution,” pp. 479-514.
Suny, The Soviet Experiment, Chapter 20, “The Second Russian Republic and the Near Abroad,” pp. 515-548. Thomas Graham, “A Russia Problem, Not a Putin Problem: Rebuilding US-Russia Relations.” August 2014 http://perspectives.carnegie.org/us-russia John J. Mearsheimer, “Why the Ukraine Crisis Is the West's Fault: The Liberal Delusions That Provoked Putin Foreign Affairs,” Foreign Affairs (September-October 2014). www.foreignaffairs.com
Due back by Friday, December 18, 2014 at 3:30 PM

6

