Muslims in Contemporary Europe History 324 Fall 2015

Instructor: Prof. Rita Chin Office: 1666 Haven Hall Phone: 615-9320

Meeting Time: MW 11:30am-1:00pm Meeting Place: 3437 Mason Hall Office Hours: M 1:30pm-2:30pm, by appt.

This course examines one of the most important aspects of contemporary European society: the permanent presence of millions of Muslim immigrants. The long-term residence of Muslims has transformed postwar Western European nations, which had generally thought of themselves as homogeneous, into ethnically diverse, multicultural societies. We will examine
how this radical transformation took place. More specifically, we will consider the physical and economic rebuilding of Europe in the wake of total war, which served as a springboard for waves of non-white immigrants to travel to, work, and eventually, settle on the continent. We will investigate the social, economic, and legal parameters that shaped the lives of these new arrivals, as well as European responses to Muslims once their presence became more
permanent. We will also look at the ways this new diversity has changed the cultural landscape of Europe, focusing on film and literature. Finally, we will devote significant time to analyzing several major public debates/events (i.e., the Rushdie Affair in Britain, the headscarf controversy in France, the murder of Theo van Gogh in the Netherlands, the Muslim
citizenship test controversy in Germany) in which Muslim immigrants have emerged as the focal point for questions about the viability of multiculturalism in Europe today.

Course Specifics: This is an upper division course, which is structured around intensive reading and discussion. Each week I will typically spend the first class (Mondays) introducing
new topics by placing them in a series of historical and scholarly contexts. We will devote the second class (Wednesdays) to collective discussion of the readings, many of which are challenging and deal with highly complicated issues. This format is designed to give you many chances to wrestle with the course materials and to exchange ideas with your classmates and
me. The ultimate success of the class depends on your dedication and commitment to our collective endeavor.

This class has three primary goals: 1) to develop your understanding one of the most pressing issues in contemporary European society; 2) to expose you to some of the best and most
cutting-edge scholarship produced by historians and other scholars of Europe; and 3) to help you wrestle with some of the trickiest dilemmas around minority rights, cultural traditions, and liberal democratic values facing Western societies today.

Course Requirements:
• Reading. You must complete each of the reading assignments on time and come to class ready to discuss them. Please bring the assigned books/articles to class on the designated discussion days, so that we can refer to and collectively examine the texts in class.
• Online Responses. In order to help you prepare for our group discussions, you will be required to write online responses throughout the semester in which you “digest” the assigned readings. You do not need to worry about thesis statements or formalized arguments. This assignment is about marking your progress through the readings, getting you to think about your responses to the readings before our in-class discussions, and encouraging you to be more self-conscious about writing as a key tool for analytical thinking. The goal of the exercise is to help you read actively, consciously, and critically.

2
You will need to write an online response in the “Forums” section of the CTools course website for every class discussion (with a few exceptions, these will be every Wednesday). Entries will not receive individual grades because there are no “right” or “wrong” answers or approaches to the material; I am primarily interested in seeing how you struggle with the readings. Please post your online response no later than 10:00pm on the evening before our class discussion (in most cases, that would mean on Tuesday evening). • Participation and Attendance. Your engaged participation is essential to make this course a rewarding experience. Discussions will be intensive, and you will be expected to contribute regularly. The central place of participation means that attendance is absolutely required. More than one unexcused absence will result in a grade penalty for the course as a whole of at least 1/3 (i.e. from B+ to B). More than two unexcused absences will lead to automatic failure in the course. Medical problems or family emergencies count as excused absences, but you need to notify me via e-mail or phone, preferably before the class meeting but within 24 hours of the missed class at the latest. • Exam. There will be an at-home midterm, timed to our class meeting, on Wednesday, October 14. The exam will consist of short answer essay questions. • Writing. You will be required to write a final paper of 8-10 pages that either examines both sides of one of the controversies we cover in class (the Rushdie Affair in Britain, the headscarf controversy in France, the murder of Theo van Gogh in the Netherlands), or analyzes one of the cultural documents (novel or film) in terms of the larger themes of the course.
Course Grading: Your overall grade for the course will be determined as follows:
40% for verbal participation, attendance, online responses 30% for the midterm 30% for the final paper
I reward improvement over the course of the semester. I also look favorably on students who stake ownership in the class, demonstrated by regular attendance, note taking, sharing ideas, and respecting the opinions of others.
Course Readings: The following books have been ordered for the course, and all are required. These books can be purchased at Ulrich’s or online.
• Ian Buruma, Murder in Amsterdam (Penguin, 2007), ISBN 0143112368 • Medhi Charef, Tea in the Harem (Serpent’s Tail, 1990) ISBN 1852421517 • Kenan Malik, Multiculturalism and Its Discontents (Melville House, 2010), ISBN
9780857421142 • Joan Scott, The Politics of the Veil (Princeton UP, 2007), ISBN 0691125430
Beyond the books for purchase, there will be other required reading materials available on CTools. All these are marked with a (CT) on the course schedule.
COURSE SCHEDULE
Week 1, Introduction & Aftermath of World War II Wed. Sept. 9: Welcome, Introductions, Aftermath of the War and the Reordering of Europe • Anthony Messina, The Logics and Politics of Post WW2 Migration to Western Europe (CT) • Yasmin Alibhai-Brown, Imagining the New Britain, Introduction (CT)

3
Week 2, New Commonwealth Immigration to Britain Mon. Sept. 14: Legacy of the British Empire & New Commonwealth Immigration Wed. Sept. 16: Discussion • Mike Phillips & Trevor Phillips, Windrush, Chapters 1, 4 & 5 (CT) • Zig Layton-Henry, The Politics of Immigration, Chapter 1 (CT) • Yasmin Alibhai-Brown, Imagining the New Britain, Chapter 2 (CT)
Week 3, France, Algeria & Postcolonial Immigration Mon. Sept. 21: Legacy of French colonialism, the Algerian War & Postwar Immigration Wed. Sept. 23: Discussion • Julia Clancy-Smith, “Islam, Gender, and Identities in the Making of French Algeria” (CT) • Laurent Dubois, “La Republique metisée” (CT) • Aimé Césaire, from Discourse on Colonialism (CT) • Frantz Fanon, from A Dying Colonialism (CT)
Week 4, Germany and the Guest Worker Program Mon. Sept. 28: Economic Miracle & German Postwar Labor Recruitment Wed. Sept. 30: Discussion • Deniz Göktürk, et al., eds., Germany in Transit, pp. 26-42, 336-51 (CT) • Phil Triadafilopoulos, Becoming Multicultural, Chapter 3, pp. 70-84 (CT) • Rita Chin, The Guest Worker Question in Postwar Germany, Introduction & Chapter 1
Week 5, Multicultural Europe and the Contours of the Contemporary Problem Mon. Oct. 5: Multicultural Europe, the Muslim Problem; Conversation with Kenan Malik
**Attend Kenan Malik lecture, 4PM, 1636 SSWB** Wed. Oct. 7: Discussion • Kenan Malik, Multiculturalism and Its Discontents
Week 6, Midterm Mon. Oct. 12: Review session Wed. Oct. 14: Take-home MIDTERM
Week 7, Initial European Responses to Immigration Mon. Oct. 19: No class – Fall Break Wed. Oct. 21: Europeans Respond to Immigrants • Mike Phillips & Trevor Phillips, Windrush, Chapters 6 & 12 (CT) • Ruth Glass, excerpt from The Newcomers (CT) (BBC interview with Teddy Boys) • Zig Layton-Henry, The Politics of Immigration, Chapter 2 (CT) • Rita Chin, The Guest Worker Question in Postwar Germany, Chapter 1 (CT) • Neil MacMaster, Colonial Migrants and Racism, Chapter 11 (CT)
Week 8, Here to Stay Mon. Oct. 26: Immigrant Settlement, Family Reunion Wed. Oct. 28: Discussion for Weeks 7 & 8 • Mike Phillips & Trevor Phillips, Windrush, Chapters 15, 16 & 18 (CT) • Rita Chin, The Guest Worker Question, Chapter 2

4

Week 9, Conservatism, New Racism & the Far Right Mon. Nov. 2: Thatcher and Kohl Wed. Nov. 4: Discussion
• Yasmin Alibhai-Brown, Imagining the New Britain, Chapter 3 (CT) • Rita Chin, The Guest Worker Question in Postwar Germany, Chapter 3 • Adrian Favell, Philosophies of Integration, Chapter 3 (CT)

Week 10, The Rushdie Affair Mon. Nov. 9: The Rushdie Affair Wed. Nov. 11: Discussion
• Salman Rushdie, excerpts from The Satanic Verses (CT) • Kenan Malik, From Fatwa to Jihad, Introduction & Chapter 1(CT) • Talal Asad, “ Multiculturalism and British Identity in the Wake of the Rushdie Affair” (CT) • Fay Weldon, Sacred Cows (CT)

Week 11, Immigrant Youth in the Inner City Mon. Nov. 16: Gender, Second Generation, Social & Economic Exclusion Wed. Nov. 18: VIEW in class: La Haine, Mathieu Kassovitz (1995)
• Mehdi Charef, Tea in the Harem

Week 12, Gender, Discrimination, Disillusionment & Resistance Mon. Nov. 23: Discussion for Weeks 11 & 12 Wed. Nov. 25: THANKSGIVING
• Roxanne Silberman & Irene Fournier, “Second Generations on the Job Market in France” (CT)
• Katherine Ewing, Stolen Honor: Stigmatizing Muslim Men in Berlin, Chapter 1 (CT) • Hisham Aidi, Rebel Music, Chapters 3 & 4 (CT)

Week 13, The Headscarf Affairs Mon. Nov. 30: Gender, the Affairs & Secularism in France Wed. Dec. 2: Discussion
• Joan Scott, The Politics of the Veil • Ayaan Hirsi Ali, from The Caged Virgin (CT) • Eric Fassin, “National Identities and Transnational Intimacies,” Public Culture (CT)

Week 14, Liberalism & Tolerance Mon. Dec. 7: Does Multiculturalism Work? Wed. Dec. 9: Discussion
• Ian Buruma, Murder in Amsterdam • Rita Chin, The Guest Worker Question in Postwar Germany, Chapter 4

Week 15, The Future of Multicultural Europe

Mon.

Dec. 14: Final discussion

The Final Paper is due Friday, December 18, no later than 3:30pm in my History Department mailbox.

