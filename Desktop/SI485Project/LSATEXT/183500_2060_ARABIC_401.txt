University of Michigan

Department of Near Eastern Studies

ARABIC 401 : Advanced Modern Standard Arabic I
Fall 2015

!‫أه ًلا وسه اًل ومرحباًا بكم في السنة الثالثة من اللغة العربية‬

Instructor: Tara Beebani (tbeebani@umich.edu)

Section 1: M 8:00-10:00 am, W/F 8:30-10:00 am, in room 5000 STB (http://campusinfo.umich.edu/campusmap/111 )
Office hours: M/W/F 10:10-11 am and by appointment at 4028 Thayer

Section 2: M 11:00 am-1:00 pm, in room 5000 STB (http://campusinfo.umich.edu/campusmap/111 ) W/F 11:30 am -1:00 pm in room 6000 STB (http://campusinfo.umich.edu/campusmap/111 )
Office hours: M/W/F 10-11 am and by appointment at 4028 Thayer

This syllabus provides information about course objectives and policies for the class. Detailed information about material to be covered and assignments due for each class meeting can be found on a detailed Daily Schedule available at the class CTools site. NOTE: I will generally adhere to the posted Daily Schedule, however, it may be somewhat modified (if certain areas are found to need more or less attention, etc) so watch for email notifications regarding modifications to the Daily Schedule.
Prerequisite: Completion of AAPTIS 202 or 205 with a minimum grade of C-, or placement
through department placement testing.

Required Course Materials:
1. Al-Kitaab fii Tacallum al- cArabiyya, Part 2 (Third Edition) by Brustad, Al-Batal & Al-Tonsi. ISBN: 9781589019621.
2. Al-Kitaab fii Tacallum al- cArabiyya, Part 2 (Second Edition) by Brustad, Al-Batal & Al-Tonsi, ISBN: 9781589010963.
3. The 3rd edition of Alkitaab has an online website companion with a variety of interactive activities. Access to these sites is required and can be purchased separately through the website or you can purchase a bundle of the 3rd edition book plus a website access card (ISBN: 9781626161238). Access is valid for 18 months. Instructions on how to register for the companion website will be posted on Ctools.
4. A Dictionary of Modern Written Arabic by Wehr, H. Fourth edition. ISBN: 0879500034.
5. (Recommended) Modern Standard Arabic Grammar: A Learner’s Guide by Alhawary. ISBN: 9781405155021
CTools and email: In this class I will use CTools to distribute course materials, communicate online, and post assignments and grades. Additionally, you will receive emails with important course information and announcements via your @umich.edu email so be sure to check that frequently.
1

‫‪Course Objectives: ARABIC 401 is the first of a two semester sequence of Advanced level Arabic.‬‬ ‫‪Students will learn to understand, speak, read and write Arabic at the Advanced Low to Advanced Mid‬‬ ‫‪level according to the ACTFL (American Council on the Teaching of Foreign Languages), in addition to‬‬ ‫‪gaining an understanding of cultural aspects of language use.‬‬
‫‪:‬الأهداف العامة‬ ‫هدفنا في هذا الفصل الدراسي هو تحقيق التقدم في قدرتنا باللغة العربية حتى نستطيع الوصول في نهاية الفصل‬ ‫الدراسي الى ما بين المستوى المتقدم الادنى (‪ )Advanced Low‬والمستوى المتقدم الأوسط (‪ )Advanced Mid‬من‬ ‫الكفاءة اللغوية‪ .‬وبالنسبة للقواعد سنقوم بالعمل على تفعيل قواعدنا القديمة‪ ،‬وسنقوم أيضا بدراسة قواعد جديدة مثل‪:‬‬ ‫التمييز ‪ ،‬والإضافة غير الحقيقية ‪ ،‬والحال ‪ ،‬والأمر والنهي ‪ ،‬وما التعجبية ‪ ،‬واستعمال كان وأخواتها في السرد‬
‫وسنتعرف على المعاني المختلفة لأوزان الفعل وسنتعلم تفاصيل جديدة عن الإعراب‪.‬‬
‫أما أهدافنا الخاصة بكل مهارة لغوية فهي‪:‬‬ ‫‪. ۱‬في الكلام والاستماع‪ :‬سنعمل على زيادة مفرداتنا وتوسيعها حتى نستطيع الكلام بطلاقة في مواضيع مختلفة‬ ‫(شخصية وسياسية وثقافية واجتماعية ‪ ...‬الخ)‪ .‬وسنقوم بالكلام في جمل طويلة في بداية الفصل الدراسي ثم فقرات في‬
‫نهاية الفصل الدراسي‪ .‬سنقوم كذلك بالاستماع الى أخبار وبرامج متنوعة من قنوات التلفزيون العربي حتى نح ّسن‬ ‫قدرتنا على فهم الأفكار العامة بالإضافة إلى فهم التفاصيل‪ .‬وبالإضافة إلى ذلك‪ ،‬ستقومون بالكلام مع الناطقين باللغة‬
‫العربية مرة كل أسبوع (التفاصيل في الصفحة التالية)‪.‬‬ ‫‪. ۲‬في القراءة‪ :‬سنعمل على تطوير استراتيجياتنا في القراءة والفهم‪ ،‬وسنر ّكز بشكل خاص على تحسين قدراتنا‬ ‫على قراءة نصوص طويلة وعلى فهم الأفكار فيها وكذلك على استخدام الوزن والجذر لتخمين معنى الكلمات التي لا‬
‫نعرف معانيها‪.‬‬ ‫‪. ۳‬في الكتابة‪ :‬في هذا الفصل سنهتم أيضا بالكتابة وسنعمل عليها بشكل مستمر للتعبير عن آرائنا وأفكارنا‬ ‫والاتصال مع الآخرين وتحسين قدراتنا على الكتابة بأسلوب عربي جميل وصحيح باستخدام أدوات الربط والفقرات‬
‫والجمل الطويلة‪ .‬وسنقوم بأنواع مختلفة من الكتابة (إنشاءات‪ ،‬رسائل‪ ،‬ملخصات ‪ ...‬إلخ)‪.‬‬ ‫‪. ٤‬في الثقافة‪ :‬سنوسع معرفتنا بالجوانب المختلفة للثقافة العربية أدبياً وسياسياً واجتماعياً وتاريخياً لنحصل على‬
‫فهم أفضل لهذه الثقافة ومكوناتها المختلفة‪.‬‬
‫‪2‬‬

Grade Distribution:
Attendance Participation Homework & Homework Correction Writing Portfolio Written Exams during semester Conversation Partner Work Oral Performance Final Exam

5% 10% 20%
8% 20%
7% 10% 20%

Below you will find more detail on each of the above components of your class grade, as well as a section on final grade calculation and grade policies and a section on University of Michigan policies applicable in the class, a section with tips and guidelines for how to thrive in third year Arabic and finally a section that explains our mixed register curriculum and outlines what Arabic you will learn in the class.

Grade Distribution/Course Requirements in Detail:

Attendance (5%)
You will begin the semester with a full grade for attendance (5 out of 5). Absence from one class session is considered one absence. Two absences will be a “no questions asked”absence that will not count against you in your grade calculation. Each absence beyond these two absences will automatically lower your grade by 1%. Deduction for absence will continue beyond 5% in the event that absences, using the previous calculation, call for more than a 5% deduction.
You do not need to provide a doctor’s note or other formal documentation to justify your absence, but it is a good idea to communicate with me to let me know why you are not in class and when you expect to be back.
If you want credit for homework due on a day that you must be absent, you need to make sure it is turned in by the beginning of the class period in which it is due.
Arriving more than 20 minutes late for class or leaving 20 minutes before the end of class will be considered a tardy. Tardiness is disruptive and so you are expected to arrive to class and leave class on time. 3 tardies (of up to 20 minutes) will count as an absence for purposes of grade calculation.
Participation (10%)
Class time will be devoted to activating the vocabulary and structures that you have prepared at home through your homework. At least 75% of class time will be spent on doing activities in pairs and small groups. Thus, preparation at home is absolutely necessary in order to be able to participate actively in class, and your in-class participation is essential to your making good language proficiency gains and doing well in the class. This part of your grade will be based on a biweekly participation grade. You will earn full credit for participation if you are actively engaged in developing your Arabic proficiency and communicating in Arabic with your partner/classmates/teacher from the moment you walk into the
3

class until you leave it. Controlling the urge to blurt out even a few words in English is an important part of keeping your brain in “Arabic-mode” and of maintaining our “Arabic immersion” learning environment.
Classroom Conduct Policies: ● Use of laptops and cell phones is not permitted, unless otherwise instructed by your instructor. Turn your cellphone off completely if it is with you. ● Eating during class time is not permitted. Beverages in spill proof containers are ok.

Homework & Homework Correction

(20%)

Expect to spend from 2 to 3 hours on homework for every in-class hour, or about 10 to 15 hours each week. In estimating the time that homework will take you, I will assume that you know the Arabic keyboard well and can type in Arabic quickly when doing the online homework.

To prepare for the learning that takes place through practice in class, you will listen, read and study new
material at home. This material will not be presented in class. To help you process this material, you will have daily online assignments at the online companion website (for lessons 9, and 10 of AK part 2, 3rd edition) in addition to written assignments. Once we move on to the 2nd edition, you will only have daily
written assignments (no online assignments). Your homework will often involve reading a grammar
explanation and then doing an exercise based on it. This reading is NOT skim reading. Be sure to read
the grammar explanation and study the examples carefully FIRST in order to understand how the
examples reflect the grammar point, so that you can implement the grammar point correctly in the
exercise.

Assignments are to be turned in before class activities begin unless otherwise announced. Late homework assignments will not be accepted unless you have received an extension via email from me in advance. Your lowest 2 homework grades will be dropped for purposes of grade calculation.

On each homework assignment, write the homework number (from homework schedule), date, drill number, staple pages together, and leave at least one blank line between each line of writing so that there is space for me to make corrections/give feedback.

Daily homework assignments will be graded on a scale from 1 to 10. You earn full credit on your assignment when your work 1) is submitted on time and is complete; 2) is clearly written, both in form and content; 3) displays your best effort to apply the material you have studied; 4) displays an effort to review old material and make connections between it and the new material (recycling old vocabulary, using complex structures we have studied); 5) reflects progress and improvement. Your work will be evaluated for overall quality, not only for the number of correct or incorrect answers. Homework assignments are intended to enhance your learning of the new material. So, spend time on your homework and make sure that what you submit reflects your absolute best. It is important to follow up on my comments on your homework, and it is your responsibility to come to office hours or make an appointment to see me if you need additional help.

Note that the assigned homework at the companion website is required homework. Failure to complete the online portion of the homework will result in points being deducted from your homework grade. Put your best effort into each attempt at the work you do on the website in order to get the most

4

benefit from it. You should correct and resubmit your answers based on the feedback until they are 100% correct. Your grade may be lowered if you have to make too many attempts (more than ~3) to get the answers 100% correct. As you work on the website, remember to practice engaged learning: think about why your answer was incorrect and remember it to correct it on your next attempt and to avoid the mistake in the future, rather than just copying and pasting the correct answer.
Do your own work — using an answer key, copying someone else's homework, getting help from a fluent Arabic speaker, or having your work checked before submitting it are all violations of the Code of Academic conduct. These and any other academically dishonest behaviors will not be tolerated. If you are in doubt of whether you are breaking this policy, ask. Your honesty will be appreciated.

Homework Correction:
Mistakes on homework are a natural and useful part of the learning process. For you to really learn from mistakes, you need to go back and correct them. When you submit your homework the first time, I will underline or circle the mistakes and label them with correction symbols using a correction code (posted on CTools). Take time to consider how you can correct these mistakes. For several assignments, you will be asked to make corrections based on this feedback and resubmit the homework for an additional, separate grade.
When correcting an assignment, go over it carefully and in full. Correct mistakes (using a different color ink or pencil) on the original paper next to what you wrote originally.
Homework corrections will receive a separate grade on a scale from 1 to 10. Late corrected homework will not be accepted. Corrections are due when they are listed on your schedule.

Writing Portfolio

(8%)

You will create a writing portfolio with essays ‫ إنشاءات‬in Arabic to practice the new vocabulary and structures you are learning in class. Each essay will be due at the beginning of class.

Each essay will be commented on by your instructor and returned with feedback using the same correction code that is used to correct homework. You will be asked to rewrite some of your essays, based on feedback either from your instructor or classmates after peer review work. Occasionally, your peers will give you feedback on your essay. When you resubmit the edited version you must submit the original essay also, which contains your instructor or peer’s feedback, so that your instructor can evaluate the extent to which you were able to make the requested revisions. First drafts will receive a grade out of 10 and any revisions will receive a separate grade, also out of 10. The due dates for your portfolio writing and revisions will be specified in your homework schedule. The evaluation rubric is posted on Ctools under resources. Late writing will not be accepted. Your writing must be typed.

Writing activities in this course are meant to allow you to practice and hone the structures and vocabulary you are learning in class. Thus, we ask you NOT to use outside dictionaries or translation tools to find new words for your writing assignments – whether essays or homework- with the exception for a key word that you deem essential to the essay.

Each draft of your essay will be graded. Be sure each draft reflects your best effort. Check your essay before submitting it using the editing check list available on CTools. Writing should be typed in a large font (14 or 16) with triple spacing so that your instructor can make comments. When you turn in a final

5

draft, be sure to submit the instructor/peer-edited original with your final draft; solo final drafts will not be graded. Please include a word count on all of your essays.

Written Exams during semester

(20%)

There will be four in-class exams over the course of the semester, worth a total of 20% of your final grade. Make-up exams will not be allowed except in serious, extenuating circumstances. If an emergency situation causes you to miss an exam, contact me as soon as possible. Requests for make-ups will be considered on a case-by-case basis. If you have not contacted me within 24 hours after the exam has been given to discuss the possibility of a make-up, you will receive a “0”.

Exam dates will be confirmed at least a week in advance on the daily schedule posted on the class CTools site. In addition to the exams, there may occasionally be short unannounced quizzes in class.
Conversation Partner Work (7%)
During the semester, you will be asked to meet with a native Arabic speaking conversation partner to practice your Arabic. More details about this is posted on Ctools under Resources (Conversation Partner Work).

Oral Performance

(10%)

During the semester there will be two assessed oral performances, one in the form of a presentation in class, and the other will be a final oral interview. Each of these will count as 5% of your grade. More details about these oral assessments, including the rubrics which will be used to evaluate them, will be provided in advance of the due date for each on the daily schedule on the CTools site.

Final Exam

(20%)

The final exam will consist of a final in-class written essay towards the end of the semester (4%), in
addition to a comprehensive final exam testing listening, reading, vocabulary and grammar (16%). The final exam date is for both sections is Wednesday, December 16th from 8:00 am sharp to 10:00 am. The
location of the final exam will be announced well in advance.

Final Grade Calculation and Grade Policies:

Number/Letter grade equivalents are:

Number Letter Number Letter

97 – 100

A+ 87 – 89.9

B+

93 – 96.9

A 83 – 86.9

B

90 – 92.9

A- 80 – 82.9

B-

Number 77 – 79.9 73 – 76.9 70 – 72.9

Letter C+ C C-

Number Letter

67 – 69.9

D+

63 – 66.9

D

60 – 62.9

D-

(Below 60% F)

● Grades close to the threshold grade for the next higher letter grade (89.9 for instance) may or may not be rounded up to the higher letter grade.

6

● According to LSA policy, students must earn a grade of C- or better in order to proceed to the next level language class. However, we strongly encourage you to aim for no less than a B- if you plan to continue your study of Arabic.
● Even if you are taking the class pass/fail, auditing, or not for credit, you are required to do all homework, take all quizzes and participate fully in all class activities.
● Incompletes will not be given in this class.
7

Important University policies and how they apply to this course
Class Attendance – first day of the semester
If a student does not attend the first meeting of a class his/her seat in the class may be given to a student waiting to get into the class.
Academic Integrity
All students are expected to be familiar with and adhere to the LSA Community Standards of Academic Integrity.
All students who have completed either AAPTIS 202 or 205 at the University of Michigan can enroll in ARABIC 401. Other students must take the department placement test in order for their placement to be determined. It is a violation of academic integrity to purposefully perform poorly on the test in order to place into a lower level language class and any student found to have done so will have to drop the class.
Do your own work — using an answer key, copying someone else's homework, getting help from a fluent Arabic speaker, or having your work checked before submitting it are all violations of the Code of Academic conduct. These and any other academically dishonest behaviors will not be tolerated under any circumstances. If you are in doubt of whether you are breaking this policy, ask. Your honesty will be appreciated.
Students are encouraged to study and work on homework assignments together; however, each student must participate fully in doing the work and submit his/her own individual paper. Doing otherwise is a violation of academic integrity and may result in disciplinary action.
Accommodations for Students with Disabilities If you think you need an accommodation for a disability, please let me know at your earliest convenience. Some aspects of this course, the assignments, the in-class activities, and the way the course is usually taught may be modified to facilitate your participation and progress. As soon as you make me aware of your needs, you and I can work with the Office of Services for Students with Disabilities (SSD) to determine appropriate academic accommodations. SSD (734-763-3000; http://ssd.umich.edu ) typically recommends accommodations through a Verified Individualized Services and Accommodations (VISA) form. Any information you provide is private and confidential and will be treated as such.
Athletes and Other Students Representing the University in an Official Capacity
Students who must miss class while representing the University in an official capacity or as a member of an athletic team, must present to instructor, as early in the semester as possible, a written statement signed by the appropriate authority specifying the exact date of any such proposed absence. Absence from classes while representing the University does not relieve students from responsibility for any part of the course missed during the period of absence. Within reason, your instructor may provide appropriate accommodation to the student for missed work, providing such accommodation does not
8

place unreasonable burden on the instructor or fundamentally alter the integrity of the course. When the absence coincides with an exam or other assignment due date, the options to make up that missed work may be limited and will be determined by the instructor within the boundaries of the class. In our class, you have two “free” absences which do not negatively impact your grade. Absences while representing the university will not be considered additional excused absences (above the 2 already allowed) unless the number of such absences exceeds 2. So, if you are absent 2 times because you are representing the university, you will have used your 2 “free” absences. If you should need to be absent additional days to represent the university, they will be allowed as additional “free” absences that will not impact your grade negatively.
Religious-Academic Conflicts It is the policy of the University of Michigan to make every reasonable effort to allow members of the University community to observe their religious holidays without academic penalty. Absence from classes or examinations for religious reasons does not relieve students from responsibility for any part of the course work required during the period of absence. Students who expect to miss classes as a consequence of their religious observance shall be provided with a reasonable alternative opportunity to make-up missed academic work. It is the obligation of students to provide faculty with reasonable notice of the dates on which they will be absent. When the absence coincides with an exam or other assignment due date, the options to make up that missed work may be limited and will be determined by the instructor within the boundaries of the respective class. In ARABIC 401, we ask that you let us know by the first Monday of the term, September 14th, if you anticipate being absent to observe a religious holiday during the semester. Absences for religious observance will not be considered additional excused absences (above the 2 already allowed) unless the number of such absences exceeds 2. So, if you are absent 2 times for religious observance, you will have used your 2 “free” absences. If you should need to be absent additional days for religious observance, they will be allowed as additional “free” absences that will not impact your grade negatively.
9

Thriving in Third Year Arabic
Things to Keep in Mind this Year
▪ Third year language study is your “maturing” year in language study, and the world is your oyster. The higher levels of proficiency demand greater accuracy and paragraph-level communication, which in turn requires more fine-tuning of the old and new structures that you lean. You must more than double your vocabulary in your third year, which in turn requires more lengthy production of speech and writing, and a greater understanding of Arab culture. Progress at this level often takes a longer time and a lot of energy. Keep at it!
▪ We will continue our “marathon training.” Notice that sentences, texts, video passages and drills are getting longer as you approach a solid advanced proficiency. We will be building memory capacity and endurance. Push yourself to speak and write in longer paragraphs.
How to Do Well in Class
Recognize that this class is a group effort! We can make much more progress as a class than as individuals by creating an Arabic-speaking community, of which you will be a fully participating member. The following suggestions will help you get the most out of the course:
1. Be an active learner. The approach we use depends on you learning new material at home, and encourages you to use analogy and logical thinking to master grammar a little bit at a time. You will learn better and remember more when you are able to answer your own questions. Active learners often have questions that reflect their engagement of the material. Take initiative in class and on homework: this effort will be rewarded both in your grade and in your language ability. Feel free to write me questions on specific language features in your daily homework—just remember to be specific.
2. Put yourself out there. Language learning requires you to make mistakes, both in your assignments and in front of your peers. In this class, you will never be penalized for a mistake that you make when trying something new. You will be corrected in class. My feedback is intended to support you as you develop your language proficiency, and not as criticism.
3. Personalize vocabulary. You will remember vocabulary when you “own” it. Make words relevant to your life by thinking of what you can say about yourself and your life using them. Write extra sentences that are meaningful to you so that the vocabulary becomes “yours” and expresses something about your world.
4. Prepare for active participation in class by anticipating what you will do. You should soon be able to predict what kinds of activities we will be doing in class and how. As you study new material and complete assignments before each class, think about how you will use the new material. This will help you to prepare for the activities we’ll be doing in class.
5. Develop your memorization skills. Experiment with different techniques, combining listening, speaking and writing together as much as possible. Cross-train: Try the following and find what combination of techniques works best for you: ▪ listening to words and repeating them aloud—not once but ten or twenty times, until the word is easy for you to pronounce
10

▪ using flashcards—but without English definitions, and make yourself say the word aloud and use it in a phrase or sentence
▪ writing out vocabulary over and over—but remember to pronounce it out loud while you write!
▪ putting the words in sentences or a paragraph or story ▪ studying in groups and quizzing each other ▪ using word association techniques
6. Study out loud, and repeat new words many times. The only way to train your brain and your mouth to speak this language is by doing it aloud. Certain muscles need to be strengthened before they’ll be able to produce some Arabic sounds correctly; studying aloud and exaggerating these sounds will help your muscles develop! Pronounce new vocabulary words at least fifteen times, and LISTEN to how you are saying the words. Compare your pronunciation to the DVD. Keep pronouncing the words aloud until you can say them easily and it feels natural.
7. Guess. Think about how you acquired your native language: you did not use a dictionary. Rather, you learned new words by guessing their meaning from context, and you learned how to produce sentences by imitating and using patterns. As adult learners, we can take some shortcuts, but guessing skills remain central to language acquisition. Do not leave blanks on the homework, but do not allow yourself to become frustrated; give it your best shot, and move on. If you are not sure you have understood a sentence or paragraph in the homework, you may write a translation or a question on your homework for me to check or answer.
8. Correct yourself. Good language learners learn from their own successes and mistakes and those of others. Correcting mistakes is an essential part of the learning process. In this class, you will never be penalized for a mistake that you make when trying something new. When classmates and I are speaking, be an active listener by listening both to what they are saying and how they are saying it. Correcting the mistakes of others in your head or in your notebook is also good practice.
9. Extend language learning beyond the classroom. Practice Arabic as much as possible. Study with classmates: ask each other questions, brainstorm about assignments, go over materials – and do this in Arabic as much as possible. Look out for films and cultural activities on campus and around town and go attend them. Find a native speaker conversation partner. If you need help with finding a conversation partner, check out the conversation partners directory at the Language Resource Center.
Principles for Group Activities There can be no language without community. In our class community, respect for each other at all times will ensure a positive and fruitful learning experience. We will spend at least 75% of class time in small group or pair activities so that each one of you gets the maximum possible time to participate and practice. During these activities, I play the role of personal trainer. Learning a language is a lot like going to the gym: the personal trainer guides you and pushes you to do your best, but you have to do the work to get the benefit. Please keep in mind the following principles for group work:
▪ Help and encourage your colleagues, and accept help from them: each of you can learn with and from everyone else.
▪ Work with different people on a regular basis.
11

▪ Come prepared. Don’t be someone who slows down colleagues by not being able to fully participate in the work. If you are not prepared for class let me know beforehand and s/he will arrange for you to learn by listening on that day.
▪ Stay focused on the task at hand and do not rush to finish. Most activities are designed to elicit creative play with vocabulary and structures on which we are working. If you do the minimum, you will get only minimum benefit. Take advantage of class time by pushing yourself to be creative and say and write as much as you can.
▪ Speak only in Arabic while you are engaged in the activity. If you feel frustrated, raise your hand and ask for help.
▪ When your group finishes the task, raise your hand to let me know. ▪ I may call time before everyone is finished. If your group does not finish, you may want
to take a few minutes after class to study the rest of the exercise.
12

What Arabic will you Learn at UM?
The Arabic speaking world is characterized by a linguistic situation commonly referred to as “diglossia”, in which two distinct varieties of language exist to serve speakers’ communicative needs. Each country in the region has its own dialect with its own distinguishing features. This is the language variety that is used for virtually all interpersonal communication. In addition, there is a more formal variety of Arabic called fuSHa that is used for delivering the news (broadcast and print) and the great majority of literature of all kinds is written in fusha. For the most part, speakers of a particular dialect can communicate with speakers of another dialect, although that communication may require some accommodation from both to be successful. Although this description may make it sound like there is a nice clear distinction between the two varieties of language, the reality is that educated speakers of any given region use the two registers in a rather fluid way, making use of expressive resources from both to communicate and making choices about register based on who they are talking to/writing for, about what and under what circumstances. Traditionally in the US, the two varieties of Arabic have been taught in separate classes. However, the Arabic program at the University of Michigan has adopted an integrated approach that exposes students to one dialect (either Egyptian or Levantine) as well as fuSHa, in one class. Roughly speaking, you will learn 1) to speak using many elements of a dialect; 2) to listen and understand both dialect and fuSHa; 3) to read fusHa; and 4) to write initially using both dialect and fuSHa but gradually learning to use mostly fuSHa in your writing, as is the standard in the Arab speaking world. The materials used in the first two years of the program, the 3rd edition of the alKitaab fi ta3allum al3arabiyya series, were designed for the purpose of implementing such an integrated curriculum and have been proven to be very effective. In the third year, there is an increased focus on using fusHa for communication. While it may seem daunting at first to have to deal with two registers of Arabic, we believe that the benefits of doing so from day one of studying Arabic make it well worth the effort. You will develop the ability to deal with the variety of Arabic that you will encounter in the “real world” and learn to make your own appropriate choices about what register to use under what circumstances. One of the great benefits that we have found of the approach is that you will “desensitize” to variety by being exposed to variety – that is, you will develop the ability to not notice what variety is being used because you will be focused on understanding the meaning that is being communicated and that, after all, is the goal – to be focused on the message that is being communicated. Your instructors and GSIs will help you make appropriate choices about what variety of Arabic to use in a particular situation but in the end you will have a certain amount of freedom to make that choice for yourself depending on your own objectives in learning Arabic, provided of course that your choices are consistent with successful communication inside and outside the classroom.
!‫فصل دراسي سعيد وممتع إن شاء الله‬
13

