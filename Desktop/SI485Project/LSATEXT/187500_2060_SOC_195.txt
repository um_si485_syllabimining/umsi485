SOC. 195 INTRODUCTION TO SOCIOLOGY THROUGH AMERICAN AND IRAQI SOCIETIES
Time: T Th 10:00 – 11:30 am Place: 4155 LS&A Instructor: Fatma Müge Göçek gocek@umich.edu Office: 4207 LS&A Phone: 647-4228 Office Hours: T 1 - 3 pm

This class is designed specifically to introduce you to the field of sociology through the study of two societies that are of deep concern to us at the moment, one because we live in it and the other because we still fight in it: namely, the American and Iraqi societies. The purpose of the course is to gain insight into how one ought to approach and analyze American and Iraqi societies that are alternately portrayed as being structurally and culturally very different on the one hand, yet very similar on the other.
Each week commences with an outline on the topic that summarizes the pertinent sociological points as well as a summary of the significant aspects of the assigned readings. The outline will be available on the CTools website ahead of time so as to provide guidance to the readings; I shall also try to have it available during the lecture. The students are expected to do the readings on a weekly basis as they will be tested on them.

The course requirements comprise the following: I. Midterm Examination (30%): Midterm examination will take place on 5 November Thursday 2015 in class;

II. Final Paper (30%): Final Paper due on 17 December 2015 Thursday; it will be about 10-12 pages long on any topic of your choice on American or Iraqi society.

III. Surprise Quiz (40%): To ensure you are on top of the readings.

_____________________________________________________________________________________________

COURSE OUTLINE

September 8 T

Introduction

9/10 Th

Issues of Our Times

[Lecture Outline I]

9/15 T

Issues of Our Times

[Lecture Outline II]

9/17 Th

The Sociological Lens

[Lecture Outline III]

9/22 T

The Sociological Lens

[Lecture Outline IV]

Sociological Approaches to Society:

9/24 Th

I. Social Conflict

9/29 T

Social Conflict

10/1 Th

Film on Iraq

10/6 T

II. Social Consensus

10/8 Th

Social Consensus

10/13 T

III. Symbolic Interaction

10/15 Th

Symbolic Interaction

10/20

FALL BREAK

10/22 Th

IV. Critical Interrogation

10/27 T

Critical Interrogation

Midterm Exam Questions Handed Out

10/29 Th

In-class Review for the midterm

11/3 T

No class, students study for midterm

11/5 Th

Midterm examination

[Lecture Outline V] [Lecture Outline VI]
[Lecture Outline VII] [Lecture Outline VIII] [Lecture Outline IX] [Lecture Outline X]
[Lecture Outline XI] [Lecture Outline XII]

1

Sociological Dimensions of Society:

11/10 Tuesday

i. Gender and Sexuality

11/12 Th

Gender and Sexuality

11/17 T

ii. Race and Ethnicity

11/19 Th

Race and Ethnicity

11/24 T

Film on Iraq

11/26 Th

THANKSGIVING BREAK

12/1 T

iii. Politics and the State

12/3 Th

Politics and the State

12/8 T

iv. Economy and the Market

12/10 Th

Economy and the Market

[Lecture Outline XIII] [Lecture Outline XIV] [Lecture Outline XV] [Lecture Outline XVI]
[Lecture Outline XVII] [Lecture Outline XVIII] [Lecture Outline XIX] [Lecture Outline XX]

17 December Thursday Final Papers Due electronically (by e-mail to gocek@umich.edu) by midnight. --------------------------------------------------------------------------------------------------------------------------------------------

COURSE READING OUTLINE

INTRODUCTION (9/8) T

_____________________________________________________________________________________________

ISSUES OF OUR TIMES

(9/10) Th

[Lecture Outline I]

The U.S. Experience in Iraq through Political Cartoons

The Doonesbury Cartoon: B.D. in Iraq

Newton, Julianne. 2007. "Trudeau Draws Truth." Critical Media Studies in Communication 24/1: 81-5 [4]

Trudeau, Gary. 2007. “Death and Politics on the Funny Pages: Garry Trudeau Addresses American Newspaper Editors.” Critical Studies in Media Communication 24/1: 86-92 [6].

Bronner, Stephen Eric. 2005. “Iraq Redux: How Things Looked Then and How They Look Now.” Logos 4.3: 1-10 [9]

Everts, Philip and Pierangelo Isernia. 2005. “The Polls – Trends: the War in Iraq.’ Public Opinion Quarterly. 264-75 [11]

Ramirez, Levin. 2005. “A Rough Road for Recruiters in 2005.” Objector. 3-5 [2]

ICG (International Crisis Group). 2006. “After Baker-Hamilton: What to do in Iraq.” [6]

(9/15) T

[Lecture Outline II]

Wolman, Neil. 2005.

“Despite Rise in Overall Poverty in 2004, Gaps between Races, Social Groups and Gender Continue to Drop.” [2]

Sikkink, Kathryn. 2004. “Understanding the September 11 Terror Attacks: A Human Rights Approach.” Pp. 309-13 in Seeing Ourselves J.J. Macionis and N.V. Benokraitis, eds. Upper Saddle River, NJ: Prentice Hall. [4]

2

Bronner, Stephen Eric. 2002. “Us and them: The State of the Union and the Axis of Evil.” Logos 1.2: 42-52. [10]

“A Conversation between American and Iraqi Intellectuals: Baghdad, Iraq – January 16th, 2003.” Logos 2.1: 1-12. [12]

“The Prospect for Democracy in the Middle East: a conversation with Saad Eddin Ibrahim in 2005” Logos 4.2: 1-7 [6] ____________________________________________________________________________________________

THE SOCIOLOGICAL LENS

(9/17) Th

[Lecture Outline III]

Berger, Peter L. 2004.

“Invitation to Sociology.” Pp. 6-9 in Seeing Ourselves J.J. Macionis and N.V. Benokraitis, eds. Upper Saddle River,

NJ: Prentice Hall. [3]

Mills, C. Wright. 1999. “The Sociological Imagination.” Pp. 348-352 in Social Theory Charles Lemert, ed. Boulder, CO: Westview. [4]

Restivo, Sal. 1991. “Thinking About Society.” Pp. 14-30 in The Sociological Worldview. New York: Basil Blackwell. [16]

Vander Zanden, James. 1993. “The Sociological Perspective” and “Perspectives in Sociology.” Pp. 2-29, 52-61 in Sociology. New York. [36]

(9/22) T

[Lecture Outline IV]

Strover, Eric, H. Megally and Hania Mufti. 2005.

“Bremer’s ‘Gordian Knot:’ Transitional Justice and the US Occupation of Iraq.” Human Rights Quarterly. 27/3:

830-57 [27]

Brown, Nathan J. 2005. “Iraq’s Constitutional Conundrum.” Paper for the Carnegie Endowment for International Peace. [5] ___________________________________________________________________________________________

SOCIOLOGICAL APPROACHES TO SOCIETY:

I. THROUGH SOCIAL CONFLICT

(9/24) Th

[Lecture Outline V]

Marx, Karl. 1994.

“The Workings of Social Class.” Pp. 84-8 in Windows on Society J. Heeren and M. Mason, eds. New York:

Roxbury. [4]

Mills, C. Wright. 2002. “The Power Elite.” Pp. 420-427 in Mapping the Social Landscape Susan Ferguson, ed. Boston: McGraw Hill. [7]

Hacker, Andrew. 2004. “Who Has How Much and Why.” Pp. 192-8 in Seeing Ourselves J.J. Macionis and N.V. Benokraitis, eds. Upper Saddle River, NJ: Prentice Hall. [7]

Cookson, P.W. and C.H. Persell. 2002. “Preparing for Power: Cultural Capital and Curricula in America’s Elite Boarding Schools.” Pp.572-582 in Mapping the Social Landscape Susan Ferguson, ed. Boston: McGraw Hill. [10]

3

(9/29) T

[Lecture Outline VI]

Schor, Juliet. 1994.

“The Overworked American.” Pp. 162-9 in Windows on Society J. Heeren and M. Mason, eds. Los Angeles, CA:

Roxbury. [7]

Harris, Jerry. 2008. "US Imperialism after Iraq." Race and Class 50/1: 37-58. [21]

Reyna, Steve. 2005. “’We Exist To Fight:’ The Killing Elite and Bush II’s Iraq War.” Social Analysis 49/1: 190-7. [7].
Eglitis, Daina Stukuls. 2004. “The Uses of Global Poverty: How Economic Inequality Benefits the West.” Pp. 199-206 in Seeing Ourselves J.J. Macionis and N.V. Benokraitis, eds. Upper Saddle River, NJ: Prentice Hall. [7] __________________________________________________________________________________________

(10/1) Th

Film on Iraq

__________________________________________________________________________________________

II. THROUGH SOCIAL CONSENSUS

(10/6) T

[Lecture Outline VII]

Weber, Max. 1994.

“The Protestant Ethic and the Spirit of Capitalism.” Pp. 184-8 in Windows on Society J. Heeren and M. Mason, eds.

Los Angeles, CA: Roxbury. [4]

Durkheim, Emile. 1999. “The Division of Labor in Society.” In Social Theory Charles Lemert, ed. Boulder, CO: Westview. [3]

Durkheim, Emile. 2004. “The Functions of Crime.” Pp. 136-8 in Seeing Ourselves J.J. Macionis and N.V. Benokraitis, eds. Upper Saddle River, NJ: Prentice Hall. [2]
Merton, Robert. 2004. “Manifest and Latent Functions.” Pp. 35-6 in Seeing Ourselves J.J. Macionis and N.V. Benokraitis, eds. Upper Saddle River, NJ: Prentice Hall. [2]

Gans, Herbert. 2002. “Positive Functions of the Undeserving Poor: uses of the underclass in America.” Pp. 300-13 in Mapping the Social Landscape Susan Ferguson, ed. Boston: McGraw Hill. [13]

(10/8) Th

[Lecture Outline VIII]

Ritzer, George. 2002.

“The McDonaldization of Society.” Pp. 625-633 in Mapping the Social Landscape Susan Ferguson, ed. Boston:

McGraw Hill. [8]

Huber, John. 2003. “Sleepwalking Democrats and American Public Support for President Bush’s Attack on Iraq.” Constellations 10/3: 392-407. [15] _____________________________________________________________________________________________
III. THROUGH SYMBOLIC INTERACTION

(10/13) T Goffman, Erving. 2004.

[Lecture Outline IX]

4

“The Presentation of Self.” Pp. 93-8 in Seeing Ourselves J.J. Macionis and N.V. Benokraitis, eds. Upper Saddle River, NJ: Prentice Hall. [5]

Berger, Peter and T. Luckmann. 1996. “Socialization: The Internalization of Society.” Pp. 34-6 in Joel Charon, ed. The Meaning of Sociology. New York.

Miner, Horace. 2004. “Body Ritual among the Nacirema.” Pp. 14-17 in Seeing Ourselves J.J. Macionis and N.V. Benokraitis, eds. Upper Saddle River, NJ: Prentice Hall. [3]

Bernstein, Stan. 1994. “Getting It Done: Notes on Student Fritters.” Pp. 7-12 in Windows on Society J. Heeren and M. Mason, eds. Los Angeles, CA: Roxbury. [5]

Tannen, Deborah. 2004. “You Just Don’t Understand: Women and Men in Conversation.” Pp. 99-104 in Seeing Ourselves J.J. Macionis and N.V. Benokraitis, eds. Upper Saddle River, NJ: Prentice Hall. [5]

(10/15) Th

[Lecture Outline X]

Fassihi, Farnaz. 2004.

“Baghdad Diary.” Columbia Journalism Review 43/4: 36-41. [5]

Graham, Patrick. 2004. “When the Fighting is Glimpsed from Another Perspective.” Nieman Reports 58/3: 66-9. [3]

Bonds, Eric. 2009. "Strategic Role Taking and Political Struggle: Bearing Witness to the Iraq War." Symbolic Interaction 32/1: 1-20 [19] ___________________________________________________________________________________________

(10/20) T

FALL BREAK

___________________________________________________________________________________________

IV.THROUGH CRITICAL INTERROGATION

(10/22) Th

[Lecture Outline XI]

Weber, Max. 2004.

“The Disenchantment of Human Life.” Pp. 486-7 in Seeing Ourselves J.J. Macionis and N.V. Benokraitis, eds.

Upper Saddle River, NJ: Prentice Hall. [2]

Durkheim, Emile. 2004. “Anomie and Modern Life.” Pp. 481-5 in Seeing Ourselves J.J. Macionis and N.V. Benokraitis, eds. Upper Saddle River, NJ: Prentice Hall. [4]

Levy, Daniel and Natan Sznaider. 2004. “The Holocaust.” Pp. 378-81 in Encyclopedia of Social Theory. G. Ritzer, ed. Thousand Oaks, CA: Sage. [3]

Zimbardo, Philip E. 1996. “Pathology of Imprisonment.” Pp. 76-8 in Joel Charon, ed. The Meaning of Sociology. New York. [2]

Meyer, Philip. 1996. “If Hitler Asked You to Electrocute a Stranger, Would You? Probably.” Pp. 79-83 in Joel Charon, ed. The Meaning of Sociology. New York. [4]

Kelman, Herbert and V.L. Hamilton. 1996.

5

“The My Lai Massacre: a military crime of obedience.” Pp. 88-101 in Joel Charon, ed. The Meaning of Sociology. New York. [3]

Storey, John. 2003. “The Articulation of Memory and Desire: From Vietnam to the War on the Persian Gulf.” Pp. 99-119 in Memory and Popular Film P. Grainge, ed. Manchester, UK: Manchester University Press. [20]

(10/27) T

[Lecture Outline XII]; Midterm exam questions handed out

Solomon, Alisa. 2005.

“War Resisters Go North.” The Nation 280/1: 4-6. [2]

Arsenault, Amelia and Manuel Castells. 2006. “Conquering the Minds, Conquering Iraq: The Social Production of Misinformation in the United States – a case study.” Information, Communication and Society 9/3: 284-307. [23]

Aday, Sean. 2010. "Chasing the Bad News: An Analysis of 2005 Iraq and Afghanistan War Coverage on NBC and FOX News Channel." Journal of Communication 60/1: 144-64. [20]

Johnson, Allan G. 2002.

“What Can We Do? Becoming Part of the Solution.” Pp. 655-60 in Mapping the Social Landscape Susan Ferguson,

ed. Boston: McGraw Hill. [6]

___________________________________________________________________________________________

(10/29) Th

In-class Review for the Midterm

(11/3) T

No class, students study for midterm

(11/5) Th

MIDTERM EXAMINATION IN CLASS

_____________________________________________________________________________________________

DIMENSIONS OF SOCIETY: I. GENDER AND SEXUALITY

(11/10) T

[Lecture Outline XIII]

Benokraitis, Nikole V. 2004.

“How Subtle Sex Discrimination Works.” Pp.213-218 in Seeing Ourselves J.J. Macionis and N.V. Benokraitis, eds.

Upper Saddle River, NJ: Prentice Hall. [5]

Williams, Christine L. 1999. “Real-Life Sexual Harassment.” Pp. 49-57 in Qualitative Sociology as Everyday Life B. Glassner and R. Hertz, eds. Thousand Oaks, CA: Sage. [8]

Collins, Patricia Hill. 2004. “Controlling Images and Black Women’s Oppression.” Pp. 232-9 in Seeing Ourselves J.J. Macionis and N.V. Benokraitis, eds. Upper Saddle River, NJ: Prentice Hall. [7]

Michael, Robert et al. 2004. “Sex in America: How Many Partners Do We Have?” Pp. 166-72 in Seeing Ourselves J.J. Macionis and N.V. Benokraitis, eds. Upper Saddle River, NJ: Prentice Hall. [6]

(11/12) Th

[Lecture Outline XIV]

Martin, Patricia and R. Hummer. 1996.

“Fraternities and Rape on Campus.” Pp. 220-230 in Joel Charon, ed. The Meaning of Sociology. New York. [10]

Sherman, Nancy. 2010.

6

"The Guilt They Carry: Wounds of Iraq and Afghanistan." Dissent 57/2: 80-84. [4]

Al-Mashat, Kasim, et al. 2006. “Iraqi Children’s War Experiences: The Psychological Impact of ‘Operation Iraqi Freedom.’” International Journal for the Advancement of Counseling 28/2: 195-211. [16] ____________________________________________________________________________________________

II. RACE AND ETHNICITY

(11/17) T

[Lecture Outline XV]

Du Bois, W.E. 2004. “The Souls of Black Folk.” Pp. 227-31 in Seeing Ourselves J.J. Macionis and N.V. Benokraitis, eds. Upper Saddle River, NJ: Prentice Hall. [4]

Hacker, Andrew. 1996. “Being Black in America.” Pp. 178-86 in Joel Charon, ed. The Meaning of Sociology. New York. [10]

(11/19) Th

[Lecture Outline XVI]

Moskos, Charles. 1994.

“The Army’s Racial Success Story: How Do They Do It?” Pp. 170-174 in Windows on Society J.W. Heeren and M.

Mason, eds. Los Angeles, CA: Roxbury. [4]

Brodkin, Karin. 2004. “How Did Jews Become White Folks?” Pp. 240-9 in Seeing Ourselves J.J. Macionis and N.V. Benokraitis, eds. Upper Saddle River, NJ: Prentice Hall. [7]

Popp, Richard and Andrew Mendelson. 2010. "X-ing Out Enemies: Time Magazine, Visual Discourse and the War in Iraq." Journalism 11/2: 203-21 [18]

Wimmer, Andreas. 2003-04. “Democracy and Ethno-Religious Conflict in Iraq.” Survival 45/4: 111-34. [23] _____________________________________________________________________________________________

(11/24) T

Film on Iraq

(11/26) Th

THANKSGIVING RECESS

______________________________________________________________________________

III. POLITICS AND THE STATE

(12/1) T

[Lecture Outline XVII]

Gotham, Kevin Fox. 2004. “The State.” Pp. 789-93in Encyclopedia of Social Theory. G. Ritzer, ed. Thousand Oaks, CA: Sage. [4]

Perrow, Charles. 2004. “Organizing America.” Pp. 29-42 in The Sociology of the Economy. Frank Dobbin, ed. New York: Russell Sage. [13]

McCartney, Paul. 2004. “American Nationalism, U.S. Foreign Policy from September 11 to the Iraq War.” Political Science Quarterly 119/3: 399-423. [24]

7

Schmitt, Michael. 2005. “Humanitarian Law and Direct Participation in Hostilities by Private Contractors or Civilian Employees.’ Chicago Journal of International Law 5/2: 511-22 [11]

(12/3) T

[Lecture Outline XVIII]

Diamond, Larry. 2004.

“What Went Wrong in Iraq.” Foreign Affairs 83/5: 34-44. [10]

Encarnacion, Omar. 2005. “The Follies of Democratic Imperialism.’ World Policy Journal 22/1: 47-60. [13]

Montgomery, John and D. Rondinelli. 2004. “A Path to Reconstruction: Proverbs of Nation-Building.” Harvard International Review 26/2: 26-9. [3]

Hashim, Ahmed S. 2003. “Military Power and State Formation in Modern Iraq.” Middle East Policy 10/4: 29-47. [18] _____________________________________________________________________________________________

IV. ECONOMY AND THE MARKET

(12/8) Th

[Lecture Outline XIX]

Dobbin, Frank. 2004.

“The Sociological View of the Economy.” Pp. 1-42 in The Sociology of the Economy. Frank Dobbin, ed. Princeton,

NJ: Princeton University Press. [41]

Mazaheri, Nimah. 2010. "Iraq and the Domestic Political Effects of Economic Sanctions." Middle East Journal 64/2: 253-68. [15]

(12/10) T

[Lecture Outline XX]

Looney, Robert. 2004.

“Petroeuros: A Threat to the U.S. Interests in the Gulf?” Middle East Policy 11/1: 26-37. [11]

Jhaveri, Nayna. 2004. “Petroimperialism: US oil interests and the Iraq war.” Antipode 2-11. [9]

Looney, Robert. 2005. “Postwar Iraq’s Financial System: Building from Scratch.” Middle East Policy 12/1: 134-49. [15] _____________________________________________________________________________________________

17 December Thursday Final Papers Due electronically (by e-mail to gocek@umich.edu) by midnight.

8

