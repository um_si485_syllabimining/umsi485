Math 216/Syllabus/

http://www.math.lsa.umich.edu/courses/216/20Syllabus/index_no_naviga...

Syllabus and Course Information / Fall 2015

Assignments: In addition to exams, there are four types of recurring assignments that you will be responsible for throughout the semester:
Written homework sets. These assignments may be obtained from the menu on the left side-bar. They are to be written up neatly, and will be evaluated for both mathematical correctness and clarity of explanation. Be aware that time constraints are such that only a subset of the assigned problems will be graded carefully. In any case, you should plan on doing more than just the assigned problems--see for example the Practice Problems--in order to learn the material and be prepared for exams. Web homework. There are also web-based homework sets that offer instant feedback on your work. (Sign on to the homework server using the menu on the left.) You are allowed six attempts (five wrong answers) on each problem, and partial credit is given on questions with multiple parts. These sets follow a roughly weekly schedule, and each "closes" at a precise date and time--see your syllabus. Approximately one day after the closing date, correct answers to the problems are available. Pre-lab assignments. In order to be prepared for a computer lab session with your recitation instructor (see below), you must complete a prelab assignment. These assignments are provided in the corresponding lab manuals. Lab writeups. Each lab manual has a list of problems that must be completed as part of your writeup. Each lab assignment is to be written up neatly.
The written homework, the pre-lab assignments, and the lab write-ups are to be handed in to your recitation instructor. See the syllabus for the due dates.
Academic Integrity: Academic integrity is fundamental to learning: if you seek to obtain a grade by cheating, you are undermining your own learning and violating the standards of our community. Accordingly, any evidence of cheating may result in a penalty up to a failing grade in the course, and will be reported to the administration.
Note that working with class members on assignments (other than the exams) is permitted and encouraged, and may be to your advantage as you seek to learn the material. However, the work you hand in for evaluation must be entirely your own.
Exam information: The exam dates and times for this class are as follows:

Exam

Date and Time Location Material

First Exam Monday, October 12 Solutions 8-10 PM

Second Exam Monday, November 16 Solutions 8-10 PM

Final Exam

Thursday, December 17 8-10 AM

Calculators, cell phones, and other aids will not be allowed during exams. Numerical calculations will be simple and minimal.
Recitation and lab sessions: When you registered for a lecture section, you also registered for a subsection that meets on Tuesdays or Thursdays. Each Tuesday or Thursday you will meet at the designated time with a recitation instructor. These sessions are of two different types:
Recitation sessions. These meet in the room designated for your subsection in the course listing. The purpose of these sessions is for you to be able to discuss coursework and go over recent lecture topics with your recitation instructor. Lab sessions. These meet in the computer labs in the basement of East Hall. The purpose of these sessions is to meet with your recitation instructor to carry out computational projects involving the use of Matlab. There are five lab sessions throughout the semester, and each has a corresponding lab manual that you can download from the menu on the left. You should read this manual before coming to your lab session. In particular, the lab manual may contain a prelab assignment that is due at the beginning of the lab session.
See the syllabus for your lecture section to find out whether you will have a recitation session or a lab session on any given Tuesday or Thursday.
Grading Policy: The various components of your work in the course will be weighted as follows:

Course Component First Exam Second Exam Final Exam Web Homework Written Homework Lab Writeups

Weight 20% 22% 30% 10% 10% 8%

In determining your course letter grade your performance will be compared to that of other students in your lecture section only. In recent years, the median grade in Math 216 has been a B.

©2015 The Regents of the University of Michigan Last Modified: Fri Sep 4 09:10:54 2015

1 of 1

9/18/2015 11:25

Math 216/Schedule/

http://www.math.lsa.umich.edu/courses/216/10Schedule/index_no_navig...

Book section numbers are for the 5th edition. Section 5.4 in the 5th edition is section 5.3 in the 4th edition.

Math 216 Fall 2015 Schedule/syllabus

Wk Day

Lecture Section 3x, 4x, 6x

Section 1x, 2x, 5x WebHW Exam

Due HW Lab

Room

Due HW Lab

Room

due 10pm

Mon Sep 7

Labor day

Tue Sep 8

no meeting

1 Wed Sep 9 1

1.1, 1.2

Thu Sep 10

no meeting

Fri Sep 11 2

1.3

Mon Sep 14 3

1.4

Tue Sep 15

1 Lab 1

2 Wed Sep 16 4

1.5

Thu Sep 17

1 Lab 1

Fri Sep 18 5

2.1

0,1

Mon Sep 21 6

2.2

Tue Sep 22

2 1 Recitation

3 Wed Sep 23 7

2.4

Thu Sep 24

2 1 Recitation

Fri Sep 25 8

2.5, 2.6

2

Mon Sep 28 9

Review

Tue Sep 29

3 Lab 2

4 Wed Sep 30 10

3.1,3.2

Thu Oct 1

3 Lab 2

Fri Oct 2 11

3.1,3.2

3

Mon Oct 5 12

Complex numbers (see website pdf)

Tue Oct 6

4 2 Recitation

5 Wed Oct 7 13

3.3

Thu Oct 8

4 2 Recitation

Fri Oct 9 14

3.4

Mon Oct 12 15

Review

MT 1

Tue Oct 13

Recitation

6 Wed Oct 14 16

3.5

Thu Oct 15

Recitation

Fri Oct 16 17

3.5

4

Mon Oct 19 Tue Oct 20

FALL BREAK

7 Wed Oct 21 18

3.6

Thu Oct 22

Recitation

Fri Oct 23 19

Review

5

Mon Oct 26 20

4.1, 4.2

Tue Oct 27

5 Lab 3

8 Wed Oct 28 21

4.2, 4.3

Thu Oct 29

5 Lab 3

Fri Oct 30 22

5.1

6

Mon Nov 2 23

5.1

Tue Nov 3

6 3 Recitation

9 Wed Nov 4 24

5.2

Thu Nov 5

6 3 Recitation

Fri Nov 6 25

5.2

7

Mon Nov 9 26

5.4

Tue Nov 10

7 Recitation

10 Wed Nov 11 27

5.4

Thu Nov 12

7 Recitation

Fri Nov 13 28

6.1

1 of 2

9/18/2015 11:27

Math 216/Schedule/

http://www.math.lsa.umich.edu/courses/216/10Schedule/index_no_navig...

Mon Nov 16 29 Tue Nov 17 11 Wed Nov 18 30 Thu Nov 19 Fri Nov 20 31 Mon Nov 23 32 Tue Nov 24 12 Wed Nov 25 33 Thu Nov 26 Fri Nov 27 Mon Nov 30 34
Tue Dec 1 13 Wed Dec 2 35
Thu Dec 3 Fri Dec 4 36 Mon Dec 7 37 Tue Dec 8 14 Wed Dec 9 38 Thu Dec 10 Fri Dec 11 39 Mon Dec 14 40 Tue Dec 15 15 Wed Dec 16 Thu Dec 17 Fri Dec 18

Review Lab 4
6.2
6.3 6.4 Recitation Review

Lab

4

THANKSGIVING

7.1

8 4 Lab

5

7.2

8 4 Lab

5

7.3

7.4

9 5 Recitation

7.5

9 5 Recitation

7.6

Review

Classes over

©2015 The Regents of the University of Michigan Last Modified: Fri Sep 4 10:00:47 2015

MT 2 8
9 10
FINAL

2 of 2

9/18/2015 11:27

