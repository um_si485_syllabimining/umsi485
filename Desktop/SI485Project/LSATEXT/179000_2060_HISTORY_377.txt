American Culture 315/History 377 History of Latina/os in the U.S Monday-Wednesday 2:30-4 PM 1436 Mason Hall
Professor: Jesse Hoffnung-Garskof Office Hours: W 4-5:30 PM, 3751 Haven Sign up on Ctools
COURSE OVERVIEW:
This course introduces students to the history of Latinas/os in North America. Rather than aiming for complete coverage of this long and multifaceted trajectory, the class presents selected texts and episodes. These are chosen to present a diverse and challenging picture, and to help students to deepen their understanding of the workings of race and ethnicity, satisfying the LSA Race and Ethnicity requirement. Students consider the ways that race and ethnicity shape and are shaped by histories of imperialism, immigration and labor, gender and sexuality, and social and cultural movements. This is not a class about memorization of dates and facts (although knowing facts is often necessary). Emphasis is rather placed on the specific methods of historical analysis. Students learn to work with primary historical sources and to produce their own historical interpretations. Finally, students are asked to think about what history can tell us about the present day. They apply their understanding of the past and their skills at historical research and interpretation to analyses of contemporary social issues related to race and ethnicity in the United States.
ASSIGNMENTS:
Quizzes: 33% Class Participation: 26% Group Presentations: 15 % Final Papers (5-7 pages): 26%
Quizzes:
At least once each week, the first ten minutes of class will consist of a short quiz on the reading assigned for that day. There will be four multiple-choice questions on each quiz, and one short answer question. The purpose of these quizzes is to reward you for regular preparation and review and to ensure that we all benefit from high quality class discussions informed by careful preparation.
The first quiz is for practice, and will not count toward your final grade. After that, you have 12 quizzes (each worth three points) with which to earn a total of 33 possible points on the final grade. How you deploy this flexibility is up to you. You

can drop a low quiz grade or miss up to one quiz without a penalty. Or you can use the three points to make up for points missed on other quizzes. Quizzes missed on days when you are legitimately excused from attending may be made up during office hours.
Class Participation
The most important requirement in this class is weekly attendance and participation. Reading assignments are a crucial part of this participation and should be done every week before our class meetings. Each week you will be given guiding questions for your reading assignments. Think of this as a kind of problem set to structure regular weekly work in the class. It will not be collected and graded, but any one of the questions is fair game for your in-section quiz or for in class discussion.
Students are each permitted one unexcused absence with no penalty. For every subsequent unexcused absence three quarters of a point will be deducted from the class participation grade. So if you have four unexcused absences the maximum you could earn in class participation points (presuming you were an A+ participant in all the other class meetings) would be 23 out of 26 (a B+).
Absences will be excused in cases of illness or emergency, or in cases of conflict with travel for athletic teams or other student activities. Unless it is impossible (as in the case of sudden illness), a request to be excused from class should be made in advance of the missed class, not after the fact. Requests for excused absences due to illness should include a doctor’s note or other documentation.
Group Presentations
Beginning in the first part of the semester you will work with groups of 3-5 students to research and develop a classroom presentation on a topic relating to Latina/o History. These topics are designed to integrate with and build on the themes we will be addressing in the last three weeks of the course. Group presentations have required elements that will be clearly spelled out in several in-class workshops over the course of the term. All members of a group will receive the same grade.
Final Papers
At the end of term each student will complete a five-to-seven page paper analyzing one or more primary historical documents. These documents will probably be drawn from the group research projects and presentations or other class activities (such as the visit to the Bentley Library). But you are welcome to find your own documents if you wish.

Policy on Disabilities:
If you need academic adjustments or accommodations because of a disability, please see Professor Hoffnung-Garskof during the first week of class. Also, if you have not yet done so you should notify the Disability Services Office, located in G 219 Angell Hall (phone 763-3000) of any special needs.
Policy on Gender Identity:
Please feel free to make me aware of your preferred gender pronouns.
Academic Integrity
American Culture 315/History 377 follows the academic integrity guidelines set forth by LSA [http://www.lsa.umich.edu/academicintegrity/] and by History [http://www.lsa.umich.edu/history/undergraduate/courses/guidelinesandpolicies /academicintegrity.pdf]. Students should familiarize themselves with both of these documents, which explain the standards of academic integrity and clarify the prohibited forms of academic misconduct including plagiarism and cheating. Students in AC 315/History 377 should follow the Chicago Manual of Style Online for all issues of source citation, along with any specific guidelines provided in the course assignments. Clarifying the disciplinary standards of research ethics and source citation is part of the educational mission of this course, and students should consult the faculty instructor and/or GSI regarding any questions. The penalty for deliberate cases of plagiarism and/or other forms of academic misconduct is a zero on the assignment. Cases that the instructor judges to be particularly serious, or those in which the student contests the charge of academic misconduct, will be handled by the office of the Assistant Dean for Undergraduate Education. All cases of deliberate academic misconduct that result in formal sanctions of any kind will be reported to the dean’s office, as required by LSA policy, which also ensures due process rights of appeal for students.
Policies on Grading
Requests for reconsideration or changes in grade on a particular assignment, or permission to rewrite the assignment, should be made within a week of receiving the initial grade. Requests for reconsideration of final grade must be made within 3 weeks of the posting of grades.
Laptops.
For many reasons, which I am happy to explain to anyone who wishes to discuss it, use of laptop computers, readers, and tablets (as well as other electronic devices) is not permitted during class discussions.

Readings
On average the reading for this class is less than reading in other upper level history and American Culture classes (between 20-60 pages per class meeting). You are therefore expected to actually do it. There are no books to purchase for this class. Course packs are available at Dollar Bill Copying. Every effort has been made to reduce the cost of these course packs but Dollar Bill follows copyright law and has to pay fees for the right to reproduce much of this reading material. A course pack has been placed on reserve at the UGL.
SCHEDULE OF READINGS AND LECTURES:
Week One:
September 9: Introductions
Week Two: Spanish Borderlands
September 14: *** Practice QUIZ*** A Latino History of the United States, Chapter One.
September 16: Bartolomé de las Casas, "The Devastation of the Indies" Fray Mathias Saenz de San Antonio, "Lord if the Shepherd Does Not Hear."
Week 3: The US and the Borderlands
September 21: *** QUIZ 1 *** A Latino History of the United States , Chapter Two.
September 23: Josaiah Gregg, Commerce of the Praries. (1844) John O'Sullivan, "Annexation" The United States Democratic Review (July-August 1845)
Week 4: War and its Aftermath
September 28: *** QUIZ 2 *** Brian Delay, “Independent Indians and the Mexican American War” Treaty of Guadalupe Hidalgo (including Article XI)
September 30 David Weber, Foreigners in Their Native Lands, selections.

Week 5: Immigrant Labor and Racial Capitalism in the West
October 5: *** QUIZ 3*** Friedrich Katz, Secret War in Mexico (selections)
October 7: *** QUIZ 4 *** Kelly Lytle Hernandez, Migra: A History of the Border Patrol (Selections) “Testimony of Mr. Moore”
Week 6: Empire, Metaphor, and Migration.
October 12: *** QUIZ 5 *** Latino History of the United States, chapter 4 (selection). Louis Pérez, Cuba in the American Imagination (Introduction) Sam Corcoran Erman, "Meanings of Citizenship"
October 14: *** Jesse Hoffnung-Garskof, “The World of Arturo Schomburg” José Martí, “My Race”
Week 7: Transforming Puerto Rico
October 19: Fall Break
October 21: *** QUIZ 6 *** Bernabe and Ayala, “Reshaping Puerto Rico’s Economy” Pedro Albizo Campos, “Observations on the Brookings Institution Report”
Week 8: Migrants and Citizens
October 26 Workshop on the question of Haitian migration to the DR. Prof. Edward Paulino, John Jay College, CUNY Maria Isabel Soldevila Brea, Editor-in-Chief Listin Diario Readings and Location TBA
October 28: *** QUIZ 7 *** Bernardo Vega, Memoirs of Bernardo Vega, Selections. (Norton Anthology) Noble Sissle, “Puerto Rican Musicians in the U.S. Army” (Major Problems) Norman Humphrey, "Mexican Repatriation from Michigan." Documents on delinquency in Los Angeles
Week 9: The Commonwealth and the Airbus
November 2: *** QUIZ 8 ***

Sonia Lee, Building a Puerto Rican Civil Rights Movement, 21-60
November 4 Luis Rafael Sánchez, “The Airbus” (Norton Anthology) Presidential Commission on Migratory Labor, Saginaw, MI (1950).
Week 10: Cold War Migrations
November 9 *** QUIZ 10 *** Maria Cristina Garcia, “Cuban Migration and US” Jesse Hoffnung-Garskof, “Yankee go home. . .and take me with you.” Reinaldo Arenas, Before Night Falls. (selection).
November 11 Maria Cristina Garcia, “Central American Wars.” And documents. Meet at Bentley Library, North Campus.
Week 11:
November 16 PRESENTATIONS ON BENTLEY RESEARCH
November 18 *** QUIZ 11 ***
Vicky Ruiz, “La Nueva Chicana, Women in the Movement” Mirta Vidal, Chicanas Speak Out.
Week of November 23.
No class meetings. Independent work on group presentations.
Week 13: The Immigrant Boom November 30. ***QUIZ 12*** D. Massey & K. Pren, “Unintended Consequences of US Immigration Policy”
December 2. Student Presentation and discussion The Latino Market
Week 14: Presentations
December 7: Student Presentation and Discussion Obama’s Immigration Executive Order
December 9: Student Presentation and Discussion Unaccompanied Minors and Family Detention

Week 15: Presentations
December 14: Student Presentation and Discussion Normalization of Ties with Cuba and Cuban Exile Politics
December 19, Final papers due via CANVAS by 11:59 PM.

