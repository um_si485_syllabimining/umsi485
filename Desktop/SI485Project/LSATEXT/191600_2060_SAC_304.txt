SAC 304-002 Cinematography (Topics in Media Production I)
Fall 2015 Syllabus Fridays 10am-2pm 1175 North Quad

Instructor: Dawn Hollison Email: dholliso@umich.edu Office: 6389 North Quad Office Phone: 734-936-2917 Office Hours: Tu/Th 11:15-12:45pm,
or by appointment

Required Text: All required readings will be available on Canvas - umich.instructure.com

Course Description – SAC 304 is an introduction to cinematography where students learn the basics of motion picture photography as it applies to the analogue film medium. The course will cover terminology, optics, 16mm camera operation, lighting equipment, lenses and accessory equipment. Via lecture and multiple hands-on workshops, various techniques of cinematography will be explored including image control through lighting, selection of film, exposure and type of camera and lens used. All techniques will be practiced with an eye toward the aesthetic considerations that are integral to the moving image medium.

Student Responsibilities – Review your syllabus, notes, and text from SAC 290, the prerequisite to this course. Information from the prerequisite course is essential background for your production process and your projects in this class will expand on the experience you gained in that course.
Please be aware that, as with other SAC production classes, the projects in this course will require substantial time commitment outside of regular class time. If you have a heavy class-load and/or work schedule, or have extracurricular commitments that require a great deal of time, please consider taking this course when your schedule is not so conflicted.
The materials and equipment necessary to complete required production projects are provided through the SAC Film Equipment Loan Room. We will be working exclusively with analogue film technology in an effort to instill a thorough understanding of light and exposure. As such you are encouraged to practice the techniques discussed as much as you are able to on your own using the analogue medium. Your instinctive understanding of image production will improve in direct correlation to the amount of thoughtful time you spend practicing these methods.
All written assignments must be typed and written in appropriate formats. For drawn production plans and notes, neatness and professionalism are critical and necessary, even when they are being created on the fly. The craft of cinematography requires meticulousness, which applies to the technical demands of the image as well as every other aspect of your role as Director of Photography. You are encouraged to begin developing your aesthetic practice in conjunction with precision and meticulousness starting now. It will pay you dividends in your development moving forward.

Workshop Dress Code – On all in-class workshop days, a dress code will be enforced due to the fact that we are working with heavy equipment in a hazardous environment. When working on set all students must wear covered shoes and clothing that protects their legs. Please avoid clothing with a tendency to catch on items, (such as long scarves, skirts, etc.).

SAC304-002.Syllabus.F15 page 1 of 5

Content – All projects are discouraged from showing weapons, illegal drugs, drugs deals, or violence, whether fake or real. Any film project that would like to entertain these scenarios will need instructor approval to rent equipment, will need all permits, insurance forms, and waivers signed and submitted to the Film Equipment Loan Room, the University Film Office, as well as city authorities. This is likely to be very time consuming and required paperwork may be costly. Approval of these scenarios will be contingent on the student’s ability to articulate the necessity of such scenes in the proposed production. If deemed unnecessary, they will not be approved.

Grading Scheme – Grades will be based on a fixed point system, as follows:

Requirement Photographic Slide Projects #1 & #2
Lighting Workshops x2 16mm Test Roll Midterm Quiz Photo Notebook
In-Class Film Shoots x3 Production Planning Final Film

Points Possible (100) 15 10 5 10 10 20 10 20

Grading criteria:
Creative and Written Assignments (60%) - Grading will be broken up into two main areas of assessment for assignments: technical and aesthetic. Technical aspects of all creative works will be judged based on the specifics of your camera work (focus, exposure, framing, etc.) as well as overall technical execution. Aesthetics aspects will be judged on the look, quality, and originality of the ideas, their conceptual elements, and how they relate to the formal elements chosen (i.e. editing and composition, etc.). This may also include the visual tone and mood of the piece.
Written assignments, pre-production materials, notebooks and plans will be graded on their overall appropriateness, neatness and professionalism, in addition to the applicable technical and aesthetic elements listed above. For the Final Film each person is required to write a review paper or report. The depth and quality of the assessment in this paper will influence the final project grade. Project hand-outs will lay out specific details for each assignment.
Participation & Workshops (30%) – In conjunction with course attendance, students will be graded on their overall participation in class discussions and critiques, preparedness, working with others, participation in workshops, etc. Participation in peer critique of projects is expected. All students should be prepared to give and receive feedback gracefully and thoughtfully. All in-class workshops will be done in small groups. Successful group collaboration is part of the process and will be a factor in grading.
Midterm Quiz (10%)

SAC304-002.Syllabus.F15 page 2 of 5

Course Attendance – Attendance is mandatory at ALL class sessions. Absences will result in your missing information that is critical to your understanding and technical mastery of the material and will likely cause hardship for you as well as for the rest of your crew/team. As this course meets only once per week, each two hour block of time constitues half a week of classtime/material; four hours is the equivalent of one week of class. Four hours of unexcused absence (i.e., two class periods or one week) will automatically lower your final grade by one full letter and deduction will continue at the rate of one full letter grade for each additional two hours of absence. Production days are tightly scheduled. Regular puctuality, attendance and professionalism are considerations in grading. Class will start promptly at 10 mintues after the hour. Please note that all documentation for legitimate absences must be delivered to me in a TIMELY manner in order for an absence to be considered excused.

We are frequently working with a professional studio staff and fixed schedules throughout this course. Absences affect the entire class and will result in an automatic loss of points. Given the group work and structure of the class, it is impossible to make up missed production work. NO INCOMPLETES WILL BE GIVEN.
The average grade in the course is a B-. Overall class grades will be determined with the following in mind:
A: Outstanding in all aspects of the course. A demonstrated excellence in production and editing skills. All work finished to deadline. 90% or above on quiz and assignments. An ability to thoughtfully critique the strengths and weaknesses of other projects and your own work. Perfect attendance and outstanding participation in all aspects of the course.
B: Good production work. A proven ability in production and editing. Project deadlines met. 80% average on quiz and assignments, good attendance (no more than one class missed) and strong participation in critique and class.
C: Average production work. Acceptable grasp of editing and production technique. Most deadlines met. 70% average on quiz and assignments. Some absences or lateness to class. Some participation in critique and discussion.
D: Below average work with significant weaknesses in one or more areas: meeting deadlines, application of production concepts, group and class participation, poor attendance.
F: Below acceptable requirements of the course including lack of participation in group projects, more than 4 absences.

*Please note that extra credit is not offered as part of this class.

Contact phone numbers:
SAC Office, 6330 North Quad: Faculty Mailboxes are located in 6341 North Quad
Alan Young, SAC Film Technician: aspeed@umich.edu Joel Rakowski, Film Equipment Loan Room: dinoclub@umich.edu

764-0147
763-0948 647-3578

SAC304-002.Syllabus.F15 page 3 of 5

Assignment Due Dates (Subject to Change)

Photographic Slide Project #1

Fri Sept 25

Final Film Pre-production

Fri Oct 23

Photographic Slide Project #2

Fri Oct 9

Midterm Quiz

Fri Oct 30

Photographic Slide Project #3

Fri Oct 16

Photo Notebook

Fri Oct 30

Notebook samples x 2

Oct 1-15

Film Rough Cuts

Nov 23-Dec 9

16mm Test Roll

Fri Oct 16

Final Film, Production Planning & Fri Dec 18 Review Paper

Weekly Schedule Breakdown
Week 1 – Sept 11 – Introduction, Composition & Still Cameras + Light Meters I: NQ 1175 - Course introduction and objectives, syllabus. Composition lecture. Read: Composition, Visual Language, The Lens on Canvas II: STUDIO A - Light meters, 35mm still cameras Assignment: Slide Project #1
*Slide Project #1 due for processing by Wed, Sept 16th, 3pm!
Week 2 – Sept 18 – Exposure, Optics, Film Balance & Color I: NQ 1175 - Exposure, optics, film stocks Read: Capturing the Film Image, Exposure, Lighting, Color II: Film Balance, color, lighting issues, color correction Assignment: Photo Notebook
Week 3 – Sept 25 – Slide Project #1 & Lighting/Grip Gear I: NQ 1175 - Due: Slide Project #1, critique. Assignment: Slide Projects #2 & #3 Read: Cameras on Canvas Photo notebook schedule sign-up II: STUDIO A - Lighting equipment and grip intro
*Slide Projects #2 & #3 due for processing by Wed, Sept 30th, 3pm!
Week 4 – Oct 2 – The Camera I: STUDIO A - Arri-S 16mm camera II: Arri-S 16mm camera, cont’d Assignment: 16mm Test Rolls
Week 5 – Oct 9 – Slide Project #2 & Lighting Lecture I: NQ 1175 – Due: Slide Project #2, critique II: STUDIO A – Lighting demo/lecture Assignment: Final Film, Production Planning

SAC304-002.Syllabus.F15 page 4 of 5

Week 6 – Oct 16 – Slide Project #3 & Lighting Workshop I: NQ 1175 - Due: Slide Project #3, critique II: STUDIO A – Lighting Workshop #1
*Test Roll Film due for processing on Fri, Oct 16th, 3pm!
Week 7 – Oct 23 – Test Rolls & Film Workshop I: NQ 1175 - Test Roll review and critique, LW #1 feedback; Due: Final film pre-production II: STUDIOS B & C – Lighting workshop #2
*Final Film Pre-production due Fri, Oct 23rd, by 5pm! (Tues, Oct 27th, 5pm - at the latest!)
Week 8 –Oct 30 – Midterm Quiz & Film Workshop I: NQ 1775 – Quiz; Due: Photo Notebook II: STUDIOS B & C - Film Workshop #1 Pre-light + Shoot; film processing going out today at 3pm
Week 9 – Nov 6 – Film Dailies #1 & Film Workshop (pre-light) I: NQ 1175 & – Review quiz, Film Workshop #1 dailies Read: On Editing II: STUDIOS B & C - Film Workshop #2, pre-light
Week 10 – Nov 13 – Editing & Film Workshop I: MLB B-108 - Editing review II: STUDIOS B & C – Film Workshop #2; film processing going out today at 3pm
Week 11 – Nov 20 – Film Dailies #2 & Film Workshop I: NQ 1175 – Film Workshop #2 dailies II: STUDIOS B & C – Film Workshop #3 pre-light + shoot; film processing going out today at 3pm
*Final cutoff for all 16mm film processing is Mon Nov 23rd, 3pm!
Week 12 – Nov 27 – Holiday Friday – No Class - Happy Thanksgiving!
Week 13 – Dec 4 – Film Dailies #3 I: NQ 1175 – Film Workshop #3, dailies II: On set etiquette
Week 14 – Dec 11 – Final Films I: MLB B-108 – Editing time
Final Exam – Fri, Dec 18th, 10am-2pm I: NQ 1175 – Due: Final Films, production planning, review papers, crits
Please note: ALL TRANSFERS for Lightworks will be held on Wednesday, Dec. 16 and Thursday, Dec. 17. In order to screen work at Lightworks, you MUST schedule an appointment during the transfer times. NO EXCEPTIONS.
Finals Week – Dec 16 – 18 & Dec 21 – 23 Lightworks Festival – December 18 & 19
SAC304-002.Syllabus.F15 page 5 of 5

