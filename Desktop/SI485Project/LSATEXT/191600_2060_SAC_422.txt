SAC 422.001 – Avant-Garde Cinema Fall 2015 Syllabus Tues/Thurs - 2:30pm to 4pm - MLB 1 Wed. Screening - 6pm to 8:30pm – NQ 1470

Instructor: David Marek
Email: dkmarek@umich.edu
Office: NQ 6417
Office Phone: 734-615-4207 Office Hrs: Tues/Thurs 12:30 - 2pm
or by appointment

Required Text:
-The majority of readings will be available on the class Canvas site. -Devotional Cinema (2nd Edition) by Nathanial Dorsky – this book is available on Amazon for $8.95 and will be needed by week 9 (Oct. 28th).
http://www.amazon.com/Devotional-Cinema-Edition-Nathaniel-
Dorsky/dp/1931157065/ref=sr_1_1?ie=UTF8&qid=1409515150&sr=8-1&keywords=devotional+cinema

Course Description:
This survey of the avant-garde cinema attempts to define the genre itself. Beginning with the earliest films up until the present the questions this course asks are: What makes a film avant-garde? Is avant-garde filmmaking an historical category or is it still a viable genre today? A range of texts is used to explore the historical, aesthetic, theoretical, political and social issues raised by film artists who work outside the confines of the studio film industry.

Attendance:
Attendance is mandatory at ALL class sessions. Class starts promptly 2:40am, after that you are late. After 2:50am you are considered absent. 2 times late = 1 Absence. After 2 absences you will lose 5% off on your final grade for each additional absence! Absences will result in you missing information that is critical to your understanding and technical mastery of the production process and will likely cause hardship not only for you but also for the rest of your crew/team. Production days are tightly scheduled. Regular attendance and punctuality are both considerations in grading.

Grading Scheme – Grades will be based on a fixed point system, as follows:

Assignments Blog Journal Scratch & Hand-painted films Midterm Presentations Final Exam Attendance and Participation

Points Possible 15 10 20 15 25 15

Total

100

Grading Criteria:

Overall class grades will be determined with the following in mind. A: Outstanding in all aspects of the course. A demonstrated excellence in analysis of the films and synthesis of the readings, as well as work finished to deadline. 90% or above on assignments. An ability to thoughtfully consider and participate in all aspects of the course, along with excellent attendance.

B: Good in all aspects of the course. A proven ability to analyze the films and synthesize the readings into writing and discussion. Project deadlines met. 80% average on assignments, good attendance and strong participation in discussion.

C: Average in all aspects of the course. Acceptable writing and ability to analyze the films and synthesize

the readings. Most deadlines met. 70% average on assignments. Some absences or lateness to class. Some participation in critique and discussion.

D: Below average work with significant weaknesses in one or more areas: meeting deadlines, quality of writing, staying up to date on the readings, group and class participation, poor attendance.

E: Below acceptable requirements of the course including lack of participation in group projects, more than 4 absences.

Assignment Due Dates (Subject to Change)

Assignments Blog Journal Scratch & Hand-painted films Midterm Presentations Final Exam

Due Date Sunday by 9pm
Oct. 13th Oct. 22nd
TBD Dec. 18th by 6pm

-Readings – Readings should be finished before class discussion on Thursdays, but I suggest reading them before the screening on Wednesday night.

-Blog Journals – Entries are due at the end of each week by 9pm Sunday night. You do NOT need to write a blog in week 7 (Midterm), and the week of your presentation (TBD).

-Midterm & Final – The Midterm and Final will be take-home, open book written exams, including both short answer and essay sections.

-Presentations – will be due based on a schedule set up in class.

Weekly Schedule Breakdown
Week 1 - Introduction -Sept. 8th -Introduction -What is Avant-Garde Cinema?
-Sept. 9th Screening – Early films and Expressionism -Trick Films by Melies - TBD -Dream of a Rarebit Fiend (1906) McCutcheon, Porter – 7min (Unseen disk 2) -The Cabinet of Dr. Caligari (1920) - Robert Wiene - 51min -Ménilmontant (1926) – Dimitri Kirsanoff – 38min (Avant-Garde disk1)
-Sept. 10th -Discussion -In Class Screening -The Passion of Joan of Arc (1928) – Carl Theodor Dreyer – Clips
-Reading Due: -Experimental Cinema, The Film Reader – Introduction Toward a New History of Experimental Cinema -Gunning – Cinema of Attractions -Kracauer – Caligari

Week 2 -
-Sept. 15th -Russian Montage -In Class Screening: -Battleship Potemkin (1925) Sergei Eisenstein - Excerpts -Man with a Movie Camera - (1929) - Dziga Vertov – Excerpts
-Sept. 16th Screening -October (Ten Days that Shook the World) - (1928) - Sergei M. Eisenstein - 95min.
-Sept. 17th -Discussion -Reading Due: Rees – Introduction Eisenstein – The Cinematic Principle and the Ideogram Vertov – Kino-Eye
Week 3 – Dada and Surrealism Part I -Sept. 22nd -Intro to Dada and Surrealism -In Class Screening: - The ABC’s of Dada: http://youtu.be/EqkIJ0odFxA -Assignment: Hand Painted and Scratch Films
Sept 23rd Screening - Le Retour á raison (1923) Man Ray - 2min - Emak-Bakia (1926) Man Ray – 18min - Entr’acte (1924) Rene Clair – 15min - Symphonie Diagonale (1924) Viking Eggeling – 5min - Blood of the Poet (1930) Cocteau – 50min - Anemic Cinema (1924-1926) Duchamp/Ray – 6min - Ballet Méchanique (1924) Leger -11min
-Sept. 24th -In Class Screening: -Land without bread (1933) - Luis Buñuel - 30min -Un Chein Andolu (1929) Dali and Buñuel – 15min -Discussion -Reading Due: Le Crice – Abstract Film and Beyond Ray – Emak-Bakia from Close Up Baldwin – Man Ray – From Dada to Surrealism Tzara – Seven Dada Manifestos
Week 4 – Surrealism continued & The First American Avant-Garde -Sept. 29th -Further Discussion -Reading due: -Ramain – The Influence of Dreams on the Cinema - Objects of Desire - Bunuel Interview -Rees – Part 1 – Canonical AG

-Sept. 30th Screening -Manhatta (1920) Scheeler and Paul Strand – 11min -A Bronx Morning (1931) Jay Leyda – 14min -The Life and Death of 9413 (1928) Slavko Vorkapich & R. Florey – 13min -Vorkapich Montage Sequences (1928 – 1937) Slavo Vorkapich – 3min -The Fall of the House of Usher (1928) James Sibley – 12min -The Hearts of Age (1934) Orson Welles & William Vance – 6min -Lot in Sodom (1933) Watson and Webber – 27min
-October 1st -Discussion -In Class Screening: -Trade Tattoo (1937) Len Lye – 5min -Reading Due: -Horak - The First American Film Avant Garde
Week 5 – Women and the Avant-Garde -October 6th -Intro to Women and the Avant-Garde -In Class Screening -The Smiling Madam Beudet (1922) Germaine Dulac - 38 min -The Seashell and the Clergyman (1926) Germaine Dulac- 40 min
-October 7th Screening -Meshes of the Afternoon (1943) - Maya Deren, Alexander Hamid - 14min -Cleo from 5 - 7 (1962) Agnes Varda - 90 min. -Asparagus (1979) - Susan Pitt - 18min -Jeanne Dielman, 23 Quai de Commerce, 1080 Bruxelles (1975) Chantal Akerman - clips
-October 8th -Discussion -Reading Due: -Lewis – The Image and the Spark: Dulac and Artaud Reviewed -Deren – Articles -Sitney – Meshes of the Afternoon -Mayne – Women in the Avant-Garde
Week 6 – Anger/Warhol -October 13th -DUE: Hand Painted & Scratch Films -Watch Hand Painted & Scratch Films
-October 14th Screening -Chelsea Girls (1966) Paul Morrissey, Andy Warhol – Excerpt – 120min -Reading Due: -Berg – Nothing to lose: Interview with Andy Warhol -Suarez - Pop, Queer or Fascist? The Ambiguity of Mass Culture in Kenneth Anger’s Scorpio Rising.
-October 15th -In Class Screeing: -Scorpio Rising (1963) Kenneth Anger - 29m -Discussion on Warhol & Anger

Week 7 – Structuralism -October 20th -NO CLASS!!! – Fall Study Break
-October 21st Screening -Zorns Lemma (1970) Hollis Frampton - 60min -Tokyo Story (1953) Yasujirô Ozu – Clips -Time Code (2000) – Mike Figgis - Clips -Wavelength (1966) Michael Snow – 45min
-October 22nd -MIDTERM DUE – MIDTERM DUE – MIDTERM DUE – MIDTERM DUE!!!!! -Discussion -In Class Screening -Serene Velocity (1970) - Ernie Gehr - 23min
-My name is Oona (1969) - Gunvor Nelson – 10min
-Reading Due: -Framption – Lecture -Hartog – Ten Questions to Michael Snow -Michelson – Toward Snow -Reese – Origins of Post-war Avant-garde
Week 8 – Brakhage & Solomon -October 27th -Further Discussion of Structuralism -Intro Brakhage & Solomon
-October 28th Screening -Mothlight (1963) Stan Brakhage - 4min -Window Water Baby Moving (1959) Stan Brakhage - 12m -Prelude: Dog Star Man (1962) - Stan Brakhage - 25min -Hymn to Her (1974) Stan Brakhage – 3min -The Dante Quartet (1987) - Stan Brakhage - 6min -Psalm II: Walking Distance (1999) - Solomon - 23min -Psalm III: Night of the Meek (2002) - Solomon - 23min -Still Raining, Still Dreaming (2009) - Solomon - 12min
-October 29th -Presentation on Brakhage and discussion -Presentation on Solomon and discussion -Reading Due: -Stan Brakhage - Metaphors on Vision -Brakhage Interview -Camper – By Brakhage; The Act of Seeing -Brakhage_Dorsky Interview -Arthur – Before the Beginning Was the Word
Week 9 – From Lyric to Transcendental Style -November 3rd -Intro to Transcendental Style in Film -In Class Screening: -Fog Line (1970) Larry Gottheim – 11min -New York Portrait: Chapter II (1981) Peter Hutton - 16min -New York Portrait: Chapter III (1990) Peter Hutton – 15min

-November 4th Screening -Arbor Vitae (2000) – Dorsky – 28min -Triste (1996) - Dorsky – 18.5min -Mother and Son (1997) Aleksandr Sokurov – 73min -Reading Due: -Dorsky – Devotional Cinema -Schrader – Transcendental Style in Cinema -Dorsky - Interview
-November 5th -Presentation on Dorsky and discussion -Presentation on Mother and Son and discussion
Week 10 – Transcendental Style continued -November 10th -Intro to Malick and Dissociative Editing
-November 11th Screening -To the Wonder (2012) Terrence Malick – 112min -Reading Due: -Mottram – All things Shining -Patterson – Poetic Visions of America -Steritt – Film, Philosophy and Terrence Malick
-November 12th -Presentation on To the Wonder and discussion
Week 11 – Chris Marker -November 17th -In Class Screening -La Jette (1962) - Chris Marker - 28min -Presentation on La Jette and discussion -Reading Due: -Chris Marker Bio
-November 18th Screening -Sans Soleil (1983) – Chris Marker – 100min -Reading Due: -Lapote – In Search of the Centaur: The Essay Film -Sans Soleil Structure -Sans Soleil review
-November 19th -Presentation on Sans Soleil and discussion -Intro - Dogma 95
Week 12 -November 24th -In Class Screening: -Julien Donkey-Boy (1999) Harmony Korine – 94min -Reading Due: -Dogma 95 Manifesto and Vows -Kelly – Dogma95

-November 25th Screening - NO CLASS!!! Thanksgiving Recess
-November 26th - NO CLASS!!! Thanksgiving Recess
Week 13 -December 1st -Presentation on Julien Donkey-Boy and discussion -Intro Tarkovsky
-December 2nd Screening -Stalker (1979) Andrei Tarkovsky – 163min -Reading Due: -Tarkovsky - Sculpting In Time -Martin – Life & Time
-December 3rd -Presentation on Stalker and discussion
Week 14 -December 8th -TBD
-December 9th Screening -Breaking the Waves (1996) Lars von Treir – 159min -Reading Due: -Makarushka – Transgressing Goodness in Breaking the Waves -Lars Von Trier – Interview - Breaking The Waves
-December 10th -Presentation on Breaking the Waves and discussion
Week 15 -December 15th -No Class – Study Day
-Final Exam Due: 12/18 by 6pm – in box outside my office (NQ 6417)
Lightworks Screenings Fri-Sat, Dec 18-19, 6pm-midnight
Contact Phone Numbers: SAC Office, 6330 North Quad (mailboxes located here): 764-0147 Rob Hoffman/Chief Media Engineer: robhoffm@umich.edu 936-1968 ISS Media video equip, and editing reservation (2nd floor MLB): 763-1104 Ralph Franklin, ISS Media Facilities and Editing: ralphf@umich.edu 936-3504 Alan Young, SAC Film Technician: aspeed@umich.edu 763-0948 Joel Rakowski, Film Equipment Loan Room: dinoclub@umich.edu 647-3578 LSA Advising link: https://www-a1.lsa.umich.edu/advappts/aa_stuselfsvc1.aspx?ctgy=SAC LEO Scholarships: http://www.leounion.org/scholarships UM Scholarships: http://www.finaid.umich.edu/Home/TypesofAid/ScholarshipsandGrants/OFAScholarshipListing.aspx

