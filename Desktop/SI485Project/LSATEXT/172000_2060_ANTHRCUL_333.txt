N on - W est er n L a w A nt hr opol ogy 333

FALL 2015

Professor Maxwell Owusu Tuesdays & Thursdays 2:30 – 4:00 pm September 8 - December 14, 2015 1512 CCL

Office: 205B West Hall Hours: Monday 10:00-12:00
or by appointment Phone: (734) 763-1063

COURSE DESCRIPTION, REQUIREMENTS, AND OUTLINE OF TOPICS
This course introduces students to the legal system and legal process in non-Western societies and cultures before and after western contact and colonial rule. It examines some of the basic concepts, theories and approaches used by anthropologists in their attempt to describe and account for the presence or absence of the different types of rules, social regulations and legal institutions found cross-culturally. It discusses the impact of western law on non-western societies, using case studies from Africa, the Middle East, Asia, and the Americas. The course provides an introduction to comparative law from an anthropological perspective.

Lecture/Discussion format. Films or videos will be shown in class when available.

COURSE REQUIREMENTS

Regular class attendance. Students must read the assigned materials for each topic before class meeting. Lectures are combined with class discussion for better understanding, and for the integration of lectures and readings. Student participation in class discussion is essential.

REQUIRED TEXTS

Donald Black

The Behavior of Law, Academic Press. Paperback.

Paul Bohannan, ed.

Law and Warfare: Studies in the Anthropology of Conflict. Univ. of Texas Press. Paperback.

L.M. Friedman

Total Justice, Beacon Press. Paperback

Laura Nader and H.F. Todd, Jr.,eds.

The Disputing Process: Law in Ten Societies. Columbia University Press. Paperback.

REQUIRED READINGS, COURSEPACK

Sally Falk Moore

"Law and Anthropology," pp 252-300.

L. Nader

"The Anthropological Study of Law" in American Anthropologist. Part 2, V.67, no.6, Dec. 1965:3-32.

A.L. Epstein

"The Case Method in the Field of Law: in A.L. Epstein, ed., The Craft of Social Anthropology. Tavistock Publications, 1967:205-230.

Anthropology 333 John Prendergast Wal Duany Maxwell Owusu

“Building on Locally-Based and Traditional Peace Processes”. In David R. Smoch, ed. Creative Approaches to Managing Conflict in Africa: Findings from USIP Funded Projects. 1997: 16-21.

A. Radcliffe-Brown E.A. Hoebel S.F. Nadel L. Pospisil Cultural Survival L. Pospisil A. Radcliffe-Brown J.P. Spradley
L. Nader Peter R. Grant
A.K.P. Kludze
Emmanuil Royz
E.A. Hoebel

Primitive Law. The Bobbs-Merrill reprint. No.A-117.
Law and Anthropology. The Bobbs-Merrill reprint. No.A-117.
Social Control and Self-Regulation. The Bobbs-Merrill reprint. no.S-208.
The Ethnology of Law. Addison-Wesley Module 12, 1972.
Brazilian Indians and the Law. Occasional Paper 5, October 1981.
"Modern and Traditional Administration of justice in New Guinea: in Journal of Legal Pluralism and Unofficial Law. No.19, 1981:93-534.
Social Sanction. Encyclopaedia of the Social Sciences. Reprint. The Macmillan Co., 1940:531-534.
"The Ethnography of Crime in American Society" in L. Nader and Thomas W. Meretzki, Cultural Illness and health. Anthropological Studies, no.9:23-34.
"From Disputing to Complaining." in Toward a General Theory of Social Control. Vol. 1. Fundamentals. 71-94.
"Recognition of Traditional Law in State Courts and the Formulation of State Legislation." in Papers of the Symposia on Folk Law and Legal Pluralism. XIth International Congress of Anthropological and Ethnological Sciences, Vancouver, Canada, Aug. 12-23, 1983, V.1:61-73.
"The Effect of the Interaction Between State Law and Customary Law in Ghana." in Papers of the Symposia and Legal Pluralism...Vol. One: 7494.
"The Impact of Soviet Law on the Way of Life of Northern Native Populations." in Papers of the Symposia on Folk Law and Legal Pluralism...Vol. One: 422-443.
The Law of Primitive Man. Atheneum. Paperback.

CLASSIC MONOGRAPHS IN ANTHROPOLOGY OF LAW: A SELECTION

Bohannan, P. *Gluckman, M. *Llewelyn and Hoebel Kennett, Austin *Maine, Henry

Justice and Judgment Among the Tiv. 1957. The Judicial Process Among the Barotse. 1955. The Cheyenne Way. 1941. Bedouin Justice. 1925. Ancient Law. 1861.

2

Anthropology 333

ADDENDUM

Nader and Todd, eds., Disputing Process Case Studies

(1) Barbara B. Yugvesson:

"The Atlantic Fishermen"; 59-189

(2) Michael J. Lowy

"A Good Name...in Urban Ghana"; 181-208

(3) Julia L. Ruffini

"Disputing...in Sardinia"; 109-246

Bohannan, ed. Law/Warfare Case Studies (1) Gluckman on Barotse: (2) Feifer on Moscow: (3) Nader on Zapotec: (4) Hoebel on Comanche: (5) Hoebel on Eskimo (6) Tewskbury on Mediaeval Europe: (7) Warner on Liberia: (9) Gibbs on Kpelle:

59-91 93 115 117-138 183-203 255-265 267-270 271-275 277-289

3

Anthropology 333
TAKE-HOME PAPERS

(Topics are given out a week before due dates)

GUIDELINES FOR SHORT PAPERS
1. It is advisable to use the maximum pages allowed. 2. Organization. Outline the points to be included and then discuss. 3. Make explicit use of lectures, videos, films, etc. 4. Make explicit use of required texts with appropriate references and of films
where applicable. 5. Read over for typographical errors. Take preparation seriously--don't leave it
until the last minute. 6. Answer all questions, or address all relevant points

FINAL GRADES ARE BASED ON THE FOLLOWING:

*A) Review of the Nature, Functions and Development of Law From an Anthropological Perspective. (September 24/October 1)

*B) Types of Dispute-settlement Procedures.

Comparative case-studies.

(October 27/29)

*C) Law in Contemporary Society.

(November 24/December 1)

*D) Contribution to class discussion.

Total:

GRADE SCALE:

96-100 = A+

79-84 = B+

56-59 = C+

90-95 = A

70-78 = B

51-55 = C

85-89 = A-

60-69 = B-

40-50 = C-

Below 24 = F

30 points.
30 points. 35 points. 5 points. 100 points
36-39 = D+ 31-35 = D 25-30 = D-

4

Anthropology 333

TOPICS AND READINGS

Week 1, September 8, 10

Topic:

Organizational Meeting.

(Film/Video: "Justice in America. Some are More Equal.")

Week 2, September 15, 17

Read:

Lawrence M. Friedman, Total Justice Part I, Chapters 1, 2, 3, 4, 5.

Film: "Justice in America. Some are More Equal.", "Law and Justice."

Week 3 & 4, September 22, 24, 29, October 1

Topic:

(1) The Anthropological Study of Law: Ethnology of Law; Ethnography of Law; Legal Anthropology.

What the anthropologist does and how he or she looks at the problem of law in society or law and society. The question of the nature, functions and development of law. The relations between law and custom; law and government, etc.

Read:

Part I. The Nature of Legal Anthropology, in P. Bohannan, ed., Law and Warfare, pp. xi-58 (Redfield, Pospisil, Bohannan).

**A. Radcliffe-Brown, Primitive Law.

**E.A. Hoebel, The Law of Primitive Man, Part I, 1 & 2; pp. 3-28 ; Part II, 11 & 12; pp. 275-333.

**L. Pospisil, The Ethnology of Law.

**L. Nader, "The Anthropological Study of Law".

* D. Black The Behavior of Law, Chap. 2,3,4

**S. Diamond, "The Rule of Law Versus the Order of Custom".

Week 5 & 6, October 6, 8, 13, 15 [Study Break October 19-20]

Topic:

(2) Social Control and Social Regulation. Types of Sanctions. Cross-cultural comparison.

Read:

D. Black, The Behavior of Law,, Introduction, Chapters 6,7.

** S.F. Nadel, Social Control and Self-Regulation.

R.M. MacIver and C.H. Page, Society: An Introduction. Chapters 7,8,19.

** A. Radcliffe-Brown, Primitive Law.

** A. Radcliffe-Brown, Social Sanction.

** A.L. Epstein, "The Case Method in the Field of Law".

5

Anthropology 333 Week 7 & 8, October 22, 27, 29

Topic:

(3) Modes of settling or resolving disputes. Self-help, negotiation, mediation, arbitration. Ordeals, divination, oracles. Variations in law-ways.

Read:

Part II. The Ethnography of Law: The Judicial Process in P. Bohannan, ed., Law and Warfare, pp. 59-91; Hoebel on Comanche Indians; Hoebel on Eskimo; Bohannan on the Tiv; Tewksbury on Medieval Europe; Warner on Liberia; Gibbs on Kpelle.

L. Nader and H.F. Todd, Jr. "Introduction" in L. Nader and H.F. Todd, Jr., eds.,The Disputing Process; Law in Ten Societies, pp. 10-40.

J. Prendergast, W. Duany, M. Owusu, “Building on Locally-Based and Traditional Peace Processes”: 16-21.

Films: "Resolving Conflicts"; "The Trial of Worker Guo". Weeks 9 & 10, November 3, 5, 10, 12

Topic:

(4) Adjudication: The sphere of courts, legal cultures, legal structures, legal actors, legal processes and institutions; rules, procedures, precedents.

Read:

Part II. The Ethnography of Law: The Judicial Process in P. Bohannan, ed. Law and Welfare, pp. 59-91. (Gluckman on Barotse; Feifer on Moscow; Nader on Zapotec.)

**A.L. Epstein, "The Case Method in the Field of Law".

D. Black, The Behavior of Law, Chapters 3,4, 5.

**James P. Spradley, "The Ethnography of Crime in American Society".

**L. Nader, "From Disputing to Complaining."

Film/Video: "Courts and Councils." "Little Injustices: Nader Looks at Law."; "Michigan Courts."

Week 11 & 12, November 17, 19, 24, December 1

Topic:

(5) The Impact of Western Culture and Law on Tribal, Aboriginal or Customary Law. Legal Pluralism and Folk Law.

Read:

**Peter R. Grant, "Recognition of Traditional Laws in State Courts and the Formulation of State Legislation.

**A.K.P. Kludze, "The Effects of the Interaction Between State Law and Customary Law in Ghana."

6

Anthropology 333 **Emmanuel Royz, "The Impact of Soviet Law on the Way of Life of Northern Native Populations." **L. Pospisil, "Modern and Traditional Administration of Justice in New Guinea". **Cultural Survivals Inc., Brazilian Indians and the Law. Film/Video: "White Justice"
Wednesday, November 25-27, THANKSGIVING!! 

Week 13 & 14, December 1, 3, 8, 10

Topic:

(6) Law in Contemporary Society, Focus on and general discussion of some of the controversial issues on the uses and abuses of the law hitting the news headlines, e.g. Human rights, Law and morals, Law and religion, Affirmative Action, etc.

December 14 (Monday) Classes End

7

Anthropology 333
September 8, 10 September 15, 17 September 15, 17 October 1, 6 October 15 October 22 October 27, 29 November 3, 5 November 5 November 10, 12 November 17, 19 November 24 November 24, Dec. 1 December 3, 8

Anthro 333 Film and Video Schedule Justice in America: Some Are More Equal Than Others Law and Justice TBA TBA Resolving Conflicts The Trial of Worker Guo Courts and Councils Little Injustices: Laura Nader Looks at the Law Case Study: Community Justice Mediation Overview White Justice TBA TBA TBA

8

