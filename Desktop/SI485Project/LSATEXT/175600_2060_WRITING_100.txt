WRITING 100 • Transision to College Writing • Fall 2015
M/W 3-4pm 1155 North Quad Instructor: Jill Darling, Ph.D. Email: jldarlin@umich.edu Office: 1319 NQ (In Sweetland Center for Writing)
Course Texts
• Paul Loeb, The Impossible Will Take a Little While • Jonathan Kozol, Amazing Grace
These books should be available in university bookstores, on amazon.com or other online booksellers. All other course readings are available (and required) as PDF files posted on Canvas.
Course Description
This course will provide you with the opportunity to practice academic writing skills that will serve you well in college and beyond. We will focus intensively on strategies for reading, analyzing, and responding in writing to texts—tasks you will be asked to do often as a college student.
This course includes a couple unique features designed to assist you as you develop as an academic writer. First, you will meet with me for one-on-one conference sessions and as part of a group during which we will discuss what you are working on for the course and your development as a writer. Second, you will create an electronic portfolio which will showcase your work for the course and your evolution as a writer. We will look at examples and talk about creating this portfolio over the course of the semester.
This course does not fulfill the first year writing requirement, though it is designed specifically to help you succeed in future writing courses as well as in all other courses with a writing component. This course does count as credit towards graduation.
Course Topic
The topic for this section of WRITING100 is contemporary culture and social engagement. Although we will focus on writing and discussion of writing process, the readings in the course will give us thoughtful material to consider, analyze, and discuss. Because everyone in the class is coming from a different set of backgrounds and experiences, we will have different perspectives and interpretations of the readings. We’ll talk a lot about objectively summarizing authors’ texts and ideas, and how to frame our own responses and analyses to those arguments. We should also remember to always respect each others’ opinions, ideas, and experiences and give each other space for participating in the conversation in the classroom. Discussing

contemporary culture in terms of social engagement can be a sensitive subject for some as we read about race, class, gender, ability and other topics that we all have different personal relationships to. I will expect everyone to be open to listening to and respecting others’ voices in our conversations.
It is imperative that you bring a copy of what you read to class with you on the day we discuss it. We will discuss the first readings from paper copies which you are responsible to print out. After that, I prefer that you bring a paper copy, but you can use an e-reader or laptop (not a phone with e-reader capabilities). If I find that you are using your e-reader in class for purposes other than to consult the text we are studying that day, you will no longer be able to use your e-reader during class.
**If financial need makes it difficult for you to purchase books, a laptop computer, or other classroom materials, the Gayle Fund (established in honor of Gayle Morris Sweetland) can loan you the necessary materials. You can talk to me about how to apply or you can get an application form from the receptionist in the Sweetland office at 1310 North Quad.
Course Requirements This course is credit / no credit. Because you will not earn a grade, it is an ideal place to practice your writing skills without anxiety. As you know, practice of any kind—whether it is for a sport, musical instrument, foreign language, or to master a body of knowledge or particular academic ability, like writing—requires your effort, dedication, and attention. If you take this course and the practice it offers you seriously, you will see improvement in your writing abilities. To receive credit for this course, you must meet all these requirements:
• Come to class and conference sessions regularly and on time. If unavoidable emergencies interfere with your ability to attend any class or conference session, please consult with me as soon as you can. You are responsible for catching up on what you miss due to any absence by getting info from your classmates or talking to me during office hours.
• Complete all assignments, large and small, with care and on time. If you know ahead of time that you will be unable to make a deadline, talk to me about it, and we will see if alternative arrangements can be made. Students who turn in careless work should expect not to receive credit for the course.
• Thoroughly revise your major assignments between rough and final drafts. You cannot receive credit for this course by only writing one draft of major assignments, or by only doing minor revision work. You will receive a lot of suggestions for revision—from me and from your peers—to assist you. Final versions of papers should be polished and refined.
• Participate actively in class. Please prepare ahead for class and be willing to engage in discussion and class activities.
• Participate actively in conference sessions. (1) look at the course schedule to see what you should prepare for the session (2) come with a paper copy of the assignment you are

working on (3) have prepared questions or topics to ask about (4) be ready to discuss; don’t just expect to passively collect advice.
• Read your fellow students’ work with care and provide constructive feedback. To help you revise your papers, you will receive feedback from your fellow students, and you will provide feedback to them on most writing assignments. This kind of Peer Critique takes time, care, and commitment to helping your peers succeed. Peer critique will provide you with useful feedback on your work and help you practice reading with a critical eye. Students who miss more than one peer critique should expect not to receive credit for the course.
• Do all of the course readings on time and carefully. Come prepared to do in-class writing and discussion for all assigned readings. Bring any homework writing printed on paper to class. It is all right to come to class confused by a reading but with questions about what you read; this shows me you are engaging with the text, even if you struggled to make sense of it.
• Create and submit an electronic portfolio. The electronic portfolio is the culmination of all your work for the course, and it provides a way for you to reflect on your development as a writer. To receive credit for this course, you must submit a complete and thoughtfully composed portfolio at the end of the semester.
Course Writing Assignments You will complete several kinds of writing assignments, from shorter and more informal writing to large essay assignments.
• Small Writing Homework Assignments and In-Class Writing: This writing will be related to readings or other topics we discuss during the semester. You may use any small writing assignments as prewriting for other assignments.
• Major Assignments: You will complete four major assignments that will vary in length from 3 to 9 pages. These assignments will ask you to perform the kind of analytical tasks you will often be asked to do during your college career and will include prewriting, outlining, drafting, and revision.
• Reflective Writing: You will do a good deal of reflective writing—that is, writing in which you think about your writing practices and development as a writer. This writing is important and I expect you to take it seriously; all good writing involves reflection, and this work will get you in the habit of thinking about your habits as a writer—what your strengths are, and how you can improve.
• Electronic Portfolio: Much of the writing described above will have a place in your electronic portfolio, which is a public document that will demonstrate your abilities as a writer. Though the portfolio is itself made up of all the writing you do all semester, it will take a bit of work to make it all make sense together, and we will spend time talking about how to compile and refine your electronic portfolio so that it is an attractive and useful text in its own right.

Other Course Policies
Turning in Major Assignments: All major assignments should be double-spaced with one-inch margins and 12-pt Times New Roman font and should always have a title. You will receive paper copies of detailed assignment sheets for each major assignment and due dates are listed on the course schedule. You should submit major assignments in Canvas in a timely manner.
Students with Disabilities: The Services for Students with Disabilities (http://www.umich.edu/~sswd/) is available to assist university students who need accommodations due to a disability. Please contact them for more info or with any questions even if you are not sure you need an accommodation.
Plagiarism: Plagiarism is the unacknowledged use of others’ materials, both words and ideas. We will discuss how to properly cite sources in this class, but if you ever have a question regarding how to correctly give credit to another writer for his or her ideas, ask me about it. A student who plagiarizes in this course should expect the following penalties: not receiving credit for the course and being placed on academic probation. If a student already on academic probation is caught plagiarizing, s/he is usually suspended from the university. Please see: http://www.lsa.umich.edu/bulletin/chapter4/conduct.
Disruptions in Class Please turn off and put away all cell phones, electronic devices, and laptops unless we are using them for class. If for some reason you need to keep your cell phone on, please notify me before class.
I reserve the right to amend the policies in this syllabus as changes become necessary.
This schedule is not set in stone; it may be necessary for me to make some changes in it as we move through the semester. Always read, highlight, and bring the assigned reading material to class with you, preferably on paper.
September
9 Introduction to course In-class writing and discussion (True Colors)
14 Due: Reflection Writing on placement exam essay (see assignment sheet handout for Reflection Writing) Read: from ReMix on cultural analysis (PDF) In-class writing, discussion, exercises _____
16 Read: from Outliers (chapters TBA) Due: “My Relationship to Writing” Essay Draft _____ *20 Sunday by midnight “My Relationship to Writing” Essay Final, Revised
post to Canvas Assignments-Essays

_____

Sept 21 Discuss Outlier’s con’t ; Henry Miller’s “Reflections on Writing” (PDF) Read: from They Say, I Say (PDF 1): chap Two, “The Art of Summarizing” (chaps 1, 3 optional) in-class: we’ll continue with the discussion of Outliers (the hockey example) and then discuss Miller– the form and content of his essay– writing summary via They Say I Say, and etc. ___

Sept 23

Due: Summary of Miller (bring typed and printed to class; see assignment sheet

and sample summary in Canvas Files)

Discussion of E-portfolio and Blogging; bring your laptop to class

In-class : setting up a blog and posting:

1. set up a blog: wordpress or blogger; links to instructions:

wordpress: https://wordpress.com/ (and start a blog) (make sure to choose a free layout and say

no to any questions that ask you if you want to pay for service; just use the free blog service)

https://en.support.wordpress.com/five-step-blog-setup/

blogger: https://support.google.com/blogger/answer/1623800?hl=en

2. create a static front page with a welcome note and a posts page for posting responses (see

instructions:

wordpress: https://codex.wordpress.org/Creating_a_Static_Front_Page

blogger: http://www.bloggertipspro.com/2012/06/creating-blogger-static-home-page.html

_____

Sept 28

Continue with blog setting up and writing, discussion of E-portfolio, online media,

etc.

Bring your laptop to class

_____

Sept 30

Read: Douglass (link here) or find PDF on Canvas

Read: King, Jr. “Letter” link here to text online

Due: Summaries of Douglass & King (bring typed and printed to class)

Work on Response writing in-class

Summary-Analysis-Response Assignment TBA

_____

October

Oct 5

Read: from Soul of a Citizen (PDF) (moved to Oct 12)

Summary-Analysis-Response draft workshop in class (see assignment sheet: draft 1)

_____

Oct 7 Summary-Analysis-Response draft workshop2 in class (see assignment sheet: draft 2) _____

Oct 12

Read: West, “Prisoners of Hope,” Zinn, and Mandela (in Impossible)

Read: from Soul of a Citizen (PDF)

_____

Oct 13 Blog Reflection Writing 1 Due (though you can write/post anytime earlier); see Assignments in Canvas _____

Oct 14 _____

Read: Nouwen and Kushner (in Impossible)

Oct 15 11pm _____

Thur: upload Summary-Response Final, Revised Draft to Canvas Assignments by

19 & 20 Fall Break No Class _____

Oct 21

Read: Walker, Starhawk, Tutu (in Impossible)

Research and Citing Sources

_____

Oct 26 _____

Argument Essay Draft + Reflection Due and Workshop in Class

Oct 28 _____

Film: Inequality for All

November *You are responsible for watching two films outside of class: Capitalism, A Love Story & I Am. These are on reserve in the Askwith Media Library in the Shapiro Library (http://www.lib.umich.edu/askwith-media-library). There are writing assignments due for each film (Nov 18 and Nov 30; see schedule below). _____

Nov 2 _____

Read: Amazing Grace (pgs 1-75/100)

Nov 4

Film con’t: Inequality for All

Upload Argument Essay Final, Revised to Canvas Assignments by 5pm

_____

Nov 9 _____

Film con’t + discussion + Final Paper discussion

Nov 11 _____

Final Paper Project Draft Due and Workshop in Class (see assignment sheet)

Nov 16

Read: Amazing Grace (pgs 75-150); come to class prepared to share specific

passages from the text, any research you have found to tell us more about the South Bronx today,

or any issues you have looked into further, and your reflection/responses/ideas.

_____

Nov 18 Final Paper Project Draft Due + Reflection and Workshop in Class (see assignment sheet) Upload draft + reflection to Canvas Assignments _____

Nov 23 Work on E-Portfolio in class; bring your laptop to class video/audio essay discussion in class; links: http://www.blackbird.vcu.edu/v9n1/gallery/ve-bresland_j/ve-origin_page.shtml http://bresland.com/ http://claudiarankine.com/ (click on situations to watch the video essays) http://www.bfi.org.uk/explore-film-tv/sight-sound-magazine/video/art-nonfiction https://vimeo.com/89302848 (wes anderson centered) https://designlab.wisc.edu/audio-essay http://web.stanford.edu/~jonahw/PWR2W08/Syllabus.html http://web.stanford.edu/~jonahw/PWR2-F07/Radio-Home.html(with shedule and links) Kitchen sisters : http://www.kitchensisters.org/ Lindy West (trolling and cyber bullying): http://www.theguardian.com/society/2015/feb/02/what-happened-confronted-cruellest-trolllindy-west http://www.thisamericanlife.org/radio-archives/episode/545/if-you-dont-have-anything-nice-tosay-say-it-in-all-caps _____

Nov 25

*Blog Response on films: Reich & Moore, and book: Amazing Grace (due on your

blog by 5pm Fri Nov 27)

Write 2-3 paragraphs (or more) that compare the films in terms of content and form (how do the

films make similar points/arguments but with different means of presentation/strategies). Use

specific examples and explain concretely what and how you see both films doing. How does this

relate, or not relate, to Amazing Grace? Give specific examples and discussion and then reflect

on the relevance or something that you find interesting about any/all of these texts. Post this

response to your blog, and then upload the URL into Canvas Assignments (Blog Reflection:

Reich, Moore, and Amazing Grace).

_____

Nov 26 _____

Thursday, Thanksgiving

Nov 30

Film: Grace Lee Boggs

*Canvas writing + print and bring to class: write a reflection in which you discuss 2-3 examples from the film I Am and explain why/how you find these intriguing _____
December 2 Film: Grace Lee Boggs 7 E-Portfolio Workshop/Share/Present 9 E-Portfolio Workshop/Share/Present 14 E-Portfolio Workshop/Share/Present; Last Day of Class Final Revised Portfolios due by midnight

