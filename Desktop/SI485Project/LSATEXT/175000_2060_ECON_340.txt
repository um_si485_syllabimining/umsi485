ECON 340, Winter 2015, Deardorff, MW 8:30-9:50 AM, 1120...

http://www-personal.umich.edu/~alandear/courses/340/syllabus...

THE UNIVERSITY OF MICHIGAN Department of Economics
ECON 340 INTERNATIONAL ECONOMICS
SYLLABUS Winter 2015, Alan Deardorff

Text Grades Exams Lectures Disabilities Contacts Readings

PURPOSE: Broad overview of international economics and the international economy

Prerequisites: Econ 101 and 102 - Principles of Microeconomics and Macroeconomics Not Econ 401 or 402 - Intermediate micro and macro theory

Intended for: Students not concentrating in economics Economics concentrators not ready to specialize in international economics (Economics concentrators may take this course and later take Econ 441 and 442.)

Content:

Compared to most economics courses, this one has: -Less emphasis on theory. You'll learn about theory, but you won't learn to use the tools of economic theory, other than supply and demand. -More emphasis on the real world, viewed through the news and on the Internet. -Less emphasis on problem sets. Homework assignments (Study Questions) will be optional and ungraded. -Much more emphasis on reading.

TEXT
The textbook for the course is
Gerber, James, International Economics, 6th edition 2013. ISBN-13 #978-0-13-294891-3 Textbook Homepage (You are welcome to use an earlier edition instead, if you prefer. Pages are listed for the 6th and 5th editions; for earlier ones, you are on your own.
There are also a large number of shorter readings. Most of these are accessible on the web, and links to them are provided here for you to view them online or to download and print. The few readings that are not in Gerber and not on the web will be posted under Resources on CTools. (You may have heard that all of these readings, in prior terms, were available as coursepacks. That is no longer the case, because too few students bought them to justify their production.)
Finally, you will also be expected to keep up with the current international economic news. For that purpose you should read regularly some news source with good coverage of international

1 of 8

1/16/15, 11:51 AM

ECON 340, Winter 2015, Deardorff, MW 8:30-9:50 AM, 1120...

http://www-personal.umich.edu/~alandear/courses/340/syllabus...

economic news. You do not necessarily have to subscribe to anything, although that may make it easier to keep up. Good sources of international economic news are:
Wall Street Journal daily newspaper The Economist weekly magazine (that calls itself a newspaper) The Financial Times daily newspaper from London
If you wish to subscribe to any of these, you may do so online. There are many other good things to read, including the New York Times, but these do not have as much international economic news. I also post links to news items on my website.
We will devote part of one class each week to discussion of items in the news, and these items will also be included in a small way on exams. I will post particular news items on the course web site, which you should read for that purpose.

EXAMS AND GRADES
Grades will be based on two midterm exams and a ﬁnal exam. Exams will be in class, closed book, at the times listed below. The weights on each in determining your grade are also indicated:

Date

Time

Weight

First midterm exam: Wednesday, February 25 in class

30%

Second midterm exam: Wednesday, April 1 in class

30%

Final exam:

Thursday, April 30

8:00-10:00 AM 40%

NOTE: The ﬁnal exam is on Thursday, April 30, which is late (the last day) in the examination period. If this is a problem for you, DO NOT TAKE THE COURSE.

LECTURES
I will be using rather detailed PowerPoint slides in lectures. I will make these slides available online, usually a day or so before each lecture. I reserve the right to make changes up to the last minute, but you may ﬁnd the online ﬁles useful for studying, for helping you to follow the lectures, or for taking notes. Feel free to print out a version of them (two versions are posted) and bring it to class, if you wish. The slides that I actually use in each class will also be posted after the class.
Attendance at lectures is required, but only in the sense that I will say things in class that you'll need to know on exams, and not all of these will necessarily be in any of the readings or even in the PowerPoint slides. I would also like to think that if you pay attention to lectures, you will

2 of 8

1/16/15, 11:51 AM

ECON 340, Winter 2015, Deardorff, MW 8:30-9:50 AM, 1120...

http://www-personal.umich.edu/~alandear/courses/340/syllabus...

learn more, including things that I will not test you on. But if you don't care about that, and if you think you can earn whatever minimal grade you want without attending, I don't mind at all. This is a large enough class that you will not be missed. Indeed, other students in the class, and I as well, would prefer that you not attend than to have you here sleeping in your seat or leaving early because you're bored. And since I grade on a curve, other students can only beneﬁt from your absence.
STUDENTS WITH DISABILITIES

If you believe you need an accommodation for a disability, please let me know at your earliest convenience. Some aspects of this course may be modiﬁed to facilitate your participation and progress. As soon as you make me aware of your needs, we can work with the Ofﬁce of Services for Students with Disabilities to help us determine appropriate accommodations. I will treat any information you provide as private and conﬁdential.

WHERE TO FIND ME

Ofﬁce: Phone:

Room 3314 Weill Hall (this building) 764-6817

Ofﬁce Hours:

Mondays 10:15-11 AM & Thursdays, 9-10 AM

(subject to change; check my homepage)

E-mail:

alandear@umich.edu

Course Home Page: http://www-personal.umich.edu/~alandear/courses/340/340.html (Also accessible through UM CTools.)

GSI
This course is lecture-only, and it does not have any section meetings. However, there will be a Graduate Student Instructor available, one of whose jobs will be to help students with the material. He or she will hold regular ofﬁce hours, as follows:
GSI: Dan Jaqua e-mail: danjaqua@gmail.com ofﬁce: 125 Lorch hours: Tuesdays 10-11:30 AM
Wednesdays 1:30-3 PM

3 of 8

1/16/15, 11:51 AM

ECON 340, Winter 2015, Deardorff, MW 8:30-9:50 AM, 1120...

http://www-personal.umich.edu/~alandear/courses/340/syllabus...

SCHEDULE OF LECTURES AND READINGS
Title full citation to the reading Click on: "Online" for the reading itself (via UM if labelled P=Proquest, J=JSTOR, etc.)
"Qs questions on the reading

Date Jan 7 Jan 12
Jan 14
Jan 19 Jan 21
Jan 26

Topic and Assigned Reading
Course Overview
Introduction to the International Economy Gerber, Ch. 1. [15p] Economics Focus, "Why the tail wags the dog" [2p] Online (P) | Qs Hufbauer and Grieco, "...Payoff from Globalization" [2p] Online | Qs powell & Udayakumar, "Race, Poverty & Globalization" [6p] Online | Qs Bhagwati, "Does the Free Market Corrode Moral Character?" [2p] CTools | Qs Economist, "Trading Up," [3p] Online-Proquest | Qs Economist, "Signs of Life," [3p] Online-Proquest | Qs
Institutions of the International Economy Gerber, Ch. 2. [20 p] Donnan, "Global deal" [2p] CTools | Qs IMF, "IMF at a Glance" [3p] Online | Qs Politi, "Governors urge support of trade pacts" [1p] CTools Mauldin, "Tea-Party Resistance to Trade Pacts" [1p] Online (P) | CTools
No Class - Martin Luther King Jr. Day
Comparative Advantage and the Gains from Trade Gerber, Ch. 3. (but omit pp. 45-51) [16p] Deardorff, "...Comparative Advantage" [7p] Online | Qs Deardorff and Stern, "What You Should Know..." Sec 2 (pp. 405-413) [9p] Online (S) | Qs Brooks, "Don't Live Simply" [2p] Online | Qs Dizikes, "Evidence for ... Comparative Advantage" [2p] Online | Qs Other views: Prestowitz, "The Losses of Trade" [2p] Online | Qs Bivens, "Marketing Gains from Trade." [4p] Online | Qs
Modern Theories and Additional Effects of Trade Gerber, Ch. 4, pp. 63-65, 68-73, and Ch. 5, pp. 92-98 [5th: 93-98] [13p]

4 of 8

1/16/15, 11:51 AM

ECON 340, Winter 2015, Deardorff, MW 8:30-9:50 AM, 1120...

http://www-personal.umich.edu/~alandear/courses/340/syllabus...

Jan 28 Feb 2 Feb 4 Feb 9 Feb 11
Feb 16

Krugman, "Is Free Trade Passé?" [14p] Online (J) | Qs Bivens, "Globalization and American Wages" [11p] Online | Qs
Tariffs Gerber, Ch. 6, pp. 114-127 [5th: 119-132], and Ch. 7, pp. 136-143 [5th: 140-145] [17p] Feenstra, "How Costly...", pp. 159-171,175-178 only. [16p] Online (J) | Qs Schavey, "Catch-22 of U.S. Trade" [3p] Online | Qs
Nontariff Barriers
Gerber, Ch. 6, pp. 127-132 [5th: 132-137] [6p] Lindsey and Ikenson, "Coming Home to Roost" Ex. Sum. [2p] Online | Qs Hufbauer & Schott, "Buy American" [11 pp] Online | Qs
Reasons for Protection Gerber, Ch. 7, pp. 143-148 [5th: 146-150] [5p] Magee, "Why Are Trade Barriers So Low?" [6p] Online (W) | Qs Other views: Mastel, "Keep Anti-Dumping Laws ..." [3p] Online | Qs Kain, "Protectionism and National Security" [2p] Online | Qs
United States Trade Policies and Institutions Gerber, Ch. 7, pp. 149-152 [5th: 151-154] [10p] Verrill, "...Trade Remedies..." [3p] Online | Qs Paletta and Hughes, "Deal on Fast Track" [2p] Online (P) | Qs Glassman, "Illogical Special Trade Deals" [2p] Online | Qs Roth, "Wage Insurance" [4p] Online | Qs
World Trade Arrangements and the WTO Gerber, Ch. 2, pp. 20-23 (again) [4p] Deardorff, "...Overview of the WTO" [38p] Online | Qs Deardorff and Stern, "What You Should Know..." Sec 3 (pp. 413-421) [8p] Online (S) | Qs Bouët and Laborde, "Cost of a Failed Doha Round" [8p] Online | Qs Ikenson, "Weep Not for Doha" [8p] Online | Qs
International Migration of Labor Gerber, Ch. 4, pp. 83-85 [5th: 84-86] [3p] Deardorff, "Migration" [3p] Online | Qs Stelzer, "Immigration: Hard Facts" [4p] CTools | Qs Borjas, "...Progress of Immigrants" [2p] Online | Qs Skerry and Rockwell, "The Cost of a Tighter Border" [3p] Online | Qs Economics Focus, "Drain or Gain?" [2p] Online (P) | Qs

5 of 8

1/16/15, 11:51 AM

ECON 340, Winter 2015, Deardorff, MW 8:30-9:50 AM, 1120...

http://www-personal.umich.edu/~alandear/courses/340/syllabus...

Feb 18 Feb 23
Feb 25 Mar 9 Mar 11 Mar 16

Other Views (on whether to limit immigration): Eldredge, "Immigration will double population" [4p] Online | Qs Griswold, "Higher Immigration, Lower Crime" [4p] Online | Qs
International Movements of Capital Zupnick, "U.S. FDI Abroad" and "FDI in the US" [32p] CTools | Qs Lipsey, "Direct Investment..." [2p] Online | Qs Economist, "Company Headquarters," Proquest | Qs
The Balance of Trade and Other Measures of International Transactions Gerber, Ch. 9 [28p] Feldstein, "Address Imbalances" [3p] Online (P) | Qs Karabell, "Global Imbalances Myth" [3p] Online (P) | Qs Edwards, "Unsustainable Deﬁcit" [2p] Online | Qs Buffett, "...Selling the Nation..." [5p] Online | Qs Other Views: Scott, "Trade deﬁcit with Mexico... " [1p] Online | Qs Griswold, "Are Trade Deﬁcits Good..." [6p] Online | Qs
*** FIRST MIDTERM EXAM *** Covers material through Feb 16 (International Migration of Labor)
************************************************ Spring Break: Mar 2-6
************************************************
Exchange Rates Gerber, Ch. 10, pp. 209-226 [5th: 209-227] [20p] Economist, "Currency envy" [2p] Online (P) | Qs Economist, "Basket of sliders" [2p] Online (P) | Qs Economist, "Fixed Rates" [4p] Online (P) | Qs
Pegging the Exchange Rate Gerber, Ch. 10, pp. 227-233 [4th: 228-235] [8p] Levy, "We're Serious" [2p] Online | Qs Ferguson, "Our Currency, Your Problem" [6p] Online | Qs
International Macroeconomics Gerber, Ch. 11 [23p] Stiglitz, "America Has Little to Teach China" [2p] Online (P) | Qs Mann and Plück, "When the Dollar Bill Comes Due" [2p] Online | Qs Other views: Perry, "Thank the Chinese Currency Manipulators" [3p] Online | Qs

6 of 8

1/16/15, 11:51 AM

ECON 340, Winter 2015, Deardorff, MW 8:30-9:50 AM, 1120...

http://www-personal.umich.edu/~alandear/courses/340/syllabus...

Mar 18 Mar 23
Mar 25
Mar 30 Apr 1 Apr 6

Krugman, "Taking On China" [2p] Online | Qs
Fixed Versus Floating Exchange Rates Gerber, Ch. 10, pp. 233-235 [4p] Makin, "Exchange Rate Stability..." [6p] Online | Qs Frankel, "International Financial Architecture" [8p] Online+ Fig 3 | Qs Buttonwood, "Forty years on" [2p] Online (P) | Qs
European Monetary Uniﬁcation and the Euro Gerber, Ch. 10, pp. 235-239 [5th: 235-240], and Ch. 14, pp. 342-350 [5th: 348-353] [12p] Levin, A Guide to the Euro, pp. 41-51. [11p] CTools | Qs Solomon, "International Effects..." [7p] Online | Qs Barber, "EU Reluctant to Discipline" [1p] Online (P) | Qs Forelle & Walker, "...Dithering at the Top..." [4p] Online (P) | Qs Kulish, "No Fireworks for Euro..." [4p] Online (P) | Qs
Preferential Trading Arrangements and the NAFTA Gerber, Ch. 13, pp. 307-316 [5th: 314-321] [9p] USTR, "Trade Agreements" & "Free Trade Agreements" [2p] Online & Online | Qs Barﬁeld, "Trans-Paciﬁc Partnership" [7p] Online | Qs Evenett and Stern, "Transatlantic trade talks" [3p] Online | Qs Kanter and Jolly, "For Trade Deal with U.S., E.U." [3p] Online-Proquest | Qs Posen, "For Trade Deal with U.S., E.U." [2p] Online-Proquest | Qs Other views: Faux, "Overhauling NAFTA" [2p] Online | Qs
International Policies for Economic Development, Trade Gerber, Ch. 15, pp. 357-366 [5th: 362-371], and Ch. 16 [36p] Bailey, "Intangible Wealth" [2p] Online (P) | Qs Copenhagen Consensus, "2008 Results" [6p] Online | Qs Prusa, "...Antidumping" [2p] Online | Qs FAO, "Cotton Subsidies..." [2p] Online | Qs Baker, "WTO and the Poor" [2p] Online | Qs
*** SECOND MIDTERM EXAM *** Covers Feb 18 (International Movements of Capital) through Mar 23 (European Monetary Uniﬁcation and the Euro)
International Policies for Economic Development, Financial Gerber, Ch. 12 and Ch. 15, pp. 366-80 [5th: 371-87] [40p] Crook, "A cruel sea of capital" [4p] Online (P) | Qs Kharas, "Ten Years After the East Asian Crisis" [2p] Online | Qs

7 of 8

1/16/15, 11:51 AM

ECON 340, Winter 2015, Deardorff, MW 8:30-9:50 AM, 1120...

http://www-personal.umich.edu/~alandear/courses/340/syllabus...

Forbes, "The Costs of Capital Controls..." [2p] Online | Qs Krueger and Srinivasan, "Harsh Consequences of Forgiveness" [2p] Online (P) | Qs Lerrick, "World Bank Lending" [2p] Online | Qs

Apr 8

International Policies for Economic Development, Aid Economics Focus, "Gauging generosity" [4p] Online (P) | Qs Schaefer, "American Generosity..." [3p] Online | Qs Rice, "...Fight Global Poverty" [3p] Online | Qs Roberts, "...Millennium Challenge Account" [5p] Online | Qs Fraser Institute, "When Foreign Aid Doesn't" [4p] Online | Qs Levinsohn and McMillan, "Does Food Aid Harm..." [2p] Online | Qs Eberstadt & Adelman, "Foreign Aid: What Works and What Doesn't" [6p] Online | Qs Economist, "Aid to the rescue" [3p] Online (Proquest) | Qs

Apr 13

Outsourcing and Offshoring Blinder, "Offshoring Rattles Me" [2p] Online | Qs Brainard and Litan, " 'Offshoring' Service Jobs" [10p] Online | Qs Amiti and Wei, "Offshoring Raises Productivity" [1p] Online | Qs Mandel, "Real Cost of Offshoring" [3p] Online | Qs Friedman, "Made in the World" [3p] | Online (P) | Qs Economist, "Offshoring: Welcome home" [3p] | Online (P) | Qs Optional: Economist Special Report on Outsourcing and Offshoring 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8

Apr 15

Environment, Labor Standards and Trade Gerber, Ch. 8 [20p] Krugman, "Climate, Trade, Obama" [2p] Online | Qs Reuters, "China denounces carbon tariff" [1p] Online | Qs Jones, "Should Environmentalists Support Free Trade?" [5p] Online | Qs Stern, "Labor Standards and Trade Agreements" pp. 1-14, 20-21 [16p] Online | Qs Krugman, "In Praise of Cheap Labor" [3p] Online | Qs Narlikar, "Don't Blame the Brands" [2p] Online | Qs Broekhuizen, "U-M says licensees must follow safety accord" [2p] Online | Qs

Apr 20

No class, probably (I should know by the end of January.)

Thursday, Apr 30

*** FINAL EXAM *** (Cumulative, 8:00-10:00 AM)

E-mail to: alandear@umich.edu

Return to: Econ 340 Deardorff's Home Page

8 of 8

1/16/15, 11:51 AM

