WRITING 420.001
Capstone to the Minor in Writing
1185 NQ / MW 4:00-5:30
Raymond McDaniel / raymcd@umich.edu / 734.647.4531
Office 1317 North Quad / F 11:00-1:00 and by appointment
Overview
Welcome to Writing 420, the Capstone course for the Minor in Writing, and congratulations on the work that has brought you here. Here’s what you need to know.
This class is composed of two major components: the project and the portfolio. Each of these supplements and informs the other. The project is a substantial writing project of sustained complexity and depth. The portfolio is the culmination of your work throughout the Minor in Writing. While Sweetland faculty will provide written feedback on your portfolio, your Writing 420 instructor will evaluate your project and portfolio and issue your course grade.
In summary, the project is limited to this course and will become part of your portfolio (either an updated and improved version of the one you began in Gateway or a new one you develop for this course), and this course also grants you the opportunity to perfect that portfolio. Think of the portfolio as a crown, and your project as the jewel that is its centerpiece.
Since you will each develop your own projects, the class will not absolutely conform to a traditional schedule, grading system or syllabus. Some class sessions will aggregate opportunities to clarify the portfolio development process, but some will be devoted to giving you a chance to share with and learn from each other as you advance your individual projects. Thus, we will

not always be marching as a group in lockstep through identical sequences and tasks, though we will have several moments when we turn our collection attention to focal tasks. The goal here is to give you enough structure to support you and provide you with a chance to usefully share this experience with your peers, but not so much as to compromise your autonomy as a writer.
For more about how this will work, read on!
The Portfolio
Role in the Minor
The first of your two tasks is the completion of a writing portfolio, one of which you initially generated in Writing 220. You continually modified that portfolio during the duration of that class, and it has evolved in the interim, as you completed your other requirements for the Minor and continued to write in multiple contexts and placed work in your archive. This semester, we will help you design a new portfolio, since at the completion of the course you will present your portfolio to members of the Minor in Writing Evaluation Committee, which will respond directly to your completion of this component of the Minor.
The final Minor in Writing Portfolio must include a minimum of five (5) artifacts, but you can include as many as you think serves your portfolio. More is not necessarily better: remember, the point of the portfolio is to demonstrate development but also to make a coherent, unified effect.
Two artifacts are mandatory and pre-determined:  
1. A Writer’s Evolution Essay   2. Your Capstone Project  
The remaining three-plus (3+) mandatory artifacts must include the following:

3. Artifact from English Department or Sweetland New Media course (ENG 225, ENG 229, ENG 325, WRITING 200)
4. Artifact from ULWC course, either within or without your major 5. One other artifact of your choice  
This combination of artifacts must display the following:
• Evidence of significant engagement with writing via revision and redrafting  
• Evidence of multimodal composition to further specific rhetorical aims   • Evidence of multimedia facility   • Evidence of reflective writing   • Evidence of research employed to deepen and complicate academic
and/or creative work  
Please note that any given artifact can serve multiple purposes. For instance, your ULWC artifact can meet the requirement of displaying evidence of research and of multimedia facility. It's up to you! Just remember that your portfolio is meant to tell the story of your evolution as a writer, and you should choose artifacts that enable you tell that story in a way that is thorough and persuasive. Your portfolio isn't simply a collection: it is an artifact unto itself.
The criteria by which your portfolio will be evaluated includes the following categories:
Portfolio Composition: the portfolio is a piece of multimodal writing in its own right, in which each artifact (alphabetic essay or multimodal) contributes to the overall impression of the portfolio, including a sense of intellectual and visual coherence and intuitive navigation. The portfolio as whole should demonstrate the your achievements of the goals of the Minor, as well as your achievements as a writer. While individual artifacts may have specialized

audiences, the portfolio should speak to a broad audience: academic, professional, and general readers alike.
Reflective Writing: this refers to the portfolio’s Writer's Evolution Essay, which should include extended self-assessment supported by examples/evidence drawn from the various artifacts in the portfolio. It also refers to contextual introductions (which may or may not employ self-assessment) to individual artifacts, sections, or pages. The reflective writing also provides you a forum to explain stylistic or genre-based choices or to analyze or frame artifacts in markedly different stages of development.
Alphabetic Artifacts: alphabetic compositions are those that rely primarily on written text. They may include academic essays, as well as popular journalism, professional writing, creative writing, and even email! Your assessors take into account disciplinary and genre differences in format, structure, and word choices when evaluating alphabetic compositions. In addition, readers should keep in mind that you have included pieces from throughout your careers, so the pieces may demonstrate varying levels of development, sophistication, and polish.
Multimodal Artifacts: refers to artifacts that rely on media other than solely alphabetic text (for instance, a video, a podcast, a slide show, etc.). While these pieces may not be completely polished, you should analyze incongruity in the artifacts through honest and rigorous self-evaluation. Readers should take into account that for some multimodal artifacts, such as videos or podcasts, substantive revisions can be time-prohibitive, making rigorous analysis in the reflective writing an important part of the artifact’s argument.
Remember: the portfolio is the centerpiece of the Minor in Writing program, but it is not the exclusive focus of Writing 420.
A Writer’s Evolution Essay, Detailed

Minor in Writing Capstone Annotated Bibliography and Writer's Evolution Essay
Annotated Bibliography Assignment: Create an Annotated Bibliography of examples from all of your Minor in Writing courses prior to the capstone course, and any other writing – from any context and any time in your life – that you feel has been meaningful to your development as a writer. Keep in mind that writing may denote multimodal and/or multimedia work, as well as words on a page.
At a minimum, your bibliography must include 10 items. Eight of these items must be drawn from the following list:

• Your Directed Self-Placement (DSP) essay (if you were required to

complete one)  

• Writing from your First-Year Writing Requirement Course  

• Writing from each course, prior to the capstone, that fulfills the Minor

 requirem ent :  

• The gateway course (WRITING 220)  

• An Upper-Level Writing Requirement course in your concentration  

• A second Upper-Level Writing Requirement course in your concentration

or  another field  

• An English department course (ENG 225, ENG 229, or ENG 325) or a 3-

credit

 N ew M edia w riting co urse (W R ITIN G

• Writing that was produced outside of coursework (from a job, a personal

 b log post,creative w riting ,a letter to the editor,etc.)  

• A selection from your Minor in Writing blog posts (e.g., posts that

represent a

 particular idea or thread)  

Objective: The purpose of this bibliography is to return you to your writing history as you begin this capstone course. This return and re-view will provide material for your capstone essay on writing development and allow you to

account for and reflect upon the work you have done for your Minor in Writing courses.  

Format: Your bibliography should follow the citation format of your primary area of study (MLA, APA, CMS, etc.). Annotations will vary in length, but they should be substantial, and should address the topic and occasion of the writing, its central idea or argument, and a brief reflection of its significance to your writing development. These annotations should both reflect on the writing and analyze its quality – relative to the occasion and context for which it was produced, and also according to your opinion about that occasion and what it did or did not allow you to do.

Essay Assignment

Write an essay of 5-7 pages that demonstrates and reflects on your

development as a writer – and in particular, what you explored and learned

during your college writing career and the Minor in Writing Program – why

it’s significant, and how that affects your current thinking about your capstone

work for the Minor (i.e., your project and portfolio). Successful essays will be

both specific enough to be unique to your writing experience, and connected

to a larger issue that

 w o uld b e o f interes

conceptualize this prompt as a multimodal and/or multimedia project, though

it must perform the same work of explicit argumentation and evidence

analysis.

Objective:

The purpose of this essay is to provide an opportunity to define and reflect on what has been most important to you in your development as a writer and to investigate how these elements continue to affect you now, as you begin the work of your capstone course. Additionally, it is meant to help you synthesize the learning you have accomplished during the Minor in Writing program, as it connects to your other writing experiences.

Structure, Argument, and Evidence:

The structure of this essay is up to you. You may wish to mount an argument that builds from a thesis to a conclusion, offering evidence for each claim. You may wish to frame your argument in terms of a narrative that begins from a key moment and expands from it into the past and future. You may wish to offer a close reading of a passage from your writing as a way of getting at your broader claims about yourself as a writer. Or you may have a completely different idea in mind.
Regardless of the structure you choose, your essay needs to be focused on an explicit guiding idea and it needs to demonstrate this idea through detailed evidence and careful analysis.
Your evidence will be drawn primarily from your own writing, including multimodal and multimedia pieces, and particularly from the writing represented in your Annotated Bibliography. You will be expected to quote, paraphrase, summarize, and synthesize these sources as you would any source you might engage in an evidence- based argument.
At a minimum, as evidence for your argument, you must draw on three different writing assignments from at least two different courses that were part of your Minor in Writing requirements. It is likely that you will draw on several additional pieces as well.
The Capstone Project
Role in the Minor
The project, as the centerpiece artifact of your portfolio, presents a tremendous opportunity. It allows you to synthesize the array of skills you sought to develop when you initially pursued the Minor. It is something that will be wholly yours; proof of the application of your passion and your precision. Because these projects are self-generated, it is impossible to predict what form they will take, and you should use this class to wander and

speculate as you discover what subjects, questions and media will best serve your concerns. So as to facilitate and structure this process, we require a project scaffold, which will also set the template for how we will assess your project. Please note that while much of your work in Gatetway focused on you as a writer, the project must present a rhetorical situation designed for a specific audience, one that will expect substantive work in an appropriate and effective style. Your standards in terms of research and writing must be both clear and high. That achieved, this presents the utmost in writing autonomy: you will create the subject, the methods, the media and the project. Your instructors and advisers will assist you, but this writing belongs to you, so make the most of it!
Your project scaffold includes the following categories:
Proposal: When considering what project you will propose, you should keep the following questions in mind: Why does the topic you have chosen matter? Why are you the best choice for addressing this question, and what methods, techniques or media best serve your topic? We want your final project to meet the highest standards of intellectual rigor, but because you cannot automatically rely on any predetermined form to guide you, it's very important that you engage your question deeply. What compels you? What requires inquiry?
Your proposal should achieve the following aims:

• Clarify the central question or topic for a general reader  

• Justify the central question in terms of intellectual, social and personal

 relevance  

• Establish goals for an exploration of the central question  

• Identify a list of potential interlocutors (a scholar or professional with

whom

 yo u

• can periodically consult as you develop your project)

 Please rem em b er

your proposal is just that: a chance for you to make a case. Your instructor

will support you and help you structure and realize your goals, but you must first convince your instructor that your question is legitimate, provocative and capable of sustaining investigation. Persuade us! Get us excited to assist you on the journey your question predicts!   

Your proposal should be no less than 1500 words, supplemented with media that situate and characterize both your topic and your initial approach.

Research List: While each proposal should describe the general parameters of the research, all research requires a twofold reading list: one of work that you find inspirational or seek to imitate, and one that includes relevant alphabetic and multimodal readings. Once your instructor receives your proposal research lists, you will meet to negotiate and modify them so that you can either begin to work on your plan or, if necessary, re-submit your proposal.  

Production Plan: By the time your production plan is due, you will have

generated plenty of material correspondent to your topic. The production

plan describes the

 structure,m etho d o

You have already justified why you are asking this question (of all possible

questions about your topic); now it is time to decide how you are going to

frame and present that question and the results of asking it.

Your production plan should achieve the following aims:

• Describe the form, genre and media your project will finally take   • Set a schedule for the delivery of the discrete components of that project
  • Create a customized rubric for the evaluation of that project  

Note that your production plan helps you make the project you want to make; in that sense, while it offers constraint, it is also meant to create a structure with which you can build something truly amazing. You want to use the production plan to create work that will earn its place as the centerpiece of your program portfolio and that you can present with pride as an exemplar

of your intellect and compositional skill.

 eOivnesceyoyuorur instructo r rec

production plan, you will meet to decide whether it validates the terms of

your initial proposal and whether your schedule and rubric are adequately

ambitious and realizable.  

Remember: once you commit to the details of the production plan, you have committed to the terms by which your performance will be evaluated.  

MAJOR AND MINOR ASSIGNMENT POOL
PROCESS REFLECTION  
A process reflection is a document in which you describe the origin and execution of a writing task with attention to methodology (as opposed to results). 400 words.
ANNOTATED BIBLIOGRAPHY FOR PROJECT
An annotated bibliography is a collection of research items relevant to any of your project writing; each annotation describes, summarizes and analyzes the source and must be at least 200 words.
MiW BLOG TOPIC  
An initiated topic is a new post for discussion in which you present material to and/or question your peers.
MiW BLOG COMMENT  
A comment is a comment; a pirouette of flippancy, alas, doesn't count.
PEER REVIEW  
A peer review is an analysis of the effectiveness of a colleague's work with suggestions for subsequent development and must be at least 200 words.
THE CHANGING WRITER  
Pick a finished draft of a paper you wrote freshman/sophomore year and compare it to your most recent finished draft of a paper. Summarize each paper in three sentences or less. Then (resisting the impulse to edit and categorize your writing as “good” or “bad”) think in terms of reader experience. In a 2-3 page informal paper/blog entry, explain how these papers read differently, and what it is that makes them do so. Then think back to your role as a writer in producing these pieces. How has your writing process

changed? Think in terms of the research process, deadline choices, word choice, thesis, choices in argumentation, etc.
MAPPING AN ISSUE SPATIALLY  
Create an annotated Google map of an issue that relates to your project. What are the key sites around which this issue forms? How might they factor in to understanding the complexities of this issue? This could work equally well for creative essays. Spatial organization is often an effective way of organizing creative nonfiction. The rationale in either case is that this is a lowstakes way to map the complex (often competing) concerns of an issue.
MAPPING AN ISSUE ON A TIMELINE  
Create an annotated timeline of an issue. This follows a trend in print and online journalism. By creating an annotated timeline, you isolate key moments or turning points in an issue or a story. This would work equally well with creative essays. The emphasis is on mapping complexity rather than making reductive arguments.
EMPLOYING MODELS  
Find a model that helps explain your idea. For academic essays, find an article making a similar kind of argument in a related field or genre. The idea might be best explained with a range of examples. Explore the possible relationships between the examples and your own work.
THE QUALITIES OF DISCOURSE
How do two disparate groups talk about an issue? This could be two very different members of a family. A historian and a layperson. A council member and an educator. An anthropologist and a sociologist. Identify key terms, areas of divergence or overlap. This maps the issue in a way that the need for analysis grows naturally from recognizing different valences.

WRITER INTERVIEW

Identify a writer currently at work on a large writing project: a book, dissertation, or long scholarly article. This writer might be your professor, GSI, or even a senior thesis writer. Interview this writer about how they decided on the topic of their long project. You might consider the following questions:

How is the topic of the long project related to the writer’s earlier work? What is the writer’s personal interest or stake in the topic?

Did the writer ever consider another topic for this long project, and if so, what were the contenders?

When did the writer decide on this topic?

 D o es the w riter reg ret t

topic?

 H o w has the to p

the writer depend on external feedback in choosing the topic? What surprises

has the writer encountered in working on this topic?

MAKING ANOTHER WRITER’S DECISIONS

This exercise is designed to help you practice talking about you interests and seeking advice from others about your topics. Pair up with another writer in the class, and interview each other about their interests. Your objective is to learn about your partner’s interests, and propose between three and five topics for their final project. Each writer will leave the conversation with a short list of topics. You will not, of course, be obliged to choose one of the topics your partner suggests for you, but you may find it helpful to hear from someone else how you describe your interests. You might consider asking your partner the following questions to help them articulate their interests and help you decide what they should write about:

• What is your concentration? • What have been your favorite writing projects to date? What were your
least favorite projects? Why?

• Which new media have you worked in, and which are you curious about?

What do you think your future career might be?

 W hat kind o f w

people in that career do?

– Wfohrastcdhoooylo, uanlidkefotro read

fun?

After you have each developed a list of topics, reflect on the list your partner

has developed for you, and write a short response. What are your

first  im pressions? H a

interesting topics? Are there any topics on the list that you would consider

pursuing? Are there topics that seem completely irrelevant or uninteresting

for you? Why do you think that your partner made these choices; what did

you say that led them to propose a certain topic? Based on this experience,

how will you change your approach to soliciting advice?

INTERVIEWING AN EXPERT  

Construct and carry out an interview with an expert to gain insider insight on a question that interests you. As with many of the mini-prompts for this course, you can take this in any direction you choose. The interview could be knowledge-based: for example, Why aren’t more electric cars produced and sold? Or experience-based: How does the everyday experience of people practicing Zen Buddhism differ from those practicing other religions?

BRAINSTORMING USING A SCIENTIFIC INTRODUCTION

Scientific introductions commonly make the following moves:

  W hat is the

problem?

  W hat is the p rio r art? (i.e.,w ha

understand/solve this problem) What is missing?

 W hat are the o b

the paper that follows? These moves are also often embedded in social

science and humanities papers. The value of laying these moves bare is that

in order to complete them you must implicitly state that, 1) that there is an

issue worthy of the reader’s attention, and 2) that you see something that

needs to be added to the conversation. Take an issue you’re interested in

writing about and try to write about it in the framework of these four moves.

AFFECTIVE ANNOTATION  
Take a final or intermediate draft you have written for any class, yet a draft to which (for whatever reason) you feel emotionally attached. It may be that you found the writing of it particularly difficult, or that it deals with a subject close to you. Using the comment function in Microsoft Word, annotate the emotional shifts you feel as you read through the draft. In your comments, briefly describe the shift and make notes on possible reasons why you felt as you did. At the end, synthesize your annotations into a short reflective essay of 2-3 pages.
AUTHOR SELF-INTERVIEW  
Conduct the following self-interview on paper or into a digital recorder and discuss what you
uncover.
• Why, specifically, are you interested in writing about/investigating this issue?
• Are there additional academic or professional reasons for pursuing the topic?
• What resources do you have as a writer?   • Areas of expertise related to the topic? • Relevant experiences related to the topic?   • Familiarity with the literature?   • Familiarity with the methods? • Access to your subjects or materials?   • Clarity of purpose, or other aspects of the writing about which you are
confident? • Practical resources, such as space? • What constraints do you feel as a writer?   • Self-perceived holes in your knowledge of aspects of the topic? • Self-perceived holes in relevant theory or methods?

• Previous difficulties with an aspect of writing? • Previous difficulty with a similar topic  ? • The influence of previous published material on this topic? • The pressure of external reasons you have for pursuing this topic? • The lack of practical resources, such as space, time, etc.?
IDEA ARCHEOLOGY  
Take a draft you have written for another course and single out an idea you were attempting to understand or argue. Use this idea as a starting point for an investigation of your own mind at work. You may want to look at the Author Self- Interview for aspects of your process (resources and constraints) that were at work during the generation of this writing. What was important to you about this topic? Why personally? Why publically? What was your academic investment in this topic? To what extent was this shaped by logistical or personal concerns? What do you see as potential further avenues for inquiry?
ILLUSTRATED PASSAGES  
Decide with a small group to select a common passage you will all read, perhaps from a novel, story, or poem. Each individually select an image that sums up not the literal content but the emotion of the passage. Discuss the responses your images represent. Include the images and your reflection on the resulting discussion in a blog post.

You are required to complete one major item and one minor item each week, with first due date being Monday 28 September and due every Monday thereafter.
You can repeat as many assignments as you like. If all you want to do are Process Reflections, be my guest. And given the relative paucity of minor items, you'll be blogging and peer reviewing a lot!
If you want to propose a major item that isn't on the list, please do so - if I like it, I'll add it.
Major Process Reflection The Changing Writer Mapping an Issue Spatially Mapping an Issue on a Timeline Employing Models The Qualities of Discourse Writer Interview Making Another Writer’s Decisions Interviewing an Expert Brainstorming Using a Scientific Introduction Affective Annotation Author Self-Interview Idea Archaeology
Minor MiW Blog Topic WiW Blog Comment Illustrated Passages Peer Review

Capstone 420.001 Schedule
Wednesday 09 September Discussion of syllabus and wake-up call Monday 14 September Review of prior Capstone portfolios
Wednesday 16 September Discussion of Capstone genre types Monday 21 September Group collaboration
Wednesday 23 September Initial project suggestions (multiple) and workshop Monday 28 September Major/Minor items due, draft of Writer’s Evolution Essay due, reflection
Wednesday 30 September Discussion of proposal norms Monday 05 October Major/Minor items due, discussion of research norms
Wednesday 07 October Project Proposal due, reflection Monday 12 October Major/Minor items due, research visits
Wednesday 14 October New draft of Writer’s Evolution Essay due (with annotated bibliography), reflection Monday 19 October NO CLASS Major/Minor items due
Wednesday 21 October Production plan and associated materials due, reflection Monday 26 October Major/Minor items due, project execution discussion
Wednesday 28 October Capstone genres, revisited Monday 02 November Major/Minor items due, progress reports
Wednesday 04 November Project workshops

Monday 09 November Major/Minor items due, project workshops
Wednesday 11 November Site workshops Monday 16 November Major/Minor items due, site workshops
Wednesday 18 November Mixed workshops Monday 23 November Major/Minor items due, in-class research, writing and editing
Wednesday 25 November Will anyone even be here? Monday 30 November Major/Minor items due, live site workshops
Wednesday 02 December In-class research, writing and editing Monday 07 December In-class research, writing and editing
Wednesday 09 December Final review Monday 14 December Final portfolio due, collapse, tears

