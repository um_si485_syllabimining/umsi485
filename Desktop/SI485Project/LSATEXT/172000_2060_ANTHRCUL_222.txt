Anthropology 222 – The Comparative Study of Culture – Fall 2015
TTh 4-5:30 pm, 1300 Chem Discussion sections: (002) W 11-12, B117 MLB; (003) W 3-4, B830 East Quad

Professor: David Frye Office: 104C West Hall Phone: 764-2336 Office hours: TTh 2:45-3:45 or by appointment email: dfrye@umich.edu

GSI: Lauren Whitmer Mail box: 236 West Hall Office hours: W 12:30-3, in 238C West Hall email: whitmer@umich.edu

“Culture or civilization, taken in its wide ethnographic sense, is that complex whole which includes knowledge, belief, art, morals, law, custom and any other capabilities and habits acquired by man as a member of society”
E. B. Tylor, Primitive Culture (1871)

“Believing, with Max Weber, that man is an animal suspended in webs of significance he himself has spun, I take culture to be those webs, and the analysis of it to be therefore not an experimental science in search of law but an interpretive one in search of meaning.”
“I want to propose... that culture is best seen not as complexes of concrete behavior patterns— customs, usages, traditions, habit clusters—as has, by and large, been the case up to now, but as a set of control mechanisms—plans, recipes, rules, instructions (what computer engineers call ‘programs’)—for the governing of behavior.”
Clifford Geertz, The Interpretation of Cultures (1973, p. 5 & p. 44)

“Most American anthropologists believe or act as if ‘culture,’ notoriously resistant to definition and ambiguous of referent, is nevertheless the true object of anthropological inquiry. Yet it could also be argued that culture is important to anthropology because the anthropological distinction between self and other rests on it. Culture is the essential tool for making other.”
“I would argue that one powerful tool for unsettling the culture concept and subverting the process of ‘othering’ it entails is to write ‘ethnographies of the particular.’ Generalization, the characteristic mode of operation and style of writing of the social sciences, can no longer be regarded as neutral description.”
“And the particulars suggest that others live as we perceive ourselves living, not as robots programmed with ‘cultural’ rules, but as people going through life agonizing over decisions, making mistakes, trying to make themselves look good, enduring tragedies and personal losses, enjoying others, and finding moments of happiness.”
Lila Abu-Lughod, “Writing Against Culture” (1991, pp. 143, 149 & 158)

Purpose of the Course. This course has two main aims. First, to introduce the concept of culture as well as a small sampling of the great diversity of cultures that exist around the world. Second, to learn about some approaches to understanding this cultural diversity—and, along the way, to learn a bit of the history of social and anthropological theory. Why do people do things in different ways? Why do they sometimes understand and value the things they do so differently? What do these cultural differences mean? What are the ways in which we think about and understand difference? How can we think about and understand cultural change and the meaning of cultures in a world where people and societies constantly collide and commingle?

Classes will be organized around the discussion of readings and materials that span several cultures and diverse approaches to studying, thinking about, and describing culture, from ethnographic accounts (both classic and recent), to theoretical statements, to fictional writing and documentary film.

Grading
1. Lectures and discussion sections will only make sense if you keep up with the reading. Come to discussion section prepared to discuss what you have read and what you have heard in lecture. There may be occasional quizzes, and the GSI may require an occasional or a regular reading journal. Lecture attendance and participation in your discussion section will count for 25% of your final grade. (Note: 0% participation will give you a grade of 0%!)
2. You will write three short (5-page) essays. Topics for the papers will be announced during the course, but you can take it for granted that the three papers will each involve a discussion of one of the books. The essays will be due on October 15, November 19, and December 14. These essays will each count for 25% of the final grade (total: 75%).
Reading List
Marcel Mauss, The Gift: Forms and Functions of Exchange in Archaic Societies, translated by Ian Cunnison (written 1923, translated 1954, reprinted 2011). A newer translation (by W.D. Hall) is also available; either is fine, but I prefer the first. [ISBN: 9781614270188].
Catherine Allen, The Hold Life Has: Coca and Cultural Identity in an Andean Community (Smithsonian Press, 2nd ed., 2002). [9781588340320]
Anne Fadiman, The Spirit Catches You and You Fall Down: A Hmong Child, Her American Doctors, and the Collision of Two Cultures (Farrar, Straus and Giroux, 1998). [0374525641]
Keith Basso. Portraits of “The Whiteman” (Cambridge University Press, 1979). [9780521295932]
(You will find the coursepack articles on CTools)
Schedule of Readings
Week 1. September 8, 10. Confronting and understanding cultural diversity. Reading: CP 1, Michel de Montaigne, “Of Cannibals” (1578-1580; from Donald Frame, ed., The Complete Essays of Montaigne, pp. 150-159).
Week 2. September 15, 17. The concept of culture. Cultural relativity. “Culture and personality.” Benedict, Patterns of Culture, chs. 1-3 Horace Miner, “Body Ritual among the Nacirema” Edward Sapir, “Culture, Genuine and Spurious,” American Journal of Sociology v.29 n.4 (Jan. 1924), pp. 401-429 (reprinted in Sapir, Culture, Language and Personality).
Week 3. September 22, 24. Culture and economics Marcel Mauss, The Gift. (This is a short book; read it all, except Chapter 3, which can be skipped.)
Week 4. Sept 29, Oct. 1. Kinship. World systems. Allen, Hold Life Has, pp. 1-101 (= 1st ed. pp. 15-124).
Week 5. October 6, 8. Culture, identity, and politics. Culture as concept and as political tool. Allen, Hold Life Has, pp. 102-248 (= 1st ed. pp. 125-236, plus “Afterword” to 2nd ed.)
October 15: 1st Paper Due
Week 6. October 13, 15. Anthropology as cultural critique. Native anthropologists. Reading: Terence Turner, “Representing, Resisting, Rethinking: Historical Transformations of Kayapo Culture and Anthropological Consciousness,” in George Stocking, Jr., ed., Colonial Situations (1991), pp. 285-313.

Week 7. October 22. Primitivism, exoticism, and marginality. Symbolism, functionalism. “The effectiveness of symbols.”
Reading: Claude Lévi-Strauss, “The Sorcerer and His Magic” (1949), in Structural Anthropology (1963), pp. 167-185.
Week 8. October 27, 29. Peasantry and capitalism. Rationality. “The West.” Great and Little traditions. Concepts of time and history.
John Berger, stories from Pig Earth (1979).
Week 9. November 3, 5. Spirit and medicine. Gender, culture, power. Collision of cultures. Fadiman, Spirit Catches You, pp. 1-139.
Week 10. November 10, 12. Family in postmodern society. Translation of culture. War, empire, culture.
Fadiman, Spirit Catches You, pp. 140-288.
November 19: 2nd Paper Due
Weeks 11. Nov 17, 19. More about society and culture. Ways of thinking and writing about culture. Readings: Clifford Geertz, “Deep Play: Notes on the Balinese Cockfight,” in The Interpretation of Cultures (1973), pp. 412-453. Clifford Geertz, “Thick Description: Toward an Interpretive Theory of Culture,” in The Interpretation of Cultures (1973). (Note, this chapter is interesting and worth reading in its entirety, but it tends to drag for first-time readers. The most important pages to read are the first two sections, pp. 332-338 in the ctools pdf; p. 345; and the last section, pp. 353-355.)
Week 12. November 24. Ethnography and personal experience. Death and grief. Rituals, rites of passage.
Renato Rosaldo, “Subjectivity in Social Analysis” (from Culture and Truth, 1989, pp. 168-195). Renato Rosaldo, “Grief and a Headhunter’s Rage” (from Culture and Truth, 1989, pp. 1-21).
Week 13. December 1, 3. Culture, humor, power, language. Basso, Portraits of “The Whiteman,” pp. ix-33.
Week 14. December 8, 10. Culture, humor, power, language. Basso, Portraits of “The Whiteman,” pp. 35-94.
December 14: 3rd Paper Due

Anthrcul 222 (Comparative Study of Culture), Fall 2015: schedule at a glance

Sept. 7 6 13 14
20 21

8 Intro: What is Culture?
15 CP: Benedict, Miner, Sapir
22 M auss, Gift

9 16 23

27 28

29 Allen 1-101

30

45

67 Allen 102-248

11 12 FIRST PAPER DUE Oct 15
18 19

13 CP: Turner
20 Study Break

14 21

25 26

27 CP: Berger

28

Nov. 1 8
15
22

2
9
16 SECOND PAPER DUE Nov 19 23

3 Fadim an 1139 10 Fadim an 140288
17 CP: Geertz
24 CP: Rosaldo

4 11 18 25

10 11 CP: Montaigne

17 The idea of culture
24 Culture and economics
Oct. 1 Kinship; local and global
8 Culture, identity, politics
15 Cultural critique
22 CP: LeviStrauss
29 Peasants; functionalism
5 Spirit, medical science
12 Gender, family

18 25 2 9 16 23 30 6 13

19 Thick description
26 Thanksgiving

20 27

29 30 67

Dec. 1 Basso ix-33
8 Basso 35-94

2 9

3 Culture, humor, power
10 Final class

4 11

13 14 15 16 17 18 FINAL PAPER DUE Dec 14

12 19 26 3 10 17 24 31 7 14 21 28 5 12 19

