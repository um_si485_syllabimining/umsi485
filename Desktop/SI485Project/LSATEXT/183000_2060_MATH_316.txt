Math 316.001

Differential Equations

Fall 2015

Instructor: Office Hours:
Office: Phone:
E-mail:

Matthew P. Masarik MWF 12:30 – 1:00, 2:00 – 2:30
1868 East Hall 517-927-0779
masarikm@umich.edu

Meeting times: MWF 1:00 – 2:00 in 4088 East Hall

Text: Elementary Differential Equations and Boundary Value Problems by W.E. Boyce and C. Diprima, 10th ed.

Grading: Homework: 20%, Midterm: 40%, Final exam: 40% (all exams are closed book with no notes)

Course Content: We will cover most of chapters 1 – 9, possibly omitting some sections along the way.

Expectations: Come to class, read the text, do the assigned homework assignments, and do as many problems from the book as you can.

Homework: We will have approximately 8 - 10 homework assignments through the
semester. Use sentences in your solutions and show your work (especially since solutions to book problems are in the back).

Note for special accommodations: If you think you need an accommodation for a
disability, please let me know as soon as possible. In particular, a Verified Individualized Services and Accommodations (VISA) form must be provided to me
at least two weeks prior to the need for a test/quiz accommodation. The Services for Students with Disabilities (SSD) Office (G664 Haven Hall; http://ssd.umich.edu/)
issues VISA forms.

