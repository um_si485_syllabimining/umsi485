The Literary, the Supernatural, and the Strange: Exploring Genre in the Contemporary American Novel
English 362 Section 01 Fall 2015

The American Novel

Office: 3024 Tisch Hall

Jeremiah Chamberlin

Office Hours: ~ Mon/Tue: 4 – 5:30pm

jchamber@umich.edu

~ Wed: 1-2:30pm

I check email Mon – Fri, typically around 10am and 5pm

~ Or by Appointment

“There is no such thing as genre fiction; there are genres of fiction.” --Michael Sheehan, from the 2015 AWP Writers Conference panel “What’s Wrong with Genre?”
“Say what you will about it, Hell is story-friendly. If you want a compelling story, put your protagonist among the damned. The mechanisms of Hell are nicely attuned to the mechanisms of narrative. Not so the pleasures of Paradise. Paradise is not a story. It’s about what happens when the stories are over.
--Charles Baxter, from Burning Down the House

Required Materials:
• The Corrections, by Jonathan Franzen • Swamplandia! by Karen Russell • The Fever, by Megan Abbott • Station Eleven, by Emily St. John Mandel • Super Sad True Love Story, by Gary Shteyngart • Zone One, by Colson Whitehead • Red Moon, by Ben Percy • Annihilation, by Jeff Vandermeer

Course Description:
There was a time when “genre” books were looked down upon as little more than pulp, mere page-turners, the sort of thing serious readers outgrew. Sure, magical realism was a part of respected South American literary tradition, and the Eastern Europeans often embraced the surreal elements of life as it was refracted through the lens of living under communism. But through most of the second half of the twentieth century, particularly in the 80s and 90s, the notion of the “Great American Novel” rested firmly on the bedrock of realism in the United States. If we were to understand ourselves as a culture, it would be through the cold light of reality. Books like Richard Ford’s Independence Day and Jane Smiley’s A Thousand Acres and more than one of John Updike’s Rabbit novels were the sorts of books garnering Pulitzer prizes for their insights into the American culture and psyche. But with the new millennium, we’ve begun to see a shift. “Literary” writers who would have once scoffed at lowering themselves to dabble in “genre,” or who would have previously done so under a pen name, were now happily moving into terrain once occupied by sci-fi, fantasy, suspense, and horror authors, exploring both the limits and assets of these various forms.

1

However, any blending of conventions will naturally result in a hybrid that has both strengths and weakness. What is gained? What is sacrificed? And in what context—what lineage, what tradition—should these texts be read, let alone weighed, evaluated, and judged? By literary or genre standards? And, fundamentally, do those standards matter? This class, then, will endeavor to answer these questions, as well as what this recent shift in literary preoccupations might tell us about the state of the Contemporary American Novel, as well as America as a culture and country, itself. We’ll begin with Jonathan Franzen’s The Corrections, a book proclaimed by critics (and the author!) as a quintessential “Great American Novel.” Yet even in Franzen’s work a bit of the strange slips in. From here we’ll begin our exploration of American novels that might be categorized as “genre.”
Critical Reading:
One of the key skills that you will learn in this class, and which will be integral to your success as a critical thinker, is learning to read like a writer. Reading like a writer is a considerably different process than content-based reading. Here, we study the work from the inside out, in order to examine how it operates and functions. By learning how something works, we can more fully understand why it works.
We will also learn to read as critics, examining texts in conjunction with both literary and critical essays. Doing so will allow us the opportunity to think about genre as a construct, as well as how we understand the construction and meaning of these texts.
I expect you to come to class prepared to talk about our texts and to have identified several passages that helped you understand the work (we’ll call these “keys”) and several passages that you had trouble with or kept you out of the work (we’ll call these “locks”).1
I also expect you to annotate your reading, whether in the texts themselves or via a reading journal. Not only will this force you to engage more critically with the work, but these notes/annotations will be invaluable for the final exam, which is open book.
Finally, it’s important to always meet the writer half way. Whether you “love” or “hate” a particular book or story or poem shouldn’t matter—we don’t read to make friends. Or to have our beliefs reinforced. Or to learn lessons. Rather, we read closely and critically to make meaning. So please come each day with several passages or moments (even a particular detail or an evocative line) that you found meaningful in some way.
Participation:
Though this course is large (50 students), I will teach it as a seminar. And a seminar-style course depends on you. Without each person contributing to the discussion, our mutual understanding of the texts will be shallow and superficial. So you will not only be expected to come to class having completed the reading for the day, but also to have brought specific questions, comments, and reflections to share.
That said, I’m of a similar mind as David Foster Wallace here, who wrote the following in his syllabi with regards to participation in his English classes:
It seems a little silly to require participation. Some students who are cripplingly shy, or who can’t always formulate their best thoughts and questions in the rapid backand-forth of a group discussion, are nevertheless good and serious students. On the other hand…our class can’t really function if there isn’t student participation—it will
1 This concept of “locks” and “keys” is borrowed from Aric Knuth, a wonderful instructor in the Department of English and the Director of NELP (New England Literature Program). I would like to thank him for this concept and acknowledge my borrowing here.
2

become just me giving a half-assed ad-lib lecture for 90 minutes, which (trust me) will be horrible in all kinds of ways. There is, therefore, a small percentage of the final grade that will concern the quantity and quality of your participation in class discussions. But the truth is that I’m way more concerned about creating an in-class environment in which all students feel totally free to say what they think, ask questions, object, criticize, request clarification, return to a previous subject, respond to someone else’s response, etc. Clinically shy students, or those whose best, most pressing questions and comments occur to them in private, should do their discussing with me solo, outside class.
Likewise, participation is composed of a number of different elements. Participation includes but isn’t limited to the following: (1) attendance, preparedness, and professionalism; (2) thoughtful contribution to discussions, including asking good questions; (3) completion of work in a thorough and timely manner; and (4) in-class writing and short responses.
Attendance:
You can’t participate if you aren’t in class. Because most of the work and learning for this course takes place in the classroom, attendance is crucial. Here are the details:
• You may miss two classes without penalty. Thereafter, each additional unexcused absence may lower your final grade up to three points (e.g., from a 92% to 89%).
• I will excuse absences due to family emergencies, medical emergencies, or required attendance at university-sponsored events. However, you must bring a note from a doctor or health professional, a signed letter from a University team or program, or documentation of a family emergency.
• More than five unexcused classes will likely result in failure of the course, regardless of the work completed.
• Tardiness is a form of absenteeism. Three “lates” constitute one absence.
For each class session that you are absent, regardless of the reason, you will be asked to submit a one-page, single-spaced response to the day’s reading. This is due within one week. Note: I will not track you down. It is your responsibility to follow up on this work.
The Work
I believe in professionalism. Here are the details:
• You must turn in your essays at the beginning of class; do no show up to class late because you’ve been searching for the perfect end to your essay.
• Late papers will be penalized two points per twenty-four-hour period they are late.
• Papers more than a week late will likely receive a failing grade.
• Don’t be a victim of technology. Save your material, keep an extra ink cartridge handy, and make back-up copies. And because computer labs can be crowded, don’t wait until the last minute to print. These aren’t excuses for late work. Likewise, hard drives die and jump drives tend to get lost right before papers are due. As such, all work for this class must be saved on mfile: http://www.mfile.umich.edu
• Laptops are not allowed in class.
3

The Grading: How Will You Be Evaluated?
Grades are measures, not rewards. And the expectations in this class and at this University are high. As such, so are the grading standards. A “C” is average and means that you have satisfied the minimum requirements of an assignment. A “B” means that you have exceeded them and should be proud. An “A” means that you have exceeded them wildly. In my view, getting an “A” on a paper should be like earning a spot on the starting line up of the Wolverines or landing the starring role in a play—it takes both talent and effort. Plain and simple: you must do consistently exceptional work to earn an “A” in this class.
Although I will hold you to high expectations, in return I will offer you as much assistance as my schedule will allow—a generous amount. I see this class as a collaborative project. If we both do our jobs well, you will produce strong written work and learn the crafts and skills necessary to becoming a better reader, writer, and thinker. Perhaps just as importantly, you will learn a great deal about different and more complex ways of seeing literature (as well as yourself) in this process.

Grading Scale:
94.0 - 100 90.0 – 93.9 87.0 – 89.9 84.0 – 86.9 80.0 – 83.9 77.0 – 79.9

A AB+ B BC+

74.0 – 76.9 C 70.0 – 73.9 C67.0 – 69.9 D+ 64.0 – 66.9 D 60.0 – 63.9 D59.9 & Below E

Grading Breakdown:

30% 25% 25% 10% 10%

First Passes (10% each; best 3 out of 4 passes) Comparative Analysis Essay (due Friday, December 11 in Discussion) Final Exam (Friday, December 18 from 4-6pm) Participation in Lecture Participation in Discussion Section

Office Hours:

I look forward to meeting with you individually during office hours. We can talk about your assignments, issues that we’ve been discussing in class, any difficulties that you’re having, and/or your future plans. If your schedule conflicts with my weekly office hours, please let me know so that we can arrange to meet at another time.

Sweetland Center for Writing:
Few universities have a resource like Sweetland that is dedicated exclusively to helping students improve their writing. I recommend you take full advantage of their services, for this class as well as all your others. Sweetland is located at 1310 North Quad.
Each University of Michigan Undergrad is eligible for one appointment per week. Walk-ins are sometimes available, but appointments are recommended. You can schedule an appointment up to a week in advance by calling the front desk at 764-0429. Also visit the website for more information: www.lsa.umich.edu/swc

4

Religious Observances:
If a class session or due date conflicts with your religious holidays, please notify me so that we can make alternative arrangements. In most cases, I will ask you to turn in your assignment ahead of your scheduled absence. In accordance with U-M policy on Religious/Academic conflicts, your absence will not affect your grade in the course.
Plagiarism:
Building on others’ words and ideas is an essential element of effective scholarship. However, we must give credit to those whose words and ideas we incorporate into our writing. Using someone else’s words, ideas, or work without proper attribution is plagiarism, and such an act is considered a serious ethical violation within the university community.
If you complete an assignment for one course and then submit that same assignment as original work for a different course, you are also committing plagiarism. We will discuss what constitutes plagiarism, but if you have additional questions about how to reference material that you find in books or online, please let me know.
If you commit an act of academic dishonesty in this course either by plagiarizing someone’s work or by allowing your own work to be misused by another person, you will face these consequences:
• You will fail the assignment and may fail the course. • I will report the incident to the Director of the English Department Writing
Program. • I will also forward your case, with an explanatory letter and all pertinent
materials, to the LSA Assistant Dean of Student Affairs. • The Dean will determine an appropriate penalty, which may involve
academic probation and/or community service. • If you commit plagiarism while you are already on probation for plagiarism,
you may be asked to leave the University.
Accommodations for Special Needs:
The University of Michigan is committed to ensuring the full participation of all students, and I am committed to making learning as accessible as possible for all of my students. If you have a disability and need an accommodation to participate in this class or to complete course requirements, please ask Services for Students with Disabilities (SSD) to provide documentation of the accommodations that you need. Then, please share this documentation with me as soon as possible, preferably within the first few weeks of class. I will treat as private and confidential any information that you share.
• If you suspect that you may have a disability and would like to be tested, Services for Students with Disabilities can provide free screenings and referrals to low-cost diagnostic services.
• Here is the contact information for Services for Students with Disabilities:  location: G-664 Haven Hall  phone: (734) 763-3000  website: http://ssd.umich.edu/
5

Other Campus Resources:
• Resources for ESL students: http://sitemaker.umich.edu/eli.resources/home • Counseling and Psychological Services (CAPS): http://www.umich.edu/~caps/
CAPS is a FREE and confidential service for students who are looking for emotional and psychological support, for anything from the daily rigors of college life to lifechanging events like parental divorce, break-ups, grief over loss, etc. • Research Assistance: http://www.lib.umich.edu/guides/
6

English 362 Reading Schedule Fall 2015
NOTES: 1) This calendar is subject to change. 2) This calendar contains only primary readings

Tuesday, September 8 Intro to the Class

Week One:

Thursday, September 10 The Corrections (St. Jude and The Failure, pgs. 3 – 134)

Week Two: Sunday, September 13 Kerrytown Book Festival Event 415 North Fifth Avenue http://www.kerrytownbookfest.org/
2:45pm in the Kerrytown Concert House
Suspenseful Reads: moderated by Andrew Grant, with Owen Laukkanen, Jenny Milchman, P.J. Parrish, and Vu Tran

Tuesday, September 15 The Corrections (The More He Thought About it, the Angrier He Got, pgs. 137 – 235)
Thursday, September 17 The Corrections (At Sea, pgs. 239 – 336)

Week Three: Tuesday, September 22 The Corrections (The Generator, pgs. 339 – 455)
Thursday, September 24 The Corrections (One Last Christmas and The Corrections, pgs. 459 – 566)

Week Four: Tuesday, September 29 Swamplandia! (Chapters One through Seven, pgs. 3 – 119) First Pass 1 Due
Thursday, October 1 Swamplandia! (Chapters Eight through Twelve, pgs. 120 – 212)

1

Week Five: Tuesday, October 6 Swamplandia! (Chapter Thirteen through Seventeen, pgs. 213 – 310)
Thursday, October 8 Swamplandia! (Chapters Eighteen through Twenty-Three, pgs. 311 – 397)

Week Six: Tuesday, October 13 The Fever (Before through Chapter 8, pgs. 3 – 91) First Pass 2 Due
Thursday, October 15 The Fever (Chapters 9 through 12, pgs. 92 – 185)

Tuesday, October 20 Fall Break: NO CLASS

Week Seven:

Thursday, October 22 The Fever (Chapters 13 through 19, pgs. 186 – 303)

Week Eight: Tuesday, October 27 Station Eleven (Books 1 -3, pgs. 3 – 115) First Pass 3 Due
Thursday, October 29 Station Eleven (Books 4 – 6, pgs. 119 – 228)

Week Nine: Tuesday, November 3 Station Eleven (Books 7 – 9, pgs. 231 – 333)
Thursday, October 5 Super Sad True Love Story (pgs. 1 – 98) First Pass 4 Due
Week Ten: Tuesday, November 10 Super Sad True Love Story (pgs. 99 – 248)
Thursday, November 12 Super Sad True Love Story (pgs. 249 – 331)

2

Week Eleven: Tuesday, November 17 Zone One (Friday, pgs. 3 – 128)
Thursday, November 19 Zone One (Saturday, pgs. 131 – 271)
Week Twelve: Tuesday, November 24 Zone One (Sunday, pgs. 275 – 322)
Thursday, November 26 No Class: Thanksgiving Break
Week Thirteen: Tuesday, December 1 Red Moon Pages TBD
Thursday, December 3 Red Moon Pages TBD
Week Fourteen: Tuesday, December 8: Annihilation (Chapters 1 – 3, pgs. 3 – 119)
Thursday, December 10 Annihilation (Chapters 4 – 5, pgs. 121 – 195)
Friday, December 11 Essay 2 Due in Discussion Section (AND uploaded to DropBox)
Week Fifteen:
Tuesday, December 15 Optional Final Exam Review Session In our regular classroom at regular time
Friday, December 18 FINAL EXAM: 4 – 6pm In our regular classroom

3

