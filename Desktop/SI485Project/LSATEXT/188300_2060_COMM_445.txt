Communication Studies 445/Seminar in Media & Culture—Music & Identity Fall 2015/ /TuTh 1-2:30 PM/G311 DENT version 2.0

Professor Derek Vaillant 5443 North Quad

dvail@umich.edu Office Hrs: TH 3-4 or by app’t

Some years back, archaeologists unearthed remnants of a 35,000-year-old flute in Southwestern Germany; It's clear that music and being/becoming human are closely linked. Across academic disciplines, musicologists, historians, psychologists, sociologists, communication researchers, and many others puzzle over the relationship between music, communication, and human experience. Music and identity formation are linked, but how? And toward what, if any end or ends? How can interdisciplinary study of music help us better comprehend the modern world, social life, consumerism, technology, and mediated and interpersonal communication? This course draws on history, musicology, literature, and cultural/media studies to examine themes of music and identity in the U.S. from the late nineteenth century to the present. Note: This is not an exhaustive survey of all that could be termed “American” music; It contains biases and gaps, and focuses chiefly on popular music. Nevertheless, by using selected case studies, we will begin to develop critical perspectives with which to analyze music as communication in the everyday life of the past and present.

Overview of Seminar Rules: This is a discussion-driven seminar. Students must read assigned material in advance of each class meeting and arrive with discussion questions. Attendance is required. Tardiness or chronic absenteeism is unacceptable. If you cannot attend a class for any reason, please notify the instructor in advance. More than two absences will likely diminish your participation score. See the “Accommodations” document in <Resources> for further details about illness policy, procedure, and so forth.

Requirements: In addition to active participation requirement, each student will be assigned to introduce a reading and help to lead discussion. You will also write a critical essay on a prompt concerning concepts in the reading (5-6 pp.), take a midterm, participate in the experiential learning exercise 'Dress Up Day,' (win prizes!), write an original research paper (12-14 pp.), and deliver a short oral presentation on your project in the student conference at the end of the semester. I will provide further details/point breakdowns on each of these requirements for your convenience and planning in the days ahead.

Grading: Final mark is based on informed participation/preparation in discussion, including leading one class discussion (15%), a critical essay (25%), a midterm (20%), and a research paper/conference presentation (40%). All work is due as assigned. Any late assignment will be docked one third of a letter grade for each day/24-hr period it is late (e.g., a paper that would have received a B+ will be lowered to a B if it is late, B- if it is two days overdue). Any evidence of cheating, submitting work from another course or that is not yours alone, or of plagiarism will result in a failing grade on the assignment and failure in the course in the case of plagiarism. Action by the Dean’s office is likely. Please—please—please read and understand the CTools documents about what plagiarism is, and how to avoid it. This syllabus is subject to change at the instructor’s discretion.

Required Text: Dick Hebdige, Subculture, the Meaning of Style (pbk.)

Electronic Readings: (.pdf readings marked with a # are available in the <Resources> folder of CTools. CTools: https://ctools.umich.edu/portal. Here you’ll find the syllabus, class assignments, most readings, bibliographies for your long papers, and links to research and relevant sites. If you have trouble locating any assignment, or notice a problem with an electronic file, please let me know. Thanks!

1a 8 Sep

Introduction

Handout: Carina Chocano, “Revolution Blues,” The New York Times Magazine, 16 August 2015.

2

1b 10 Sep

What is Music?

#Nicholas Cook, “Musical Values,” in Music, a Very Short Introduction, pp. 1-18.

#David Byrne, "Creation in Reverse," in How Music Works, 13-30.

2a 15 Sep

Is Music a Verb?

#Small, C. “Prelude: Music and Musicking,” and “Chapter 1: A Place for Hearing” in Small, Musicking.

2b 17 Sep

The Music & Society Nexus: A “Frankfurt School” Perspective

#Adorno, T.W., “On the Fetish-Character of Music and the Regression of Listening” (1938)

3a 22 Sep

The Music & Society Nexus: A Feminist Perspective

#McClary, S. “Same as it Ever Was: Youth Culture and Music. In Microphone Fiends: Youth Music and Youth Culture, edited by A. Ross and T. Rose. New York: Routledge, 29-40.

Distribute critical essay assignment this week

3b 24 Sep The Music & Society Nexus: A Sociological Perspective

#DeNora, T. “Music as a Technology of Self” in Music in Everyday Life, 46-74.

4a 29 Sep Before Lollapalooza: Social Tension & Nineteenth-Century Musical Festivals

#Vaillant, D. “Battle for the Baton” from Sounds of Reform, 58-90, 307-14.

4b 1 Oct Inventing American Roots: The problem of the folk

#Benjamin Filene, ‘Our Singing Country’: John and Alan Lomax, Leadbelly, and the Construction of the American Past,” American Quarterly 43, 4 (1991): 602-24.

5a 6 Oct Josephine Baker: Race & the Transnational Black Body in the Modern Era

#Jules-Rosette, Josephine Baker in Art and Life, pp. 47-71 and 127-54.

5b 8 Oct Josephine Baker, cont.

#Jules-Rosette, Josephine Baker in Art and Life, pp. 47-71 and 127-54.

Critical essay due by 5 PM. Upload electronic Word copy to CTools <Drop Box> by 5 PM

6a 13 Oct Blues Aesthetics & African American fiction

#James Baldwin, “Sonny’s Blues” and #Maya Angelou, “The Reunion” in Breton, M. ed., Hot and Cool: Jazz Short Stories.

3
6b 15 Oct Midterm 7a 20 Oct Fall Break! 7b 22 Oct The Art of Protest: Jazz & Identity Politics #Gary Giddens, “Louis Armstrong (the Once and Future King),” 83-102. #“America’s Secret Sonic Weapon,” “Louis Armstrong on Music & Politics” in Walser, R., ed. Keeping
Time, 240-41; 246-49 8a 27 Oct Put Some South in Your Mouth: Elvis & Southern Change #Marcus, G., “Elvis: Presliad” in Mystery Train, 120-75 Recommended: #Bangs, L. “Where Were You When Elvis Died?” (1977), 212-16. 8b 29 Oct Why Elvis Has Not Left the Building #Daniel, P., “A Little of the Rebel,” in Lost Revolutions, 148-175, 337-41 9a 3 Nov It Ain’t Me Babe: Bob Dylan #Marqusee, Excerpt from Wicked Messenger, "The Whole World is Watching,'" 7-91 9b 5 Nov Bob Dylan, continued #Marqusee, Excerpt from Wicked Messenger, " 139-214. 10a 10 Nov Women & Folk: Mercedes Sosa & Joan Baez #Jane A. Bernstein, "'Thanks for My Weapon in Battle--My Voice and the Desire to Use It," Women and Protest
Music, 166-186. 250-word e-mail prospectus of final project due Friday 11/13 by 5 PM 10b 12 Nov Much Ado About Nothing: Punk as a Subculture Hebdige, D., Subculture, the Meaning of Style, 1-70 Dress Up Day! Come to class as a mod, a rocker, a punk, or your favorite subcultural musical personality/foole from the course, who embodies a subculture of musical style that you can explain after Hebdige. Prizes! 11a 17 Nov The Art of Noise: Music, Style Wars, and Identity Hebdige, D. Subculture, the Meaning of Style, 73-127.

4

11b 19 Nov (Gender) Bending the Notes: Riot Grrrls and Women in Popular Music

#Mary Celeste Kearney, “The Missing Links, Riot Grrrl-Feminism-Lesbian Culture,” in Whiteley, ed., Sexing the Groove, 207-229

Electronic Version of Rough drafts of final projects due in CTools <Drop Box> by 5 PM Today

12a 24 Nov

On the Race Beat: Exploring Rap & Hip-Hop Politics

#Rose, T., excerpt from The Hip-Hop Wars

12b 26 Nov

Happy Thanksgiving!

13a 1 Dec

Out, Out, d*mned Spottify! The Future of Digital Streaming

#John Seabrook, "Revenue Streams," The New Yorker, November 24, 2014, 68-76.

#news clipping TBA.

13b 3 Dec

Student Conference/Presentations

14a 8 Dec

Student Conference/Presentations

14b 10 Dec

Student Conference/Presentations

Final drafts research papers due Fri 12/11 by noon in <Drop Box>

