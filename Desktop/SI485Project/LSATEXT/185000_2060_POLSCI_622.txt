American Political Institutions PS 622, Fall 2015
Professor Chuck Shipan

Office: 7764 Haven Phone: 615-9140

Office Hours: T 1:30-3, W 1:30-2:30

Email:

cshipan@umich.edu

Course Description
A course on American political institutions would ideally cover Congress, the president, the courts, and the bureaucracy at the national level, as well as the corresponding institutions at the state level. Given that it would be impossible to cover all of these institutions in detail in one semester, the approach in this course will be to focus selectively on a handful of topics. Here are some of the features of the approach we’ll take:

• Our emphasis will be on American political institutions at the national level, although occasionally we will turn elsewhere (e.g., the states) for additional insights.

• Some of the studies we will read are about politics within specific institutions. But many of them examine the interactions across institutions, which reflects both my own interests and
where I think the study of institutions has been heading in recent years.

• The readings will be weighted toward empirical studies of theoretical models. Theoretical studies are obviously important, but there is a tendency – an unfortunate one, in my view – to
automatically venerate theoretical work while downplaying the value of good empirical or descriptive work. Consequently, we generally will focus on empirical studies that use actual data about actual institutions. At the same time, these empirical studies are almost always informed by theory, either a theory developed in the paper or one developed elsewhere.

• Most of the studies we will read are relatively current (i.e., from the past two decades). If you become especially interested in a topic and want to read earlier foundational work, I’d be
happy to help you identify the relevant studies.

• Think of this syllabus as a blueprint rather than a contract. It gives us a sense of where we’re going, but we may – and almost certainly will – end up changing it along the way.

Readings
The following books are available for purchase at Ulrich’s and online: Bailey, Michael A., and Forrest Maltzman. 2012. The Constrained Court: Law, Politics, and the Decisions Justices Make. Princeton, NJ: Princeton University Press. Cameron, Charles M. 2000. Veto Bargaining: Presidents and the Politics of Negative Power. New York: Cambridge University Press. Krehbiel, Keith. 1998. Pivotal Politics: A Theory of U.S. Lawmaking. Chicago: University of Chicago Press.

In addition to these books, we will read journal articles and book chapters. I have marked required readings for each week with an asterisk (*). In addition, I have listed several optional readings each week, a list that may be of use if you wish to pursue a topic in more detail.

Assignments and Grading
Your grade will be based on three factors. First, 40% of your grade will be based on your class participation. The format of the class will revolve primarily around discussion, which means you need to come to class every week prepared to discuss each of the readings. I cannot overstate how important this is; the success of the class will depend heavily on your preparation and willingness to contribute your ideas to the discussion. For every class, every reading, you should be prepared to write down three key criticisms and be able to defend the paper against other criticisms. I may, at times, ask you during class to write down your criticisms and hand them in.
Second, 30% of your grade will be based on short assignments. These assignments include the following: a presentation of one of the assigned readings to the class; acting as a discussant for one of the presentations; two referee reports on articles we read; four brief research proposals (including one that is co-authored with another student in the class); and a couple additional brief assignments. None of these individual assignments will be especially time-consuming; but they do add up, so you should plan ahead and think about how to distribute these over the semester.
The remaining 30% of your grade will be based on a project due at the end of the semester. You have the choice of either writing two detailed research proposals or writing a research paper (either on your own or with another student in the class). I will provide more information about these assignments in class.
Schedule
September 8 – Introduction
*Miller, Beth, et al. 2013. How to be a peer reviewer: A guide for recent and soon-to-be PhDs. PS: Political Science & Politics 46 (1): 120-123.
September 15 – Legislatures, Executives, and Policymaking, Part I
*Krehbiel, Pivotal Politics, ch. 1, 2, 3, and 6. *Howell, William G. 2003. Power without Persuasion: The Politics of Direct Presidential Action.
Princeton: Princeton University Press. Chapters 3 and 4. *Chiou, Fan-Yi, and Lawrence S. Rothenberg. 2014. The Elusive Search for Presidential Power.
American Journal of Political Science 58: 653–668. Tsebelis, George. 1999. Veto Players and Law Production in Parliamentary Democracies: An
Empirical Analysis. American Political Science Review 93 (3): 591-608. Binder, Sarah A. 1999. The Dynamics of Legislative Gridlock: 1947-96. American Political Science
Review 93: 519-33. See also exchange with Chiou and Rothenberg in Political Analysis 2007. Martin, Lanny W., and Georg Vanberg. 2004. Policing the Bargain: Coalition Government and
Parliamentary Scrutiny. American Journal of Political Science 48 (1): 13-27. Clinton, Joshua D. and John S. Lapinski. 2004. Measuring Legislative Accomplishment, 1877-1994.
American Journal of Political Science 50(1): 232-249. Maltzman, Forrest, and Charles R. Shipan. 2008. Change, Continuity, and the Evolution of the Law.
American Journal of Political Science 52(2): 252-267. Volden, Craig, and Alan D. Wiseman. 2014. Legislative Effectiveness in the United States Congress.
New York: Cambridge University Press. 2

September 22 – Legislatures, Executives, and Policymaking, Part II
*Cameron, Veto Bargaining, chapters 1, 2, 4, 5, and 6. *Berry, Christopher R., Barry C. Burden, and William G. Howell. 2010. The President and the
Distribution of Federal Spending. American Political Science Review 104: 783-799. *Bolton, Alexander, and Sharece Thrower. 2015. Legislative Capacity and Executive Unilateralism.
American Journal of Politics Science (forthcoming). Mayer, Kenneth R. 1999. Executive Orders and Presidential Power. Journal of Politics 61(2):445-66. Groseclose, Tim, and Nolan McCarty. 2001. The Politics of Blame: Bargaining Before an Audience.
American Journal of Political Science 45 (1): 100-119. Sin, Gisela, and Arthur Lupia. 2010. How the Senate and President Affect the Timing of Major Rule
Changes in the US House of Representatives. Working Paper, University of Michigan. Howell, William G., and Jon C. Rogowski. 2013. War, the Presidency, and Legislative Voting
Behavior. American Journal of Political Science 57: 150–166. Kriner, Douglas L., and Andrew Reeves. 2015. Presidential Particularism and Divide-the-Dollar
Politics. American Political Science Review 109 (1) 155-171. Gelman, Jeremy, Gilad Wilkenfeld, and E. Scott Adler. 2015. The Opportunistic President: How US
Presidents Determine Their Legislative Preferences. Legislative Studies Quarterly 40 (3): 363-390.
September 29 – Political Influence on the Bureaucracy, Part I
*McCubbins, Mathew D., Roger G. Noll, and Barry R. Weingast. 1989. Structure and Process, Politics and Policy: Administrative Arrangements and the Political Control of Agencies. Virginia Law Review 75: 431-482. (Read only the theory part, not the case study.)
*Wood, B. Dan, and Richard W. Waterman. 1991. The Dynamics of Political Control of the Bureaucracy. American Political Science Review 85 (3): 801-828.
*Carpenter, Daniel P. 1996. Adaptive Signal Processing, Hierarchy, and Budgetary Control in Federal Regulation. American Political Science Review 90 (2): 283-302.
*Huber, John D., and Charles R. Shipan, Deliberate Discretion? New York: Cambridge University Press. Chapters 4 and 6.
*Shipan, Charles R. 2004. Regulatory Regimes, Agency Actions, and the Conditional Nature of Political Influence. American Political Science Review 93 (August): 467-480.
Weingast, Barry R., and Mark J. Moran. 1983. Bureaucratic Discretion or Congressional Control: Regulatory Policymaking by the FTC. Journal of Political Economy 91: 765-800.
Moe, Terry M. 1989. The Politics of Bureaucratic Structure. In Can the Government Govern?, eds. John E. Chubb and Paul E. Peterson. Washington, DC: The Brookings Institution.
Ferejohn, John and Charles R. Shipan. 1990. Congressional Influence on Bureaucracy. Journal of Law, Economics, and Organization 6: 1-20.
Hammond, Thomas H., and Jack H. Knott. 1996. Who Controls the Bureaucracy? Presidential Power, Congressional Dominance, Legal Constraints, and Bureaucratic Autonomy in a Model of Multi-Institutional Policy-Making. Journal of Law, Economics, and Organization 12: 119-166.
Epstein, David, and Sharyn O’Halloran. 1999. Delegating Powers. NY: Cambridge Univ. Press. Howell, William G., and David E. Lewis. 2002. Agencies by Presidential Design. Journal of Politics
64(4): 1095-1114.
3

October 6 – Political Influence on the Bureaucracy, Part II
*Huber, Gregory A. 2007. The Craft of Bureaucratic Neutrality. New York: Cambridge. Ch. 1. *MacDonald, Jason A. 2010. Limitation Riders and Congressional Influence over Bureaucratic
Policy Decisions. American Political Science Review 104 (November): 766-782. *Gordon, Sanford A. 2011. Politicizing Agency Spending Authority: Lessons from a Bush-era
Scandal. American Political Science Review 105(04): 717-734. *Clinton, Joshua D., David E. Lewis, and Jennifer L. Selin. 2014. Influencing the Bureaucracy: The
Irony of Congressional Oversight. American Journal of Political Science 58: 387–401. *Clouser McCann, Pamela J. 2015. The Strategic Use of Congressional Intergovernmental
Delegation. Journal of Politics 77 (3): 620-634. *Farhang, Sean, and Miranda Yaver. 2015. Divided Government and the Fragmentation of American
Law. American Journal of Political Science (forthcoming). Volden, Craig. 2002. Delegating Power to Bureaucracies: Evidence from the States. Journal of Law,
Economics, and Organization 18 (1): 187-220. Bendor, Jonathan, and Adam Meirowitz. 2004. Spatial Models of Delegation. American Political
Science Review 98 (2) 293-310. Reenock, Christopher, and Sarah Poggione. 2004. Agency Design as an Ongoing Tool of
Bureaucratic Influence. Legislative Studies Quarterly 29(3): 383-406. MacDonald, Jason A. 2007. The U.S. Congress and the Institutional Design of Agencies. Legislative
Studies Quarterly 32(3): 395-420. Boehmke, Frederick, and Charles R. Shipan. 2015. Oversight Capabilities in the States: Are
Professionalized Legislatures Better at Getting What They Want? State Politics and Policy Quarterly 15: 366-386.
October 13 – Bureaucracy in the Political System, Part I
*Krause, George A., David E. Lewis, and James W. Douglas. 2006. Political Appointments, Civil Service Systems, and Bureaucratic Competence: Organizational Balancing and Gubernatorial Revenue Forecasts in the American States. American Journal of Political Science 50(3): 77087.
*Lewis, David A. 2009. The Politics of Presidential Appointments. Princeton, NJ: Princeton University Press. Chapters 3 and 5.
*Bertelli, Anthony, and Christian Grose. 2011. The Lengthened Shadow of Another Institution: The Ideological Preferences of the Executive Branch and Congress. American Journal of Political Science. 55 (4):767-781.
*Clinton, Joshua D., Anthony Bertelli, Christian Grose, David E. Lewis, and David C. Nixon. 2012. Separated Powers in the United States: The Ideology of Agencies, Presidents, and Congress. American Journal of Political Science. 56:341-54.
*Chen, Jowei, and Tim Johnson. 2015. Federal employee unionization and presidential control of the bureaucracy: Estimating and explaining ideological change in executive agencies. Journal of Theoretical Politics 27.1 (2015): 151-174.
Krause, George A. 1999. A Two-Way Street: The Institutional Dynamics of the Modern Administrative State. Pittsburgh: University of Pittsburgh Press.
Carpenter, Daniel P. 2001. The Forging of Bureaucratic Autonomy. Princeton, NJ: Princeton University Press.
Gailmard, Sean, and John W. Patty. 2007. Slackers and Zealots, Civil Service, Policy Discretion and Bureaucratic Expertise. American Journal of Political Science 51 (4): 873-889.
4

Lewis, David A. 2007. Testing Pendleton’s Premise: Do Political Appointees Make Worse Bureaucrats? Journal of Politics 69 (4): 1073-1088.
Gordon, Sanford C. 2009. Assessing Partisan Bias in Federal Public Corruption Prosecutions. American Political Science Review 103 (4): 534-554.
Gailmard, Sean, and John W. Patty. 2013. Learning While Governing. Chicago: UC Press. Callander, Steven, and Keith Krehbiel. 2014. Gridlock and Delegation in a Changing World.
American Journal of Political Science, 58: 819–834.
October 20 – No Class (Fall Break!)
October 27 – Bureaucracy in the Political System, Part II
*Canes-Wrone, Brandice. 2003. Bureaucratic Decisions and the Composition of the Lower Courts. American Journal of Political Science 47 (2): 205-214.
*Wiseman, Alan. 2008. Delegation and Positive-Sum Bureaucracies. Journal of Politics 71 (3): 9981014.
*Yackee, Jason Webb and Susan Webb Yackee. 2010. Is Agency Rulemaking ‘Ossified’? Testing Congressional, Presidential, and Judicial Procedural Constraints. Journal of Public Administration Research and Theory, 20: 261-282.
*Haeder, Simon F., and Susan Webb Yackee. 2015. Influence and the Administrative Process: Lobbying the U.S. President’s Office of Management and Budget. American Political Science Review 109 (3): 507-522.
*Bolton, Alexander, Rachel Potter, and Sharece Thrower. 2015. Organizational Capacity, Regulatory Review, and the Limits of Political Control. Manuscript, University of Virginia.
Eskridge, William N., Jr., and John A. Ferejohn 1992. The Article I, Section 7 Game. Georgetown Law Journal 80 (3): 523-564.
Croley, Steven. 2003. White House Review of Agency Rulemaking: An Empirical Investigation. University of Chicago Law Review 70: 821-886.
Stephenson, Matthew C. 2006. Legislative Allocation of Delegated Power: Uncertainty, Risk, and the Choice between Agencies and Courts. Harvard Law Review 119 (4): 1035-1070.
Miles, Thomas J., and Cass R. Sunstein. 2006. Do Judges Make Regulatory Policy? An Empirical Investigation of Chevron. The University of Chicago Law Review 73 (3): 823-881.
O’Connell, Anne Joseph. 2008. Political Cycles of Rulemaking: An Empirical Portrait of the Modern Administrative State. Virginia Law Review 94: 889-986.
Yackee, Jason Webb and Susan Webb Yackee. 2009. Divided Government and U.S. Federal Rulemaking. Regulation and Governance, Vol. 3: 128-144.
November 3 – Congress, Courts, and Strategic Behavior, Part I
*Segal, Jeffrey A. 1997. Separation-of-Powers Games in the Positive Theory of Congress and Courts. American Political Science Review 91: 28-44.
*Helmke, Gretchen. 2002. The Logic of Strategic Defection: Court-Executive Relations in Argentina Under Dictatorship and Democracy. American Political Science Review 96 (2): 291-304.
*Harvey, Anna, and Barry Friedman. 2006. Pulling Punches: Congressional Constraints on the Supreme Court’s Constitutional Rulings, 1987-2000. Legislative Studies Quarterly 31(4): 533-562.
*Randazzo, Kirk A., Richard W. Waterman, and Jeffrey A. Fine. 2006. Checking the Federal Courts: The Impact of Congressional Statutes on Judicial Behavior. Journal of Politics 68(4): 1006-
5

1017. *Clark, Tom S. 2009. The Separation of Powers, Court-curbing and Judicial Legitimacy. American
Journal of Political Science 53 (4): 971-989. *Harvey, Anna. 2014. A Mere Machine. Chapters tba. Shipan, Charles R. 2000. The Legislative Design of Judicial Review: A Formal Analysis. 2000.
Journal of Theoretical Politics 12 (3): 269-304. Revesz, Richard L. 2001. Congressional Influence on Judicial Behavior? An Empirical Examination
of Challenges to Agency Action in the D.C. Circuit. NYU Law Review 76: 1100-1141 Bergara, Mario, Barak Richman, and Pablo T. Spiller. 2003. Modeling Supreme Court Strategic
Decision Making: The Congressional Constraint. Legislative Studies Quarterly 28 (2): 247280. Sala, Brian R., and James F. Spriggs, II. 2004. Designing Tests of the Supreme Court and the Separation of Powers. Political Research Quarterly 57 (2): 197-208Collins, Paul M., Jr. 2008. The Consistency of Judicial Choice. Journal of Politics 70(3): 861-873. Smith, Joseph L. 2006. Judicial Procedures as Instruments of Political Control: Congress’ Strategic Use of Citizen Suits. Legislative Studies Quarterly 31(2): 283-305. Chutkow, Dawn M. 2008. Jurisdiction Stripping: Litigation, Ideology, and Congressional Control of the Courts. Journal of Politics 70 (4): 1053-1064. Linzer, Drew A., and Jeffrey K. Staton. 2015. A Global Measure of Judicial Independent, 19482012. Journal of Law and Courts 3 (2): 223-256.
November 10 – Congress, Courts, and Strategic Behavior, Part II
*Bailey, Michael A., and Forrest Maltzman. 2012. The Constrained Court. Chapters tba. *Segal, Jeffrey A., Chad Westerland, and Stefanie A. Lindquist. 2011. Congress, the Supreme Court,
and Judicial Review: Testing a Constitutional Separation of Powers Model. American Journal of Political Science 55 (1): 89-104. Dahl, Robert A. 1957. Decision-Making in a Democracy: The Supreme Court as a National Policymaking. Journal of Public Law 6: 279. Epstein, Lee, Jack Knight, & Andrew Martin. 2001. The Supreme Court as a Strategic National Policy Maker. Emory Law Journal 50 (2): 583-611. Smith, Joseph L. 2007. Presidents, Justices, and Deference to Administrative Action. Journal of Law, Economics, and Organization 23 (2): 346-364. Carrubba, Clifford J., Matthew Gabel, and Charles Hankla. 2008. Judicial Behavior under Political Constraints: Evidence from the European Court of Justice. American Political Science Review 102 (4): 435-452. Harvey, Anna, and Barry Friedman. 2009. Ducking Trouble: Congressionally-Induced Selection Bias in the Supreme Court’s Agenda. Journal of Politics 71 (2): 574-592. Owens, Ryan J. 2012. The Separation of Powers and Supreme Court Agenda Setting. American Journal of Political Science 54 (2): 412–427.
November 17 – Nominations
*Moraski, Bryon J., and Charles R. Shipan. 1999. The Politics of Supreme Court Nominations: A Theory of Institutional Constraints and Choices. American Journal of Political Science 43:1069-1095.
*Johnson, Timothy R., and Jason M. Roberts. 2005. Pivotal Politics, Presidential Capital, and Supreme Court Nominations. Congress and the Presidency 32 (1): 31-48.
6

*Krehbiel, Keith. 2007. Supreme Court Appointments as a Move-the-Median Game. American Journal of Political Science 51(2): 231-241.
*Anderson, Richard J., David Cottrell, and Charles R. Shipan. 2015. The Power to Appoint: Nominations and Change on the Supreme Court. Manuscript, University of Michigan
Snyder, Susan K., and Barry R. Weingast. 2000. The American System of Shared Powers: The President, Congress, and the NLRB. Journal of Law, Economics, and Organization 16(2): 269-305.
Binder, Sarah A., and Forrest Maltzman. 2004. The Limits of Senatorial Courtesy. Legislative Studies Quarterly 29(4): 5-22.
McCarty, Nolan M. 2004. The Appointments Dilemma. American Journal of Political Science 38 (3): 413-428.
Nemacheck, Christine. 2007. Strategic Selection: Presidential Nomination of Supreme Court Justices from Herbert Hoover through George W. Bush. Charlottesville: University of Virginia Press.
Rohde, David W., and Kenneth A. Shepsle. 2007. Advising and Consenting in the 60-Vote Senate: Strategic Appointments to the Supreme Court. Journal of Politics 69(3): 664-677.
November 24 – Individual Meetings about the Final Paper (no new readings)
December 1 – Confirmations
*Krutz, Glen S., Richard Fleisher, and Jon R. Bond. 1998. From Abe Fortas to Zoe Baird: Why Some Presidential Nominations Fail in the Senate. American Political Science Review 92 (4): 871-882.
*Cameron, Cover, and Segal, 1990. Senate Voting on Supreme Court Nominees: A Neo-Institutional Model. American Political Science Review 84: 525-534.
*Epstein, Lee, Rene Lindstadt, Jeffrey A. Segal, and Chad Westerland. 2006. The Changing Dynamics of Senate Voting on Supreme Court Nominees. Journal of Politics 68(2): 296-307.
*Charles M. Cameron, Jonathan P. Kastellec and Jee-Kwang Park. 2013. Voting for Justices: Change and Continuity in Confirmation Voting 1937–2010. Journal of Politics 75 (2): 283-299.
McCarty, Nolan, and Rose Razaghian. 1999. Advice and Consent: Senate Responses to Executive Branch Nominations 1885-1996. American Journal of Political Science 47(4): 654-668.
Binder, Sarah A., and Forrest Maltzman. 2002. Senatorial Delay in Confirming Judges, 1947-1998. American Journal of Political Science . 46 (1): 190-199.
Shipan, Charles R., and Megan L. Shannon. 2003. Delaying Justice(s): A Duration Analysis of Supreme Court Confirmations. American Journal of Political Science 47 (4): 654-668.
Primo, David, Sarah Binder, and Forrest Maltzman. 2008. Who Consents? Competing Pivots in Federal Judicial Selection. American Journal of Political Science 52(3): 471-489.
Shipan, Charles R. 2008. Partisanship, Ideology, and Senate Voting on Supreme Court Nominees. Journal of Empirical Legal Studies 5(1): 55-76.
Zigerell, LJ. 2010. Senator Opposition to Supreme Court Nominations: Reference Dependence on the Departing Justice. Legislative Studies Quarterly 35 (3): 393-416.
December 8 – Institutions and Preferences
*Lupia, Arthur, Yanna Krupnikov, Adam Seth Levine, Spencer Piston, and Alexander Von HagenJamar. 2010. Why State Constitutions Differ in their Treatment of Same-Sex Marriage. The Journal of Politics 72 (4): 1–14.
7

*Kastellec, Jonathan P., Jeffrey R. Lax, and Justin Phillips. 2010. Public Opinion and Senate Confirmation of Supreme Court Nominees. Journal of Politics 72(3): 767-84.
*Lax, Jeffrey R., and Justin H. Phillips. 2009. Gay Rights in the States: Public Opinion and Policy Responsiveness. American Political Science Review 103: 367-386.
*Canes-Wrone, Brandice, and Kenneth Schotts. 2004. Conditional Nature of Presidential Responsiveness to Public Opinion. American Journal of Political Science. 48: 690- 706.
*Hall, Matthew E.K. 2014. The Semiconstrained Court: Public Opinion, the Separation of Powers, and the U.S. Supreme Court's Fear of Nonimplementation. American Journal of Political Science 58: 352–366.
Gerber, Elisabeth R. 1996. Legislative Response to the Threat of Popular Initiatives. American Journal of Political Science 40 (1): 99-128.
Boehmke, Frederick J. 2005. The Indirect Effect of Direct Legislation. Columbus: Ohio State University Press. Ch. 2-3.
Canes-Wrone, Brandice. 2006. Who Leads Whom? Presidents, Policy, and the Public. Chicago: University of Chicago Press. (See also the Canes-Wrone and Shotts article.)
Boehmke, Frederick J., Sean Gailmard, and John Wiggs Patty. 2006. Whose Ear to Bend? Information Sources and Venue Choice in Policy Making. Quarterly Journal of Political Science 1 (2): 139-169.
Shipan, Charles R., and Craig Volden. 2006. Bottom-up Policy Federalism: The Spread of Antismoking Laws from U.S. Cities to States. 2006. American Journal of Political Science 50 (October): 825-843.
Pacheco, Julianna. 2012. The Social Contagion Model: Exploring The Role of Public Opinion on the Diffusion of Anti-Smoking Legislation across the American States. The Journal of Politics 74 (1): 187-202.
Krupnikov, Yanna, and Charles R. Shipan. 2015. Voter Uncertainty, Political Institutions, and Legislative Turnover. Manuscript, University of Michigan.
Potentially helpful review articles:
Miller, Gary J. 2005. The Political Evolution of Principal-Agent Models. Annual Review of Political Science 8: 203-225.
Weingast, Barry R., Rui J. P. de Figueiredo, Jr., and Tonja Jacobi. 2006. The New Separation of Powers Approach to American Politics. In The Oxford Handbook of Political Economy, eds. Barry R. Weingast and Donald Wittman. New York: Oxford University Press.
Bailey, Michael A., Forrest Maltzman, and Charles R. Shipan. 2010. The Amorphous Relationship between Congress and the Courts. The Oxford Handbook of the American Congress.
Moe, Terry M. 2012. Delegation, Control, and the Study of Public Bureaucracy. The Forum 10 (2). Graham, Erin, Charles R. Shipan, and Craig Volden. 2013. The Diffusion of Policy Diffusion
Research. 2013. British Journal of Political Science 43 (3): 673-701.
8

