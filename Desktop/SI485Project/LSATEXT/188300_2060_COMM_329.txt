University of Michigan
Poli Sci 329/Comm 329. Mass Media and Political Behavior
Fall Term 2015 8:30-10am, Monday/Wednesday Lecture Room 1300 Chem building Professor Nicholas Valentino (nvalenti@umich.edu)
7758 Haven Hall Office Hours: Tuesdays, 12–2pm or by appointment
GSI: Logan Woods (ltwoods@umich.edu) Location: 7726 Haven Hall
Office Hours: Wednesdays 11-12, 3:30-5:30
Overview We assess the role of the mass media in the American political system with a particular emphasis on its impact during campaigns and elections. We analyze the dynamic relationship between three sets of actors: the press, politicians, and the public. We investigate interactions between the press and politicians, and think about how these affect public opinion and behavior. We discuss our democracy’s health and consider reforms that could help (but might harm!) the public’s ability to hold elected officials accountable. Finally, we explore how new communication platforms such as social media reshape old problems and offer new solutions. According to reliable sources on the Internet, this course can be a lot of fun for students who get attend class. Please do not ask the professor to document this claim. He just put it here to see if anyone reads the syllabus.
Requirements Students should come to class prepared to discuss the readings and give their informed opinions about the topics covered in lecture. Class discussion is vital, so all relevant questions and comments, time permitting, are welcome. The professor and GSI have developed several sneaky techniques for rewarding attendance and participation. Do not ask what they are, because they are top secret.

Formal requirements- You will be evaluated based on two midterms, a final, a 12- page term paper, short section drafts of the paper, and attendance/participation in class and section.
Exams- Two short multiple choice/short answer section exams will be given on October 7th, and November 16th. The final exam will be given on Friday, December 18th from 10:3012:30 in the lecture hall (1300 Chem Bldg). Do not arrive late on the day of the exams; no additional time can be given for late-comers. If a student requires special exam conditions due to a documented disability, the student must notify Professor Valentino in the first two weeks of class so that arrangements can be made.
Term Paper: due at 5pm on Friday, December 11th. One of the most important goals of the course is to understand how candidates choose their strategies for communicating with citizens, and how journalists react to those strategies. Your job in this paper is to select two of the current presidential campaigns that you are interested in and systematically contrast the media strategies that the candidates are using, and the effect of these strategies on news coverage. The question you will be answering is “what leads a candidate to use a particular online communication strategy?” You will be able to draw on several hypotheses from the readings in class. For example, candidates who are behind in the polls often use different strategies, they might be more controversial or negative, than those who are leading. Can you document such changes in strategy over the few months you have to observe these campaigns?
So what does “communication strategy” mean? It can mean a lot of things these days, and it is up to you to decide which definition to use. You must think carefully about what materials you will collect that will become your evidence. You might analyze the candidates’ websites, Facebook pages, twitter feeds, and speeches. However, you might also want to consider the candidates’ appearances on entertainment sites and blogs.
What does “systematically” mean? It means you need to think about your research design carefully. First, how much material will you collect, and how will you choose it? On what date

does the campaign begin? How many individual pieces of evidence (articles, tweets, facebook posts, interviews, speeches, etc.) will you collect? How will that evidence be distributed across the campaign? Will you choose a random sampling strategy, or some other process? Once you have a sample of content, how will you analyze it? What dimensions of strategy will you be looking for? Regardless of how you answer these questions, you will need to defend your research design decisions in a section of the paper called Research Methods. We will be giving more specific guidance to you in Section, but doing more will be rewarded. Good papers will analyze at least 30 or so relevant items about a given campaign.
Finally, in this paper we want you to think critically and normatively about the material you are analyzing. What are its strengths and weaknesses in terms of informing the average citizen about what is important in the campaign? If a citizen were to be exposed to all this back and forth between candidates and journalists, would they be in a good position to make a choice? You must propose a specific study in your concluding section that will allow you or a future researcher to determine the actual impact of this interaction between candidates and journalists on the public. In other words, how would we know if citizens get what they need in order to make a good decision on Election Day?
The final product will be 12 double spaced pages in length, not including title page and bibliography. Do not try to fool the professor with Jedi mind tricks (i.e., font, margin, and line spacing manipulations). They will be ineffective.
Feel free to draw on materials we’ve covered in the class and section, but citing other books or journal articles on this topic is important. If you have any questions about whether a book or article is appropriate, ask your GSI or the professor. Proper citation is required when you use a source. Be sure to ask if you are unsure about citation protocol. Citing the Professor’s lectures should be avoided, because he rarely believes what he says, especially at 8:30am. Let us instead place our sacred trust in the magic of anonymous peer review.
Late papers and makeup exams- Late papers submitted without a note from a doctor will be severely penalized. No incompletes can be given, and no makeup exams can be scheduled. If

you cannot attend the exams scheduled in this class, please do not enroll in this course. The professor did not choose, and cannot change, the time of the final exam. He is extremely powerful, but there are limits as with all things. Further, the Registrar is like that fellow who must not be named. Exams are closed book, without notes. To be honest, the professor doesn’t like giving exams at all. However, they seem to be a thing in college, so he provides them in order to fit in.
Accommodations for Students with Disabilities –If a student requires classroom or exam accommodations due to a documented disability, the student must notify the professor in the first two weeks of class to make proper arrangements. Students are encouraged to contact the office of Services for Students with Disabilities (http://ssd.umich.edu/), which can help document disabilities and determine appropriate accommodations.
Grading The short midterm exams are worth 15% each. The Final is worth 20%. The term paper is worth 30%. Section assignments are worth 10% and attendance/participation in section and class is worth 10%.
Readings Readings- Readings should stimulate thoughtful discussion in class and especially in section. Think critically about the arguments in the readings. Are you convinced? What else would you like to know before you became convinced? What questions occur to you as you read a piece? Lectures and sections are intended to be complimentary, not redundant. Students are responsible for all material discussed in both. The professor assigns these readings because he thinks they are all really great, even the ones that are dead wrong. Which, he realizes, may seem a little weird. But that’s science: finding out which claims are wrong, and which ones are not, and then arguing about which is which.
Texts: Why Americans Hate the Media and How it Matters (WAHM!) by Jonathan Ladd Making the News: Politics, Media and Agenda Setting (MTN) by Amber E. Boydstun

Media Politics, 3rd ed. (MP) by Shanto Iyengar

I-clickers- You will need one for this course. Please acquire one at the bookstore.

Course Website:
There is a course website on Coursetools:
https://ctools.umich.edu/portal/site/7e8e78f8-b516-49f3-a518-40cb8385d8bf
The website is identical for those of you taking this course as Poli Sci 329 or Comm Studies 329. This site will be used to post readings, announcements, the syllabus, web content, and lecture slides. Students are responsible for downloading readings. The schedule will identify the readings on the website. They are all required reading. Some students tell the professor this is a lot of reading. The professor isn’t sure he agrees, but since he really likes reading, he usually replies “You’re welcome!” This response often does not go over as well the professor thinks it will.

Sept. 9

Course Calendar
Introduction- Syllabus review, attendance, enrollment and waitlist issues. Course mission.

PART I. The Press

Sept. 14-16 News and the Truth

Text: MP Ch. 1. Reader:

Lippmann, Walter. "Chapters 1, 23, 24," in Public Opinion. New York: Free Press Paperbacks, 1922, pp. 3-20, 214-225, 226-230. REMEMBER TO ADD THE
STEREOTYPES CHAPTER.

Sept. 21-23 The Role of the Press in Democracy- What does it do, what should it do?

Text: MP Ch. 2. Reader:

Kull, Steven, Clay Ramsay, and Evan Lewis. "Misperceptions, the Media, and the Iraq War." Political Science Quarterly, Vol. 118, No. 4, December 2003, pp. 569-598.

Sept. 28-30 The History of Newsmaking. Journalistic/Political conflict in historical context.

Text: Ladd WAHM! Chaps 1-4.

Oct. 5

Media Markets and Press Norms

Oct. 7 Oct. 12-14 Oct. 19 Oct. 21
Oct. 26 Oct. 28
Nov. 2

Texts: MP, Ch. 3-4. Reader:

Hallin, Daniel. "Sound Bite News: Television Coverage of Elections." Journal of Communication, Vol. 42, No. 2, Spring 1992, pp. 5-24.

Short Exam 1.

Newsmaking in the Contemporary Period- Changes in political coverage that result from changes in news media institutions.

Text: MTN, Chapters 1-4, 7.

Fall Break

Part II. Politicians

New Media, New Campaigns: Social Media and the Fragmentation of the Audience

Texts: MP Ch 5. MTN Ch. 8. Reader:

Gimpel et al. 2010. “Battleground States versus Blackout States: The Behavioral Implications of Modern Presidential Campaigns.” Journal of Politics vol 69: 786-
797.

Managing the Media: Issue Ownership, Advertising Strategy

Texts: MP Ch 6 except for pps 180-186.

Issue Advocacy and Campaign Finance

Texts: MP Ch. 6 pps 181-186. Reader: Mann, Thomas E. 2003. Linking Knowledge and Action: Political Science and
Campaign Finance Reform. Perspectives on Politics, Vol. 1, pp. 69-83

Corrado, Malbin, Mann & Ornstein. 2010. Reform in an Age of Networked CampaignsHow to Foster Citizen Participation Through Small Donors and Volunteers. Brookings
Institution Report.

Governance via the Media. “Going public” and the permanent campaign.

Text: MP, Chs 7, 10.

Nov. 4

Part III. The Public Minimal Consequences versus Major Impact.

Text: MP 8, pp. 230-240 and MP Ch. 9 pp. 270-274.

Sears, David O. and Rick Kosterman, "Mass Media and Political Persuasion," in
Persuasion: Psychological Insights and Perspectives, edited by Sharon Shavitt and Timothy C. Brock Boston: Allyn and Bacon, 1994, pp. 251-278.

Nov. 9-11 The Active Audience: Selective Exposure and Motivated Reasoning

Sears, David O. and Jonathan L. Freedman. 1967. “Selective Exposure to Information: A Critical Review.” Public Opinion Quarterly, 31, 194-213.

Stroud, Natalie Jomini. 2008. Media Use and Political Predispositions: Revisiting the Concept of Selective Exposure. Political Behavior 30:341–366

Taber, Charles & Milton Lodge. 2006. “Motivated Skepticism in the Evaluation of Political Beliefs.” American Journal of Political Science, 50, 3, 755-769.

Dan M. Kahan, Maggie Wittlin, Ellen Peters, Paul Slovic, Lisa L. Ouellette, Donald Braman & Gregory Mandel. 2012. The polarizing impact of science
literacy and numeracy on perceived climate change risks, Nature Climate Change, advance on line publication.

Nov. 16

Short Exam 2

Nov. 18

Persuasion during Campaigns

Text: MP Ch. 8, 260-267

Zaller, John. “The Myth of a Massive Media Impact Revived: New Support for a
Discredited Idea,” in Mutz, Sniderman and Brody, eds., Political Persuasion and Attitude Change (Ann Arbor: UM Press, 1996), 17-60.

Nov. 23-25 Learning, Knowledge Gaps and Media Institutions

Text: WAHM! Chs. 5-7 MP Ch. 8, pp. 240-242, Ch. 9. 291-294.

Prior, Markus (2005). News v. Entertainment: How Increasing Media Choice Widens Gaps in Political Knowledge and Turnout. American Journal of Political
Science, 49 (3): 577-592.

Nov. 30-Dec. 2

Indirect Effects of Mass Media: Agenda setting, priming framing.

Dec. 7-9

Text: MP Ch. 8, pp. 243-259. Ads and Emotion: Are we blinded by our passion for politics?

Brader, Ted. 2005 Striking a Responsive Chord: How Political Ads Motivate and Persuade Voters by Appealing to Emotions. American Journal of Political Science, Vol. 49, No. 2 (Apr., 2005), pp. 388-405

Brader, Ted, Nicholas A. Valentino, and Elizabeth Suhay. 2008. “What Triggers Public Opposition to Immigration? Anxiety, Group Cues, and Immigration Threat.” American
Journal of Political Science, 52(4), 959-978.

Valentino, Nicholas A., Ted Brader, Eric Groenendyk, Krysha Gregorowicz, and Vincent L. Hutchings. 2011. “Election Night’s Alright for Fighting: The Role of Emotions in
Political Participation.” The Journal of Politics, 73(1): 156-170.

PAPER DUE FRIDAY, December 11 at 5pm.

Dec. 14

Conclusions: Lessons from the Chelsea Drugstore. Reading: WAHM! Ch. 8; MTN Ch. 9

Final exam is Friday, December 18th from 10:30-12:30 in the lecture hall (1300 Chem Bldg).

Happy Holidays to you all!

