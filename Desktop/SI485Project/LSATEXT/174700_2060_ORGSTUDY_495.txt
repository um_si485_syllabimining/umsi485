Organizational Studies 495 The Psychology of Prejudice and Inequality:
Theoretical Foundations and Implications for Organizations
Fall 2015: Tuesdays 1:00-4:00pm, G449 Mason Hall

Instructor Information

Professor: Office hours: Email: Course website:

Arnold Ho Tuesdays 4:00-6:00pm, 2016-H Ruthven Museum Building, or by appointment arnoldho@umich https://ctools.umich.edu/portal/site/89173a0b-4491-4669-a7bf-43fde105eb3f

Course Overview

This course provides a survey of the psychology of prejudice and inequality, the scientific study of human feeling, thinking, and behavior in contexts involving conflict between groups. More broadly, the course examines the psychological factors that contribute to the perpetuation of inequality and discrimination. The course considers both proximate (immediate) influences on behavior, such as the immediate social situation as well as distal (more remote) influences on behavior, such as human evolution. Both motivational approaches to understanding prejudice (e.g., explaining prejudice as a consequence of the desire for social dominance) as well as cognitive approaches (e.g., explaining prejudice as a byproduct of automatic associations people learn) are examined. Implications for societal organization and institutional discrimination will be explored.

This course has several goals:

1. To inform you about important theories and research findings in the psychological literature on prejudice and intergroup relations.
2. To illustrate how theories and research from psychology can inform our understanding of: a) discrimination within organizations, b) discrimination by organizations (i.e., institutional discrimination), and c) inequality in societal organization.
3. To help you become a better consumer of research in your daily life. 4. To give you a forum to develop as a critical thinker and writer.

Assignments and Grading

Participation and Article presentations (30%):

Since this is a discussion-centered course, active participation is vital to the success of this course. Thus, active participation represents a key component of your grade. You should be prepared to discuss the ideas and questions you present in your article profiles (see below) and any other ideas or reactions you have to the readings. You will also be expected to present two articles throughout the term – for each article you present, you should provide a summary of the article, including the general setup of the studies and key findings, as well as raise one question to get discussion started. Finally, you should be engaged during discussion, and respond to your classmates comments as appropriate.
1

Additional discussion guidelines to keep in mind/refer back to:
• Respect others’ rights to hold opinions and beliefs that differ from your own. Challenge or criticize the idea, not the person.
• Listen carefully to what others are saying even when you disagree with what is being said. Comments that you make (asking for clarification, sharing critiques, expanding on a point, etc.) should reflect that you have paid attention to the speaker’s comments.
• Be courteous. Don’t interrupt or engage in private conversations while others are speaking. • Support your statements. Use evidence and provide a rationale for your points. • Allow everyone the chance to talk. If you have much to say, try to hold back a bit; if you are hesitant
to speak, look for opportunities to contribute to the discussion. • If you are offended by something or think someone else might be, speak up and don't leave it for
someone else to have to respond to it. • Try not to silence yourself out of concern for what others will think about what you say. • If you think something is missing from the conversation, don’t wait for someone else to say it; say it
yourself. • Be careful about putting other students on the spot. Do not demand that others speak for a group
that you perceive them to represent. • Encourage disagreement with one another and with the professor. • Keep confidential any personal information that comes up in class.
Article Profiles (40%):
In order to facilitate discussion, and provide an incentive to do the readings, you are required to complete a brief (i.e. about half a page, and never more than a single-page) profile of each reading assignment. The profiles will consist of the following 3 components (see Appendix 1 for Sample):
1. A brief overview of the study’s approach, methods, and findings in your own words. You do not need to mention every variable included in the study nor should you feel obligated to discuss every finding in the article/book chapter. The main goal here is to get you to think about the big picture. Think about how you would summarize and describe the article to a friend.
2. A bullet point or two addressing any unique findings or interesting aspects of the study. What did you like about the article or book chapter? Was there something particularly innovative in the authors’ approach to this research question? Were there any results that you found surprising?
3. Two or more bullet points offering any questions or concerns about the study. What limitations did you see in the article? Was there a logical flaw in the authors’ study or did the authors rely upon a flawed or deficient study design? Did the study adequately measure what the authors claimed to measure? Were there alternative methods that would have been more convincing? Did the authors overlook something in their analyses? Were any of the inferences the authors made unwarranted? Are there alternative explanations for some of the findings? This is the portion of the profile that is most meaningful and that can show you thoughtfully and critically engaged with the article.
Since there are 35 reading assignments listed on the syllabus, you are required to complete 35 profiles throughout the semester. However, I will only grade one of your profiles for each week (to be picked
2

randomly). You will be asked to post article profiles to the course website for each class by 11:59pm the day before class. Your article profiles will be assessed on a +, , - basis (for grading purposes, this translates into 100%, 90%, or 80%. You will receive a + if your profile reflects thoughtful, critical thinking, and could potentially stimulate class discussion. You will receive a  if you only raise minor points. A - will be given for profiles that do not reflect critical thinking (i.e., that do not go beyond point 1 and 2, above). No credit will be awarded if any of the profiles for a week are missing, or if any of the profiles are late. The lowest grade will be dropped. Term paper (30%): A term paper (2500-3000 words, not including references) will be due on December 14. More specific information about grading and topic selection for this paper will be given later. This paper will be broken down into components as follows: Oral paper presentation (5%) – December 8 Paper (25%) – due December 14 Policies Late assignments: Late assignments will be marked down by 10% for each 24 hours that they are late. Assignments will be submitted electronically on the class website, which provides a timestamp. To ensure that the timestamp does not indicate that your assignment is late, you should initiate the submission process before the time the assignment is due. To be fair to all students, the due date/time will be strictly enforced. Note this does NOT apply to the article profiles, which must be turned in on time to receive credit, since they are designed in large part to prepare you for discussion. Cheating/Plagiarism: Don’t do it! Cheating/plagiarism will be taken seriously, and handled in accordance with university policies as outlined on the academic integrity website: http://www.lsa.umich.edu/academicintegrity/ Accessibility policy: Students who need accommodations should contact me to discuss and implement satisfactory arrangements for course materials and assignments. You should speak with me by the end of the second week of the term. Failure to do so may result in my inability to respond in a timely manner. All discussions will remain confidential, except to consult with the Services for Students with Disabilities Office to implement appropriate accommodations.
3

Course Calendar

Month Day Topic

Sept.

8 Introduction/Organizational meeting

Part I: Theoretical Foundations

15 Evolutionary Underpinnings

22 Motivational Approaches I: Social Dominance Theory Motivational Approaches II: Social Dominance Theory and
29 System Justification Theory

Oct. 6 Motivational Approaches III: Social Identity Theory

13 Social-Cognitive Approaches

20 Fall Study Break

Part II: Implications for Organizations

27 Prejudice and Education

Nov. 3 Prejudice and Politics

10 Prejudice and Law Enforcement

17 Diverse Workgroups and Employment Discrimination

24 Implications of Mixed Race for Societal Organization

Dec. 1 Prejudice Reduction and Social Equality

8 Presentations

14

Assignment due (by 11:59pm)1 Term paper

1 Article profiles will be due for each article by 11:59pm the day before we have discussions.

Reading List
Part I: Theoretical Foundations
Evolutionary Underpinnings
Olsson, A., Ebert, J. P., Banaji, M. R., & Phelps, E. A. (2005). The role of social groups in the persistence of learned fear. Science, 309, 785-787.
Bar-Haim, Y., Ziv, T., Lamy, D., & Hodes, R. M. (2006). Nature and nurture in own-race face processing. Psychological Science, 17(2), 159-163.
Kurzban, R., Tooby, J. & Cosmides, L. (2001) Can race be erased? Coalitional computation and social categorization. Proceedings of the National Academy of Sciences, 98, 1528715392.
Thomsen, L., Frankenhuis, W. E., Ingold-Smith, M., & Carey, S. (2011). Big and mighty: Preverbal infants mentally represent social dominance. Science, 331(6016), 477-480. doi:10.1126/science.1199198
Motivational Approaches I: Social Dominance Theory
Sidanius, J., & Pratto, F. (2001). Social dominance: An intergroup theory of social hierarchy and Oppression (Chapters 1-2, pp. 3-57). New York: Cambridge University Press.
Ho, A. K., Sidanius, J., Kteily, N., Sheehy-Skeffington, J, Pratto, F., Henkel, K. E., Foels, R., & Stewart, A. L. (in press). The nature of social dominance orientation: Theorizing and measuring preferences for intergroup inequality using the new SDO7 scale. Journal of Personality and Social Psychology.
Motivational Approaches II: Social Dominance Theory and System Justification Theory
Navarrete, C.D., McDonald, M., Molina, L., & Sidanius, J. (2010). Prejudice at the nexus of race and gender: An out-group male target hypothesis. Journal of Personality and Social Psychology, 98(6): 933-45.
Jost, J. T., & Banaji, M. R. (1994). The role of stereotyping in system-justification and the production of false-consciousness. British Journal of Social Psychology, 33, 1-27.
Laurin, K., Fitzsimons, G. M., & Kay, A. C. (2011). Social disadvantage and the self-regulatory function of justice beliefs. Journal of Personality and Social Psychology, 100(1), 149171.
1

Motivational Approaches III: Social Identity Theory
Tajfel, H. & Turner, J. C. (1986). The social identity theory of intergroup behavior. In S. Worchel & W. G. Austin (Eds.), Psychology of intergroup relations (pp. 7-24). Chicago: Nelson Hall.
Hogg, M. A., & Terry, D. I. (2000). Social identity and self-categorization processes in organizational contexts. Academy of Management Review, 25(1), 121-140.
Yamagishi, T., & Kiyonari, T. (2000). The group as the container of generalized reciprocity. Social Psychology Quarterly, 116-132.
Social-Cognitive Approaches
Hamilton, D. L., & Gifford, R. K. (1976). Illusory correlation in interpersonal perception: A cognitive basis of stereotypic judgments. Journal of Experimental Social Psychology, 12(4), 392-407.
Devine, P. G. (1989). Stereotypes and prejudice: Their automatic and controlled components. Journal of Personality and Social Psychology, 56, 5-18.
Greenwald, A. G., McGhee, D. E., & Schwartz, J. K. (1998). Measuring individual differences in implicit cognition: The implicit association test. Journal of Personality and Social Psychology, 74(6), 1464-1480. doi:10.1037/0022-3514.74.6.1464
Also see http://faculty.washington.edu/agg/pdf/Real-world_samples.pdf for examples of IAT effects in various “real world” domains.
Part II: Implications for Organizations
Prejudice and Education
Steele, C. M., & Aronson, J. (1995). Stereotype threat and the intellectual test performance of African Americans. Journal of Personality and Social Psychology, 69(5), 797-811. doi:10.1037/0022-3514.69.5.797
Sidanius, J., van Laar, C., Levin, S., & Sinclair, S. (2004). Ethnic enclaves and the dynamics of social identity on the college campus: The good, the bad, and the ugly. Journal of Personality and Social Psychology, 87(1), 96.
Okonofua, J. A., & Eberhardt, J. L. (2015). Two strikes: Race and the disciplining of young students. Psychological Science, 26(5), 617-624.
2

Prejudice and Politics
Hutchings, V. L. (2009). Change or more of the same? Evaluating racial attitudes in the Obama era. Public Opinion Quarterly, 73(5), 917-942.
Enos, R. D. (2014). Causal effect of intergroup contact on exclusionary attitudes. Proceedings of the National Academy of Sciences, 111(10), 3699-3704.
Knowles, E. D., Lowery, B. S., & Schaumberg, R. L. (2010). Racial prejudice predicts opposition to Obama and his health care reform plan. Journal of Experimental Social Psychology, 46(2), 420-423.
Prejudice and Law Enforcement
Correll, J., Park, B., Judd, C. M., & Wittenbrink, B. (2002). The police officer's dilemma: Using ethnicity to disambiguate potentially threatening individuals. Journal of Personality and Social Psychology, 83(6), 1314-1329. doi:10.1037/0022-3514.83.6.1314
Goff, P. A., Eberhardt, J. L., Williams, M. J., & Jackson, M. C. (2008). Not yet human: implicit knowledge, historical dehumanization, and contemporary consequences. Journal of Personality and Social Psychology, 94(2), 292.
Eberhardt, J. L., Davies, P. G., Purdie-Vaughns, V. J., & Johnson, S. L. (2006). Looking deathworthy: Perceived stereotypicality of black defendants predicts capital-sentencing outcomes. Psychological Science, 17(5), 383-386.
Diverse Workgroups and Employment Discrimination
Phillips, K., Kim-Jun, S.Y., & S. Shim, (2011). The value of diversity in organizations: A social psychological perspective. In D. De Cremer, R. van Dick, & K. Murnighan (Eds.), Social psychology and organizations (253-272). New York: Routledge.
Bertrand, M. & Mullainathan, S. (2004). Are Emily and Greg more employable than Lakisha and Jamal? A field experiment on labor market discrimination. American Economic Review, 94(4), 991-1013.
Moss-Racusin, C.A., J.F. Dovidio, V.L. Brescoll, M.J. Graham, and J. Handelsman. 2012. Science faculty’s subtle gender biases favor male students. Proceedings of the National Academy of Sciences, 109(41), 16474-16479.
Implications of Mixed Race for Societal Organization
Ho, A. K., Sidanius, J., Levin, D. T., & Banaji, M. R. (2011). Evidence for hypodescent and racial hierarchy in the categorization and perception of biracial individuals. Journal of Personality and Social Psychology, 100(3), 492-506.
3

Ho, A. K., Sidanius, J., Cuddy, A. J. C., & Banaji, M. R. (2013). Status boundary enforcement and the categorization of Black-White biracials. Journal of Experimental Social Psychology, 49(5), 940-943.
Krosch, A. R. & Amodio, D. M. (2014). Economic scarcity alters the perception of race. Proceedings of the National Academy of Sciences, 111(25), 9079-9084.
Caruso, E. M., Mead, N. L., & Balcetis, E. (2009). Political partisanship influences perception of biracial candidates’ skin tone. Proceedings of the National Academy of Sciences of the United States of America, 106(48), 20168-20173. doi:10.1073/pnas.0905362106
Prejudice Reduction and Social Equality Pettigrew, T. F., & Tropp, L. R. (2006). A meta-analytic test of intergroup contact
theory. Journal of Personality and Social Psychology, 90(5), 751-783. doi:10.1037/00223514.90.5.751 Saguy, T., Tausch, N., Dovidio, J. F., & Pratto, F. (2009). The irony of harmony: Intergroup contact can produce false expectations for equality. Psychological Science, 20(1), 114121. doi:10.1111/j.1467-9280.2008.02261.x Walton, G. M. & Cohen, G. L. (2011). A brief social-belonging intervention improves academic and health outcomes of minority students. Science, 331, 1447-1451. Walton, G. M. (2014). The new science of wise psychological interventions. Current Directions in Psychological Science, 23(1), 73-82.
4

Appendix 1
Bargh, Chen, & Burrows (1996)
1. Overview • Bargh et al. investigated whether subtle features of the environment that people are not even
aware of can influence behavior. • In three studies, the concepts “rude,” “elderly,” and “African American” were introduced (primed),
outside of participants’ conscious awareness, and participants’ behavior was measured. Participants primed with the concept rude more readily interrupted an experimenter, those primed with “elderly” walked more slowly, and those primed with “African American” behaved in a more hostile manner. 2. Unique or interesting aspects of study • It was nice that this worked regardless of the type of prime (whether it was sentences that participants read or pictures that were presented subliminally). • I was surprised that just being exposed to words related to the elderly (e.g., bingo, Florida) could make someone walk more slowly – given this, it seems that our behavior could be influenced by all kinds of subtle environmental factors. 3. Questions and concerns • In the walking study, a separate sample of participants was given a measure of mood to ensure that this alternative explanation (increased sadness following an elderly prime) could not account for the finding – while the results were supportive, it seems possible that the explicit measure of mood is not sensitive enough to detect changes in mood. Perhaps a measure of physiological arousal would further help to rule out this alternative. • As part of the rudeness prime study, a second “voluntary” survey was given, and only 3 participants refused to complete this voluntary survey – this calls into question how strong the rudeness prime is (already, only 13/34 participants had interrupted the experimenter, which was the primary measure of rudeness). • On the other hand, it’s possible that the use of college student participants led to an underestimation of priming effects – college students participating in a lab study are not likely to behave extremely rudely or hostilely, making it harder to discover the true extent to which these unconscious primes can influence behavior.

