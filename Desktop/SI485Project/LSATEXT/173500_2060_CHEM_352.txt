Chemistry 352/3
Instructor: Dr. Kathleen Nolta Office: 3537 Chemistry nolta@umich.edu
Lecture: Fridays 2-3 in 1200 Chemistry (will be posted in Resources and Lecture Recordings on CTools) Labs: M 1-5, Tu 1-5, or W 1-5 in A513/A515 Chemistry

COURSE DESCRIPTION
Chemistry 352(3) is designed to introduce you to many of the techniques that are commonly used in biochemistry research laboratories. You will be expected to maintain a laboratory notebook throughout the term. This notebook is your primary record of what you did in the laboratory. As in any research laboratory, your record is your own and, as such, it provides the best documentation of every single thing you observed and thought during your experiments. An ideal research notebook will be complete, concise, and reproducible. Your notebook may be checked as you enter each lab (to make sure you have performed the calculations and pre-lab work that is needed) and as you leave each lab (to check that the data has been recorded).
SAFETY CONCERNS
In this laboratory you may come into contact with flammable, toxic or caustic materials; some may be potentially carcinogenic. In order to work safely in this environment, always stay aware and alert. Even biological buffers and reagents such as proteins and DNA may cause skin irritation and possibly blindness if introduced into the eye. Understand the experiment you are running and the properties of the reagents you are using. Not only will this increase your safety, it will also increase efficiency in lab. It is important to know which agents are the most dangerous! Always be conscious of your own safety as well as the safety of others. Willful neglect of safety rules will be grounds for expulsion from the laboratory and failure of the course.
o Eye protection must be worn at all times within the laboratory. o No sandals or other open shoes are to be worn in the laboratory. o Legs must be fully covered (no shorts and no bare legs for any reason) o No food (including gum or drinks) is allowed in the laboratory o Lab coats or aprons should be worn while working in the laboratory. o Use gloves routinely when handling chemicals. In addition, wash your hands with soap and warm
water frequently and before you leave the laboratory. o Maintain a clean, organized bench. You must clean up at the end of every lab period and clearly
label all tubes beakers, and flasks. o Dispose of all chemicals and waste in the appropriate waste containers; when in doubt consult with
an instructor.

THE NOTEBOOK:
An important component of your lab evaluation is maintaining a well-organized, clear laboratory notebook. Your lab notebooks will be evaluated on clarity (i.e. can someone else reproduce your work?), which is crucial in a research lab. Instead of traditional lab reports, you will turn in a laboratory summary at the end of each lab. This summary is an extension of your lab notebook. You may be asked to turn in all the pages relevant to that lab: the pages that you prepare before you come to lab with your plan for the experiment, the specific procedure you follow and write down as you do the lab, as well as

Chemistry 352/3 Fall 2015

1

Chemistry 352/3
pages where you include your results (any gels, tables, and graphs of data) and your analysis of data. You must maintain a copy of everything you do/write in a three-ring binder: this includes all pre-lab work, data and observations collected in lab, and all post-lab work. A copy of all pre-lab work must be turned in upon entering the lab when requested. You may place your data directly in the binder as soon as you have collected it; most students write directly on their protocols, making clear notes about any changes that occur during the experiment. Post-lab summaries will be different for each experiment; some might be short, but there will also be a formal report for each half of the term.
IMPORTANT: the summaries for each experiment should be done as soon as possible after the data collection is complete. However, some experiments require several weeks just for data collection (i.e. when a gel has been run and requires visualization). Generally, these brief discussions will be expected when you return to lab two weeks following every experiment. It is in your best interest to write your summary immediately, and then you have the following week in lab to discuss with your partner(s) and clarify any confusion that you may have had. This gives you another week to revise and complete a polished report after discussing questions with your team. GSIs will be collecting something almost every week as you enter the lab. Watch for due dates! Details on what is exactly expected in these “post-lab summaries will be discussed in lecture. Each experiment will be a little different.
GRADING POLICY
• The lab notebooks/summaries will be evaluated on timeliness, conciseness, clarity, and your analysis of your experimental data.
• Plagiarizing lab reports will result in loss of all points for that report as well as notification to the College. We have developed a large database of previously submitted reports. Please just do not go there.
• Your laboratory performance grade will be evaluated based upon punctual attendance, preparation, participation, laboratory skills and techniques, clean up, and safety. You are expected to attend and participate in every laboratory session (lectures will be podcasted for those with a conflict).
• A practical exam is scheduled for the end of the term.
• Formal written work (formal reports and summaries) will be approximately 70% of your grade (formals
contributing half of these points). As always in the laboratory, we are looking for effort. The quality of your work is important in every experiment, but if you show improvement we will also take that into account. This leaves roughly 15% for participation/safety/attendance and 15% for the practical exam. Those in 353 will also have a discussion component to their grade, weekly writing assignments, and two projects. This ULWR component will contribute one third of the final lab grade (1 credit compared to 2 credits for the lab itself)
Note: Departmental policy indicates the first step in inquiring about the accuracy of a final grade should be directed to the lead instructor of the course. This initial inquiry should take place within the first fifteen University business days of the first full term following the term in which the disputed grade was issued. If, after this inquiry, the student is not satisfied with the instructor’s response, the student may choose to initiate a formal grade grievance. To initiate a formal grade grievance, the student should contact the Associate Chair of Undergraduate Studies (ACUS) of the home department of the course in question before the end of the fifth week of classes in the first full term following the term in which the disputed grade was issued.

Chemistry 352/3 Fall 2015

2

Chemistry 352/3

The following is a schedule (barring problems with equipment or materials).
SCHEDULE

Lecture Date

Lecture Topic

Lab Dates

In Lab/Exp.

Writing

FRI Sept 11 Course Overview/Exp. 1 Sept 14-16 Exp. 1: Titration of Amino Acids

Pre-lab: Design your experiment, calculations

Sept 18

Henderson-Hasselbalch, buffers, buffer capacity, calculating pKa

Sept 21-23

Exp. 2: Pipets, buffers, spectropho- Pre-lab: calculations tometry, extinctions

Sept 25 Oct 2 Oct 9 Oct 16
Oct 23 Oct 30
Nov 6

DNA and digitizing gels

Sep 28-30

Exp. 3: Analysis of DNA fragments Experiment 1 summary is due

Restriction digests

Oct 5-7

Exp. 4: Restriction mapping Set up PCR for Alu work

Experiment 2 summary is due

PCR and Alu fingerprint- Oct 12-14 ing

Exp 5: Alu work

No lecture

Oct 19-21 No lab this week

HINT: Use this time to work on Pymol tutorial
Work on the Formal (3/4)
Do the final work on Exp 5

Lambert-Beer Law, Extinction, and Exp. 6
Forms of hemoglobin

Oct 26-28 Nov 2-4-27

Exp. 6: Extinction coefficients and protein concentrations
Exp 7: Column chromatography and forms of Hb

BY OCT 28 latest: Formal write-up of Exp 3-4 due, and Prelab calculations/tables
Summary of Experiment 5 (with stats and sequence work) due To prepare for Exp 7, the Pymol tutorial (Resources) is a good place to start Read Exp 7 and complete tables

Enzyme Kinetics

Nov 9-11

Exp. 8 Malate Dehydrogenase

Summary of 6 is due in lab

Chemistry 352/3 Fall 2015

3

Chemistry 352/3

Nov 13 Nov 20 Nov 27 Dec 4

Column Chromatography
No Lecture

Nov 16-18

Exp. 9 Column chromatography and protein purifications

Nov 23-25 No lab – Thanksgiving break

Formal write-up for Exp 7 due in lab (Nov 10-12)
Summary of 8 is due in lab

Read protocols/lectures Nov 30-Dec Exp. 10 ELISA and MS 2

Tie up last experiment Dec 7-9

Practical Exam

Summaries of BOTH 9 and 10 are due in lab

Summaries will consist of relevant data/tables and a brief descriptions or very specific brief assignments.
Formal write-ups (one for each half of the term) are much more extensive and will follow the layout of the experiments recorded in your notebook. Final formal reports must include all of the following:
1. A title that describes the experiment.
2. A 1-2 sentence objective that describes what the experiment entails and what question you are trying to answer.
3. A table of which reagents you will be using for your experiments and how much you are adding (in their order of addition). This list (your “materials and methods” section) should include:
a. The reagents that will be used in the experiment including, where appropriate, reagent molarities (for solutions), measured quantities (mass or volume), and number of moles.
b. The equipment and apparatus that will be needed
4. A detailed description of what you did (your procedure), including the order and rates of addition of reagents, the times that events occur, the temperature, and any other specific conditions. Write this down as you go along (use your pre-lab!)—you will not remember it accurately later! If the experiment worked correctly, you will want to be able to reproduce it exactly as it was done before. If you did not get the expected results, a detailed description will help you determine what went wrong and where you can make changes. You should be able to repeat your experiment solely using your laboratory notebook.
5. The results of your experiments. These should be included in your notebook as tables, graphs, pictures, or diagrams of your data that you collected during the lab and analyzed.
6. A discussion of your results. You should concisely summarize your data in the context of the purpose of your experiment and answer any questions posed in the protocol or lecture. If the experiment has failed or was only partly successful, you must include an analysis of why you think it did not work, as well as your recommendation for what should be done the next time.

Chemistry 352/3 Fall 2015

4

