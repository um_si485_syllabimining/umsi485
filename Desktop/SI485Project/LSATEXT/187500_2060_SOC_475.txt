SOCIOLOGY 475 Fall, 2015 OFFICE: 4109 LS&A

PROFESSOR RENEE ANSPACH PHONE: 763-0439,E-MAIL ranspach

INTRODUCTION TO MEDICAL SOCIOLOGY

COURSE DESCRIPTION

This course will explore social aspects of health, illness, and the health care system in American society. Along the way, we will explore such issues as: why health and wealth are so closely related in what is viewed as a “land of opportunity”; why patients’ race, gender and social status affects the kinds of diagnoses and treatments they receive; how health professionals and patients make life-and-death decisions; why increasing numbers of social problems, such as childrens’ “bad” behavior in the classroom, come to be defined as diseases; and why the U.S. leads the world in spending on healthcare while Americans’ health lags behind that of most other industrialized nations; and what can be done to resolve this pattern.

REQUIREMENTS

1. Lecture and discussion. The format of the class usually will consist of an interactive lecture—a lecture interspersed with discussion. Students are expected to attend lecture and participate in discussions. While lectures are the primary teaching tool, I will also supplement lectures with ungraded quizzes, surveys, films, break-out groups, or class exercises. I distribute and post an outline of the lecture. Lectures are mandatory because I present materials that go beyond the assigned readings. Everything that takes place during class may be included on the exam. .
2. Readings To receive a good grade in the course, students are expected not only to attend lectures, but to do the required readings and integrate them with what is covered in class. I try not to exceed 80 pages per week, which is considered an average workload for LS&A.

Three books should be purchased: Peter Conrad and Valerie Leiter (Eds.), The Sociology of Health and Illness, nineth edition,Worth Publishers:2013. Charles Bosk, Forgive and Remember, Chicago: 1979, 2003 Renee Anspach , Deciding Who Lives, California : 1993, 1997 Additional required readings, designated with an asterisk*, will be posted on Course Tools. In addition, I may be posting relevant news items on C-Tools from time to time that illustrate the topics we are discussing.

3. Written assignments:

1. Paper of 10 pages on a topic to be chosen by the student and approved by the instructor, and due the last day of class. Some suggested topics include: interviewing a chronically ill person about their experiences with the health care system; examining the costs, risks,

1

and benefits of a medical technology; designing or advocating a set of health policy reforms; exploring a controversial issue in health care in the U.S.; or examining the social causes or consequences of a particular disease.

2. Optional midterm--students have the option of taking the midterm for credit, for comments only, or not taking it.

3. Op-ed piece—students are required to submit an op-ed piece of 750-800 words (about 3 pages), or a column in an online or print newspaper that expresses your opinion about an issue in health care you think is important. Guidelines will be provided and discussed. It will be due November 23.

4. Final examination, December 16, 1:30 pm

Exams will be closed book and in-class. However, one week prior to the exam, I will distribute a list of 20 short answers and about 3 or 4 essays. I will choose ten short answers from the list, and students will be asked to choose one of two essays.

The exams will be graded on the basis of:

1. The clarity and organization of the answers; 2. The answer’s completeness: the extent to which it covers all the important points; and 3. The extent to which you incorporate and integrate material from both lectures and
readings.

For students who choose to take the midterm for credit, the grades will be determined as follows:

Attendance at lectures & participation in class discussions Op-ed piece Optional midterm exam Final exam Paper

10% 15% 25% 25% 25%

For students who do not take the optional midterm for credit, the grades will be determined as follows:

Attendance at lectures & participation in discussions Op-ed piece Final exam Paper

10% 16% 37% 37%

Timely completion of course requirements

All students are expected to turn in work and take the final at the time scheduled. Because the midterm exam is optional, requests for make-up exams will not be granted. Extensions for the paper or final will be given only in exceptional circumstances (death in the

2

family, illness documented by doctor’s note).
Use of office hours
Each student is expected to use office hours once during the term. However, I encourage you to visit office hours when you don’t understand concepts presented in the lectures or readings, or if something occurs during class that concerns you.
Maintaining an atmosphere conducive to learning
As members of the University of Michigan community, it is important that we work collectively to create, maintain, and sustain an atmosphere focused on learning. To this end, students are asked to:
1. Recognize that the subject matter is controversial and treat others’ opinions with respect, so all students feel free to participate in class discussions.
2. Arrive at class on time and refrain from using cell phones, I-Pads, computers, or other electronic devices during class.
3. Foster a culture of academic integrity. A student who has cheated or engaged in plagiarism on an assignment will receive a failing grade on that assignment and will be reported to the Assistant Dean for Student Academic Affairs.
REQUIRED READINGS AND COURSE OUTLINE
(Dates are approximate)
Unit I. HEALTH AND ILLNESS AS SOCIAL PHENOMENA
September 9, 14
Introduction: Medical and Sociological Perspectives on Health and Illness Contrasted
P. Conrad and V. Leiter , "The Social Nature of Disease" in The Sociology of Health and Illness: Critical Perspectives, Conrad & Leiter 7-9.
J. McKinlay and S. McKinlay, "Medical Measures and the Decline of Mortality in Conrad & Leiter, 10-23.
September 14, 16
How Society Contributes to Disease
P.Conrad and V. Leiter , "Who Gets Sick: the Unequal Social Distribution of Disease”, in
3

Conrad & Leiter, 24-27.
*V. Fuchs, “A Tale of Two States” in Conrad & Leiter, seventh edition, 55-57.
P. Conrad and V. Leiter , "Our Sickening Social and Physical Environment" in Conrad & Leiter, 78-80.
*H. Markel, “Bubonic plague visits San Francisco’s Chinatown, When Germs Travel: Six Major Epidemics that Have Invaded America and the Fears They Have Unleashed”, New York, Pantheon: pp. 49-77.
*B. Alexander, “Amid swine flu outbreak, racism goes viral”, MSNBC.com, May 1, 2009.
N. Bakalar, “Plague cases in U.S. on the rise”, New York Times, September 7, 2015,
http://nyti.ms/1UDgfN4
*M. Moran, “A looming problem: how to ration ebola vaccines and medicine”, Los Angeles Times, September 17, 2014.
September 21-28
Social Inequality in Health: Race, Class, and Gender
J. Banks, M. Marmot, Z.Oldfield, and J. Smith, “Disease and Disadvantage in the United States and in England, in Conrad & Leiter, 58-68.
E. Klinenberg, “Dying Alone: The Social Production of Urban Isolation,” in Conrad & Leiter, 102-116.
D.Williams and M. Sternthal, “Understanding Racial-ethnic Disparities in Health: Sociological Contributions”, in Conrad & Leiter, 34-44.
*J. DeParle, “Harder for Americans to Rise from Lower Rungs”, New York Times Jan. 4, 2012.
R. Snow, “Sex, Gender, and Vulnerability”, in Conrad & Leiter, 45-57.
Unit II. THE EXPERIENCE OF ILLNESS
September 30
The Sick Role
*H. Waitzkin, H. and Waterman, B. Exploitation of Illness in Capitalist Society. Indianapolis:Bobbs-Merrill., 1974 (pp. 36-64).
4

*D. Rier “The Missing Voice of the Critically Ill: A Medical Sociologist’s First-Person Account”, Sociology of Health and Illness 22(2000): 68-93.
October 5
Becoming Sick and Seeking Help: Health and Illness Behavior
P. Conrad and V. Leiter, "The Meaning of Medications: Another Look at Compliance", in Conrad & Leiter, 197-208.
*Lemert, E. (1958, 1978). “Paranoia and the Dynamics of Exclusion.”. In Rubington and Weinberg (Eds.), Deviance: The Interactionist Perspective: Third Edition. New York: Macmillan. (pp. 131-140).
*A. Kleinman, “Conflicting Explanatory Models in the Care of the Chronically Ill:TheVoice of Medicine and the Voice of the Life World.” In H. Schwartz (ed.), Dominant Issues inMedical Sociology, Third Edition. New York: McGraw-Hill: 1994: (pp. 95-104).
October 7
Medical Decision Making
S. Timmermans, “Social Death as Self-Fulfilling Prophecy” in Conrad & Leiter, 409-424.
*S. Henderson, C.L. Stacey, and D. Dolan, “Social Stigma and the Dilemmas of Providing Care to Substance Users in a Safety-Net Emergency Department”, Journal of Health Care for the Poor and Underserved 19 (2008): 1336-49.
*D. Sudnow, “Dead on Arrival”. In H. Schwartz (ed.), Dominant Issues in Medical Sociology: Third Edition.(1994, 1967) (pp. 263-270).
R. Anspach, Deciding Who Lives, Chapter 3.
October 12, 14
Life-and-Death Decisions, Sociology of Bioethics
R. Anspach, Deciding Who Lives, Chapter 1
*R. Anspach, “From Cruzan to Schiavo: How Bioethics Entered the Culture Wars”(2008), in The Sociology of Bioethics: Sociological Perspectives, Barbara Katz Rothman, Elizabeth Armstrong, and Rebecca Tiger eds., Elsevier, pp. 33-63.
5

*Vanessa Northington Gamble, (1997) “Under the shadow of Tuskegee: African Americans and health care”, American Journal of Public Health; 87, 11, pp.. 1773-1777.
*Review cases: Best, Cherrix and Mottl.
*S. Varney, “A racial gap in attitudes toward hospice care.” New York Times, August 21, 2015.
http://nyti.ms/1E9FjsY
October 19 Study day No class
October 21, 26
Patient-Practitioner Relationships E. Mishler, "The Struggle between the Voice of Medicine and the Voice of the Lifeworld" in Conrad & Leiter, 382-393.
R. Anspach, Deciding Who Lives, chapters 4, 5.
Review*S. Varney, “A racial gap in attitudes toward hospice care.” New York Times, August 21,
http://nyti.ms/1E9FjsY
October 28-November 4
Chronic Illness, Stigma and Disability A. Frank, “The Remission Society” in Conrad & Leiter, 209-212.
A.Saguy and K. Gruys, “Morality and Health: News Media Constructions of Overweight and Eating Disorders”, in Conrad & Leiter, 127-145.
A.Scott, “Illness Meaning of AIDS Among Women with HIV: Merging Immunology and Life Experience, in Conrad & Leiter, 146-157.
K. Barker, “Electronic Support Groups, Patient-Consumers, and Medicalization: the Case of Contested Illness”, in Conrad & Leiter, 180-196.
*JE Robison, “What is neurodiversity?” Psychology Today.com, October 7, 2013.
Study questions to be posted on C-Tools November 2
November 9 Midterm Exam
Unit III. MEDICINE AS A SOCIAL INSTITUTION
November 11
6

Hospitals and Patients Review Timmermans (in Conrad & Leiter), Anspach, Chapters 4 and 5.
November 16 Medicine as a Profession: Theoretical Perspectives J. McKinlay,. and L.Marceau “The End of the Golden Age of Doctoring” in Conrad & Leiter, 234-259. D.Light, “Countervailing Power: The Changing Character of the Medical Profession in the United States,” in Conrad & Leiter, 260-269.
November 18 Does Medical Education Instill a Service Ethic? R. Anspach, "The Language of Case Presentation", in Conrad & Leiter, 425-443. C.Bosk, Forgive and Remember, begin reading Bosk, especially Chapter 5. *D.Chambliss,“The Patient as Object” in Beyond Caring, Chicago: 1996 (Pp.120-149).
November 23 Medical Mistakes: Can Doctors Police Themselves in the Public Interest?, C. Bosk, Forgive and Remember , especially Chapters 2, 4 *M. Millman,. “Medical Mortality Review.” In H. Schwartz , Dominant Issues in Medical Sociology, First Edition (1987) (pp. 342-347). C. Bosk, “An Ethnographer’s Apology, a Bioethicist’s Lament”, Forgive and Remember: 215-235.
Op-ed piece due in class November 23 November 25 Professionalization in Historical Context P. Conrad and J. Schneider, "Professionalization, Monopoly, and the Structure of Medical Practice", in Conrad & Leiter 216-221. R. Wertz and D. Wertz, "Notes on the Decline of Midwives and the Rise of Medical
7

Obstetricians" in Conrad & Leiter, 222-233. T.Winnick, “From Quackery to ‘Complementary’ Medicine: The American Profession Confronts Alternative Therapies”, 282-298. November 30, December 2 The Social Construction of Illness: Medicalization and Genetization P.Conrad and V. Leiter, “From Lydia Pinkham to Queen Levitra: Direct-to-Consumer Advertising and Medicalisation”, in Conrad & Leiter, 301-311. H. Hartley, “The ‘Pinking’ of Viagra Culture: Drug Industry Efforts to Create and Repackage Sex Drugs for Women”, in Conrad & Leiter, 312-321. P.Conrad, “A Mirage of Genes” in Conrad & Leiter, 468-478. P. Conrad, “The Shifting Engines of Medicalization”, in Conrad & Leiter, 507-518. *J. Rosen, ““The Brain on the Stand,” New York Times, March 11, 2007. Pp. 1-4. *P. Conrad and D. Potter “Human Growth Hormone and the Temptations of Biomedical Enhancement”. Sociology of Health and Illness 26(2004): 184-215. *J.F. Peltz, “Botox maker Allergan buying Kythera”, Los Angeles Times, June 15, 2015. * S. Okie, 2009. “Crack Babies - The Epidemic That Wasn’t.” New York Times, January 26, 2009
Unit IV. MEDICINE AND THE WIDER SOCIETY November 7 Machines, Money and Managed Care
P. Conrad & V. Leiter 322-325; 444-447. D. Stone, “Doctoring As Business; Money, Markets, and Managed Care” in Conrad & Leiter, 329-336. K. Joyce, “MRI as Cultural Icon”, in Conrad & Leiter, 448-459. R. Fields, “God Help You. You’re on Dialysis,” in Conrad & Leiter, 460-467.
8

R.Anspach, “Beyond the Nursery” In Deciding Who Lives. Berkeley: California (Chapter 6). December 9, 14 Toward Alternatives: Perspectives on the Health Care Crisis and Proposed Solutions R. Wilkinson and K. Pickett, “Greater Equality: The Hidden Key to Better Health and Higher Scores, in Conrad & Leiter, 117-123. R. Deber, “Health Care Reform: Lessons from Canada.” In Conrad & Leiter, 558-564. J. Gabe, “Continuity and Change in the British National Health Service”, in Conrad & Leiter. 565-579. V. Pitts, “Illness and Internet Empowerment: Writing and Reading Breast Cancer in Cyberspace”, in Conrad & Leiter, 522-536. J. McKinlay, “A Case for Refocussing Upstream”, in Conrad & Leiter, 583-595. December 10: Study questions for final exam will be posted December 14 : Papers due in class Final examination: Wednesday, December 16 1:30-3:30 p.m.
9

