ASIAN 254
Spectacular History of Korea
MW 4-5:30 pm /3304 MLB
Se-Mi Oh ● smoh@umich.edu ● STB 6163 ● office hours: M 2-4
Course Description: What is a spectacular history? Why do some events capture our imagination and end up on the front page of newspapers? Why are some events more important in narrating history than others? This course introduces students to the history of modern and contemporary Korea through events. Together, we will investigate the processes through which events that took place in twentieth-century Korea were rendered spectacular and consumed in the form of images and texts. Among other things, we will examine the tumultuous processes of Korea’s modernization, colonialism, wars, nation building, dissident movements, and cold war divisions. For each topic, we will choose an event, and take a close look at how it took place (history) and how it was represented in texts and visual media (spectacular history). By looking at the processes through which experiences were transformed into spectacular images, we will not only try to gain a better perspective on the climate of the times but also on how representation and memory write history. This course is restricted to freshmen. No prior knowledge of Korea or Korean language is necessary. Requirements: Attendance and participation 20% Blogs 30% Research paper proposal 5% Research paper presentation 5% Research paper 40%
	   1	  

*For each blog, please answer the question posted in the appropriate folder for Discussion on Canvas. *There is a research workshop scheduled on September 30. *Research proposal is due on October 21. *Research paper is due at 8 am, on December 21. *All assignments should be submitted on Canvas.
*There are three Korean Studies Colloquium talks during the semester. On September 16, October 7, and December 9, please come to the talk at Room 1636 School of Social Work Building.
Readings and Image files: All readings and images are available on Canvas.
Films: Assigned films will be streaming on Canvas.

Schedule of Meetings:

Week 1

9/9 Course Introduction

I. Methodology – Image and History: Colonial Korea

Week 2

9/14 The Sino-Japanese War Readings: MIT Visualizing Cultures Unit “Throwing Off Asia II” http://ocw.mit.edu/ans7870/21f/21f.027/throwing_off_asia_02/index.html

9/16 Korean Studies Colloquium Wonjung Min, Assistant Professor, Department of History and Asian Studies Center, Political Catholic University of Chile “Korean Wave in Latin America: K-pop Reception and Participatory Fan Culture” Room 1636 School of Social Work Building

Week 3 Week 4

9/21 The Russo-Japanese War Readings: MIT Visualizing Cultures Units “Asia Rising” and “Yellow Promise/ Yellow Peril” http://ocw.mit.edu/ans7870/21f/21f.027/asia_rising/index.html http://ocw.mit.edu/ans7870/21f/21f.027/yellow_promise_yellow_peril/index.html *BLOG 1 Due
9/23 The Industrial Exhibition 1915 I Readings: Image files: The Industrial Exhibition 1915 *BLOG 2 Due
9/28 The Industrial Exhibition 1915 II

	   2	  

Week 5

Readings: Todd Henry, “Material Assimilation: Colonial Expositions on the Kyongbok Palace Grounds” in Assimilating Seoul Hong Kal, “Modeling the West, Returning to Asia”
9/30 Research Workshop
10/5 The March First Movement Readings: The Korean Independence Movement: Actual Photographs Showing Peaceful Demonstrations of the Koreans for Independence and Brutal Treatment Accorded them by Japanese Soldiery (Shanghai: The Oriental Press, 19xx) The Photo Album of King Kojong’s Funeral (Keijo: Keijo Nippo, 1919) *BLOG 3 Due
10/7 Korean Studies Colloquium Eugene Park, Associate Professor of History, University of Pennsylvania “Progeny of Fallen Royals: The Kaesong Wang in Choson Korea” Room 1636 School of Social Work Building

II. The Korean War

Week 6

10/12 South Korean Memory and Humanism Readings: Hwang Sunwŏn, “Crane” Pak Wansŏ, “Winter Outing”
*BLOG 4 DUE

10/14 North Korea and Its War Memories Readings: The Victorious Fatherland Liberation War Museum catalogue Image files: North Korean Propaganda Posters

Week 7

10/19 Fall Break

10/21 Rethinking the Enemy Film: Welcome to Dongmakgol (2005, dir. Kwang-Hyun Park) *BLOG 5 DUE

Research Proposal Due

III. Democratic Movements/Moments

Week 8

10/26 Self-immolation: Chon T’ae-il Film: A Single Spark (1995, dir. Park Kwang-su) *BLOG 6 DUE

10/28 Kim Ji-ha: Poetry and Politics Readings: “The Five Bandits”

	   3	  

Week 9

“The Politics and Poetry of Kim Chi-ha,” “A Declaration of Conscience,” “From a Korean Prison: A Path to Life,” “”Kim Chi-ha: A Poet of Blood and Fire” in Bulletin of Concerned Asian Scholars, 9.2 (April-June 1977).
11/2 Kwangju 1980 Readings: Ch’oe Yun, There A Petal Silently Falls *BLOG 7 DUE

IV. History Becomes Events

11/4 What’s in the Statue?: Yi Sun-sin Readings: The Record of the Black Dragon Year (excerpts) Film: The Admiral (2014, dir. Kim Han-min, 128 min)

Week 10

11/9 Uigwe Controversy I In-class film: Coréen 2495 (2005, dir. Ha Jun-su, 115 min)

11/11 Uigwe Controversy II Readings: UNESCO (2006), “Uigwe: The Royal Protocol of the Joseon Dynasty,” Memory of the World Register, Nomination Document, Ref No 2006-48

Week 11

11/16 Comfort Women I: Wednesdays In-class films: Her Story (2011, dir. Kim Jun-gi, 11 min) The Big Picture (2012, dir. Hyo Kwon, 92 min)

11/18 Comfort Women II: Visualizing Trauma Readings: Sangmie Choi Schellstede ed. Comfort Women Speak *BLOG 8 DUE

V. Mega-events, Mega-structures, Mega-cities

Week 12

11/23 The Palace and the Colonial Architecture Readings: Jong-Heon Jin, “Demolishing Colony: The Demolition of the Old Government-General Building of Chosŏn,” in Timothy Tangherlini ed. Sitings: Critical Approaches to Korean Geography Michael Kim, “Collective Memory and Commemorative Space: Reflections on Korean Modernity and Kyongbok Palace Reconstruction 1865-2010” *BLOG 9 DUE

11/25 Thanksgiving Break

Week 13

11/30 The City Hall Film: Talking Architecture: City Hall (dir. Jeong Jae-eun, 2013, 106 min)

12/2 Sports as Mega-Events Readings: Kang Hong-Bin, “Mega-Events as Urban Transformers” in Jinhee Park ed. Convergent Flux.

	   4	  

Week 14

Rachael Joo, Transnational Sport: Gender, Media, and Global Korea (excerpts) *BLOG 10 DUE
12/7 Research Presentations
12/9 Korean Studies Colloquium Baek Yung Kim, Associate Professor, Humanities Department, Kwangun University “Making a New Metropolis in Seoul: ‘The Age of 86/88’ as a Catalyst of Gangnamization” Room 1636 School of Social Work Building

Week 15

12/14 Research Presentations Research paper due December 21, 8 am.

	   5	  

