Communication Studies 159 • First-year Seminar: Media Microclimates Fall 2015 • Tu Th 10-11:30 a.m. • 1110 North Quad Version 2.0

Professor Derek Vaillant dvail@umich.edu Office Hrs: Tu 11-noon and by app’t

Office: 5443 North Quad Tel: 615-0449 Course Info on CTools

In the twentieth century, the United States became an economic and military superpower that boasted a profitable arsenal of media/communication technologies and a widely circulated popular culture that altered the place of “America” in the world. Meanwhile, particularly toward the end of the twentieth-century, with the advent of satellite TV and the Internet, the nation-state as the locus of modern politics and socio-cultural identity was changing. Global capitalism, human migration, and mass communication and media circulated with increasing breadth and intensity across borders and frontiers; The peoples of the world were coming together in new physical and virtual settings, particularly mediated ones. This seminar uses historical and contemporary case studies and theory to help you begin to make sense of global processes of change and of mass-mediated communication's role in these changes. It considers how the spread of U.S. popular culture and its appropriation by others has played overseas. We will examine processes through which European and non-Western cultures have blocked, taken up, and otherwise transfigured the forms of U.S. commercial cultural production at the national, transnational, and sub-national level. This seminar is designed to introduce you to critical terms and problems in processes of globalization and identity formation that are changing the ties that bind and differentiate citizens and peoples of the 21st century world. It is also designed to give you a college-level introduction to performing original research.

Seminar Rules: This is a discussion-driven seminar. Students must read assigned material in advance of each class meeting and arrive prepared with thoughtful questions and reactions. Attendance is required. Tardiness or chronic absenteeism is unacceptable. If you cannot attend a class for any reason, please notify the instructor in advance (see policy below). In general, more than two absences will diminish your participation grade.

Technology Policy: Because of the discussion-centered nature of this course, laptops, tablets, and ereaders are not allowed in class. Use of other personal electronics during class is strictly prohibited. Please turn off or silence and stow all electronics prior to the start of class.

Required texts: All assigned readings are available in .pdf form on CTools>Resources>Readings. You can read, download, and print these files. Students are STRONGLY encouraged to print copies so that you have ready access to the text in class due to the Technology Policy for this seminar. If you have trouble locating any assignment or notice a problem with an electronic file, please let me know immediately so that I can correct the issue. Thanks!

Requirements/Grading (I will post my grade scale in <Resources>)

Media Climate Interview/Self Inventory/Write Up (5%) Discussion Facilitation (5%) Attendance (10%) Discussion Participation (10%) Critical Essay (1000-1250 words/4-5 pp) (15%) Midterm (20%) Final Project (35%)

	   2
Requirements/Assigned Work. Unless indicated otherwise, all work is due at the start of class on the assignment date. A copy of the completed assignment should be uploaded as a Word document (no .pdfs) to your CTools <Drop Box> by 9:45 a.m. It is your responsibility to upload a file that I can open and read. If you have questions/concerns, talk to me in advance, or contact 764-HELP. Late work will be accepted, but marked down one third of a letter grade for each day (24-hr. period) that it is overdue (e.g., a paper that would have received a B+ will fall to a B if it is one day late, a B- if it is two days overdue, and so on). Suspected cheating, submission of work produced for a different class, or plagiarism, will result in a failing grade on the assignment. Further action by the UM Dean will likely follow. Please familiarize yourself with the LSA Code of Academic Integrity, as well as the definition of plagiarism. There is a document in CTools <Resources> explaining what plagiarism is, and how to avoid it. This syllabus is subject to change at the instructor’s discretion.
ASSIGNMENT OVERVIEW
[I will provide more detailed instructions in the days to come]
Media Climate Interview/Self Inventory/Write Up You and a randomly assigned Comm. 159 classmate will interview each other in class to gather as much information as you can about the other’s personal/familial media climate. (We'll define what we mean by "climate" beforehand). You will integrate what you learn into a self inventory of your own media climate, which you will write up in a reflective essay (3 pgs./750 words). Where appropriate you will draw your partner’s information to lend points of conceptual reinforcement, contrast, and insight.
Discussion Facilitation (I will pair you with a classmate to introduce and coordinate discussion of one of the assigned readings. You will prepare and jointly present a brief (7-9 minutes) introduction of the reading that lays out its key concerns, points out links to previous readings, and frames an agenda for discussion. You will point out key words, concepts (though you need not define them for the class). Your presentation will include three (3) open-ended questions about the reading to launch discussion. You are encouraged to be creative with your presentation and you are welcome to use presentational software (PowerPoint/Keynote/Prezi), and include a clip or artifact that helps illuminate the author's concerns. The grade for each pair will be based on demonstrating that you read and understood the material and on the quality of the presentation and the questions. The grades may vary based on each member’s contributions. Your written reports should be submitted to CTools <Drop Box> no later than the beginning of class on your discussion day.
Short Essay (4-5 pgs/1000-1250 words) You will write an analytical essay in response to a prompt asking you to work through a problem drawing on your knowledge of selected readings.
In-class Midterm (Identify and give significance of selected items/concepts from the first half of course in a paragraph. Write a brief essay responding to a prompt to analyze concepts/issues drawn from first half of course.
Final Project (1) Choose an original research topic in consultation with me; (2) Identify a research problem linked to it that draws on cases/theory from class; (3) Assemble an appropriate bibliography of sources; (4) write a 3-5 pp. secondary literature review exploring the problem; (5) Present a proposed research strategy to develop your project into an original scholarly essay); (6) Submit as a rough draft for comment; (7) Present project orally to class; (8) Resubmit revised final draft when classes end.

	   3

NOTE: You won't actually execute the final project this semester. Rather, you will develop the elements necessary to execute a top-quality research essay. More details to come!

ACCOMODATIONS

Illness: If you are sick, please go to http://lsa.umich.edu/students/. Click “What to do if You’re Sick” and complete the subsequent form. This will report your illness to all of your instructors. You must complete LSA’s form if you want to have an illness-related absence excused. If you are sick for an extended period of time, your absences will only be excused if you provide a doctor’s note in addition to filling out the LSA form.

Holidays: If a holiday is sufficiently important that you must miss class, you should know the dates in advance. Within the first two weeks of the semester, you must notify me of any religious holidays for which you will be absent.

U-M approved absences: If you are travelling to represent the U-M (e.g., sports), you will have received paperwork to distribute to your instructors. Within the first two weeks of the semester, you must notify me, and furnish your U-M paperwork.

Other absences: For personal emergencies (e.g., funerals) and other such absences, you must notify the office of the Assistant Dean of Student Affairs, who will then inform all of your instructors. The Assistant Dean’s executive secretary is Debbie Walls (dwalls@umich.edu). When you return to campus, you must bring documentation in support of your absence.

Disabilities: If you require special accommodations (http://www.umich.edu/sswd) please let me know as soon as possible. Please note that I cannot make any retroactive accommodations. Any information you provide will be treated as private.

1a/Sep 8

Introduction: What is a Media (Micro-)Climate?

1b/Sep 10

What is a Media (Micro-) Climate

#Ben Hubbard, “Young Saudis Bound by Conservative Strictures, Find Freedom on Their Phones,” 22 May 2015, New York Times.
In-class media climate interview exercise

2a/Sep 15

Nationalism and Communication

#Benedict Anderson, Imagined Communities (London, 1983), pp. 1-46.

2b/Sep 17

Nationalism and U.S. Broadcasting

#Lizabeth Cohen, "Encountering mass culture," in Making a New Deal, pp. 99-158.

3a/Sep 22

Ideology and National Identity

#Arthur A. Berger, “Ideological criticism” in Berger, Media and Communication Research Methods: An Introduction to Qualitative and Quantitative Approaches. 2nd ed. Los Angeles: SAGE Publications, 2011, 90-115.

	   4

Media Climate Self Inventory due

3b/Sep 24

Gender/Race/Sexuality

#Durham, Meenakshi Gigi. "Displaced Persons: Symbols of South Asian Femininity and the Returned Gaze in U.S. Media Culture." Communication Theory 11, no. 2 (2001): 201-17.

Distribute short essay assignment

4a/Sep 29

U.S./France Case Study: Popular culture, and the politics of the Cold War

#Kuisel, Richard, “The New American Hegemony,” in Seducing the French, the Dilemma of Americanization (Berkeley, 1993), 15-36.

4b/Oct 1

U.S./France Case Study: Popular culture, and the politics of the Cold War, cont.

#Kuisel, Richard, “Yankee Go Home, the Left, Coca-Cola, and the Cold War,” in Seducing the French, the Dilemma of Americanization (Berkeley, 1993), pp. 37-69.

5a/Oct 6

U.S./France Case Study: Hollywood, Paris, and Cosmopolitan Film Culture

#Schwartz, Vanessa, “Introduction” and “The Belle Epoque That Never Ended” in It’s So French!, pp. 153.

5b/Oct 8

Media & Globalization Processes

#Herman, Edward S., and McChesney, Robert. "The Rise of the Global Media." In Planet TV, a Global Television Reader, edited by Lisa Parks and Shanti Kumar, 21-37. New York: New York University Press, 2003.

Short essay due in class

6a/Oct 13

Media & Identity in a globalizing era

#Appadurai, Arjun. "“Disjuncture and Difference in the Global Cultural Economy." Theory, Culture & Society 7 (1990): 295-310.

6b/Oct 15

What is research?

#Berger, Arthur A. Introduction and Chapter 1, “What is Research,” Media and Communication Research Methods: An Introduction to Qualitative and Quantitative Approaches. 2nd ed. Los Angeles: SAGE Publications, 2011, pp. 1-27.

Think about a global media topic that you might want to pursue in advance of this class

7a/Oct 20

Midterm break

	   5

7b/Oct 22

In-class midterm review session

E-mail questions to me by 9 PM the day before class. Put “Comm 159 review” in message header.

8a/Oct 27

In-class midterm

8b/Oct 29

Film screening in evening/7 PM/Location TBA/no class meeting in a.m.

#Willems, Wendy. "Beyond Normative Dewesternization: Examining Media Culture from the Vantage Point of the Global South." The Global South 8, no. 1 (2014): 7-23.

9a/Nov 3

In-class visitor: Shevon Desai, communication studies librarian

Discuss film & Willems reading 250-word final project prospectus due via e-mail at start of class

9b/Nov 5

Beck [sic] to the Future: Cosmopolitanization

#Beck, Uhlrich, “The Cosmopolitan Society and Its Enemies,” Theory, Culture & Society 19 (April 2002), 17-44.

10a/Nov 10

Case Study: Cuba

#Beliso-de Jesus, Aisha. "Religious Cosmopolitanisms: Media, Transnational Santería, and Travel between the United States and Cuba." American Ethnologist 40, no. 4 (2013): 704-20.

10b/Nov 12

Case Study: Middle East

#Kraidy, Marwan M. "Reality Television, Gender, and Authenticity in Saudi Arabia." Journal of Communication 59 (2009): 345-66.

11a/Nov 17

Case Study: Korea

#Jung, Eun-Young. "Transnational Migrations and YouTube Sensations: Korean Americans, Popular Music, and Social Media." Ethnomusicology 58, no. 1 (2014): 54-82.

11b/Nov 19

Case Study: West Coast, USA

#Bahrampour, Tara, “Persia on the Pacific, Letter from Los Angeles,” The New Yorker 79.34 (November 10, 2003)
Rough draft of all final project elements due in <Drop Box>

12a/Nov 24

Case Study: Black America, USA

Bonilla, Yarimar and Rosa, Jonathan. #Ferguson: Digital Protest, Hashtag Ethnography, and the Racial Politics of Social Media in the United States,” American Ethnologist 42.1 (February 2005), 4-12.

	   6

12b/Nov 26

Happy Thanksgiving!

13a/1 Dec

TBA

13b/3 Dec

Presentations

14a/Dec 8

Presentations

14b/Dec 10

Presentations

Final draft of projects due by 5 PM Friday, December 11 in your CTools Drop Box.

