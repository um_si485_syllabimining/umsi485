PSYCHOLOGY 457: Emerging Adulthood Winter, 2015

Instructor: Office: Phone: E-mail:
Lecture/Class:
Office Hours:

Kathleen M. Jodl, Ph.D. 3266 East Hall (248) 459-2116 (cell) or (248) 344-8943 (Emergencies only!) jodlkm@umich.edu
Mondays & Wednesdays, 1:10 – 2:30 pm 373 Lorch Hall
Mondays & Wednesdays, 12 noon – 1 pm & by appointment

“It is human to have a long childhood; it is civilized to have an even longer childhood.”
~Erik Erikson
Course Objectives: This course provides an in-depth study of development during the period of emerging adulthood (ages 18 to 25+). We will examine the physical, cognitive, and socio-emotional growth of emerging adults and the contexts (e.g., families, peers, romantic relationships, workplace, culture, media) that influence their development. The goal is to provide an overview of the current state-of-theart regarding the transition to adulthood, and how larger historical, cultural, and economic forces impact emerging adults today. In this course, we use a multidimensional approach to learning in which various methods are used to convey information (e.g., discussion, text/readings, films) and test knowledge (e.g., exams, papers). Learning is viewed as a process through which the student is an active participant—not just a passive recipient.
A prerequisite for this class is PSYC 250 or a similar introductory child development course. Students are encouraged to think critically about the theory and research on emerging adulthood and to integrate this knowledge with their own observations of human behavior. Moreover, students should consider the practical implications of current research in the field of developmental psychology for emerging adults and social policy.

1

Required Readings:
Arnett, J. J. (2015). Emerging Adulthood: The Winding Road from the Late Teens Through the Twenties (2nd Edition). New York, NY: Oxford University Press.
Additional Materials:
The course pack is available on the CTools website. Please read the articles in the order listed below.
1. Henig, R.M. (2010, August 18). What is it about 20-somethings? The New York Times, 1-17. AND Stein, J. (2013, May 20). Millennials: The Me Me Me Generation. Time. Retrieved from http://time.com/247/millennials-the-me-me-me-generation/.
2. Arnett, J. J. (2007). Emerging adulthood, a 21st century theory: A rejoinder to Hendry and Kloep. Child Development Perspectives, 1, 80-82. AND Hendry, L.B., & Kloep, M. (2007). Conceptualizing emerging adulthood: Inspecting the emperor’s new clothes? Child Development Perspectives, 1, 7479.
3. Pew Research Center (2010). Millennials: Confident. Connected. Open to Change. Washington, DC: Author.
4. Parker, K. (2012). The Boomerang Generation (pp. 1-14). Washington, DC: Pew Research Center.
5. Morgan, E.M. (2012). Contemporary issues in sexual orientation and identity development in emerging adulthood. Emerging Adulthood, 1, 52-66.
6. Lehmiller, J.J., VanderDrift, L.E., & Kelly, J.R. (2011). Sex differences in approaching friends with benefits relationships. Journal of Sex Research, 48, 275-284.
2

7. Hymowitz, K., Carroll, J.S., Wilcox, W.B., & Kaye, K. (2013). Knot yet: The benefits and costs of delayed marriage in America (pp. 3-11). Charlottesville, VA: National Marriage Project.
8. Higher Education Research Institute (2014, March). The American freshman: National norms fall 2013. Los Angeles, CA: Author.
9. Pew Research Center (2014). The rising cost of not going to college. Washington, DC: Author.
10. Thompson, D. (2012, February 14). Adulthood, delayed: What has the recession done to the Millennials? The Atlantic. Retrieved from http://www.theatlantic.com/business/archive/2012/02/adulthood-delayedwhat-has-the-recession-done-to-millennials/252913/.
11. Coyne, S.M., Padilla-Walker, L.M., & Howard, E. (2013). Emerging in a digital world: A decade review of media use, effects, & gratifications in emerging adulthood. Emerging Adulthood, 1, 125-137.
12. Arnett, J.J. (2008). From “worm food” to “infinite bliss”: Emerging adults’ views of life after death. In R. Roeser, R.M., Lerner, & E. Phelps (Eds.), Positive Youth Development & Spirituality: From Theory to Research (pp. 231243). New York: Templeton Foundation Press. AND Pew Research Center (2012). Young votes supported Obama less, but may have mattered more. Washington, DC: Author.
13. Galambos, N.L., & Martinez, M.L. (2007). Poised for emerging adulthood in Latin America: A pleasure for the privileged. Child Development Perspectives, 1, 109-114.
14. Maggs, J. L., & Schulenberg, J.E. (2004). Trajectories of alcohol use during the transition to adulthood. Alcohol Research & Health, 28, 195-201
15. Ravert, R.D. (2009). “You’re only young once”: Things college students report doing now before it is too late. Journal of Adolescent Research, 24, 376-396.
3

Lecture/Discussion: PowerPoints for each mini-lecture can be downloaded from CTools at least 24 hours before class. Please print these out and bring them with you to class along with the textbook and required readings. Students are expected to take detailed notes in addition to the information provided on the slides. During class, please keep all noise to a minimum (i.e., no side conversations or pre-exit rustling). NO LAPTOPS OR CELL PHONES DURING CLASS TIME!
Exams: There will be 3 exams over the course of the semester (see Schedule of Course Topics for specific test dates). Each exam will consist of short answer and essay questions. These exams are NOT cumulative. Questions will be drawn from the readings, class lectures and discussions, presentations, guest speakers, etc. Make-up exams will ONLY be given in the event of an emergency and, in the case of illness, a doctor’s written excuse will be required. Each exam is worth 100 points. Taken together, these exams account for 60% of your final grade.
Emerging Adulthood Project: A major part of your grade this term will be based on a comparative research project in which you interview two individuals: 1) a person (18 to 25+ years) who has either not yet experienced a major adult transition OR has experienced one or more adult transitions early (e.g., full-time job, marriage, parenthood); and 2) a person (45 years or older) who experienced the transition to adulthood in an earlier time period. In addition to providing hands-on experience collecting and analyzing data, this project is designed to offer insight into the experience of emerging adulthood. Critical thinking is an important part of the assignment! A 1-page research proposal that includes interview questions and references is due in class on Wednesday, February 11th. The final paper should NOT exceed 6-8 double-spaced pages (not including title page & references) and is due on Wednesday, March 25th in class. The project is 100 points or 20% of your final grade. Detailed information regarding the project is available on CTools (see /Resources/Emerging Adulthood Project).
NOTE: Late papers will be penalized 5 points for every 24 hours past the due date (including weekends and holidays).
Group Presentation: A portion of your grade this term will be based on a 20minute in-class group presentation in which you focus on a single topic in the field of emerging adulthood. Your ability to critically think about the material at hand and work together as a team will be KEY aspects of this assignment. Each student will also fill out a Peer Evaluation Form and a Presentation Packet (due in class on
4

Wednesday, April 15th). The group presentation is 50 points or 10% of your final grade. Additional details regarding the presentation are available on CTools (see /Resources/EA Group Presentation).

Class Participation: Students are expected to actively participate in class discussions. A variety of methods will be used to facilitate discussion. For example, students might be asked to prepare a question or comment based on the readings. Alternatively, students might take a “poll” or write a brief response to a particular reading or topic. The ultimate goal is to engage students in learning and foster critical thinking skills. Your participation in this course will be based on a combination of class attendance, quality and quantity of engagement in discussions, and submission of questions and/or other activities throughout the semester. Class participation is 50 points or 10% of your final grade.

Grading Policy: This course is not graded on a curve; instead, your final grade will be based on the percentage of total points earned on exams, course project, group presentation, and class participation. Of the 500 total points, an A+ = 98-100%; A = 93-97%; A- = 90-92%; B+ = 88-89%; B = 83-87%; B- = 80-82%; C+ = 78-79%; C = 73-77%; C- = 70-72%; D+ = 68-69%; D = 63-67%; D- = 60-62%; F = below 60%.

Exam #1 Exam #2 Exam #3 Emerging Adulthood Project Group Presentation Class Participation

100 points 100 points 100 points 100 points 50 points 50 points

TOTAL:

500 points

Policies, Responsibilities & Other Helpful Hints

Students are expected to review the course syllabus and ask questions if anything is not clear; to attend all classes; and to be prepared and on time. Each student is responsible for studying the material and should not rely entirely on the instructor to present every detail. Students are expected to actively participate in discussions and to be considerate of classmates and the instructor. Cell phones & laptaps must be turned off during class time!

5

If you are having problems understanding the material or if you did poorly on an exam, please talk to the instructor as soon as possible. Do NOT wait until the end of the semester—it may be too late to remedy the situation! Any student with a disability or special situation that limits their performance in this course should contact the instructor to discuss appropriate accommodations necessary to complete the requirements. Special arrangements can be made for students with special needs, learning or physical disabilities if needed. Please notify the instructor if you have authorization to use these options.
All material (i.e., mini-lectures, discussions, textbook & course pack articles, films, guest speakers) is fair game for the exams. Questions will assess both factual information and conceptual knowledge. Be prepared!
Make-up exams will only be granted in the event of an emergency (e.g., hospitalization, serious illness, or funeral). If a serious emergency does arise and you are unable to take an exam, you must contact the course instructor within 24 hours of the missed exam. E-mail is NOT sufficient; you must contact me directly via the telephone. Documentation of the emergency will be required.
Despite my best intentions, mistakes can and do happen when recording grades. Therefore, it is always a good idea to keep a backup copy of your projects and assignments. All graded coursework (e.g., exams, projects, etc.) should be kept until the end of the semester. This is a large class and as such it is possible for projects to be misplaced or lost. The burden of proof is on you!
An incomplete (“I”) is given only in rare instances. Students are expected to plan ahead and to keep up with assignments throughout the semester. Extraordinary circumstances that may hinder a student’s progress in this course should be discussed with the instructor in a timely manner.
University standards of academic climate and integrity will apply to the students and the faculty member in this class. Please familiarize yourself with the definitions of academic misconduct. Cheating/plagiarism will not be tolerated and will result in a failing grade for the course, a formal complaint filed with the
Assistant Dean for Student Academic Affairs, and other really bad stuff. Please
see http://www.lsa.umich.edu/academicintegrity/index.html for more information about policies and procedures governing academic integrity and conduct.
6

Schedule of Course Topics & Assignments

Week Date 1 1/7
2 1/12 1/14
3 1/19 1/21
4 1/26
1/28
5 2/2 2/4
6 2/9 2/11
7 2/16 2/18

Topic

Readings

The Long & Winding Road to Adulthood Discussion Topic: The Me Me Me Generation?

Chapter 1 Reading #1

Debating Emerging Adulthood Discussion Topic: Undressing the “Emperor” Emerging Adults: Past & Present Discussion Topic: Millennials Rising

Chapter 2 Reading #2 Chapter 2 Reading #3

NO CLASS – Martin Luther King Relationships with Parents Discussion Topic: The Boomerang Generation

--Chapter 3 Reading #4

Love & Sex

Chapter 4

Discussion Topic: The New Homosexuality

Reading #5

Guest Speaker: Will Sherry, UM’s Spectrum Center

Love & Sex

Chapter 4

Discussion Topic: Friends with Benefits

Reading #6

Exam Review

EXAM #1 Cohabitation & Marriage Discussion Topic: Single Motherhood

--Chapter 5 Reading #7

The College Experience Discussion Topic: The American Freshman The College Experience Discussion Topic: Is College Worth the Cost?

Chapter 6 Reading #8 Chapter 6 Reading #9

Project Proposal DUE in class!!

Work & Career Discussion Topic: The Great Recession Work & Career Guest Speaker: Amy Longhi, UM’s Career Center

Chapter 7 Reading #10 Chapter 7

7

8 2/23 2/25

Digital Natives Discussion Topic: Plugged In & Alone? Exam Review EXAM #2

Chapter 8 Reading #11
---

9

3/2 & 3/4

NO CLASSES — Winter Break

---

10 3/9 3/11

FILM: Into the Wild FILM: Into the Wild

-----

11 3/16 3/18

Religion & Politics Discussion Topic: Election 2012 Culture, Ethnicity, & Social Class Discussion Topic: Diverse Pathways

Chapter 9 Reading #12 Chapter 10 Reading #13

12 3/23 3/25

Wrong Turns & Dead Ends Discussion Topic: Eating Disorders Wrong Turns & Dead Ends Discussion Topic: Substance Abuse Guest Speaker: Meghan Martz

Chapter 11 --Chapter 11 Reading #14

Emerging Adulthood Project DUE in class!!

13 3/30 4/1

Resilience in Emerging Adulthood Discussion Topic: The Happiness Advantage Group Presentations

Chapter 12 -----

14 4/6 4/8

Group Presentations Group Presentations

-----

15 4/13 4/15

Group Presentations Emerging Adulthood & Beyond Discussion Topic: YOLO Exam Review

--Chapter 13 Reading #15

Presentation Packet DUE in class!

16 4/20

EXAM #3

---

______________________________________________________________________

8

9

