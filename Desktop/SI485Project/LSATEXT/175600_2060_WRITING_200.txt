Sessolo 1
W 200: Ethos and New Media
Instructor: Simone Sessolo Email: sessolo@umich.edu Office: 1320 NQ Office Hours: T 3:00 pm, W 11:30 am, & by appointment. Office Phone: 647-4526 Class Meeting Time: M W 2:30 pm Class Meeting Place: 2230 USB
INTRODUCTION TO THE COURSE: Classical rhetoric defines ethos as the skilled presentation of a writer’s character. In fact, the ancient Greek rhetorician Isocrates claims that character plays an important part in persuasion. However, there are differences between establishing character in the fixity of printed-paper and in the apparent impermanence of web 2.0. In this course we will think critically about this classical concept and understand its implementation in new media venues: the blogger's ethos, the social media ethos (Facebook, Twitter, Yik Yak), and the visual ethos (Pinterest, Instagram, Vine). This course will introduce students to elements of authorial presentation in the many social situations of web 2.0, and we will try to answer this specific question: how can we present ourselves in these platforms? Throughout the course, students will actively engage with these social platforms, reflecting on the differences and similarities between real and virtual personas. Students should be familiar with the web to take this course, but they do not need any other specific technical expertise. The course will end with participation in the interactive exhibition Self[ie] Awareness, Monday November 30 – Friday December 11 2015, Space 2435 North Quad.
REQUIRED MATERIALS: There are no required texts for the course, and all reading materials can be found on Canvas. However, consider spending some money to update the format of the media platforms we’ll be using.
GRADES: Grades in this course will be determined by the use of The Learning Record, a system that requires students to compile an e-portfolio of work at the midterm and at the end of the semester. These eportfolios present a selection of your work, both formal and informal, plus ongoing observations about your learning, plus an analysis of your work development.
WRITING ASSIGNMENTS: • LR (A.1 and A.2; B.1 and C.1; B.2 and C.2) • 10 Observations • An e-portfolio platform • An in-class presentation of your e-portfolio • An in-class group presentation of a media platform • Contribution to the exhibition Self[ie] Awareness
The Gayle Morris Sweetland Fund If financial need makes it difficult for you to purchase books, a laptop computer, or other classroom materials, the Gayle Fund (established in honor of Gayle Morris Sweetland) can loan you the necessary materials. You can talk to your instructor about how to apply, or you can get an application form from the receptionist at the Sweetland office at 1310 North Quad.

Sessolo 2
DAILY SCHEDULE: This schedule is not set in stone. It may be necessary for me to make some changes as we go. I will alert you of eventual changes as soon as possible. If you are absent, it is your responsibility to check with me to see if the schedule has been amended. Always bring the assigned reading material to class with you!
Week 1
W Sep 9: Introductions Note: LR observations must be posted in your e-portfolio by the end of Fridays
UNIT 1: (THEOR)ETHICAL FOUNDATIONS
In this unit, we will engage with theories about ethos that characterized Western thought. The purpose of these readings is to help you think critically about character presentation. In short, we’re training our thinking by trying to understand a rhetorical methodology. Your task for this unit is to explore the canon with an open mind, question the assumptions that do not convince you, and apply these theories to your contemporary world and new media. Your assignments for this unit will be mostly active participation in class discussion, informal weekly written observations, and a reflective interview to situate yourselves as users and composers of new media.
Week 2
M Sep 14: The Learning Record Debate on character Readings:
• LR.doc, LR_reference.doc, and Assessment.doc • David Brooks, “The Character Factory” • David Coates, “Responding to David Brooks: The Question of Poverty and Character”
W Sep 16: Lecture The Occasional Ethos In class: setting up of e-portfolios
F Sep: 18: Observation #1
Week 3
M Sep 21: Isocrates and Aristotle Readings:
• Isocrates: Antidosis (75-79) • Aristotle: Book II (179-186, 213-219) Assigments: • LR A.1 • LR A.2

Sessolo 3
W Sep 23: Cicero and Quintilian Readings:
• Cicero: De Oratore (297-300, 328-331) • Quintilian: Book XII (412-418)
F Sep 25: Observation #2
Week 4
M Sep 28: Burke Readings: A Rhetoric of Motives (1324-1340)
W Sep 30: Foucault Readings: The Archaeology of Knowledge (1436-1445)
F Oct 2: Observation #3
UNIT 2: MEDIATED ETHOS
In this unit, we will explore social platforms that you might use to present your character to an audience. After establishing personal blogs that you will use as self-presentation, you will actively engage with several kinds of new media. With “actively engage” I mean that you will have freedom to choose and research one social platform, in groups, and then lead a class session on that platform. I want to stress that these group presentations are not a test or a quiz. I want you to be curious, methodical, and interested. That is why it will be your choice to study whatever platform appeals to you the most. You are free to organize our class session in any way you like, coming up (with me) with a specific purpose for your research and your group presentation. I’m giving you carte blanche to build your knowledge in the way you see fit.
Week 5
M Oct 5: The Blogger’s Ethos Readings:
• Sullivan, “Why I Blog” • Blood, “Weblogs: A History and Perspective”
W Oct 7: The Blogger’s Ethos Readings:
• Blood, “Weblog Ethics” • Blood, “Ten Tips For a Better Weblog”
F Oct 9: Observation #4

Sessolo 4
Week 6
M Oct 12: The Blogger’s Ethos Assignments: In-class presentation of e-portfolios, I.
W Oct 14: The Blogger’s Ethos Assignments: In-class presentation of e-portfolios, II.
F Oct 16: Observation #5
Week 7
M Oct 19: Fall break, no class.
W Oct 21: Midterm Assignments: In-class workshop on LR B.1 (midterm analysis) and C.1 (midterm evaluation) LR B.1 must be uploaded on your e-portfolio, and LR C.1 must be sent via e-mail to me, by the end of Friday, October 23
Week 8
M Oct 26: The Social Media Ethos Readings:
• Zappavigna, “Ambient Affiliation” • Brittain Richardson, “Front-Stage and Back-Stage Kantian Ethics: Promoting Truth and
Trust in Social Media Communities” • Elliott, “The Real Name Requirement and Ethics of Online Identity” • Papacharissi and Mendelson, “Toward a new(er) sociability: uses, gratifications, and social
capital on Facebook”
W Oct 28: Facebook Assignments: Group presentation
F Oct 30: Observation #6
Week 9
M Nov 2: Twitter Assignments: Group presentation

Sessolo 5
W Nov 4: Yik Yak Assignments: Group presentation
F Nov 6: Observation #7
Week 10
M Nov 9: The Visual Ethos Readings:
• Blair, “The Rhetoric of Visual Arguments” • Foss, “Theory of Visual Rhetoric” • Rice, “Imagery”
W Nov 11: Pinterest Assigments: Group presentation
F Nov 13: Observation #8
Week 11
M Nov 16: Instagram Assignments: Group presentation
W Nov 18: Vine Assignments: Group presentation
F Nov 20: Observation #9
Week 12
M Nov 23: E-portfolio day
W Nov 25: Thanksgiving, No Class
UNIT 3: SELF[IE] AWARENESS
This unit will be the culmination of the theories we read in unit 1, and the platforms we played with in unit 2. In this unit, you will become new media composers, and you will contribute to a digital exhibition on the selfie. You will produce a series of selfies and reflect on how your character is reflected through those. The purpose of this unit is to let loose your educated creativity and become new media composers. The purpose is also to exit the academic boundaries of the classroom and participate in the worldly debate on character presentation. At this point in the semester, you can consider yourselves as multimodal intellectuals. In this unit you will also be able to include

Sessolo 6
anything you’ve been working on in your blog, so that it will become a multimodal platform that expresses the evolution of your understanding of character to a larger audience.
Week 13
M Nov 30: Intro to Self[ie] Awareness Readings:
• http://selfieawareness.wix.com/sweetland • Kress, “Gains and Losses: New Forms of Texts, Knowledge, and Learning” • Deuze, “Media Life” • McLuhan, Understanding Media [Introduction, The Medium is the Message]
W Dec 2: Exhibition and workshop
F Dec 4: Observation #10
Week 14
M Dec 7: Exhibition and workshop
W Dec 9: Exhibition and workshop
Week 15
M Dec 14: Last day of class. • Course Evaluations • Workshop on LR B.2 (Final Analysis) and C.2 (Final Evaluation)
Submit complete final e-portfolio, and C.2 in an email to me, by the end of Fri, December 18.
Good luck on your finals!!!

Sessolo 7
REQUIREMENTS:
At the end of the semester, you will submit revised versions of your writings for the course as an eportfolio. The e-portfolio is intended to showcase what you have learned about ethos and new media, and what you have learned as a new media composer. It is a way to think about how far you have come. You can choose the platform you prefer: Wordpress, Tumblr, Blogger, etc. Setting up this platform will allow you start creating an online persona, and you will be able to post artifacts that you will compose during the semester. There is no required minimum for posting, and no limit to your imagination. Consider this as a space that reflects who you are. The e-portfolio will include the following parts:
Part A.1: Interview (500-750 words) Interview with a person who knows you well—for example, a parent or other family member, close friend, or teacher. Please identify the person (first name only) and how long she has known you. You are free to choose what questions to ask, but the interview should focus on the person’s impressions of your development as a reader, composer, thinker, and your attitude toward new media.
Part A.2: Reflection (250-500 words) Reflect on your own development with respect to reading, composing, thinking, and your attitude toward new media, based on your interviewee’s responses and the course strands.
Part B.1: Midterm Analysis of Data (750-1000 words) Develop your summary interpretation of your development in terms of the major course strands and the five dimensions of learning (See Assessment.doc). Be sure to connect your interpretations with specific examples included in the observations and samples of work—your work is the evidence of your development.
Part B.2: Final Analysis of Data (750-1000 words) Develop your summary interpretation of your development in terms of the major course strands and the five dimensions of learning (See Assessment.doc). Be sure to connect your interpretations with specific examples included in the observations and samples of work—your work is the evidence of your development.
10 Observations (up to 250 words) Please make sure that observations have some relationship to the readings, your activities, and your learning in the class (or your difficulties with learning something). Please also focus on what you have actually observed or read, rather than your judgments about it. This requires some care and precision, but not great length. Most observations will be brief and informal, just up to a page double-spaced. Observations should be recorded within 48 hours of what has been observed— sooner is much, much better in terms of the accuracy of the account. Observations could, for example, respond to readings and assignments, demonstrating an understanding of them and putting them into conversation with one another. You can engage ideas, question them, and build on them.

Sessolo 8
The following assignments will be sent to me in a personal e-mail:
Part C.1: Midterm Evaluation and Grade Estimate (up to 250 words) Include here any comments you’d like to add, especially concerning: your estimated evaluation in terms of the grade criteria for the course; suggestions for your own further development during the remainder of the term; suggestions for class activities or for me to better support your learning. Then, indicate your midterm grade estimate.
Part C.2: Final Evaluation and Grade Estimate (up to 250 words) Include here any comments you’d like to add, especially concerning: reflections on your learning experience in the course; any supplementary information or comments not included in Parts A and B; any suggestions for me for future classes. Then, indicate your final grade estimate.
OTHER REQUIREMENTS:
In-class Presentation of Your e-portfolio During Week 6 you will be asked to share your e-portfolio with the class, in an effort to engage in peer-critique and receive suggestions about issues you might not have thought of. You will have 5 to 7 minutes to share your e-portfolio with the class. I want to stress that there is no right or wrong way to present your e-portfolio, as there is no right or wrong e-portfolio. Presenting your eportfolio serves to practicing your ethos. My hope is that we’ll build a community of composers interested in the selfless progress of everyone in the class.
Group Presentation of a Media Platform During Weeks 8-11 we will have in-class group presentations on a media platform. You will sign up for a group based on your personal interest: if you are particularly interested in Facebook, you will sign up for the Facebook group; if you are particularly interested in Vine, you will sign up for the Vine group. Each group will be comprised of 3-4 people. As with presenting your blog, I do not want you to think of these presentations as tests or quizzes. Rather, this is an occasion for you to explore with curiosity, passion, and method, the platform that interests you the most. For your presentation, your group will have available the whole class time. As a group, you are free to coordinate our activities for that day in the way you believe more appropriate. You will be in charge of the class, and your classmates will rely on your work to learn about your specific platform. You can come up with activities, games, lecturing moments, and anything that you see fit as a group. Do not forget the aspect of play in every learning activity. These presentations are supposed to be something that you enjoy because of your interests—and if you enjoy researching something, the rest of the class will enjoy listening about it.
Contribution to Self[ie] Awareness We will spend the last two weeks of class contributing to a digital exhibition in North Quad Space 2435, and workshopping our blog portfolios. You will be asked to take a series of 4 selfies and reflect on your series: how does it present who you are to an audience? These final two weeks are intended to bring together the theory you’ve read during the semester, your knowledge of media platforms, and your passion in new media composition.
Writing Conferences: I am eager to help you develop as a composer by way of one-on-one conversations about your work in the course. You are required to schedule one one-on-one meeting with me this semester to talk about one of the requirements. Failure to attend at least one one-on-one meeting with me will count as an absence. Generally, however, you should think of my office as an

Sessolo 9
extension of the classroom, and you should feel free to use my office hours to discuss your learning whenever you like. My door is always open for you.
Class Participation: This class focuses heavily on you, the composers. As you are an integral part of class, if you come unprepared the whole class suffers. You must come to every class prepared with your class notebook, handouts, texts we are working with, and completed assignments. If you come unprepared, it counts as an absence. Note that because this is not a lecture-based class, you are required to participate in discussions and activities in every class. You should use class participation as evidence of your learning when you compile your Learning Record.
Attendance: You are required to exhibit dependability and professionalism in this course, hence I expect you to attend every class. If you must be absent because of an emergency, illness, religious observance, or university sporting event, you should speak with me about it beforehand, if possible. If you reach FOUR non-excused absences, you will not receive a passing grade for the class. Be aware that if you miss a class, it is your responsibility to get notes and assignments, complete work, and submit papers. Be sure to obtain another student’s email address so that you have someone to contact in the event of an absence.
Timeliness: We will run on Michigan time, and it is your responsibility to arrive to every class on time. If you arrive late, I will count your late arrival as half of an absence. In other words, two late arrivals equal one absence. Likewise, it is your responsibility to submit all writings and homework assignments by the time indicated in the daily schedule. Late submissions will affect your grade.
PLAGIARISM: The LSA Student Bulletin defines plagiarism as “representing someone else’s ideas, words, statements or other works as one’s own without proper acknowledgment or citation” (see http://www.lsa.umich.edu/bulletin/chapter4/conduct). If I find evidence of willful plagiarism in any formal or informal student composition, I will report the incident to the Dean for Academic Affair and you will fail the class. Talk to me if you have questions about whether or not you have concerns about committing plagiarism, and always cite all sources that influence your arguments.

