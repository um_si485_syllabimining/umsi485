AMCULT 213 LATINOAM 213 Introduction to Latina/o Studies
Fall 2015 Instructor: Dr. Anthony Mora
Office: 3767 Haven Hall Office Phone: 734-615-5766 E-mail: apmora@umich.edu
REQUIRED TEXTS:
Díaz, Junot. The Brief Wondrous Life of Oscar Wao. Riverhead Books, 2008. ISBN: 1594483299.
Molina, Natalia. How Race is Made in America: Immigration, Citizenship, and the Historical Power of Racial Scripts. (Berkeley: University of California Press, 2013). ISBN: 9780520280083.
Wherry, Frederick. The Philadelphia Barrio: The Arts, Branding, and Neighborhood Transformation. (Chicago: University of Chicago Press, 2011). ISBN: 0226894320.
Course Packet Reading as indicated in Course Schedule. Course Packet Reading is available through Canvas
DESCRIPTION:
Latino and Latina Studies is an interdisciplinary approach to the study of Mexican, Puerto Rican, Cuban, Caribbean, Central American, and Latin American communities in the United States. This course will offer an assortment of rubrics for understanding the interconnections between diverse Latina/o communities and also the differences that sometimes divide them. Students will explore the many methodologies of Latina/o Studies by taking the opportunity to meet and learn from scholars across disciplines (including history, sociology, film studies, literary studies, and more) engaged in this field of study. The class will consist of a series of lectures/projects designed in conjunction with scholars, activists and cultural practitioners working in different areas of Latino/a Studies at the University of Michigan and beyond.
Our objectives for this class will center on building critical interpretative skills. By the end of the semester, students should be able to ask interesting, important questions about the experiences and representations of Latino/as in the U.S. Moreover, students should have the skills to start to answer those questions by knowing some interdisciplinary methodologies.
1

We will always approach this topic from an academic standpoint and we will maintain a professional atmosphere within the classroom. Some individuals, for a variety of reasons, might be uncomfortable with the course content and direction. These students should discuss their concerns with the instructor as soon as possible to determine if this class is a good choice for their needs.
FORMAT:
This course will involve a combination of lecture and seminar settings. Because of the unique number of guest lectures and eclectic reading list, it might feel a bit disjointed. Together, though, we will work to make sense of it all. We will explore the major themes of each class by discussing the readings and lectures as a group. All students will be expected to attend lecture and come prepared by having read the assigned reading to engage in the group discussion. Attendance and active participation are expected, with the understanding that missed classes and/or discussion sections without university-approved excuses will be penalized as outlined below.
REQUIREMENTS AND ATTENDANCE POLICY:
Students should expect reading and writing assignments appropriate for an intro-levelhumanities class. The final grade will be computed based on the assignments described below: Attendance and participation (worth 200 points, ~20% of final grade); two exams (worth 250 points each) indicated in the course schedule (total 500 points, ~50% of final grade); and a final project (worth 300 points, 30%). Letter grades will be assigned based on percent of total points as follows: A= 90-100, B= 80-89, C= 70-79, D= 60-69, F = 0 - 59.
Attendance, Participation, and Discussion Questions (200 Points):
Successful students will attend and participate in class discussions. Because this class is focused on learning from each other, students need to attend in order to listen to their colleagues’ ideas. It is not required that students speak in every class. To get passing credit for their attendance and participation, however, students must verbally contribute to at least four class discussions. Those students who participate more frequently and/or provide more qualitative comments will receive a higher mark for their participation grade. Students should also be aware that I will occasionally call on individuals to answer questions in class about the reading or the lectures to ensure they are keeping up with their assignments. If a student cannot answer a question, he/she will lose points in their participation grade. Please note, simply attending class is not considered “participation.” Each student should arrive having read the assigned reading.
Because this course depends heavily on group discussion, un-excused absences or tardiness will adversely affect your grade. Even a single absence can impact your grade. After the third un-excused absence, a student will receive zero points for
2

attendance and participation (regardless of how much they might have contributed when they did attend class). After the eighth un-excused absence, a student will fail this course (regardless of their other grades). Tardiness to class will count as “half” unexcused absence.
Sleeping, text messaging, and using the internet in class is disruptive and disrespectful to your peers and to the professor. So, too, we will not allow the use of laptops or cell phones during lecture or discussion unless it is required for a documented learning disability. See the instructor if this is the case. Students who engage in these activities during class or the discussion section will be asked to leave and will be counted as absent without a university approved excuse.
Students who will miss class due to a “university approved” event (religious holy day, athletic team, major surgery, death in the immediate family, court appearance, etc) must contact the instructor as soon as possible. In order for absences to be considered “excused,” students must also be able to provide supporting documentation within a week of missing class. Please note, family vacations or illness without a doctor’s note are not excused absences. As per student regulations, if a student is absent an unusually long time for excused reasons (medical problems, etc.), the student will be encouraged to meet with their academic advisor to develop a plan for salvaging the semester’s work.
Exams (500 Points Total):
We will have two exams over the course of the semester (each worth 250 points) during discussion sections. These exams will cover reading, discussion, and lectures from each half of the semester. The format will involve both multiple choice and short answer. Consult the course schedule for dates.
Final Projects (300 Points):
Students have three options for the final project. Each of these projects involves a series of short preparatory assignments (3 total) spaced at regular intervals throughout the semester. The short assignments will help you get an early start on your final project so that you are not scrambling to finish at the end of the semester. You must decide which final project that you would like to undertake by the first week of October. These descriptions below are brief overviews of your final project options:
Option No. 1: Wikipedia Project (Collaborative Project)
Wikipedia has emerged as one of the first sources that most people turn to for quick information. Yet, Latino/a topics, persons, or organizations remain woefully under-represented on this forum. Students will work ins pairs or
3

groups of three from their discussion sections to create a unique Wikipedia entry dedicated to specific Latina/o experiences in the U.S. These projects can either be focused thematically (e.g. an entry dedicated exclusively to George I. Sánchez) or broadly (e.g. a history of all Latina women in the U.S.). Whatever the case, students must produce several “drafts” that are both educational and entertaining for visitors drawing from lectures, readings, and their own research. The final entry should be equivalent to 810 pages of written content.
Option No. 2: Media Project (Collaborative Project)
Students will create a visual media project that illuminates the experiences of Latina/os in the U.S. These might be either fictional or not. Whatever the case, however, the content must be supported by course materials and be accurate.
Option No. 3: Essay on Race and Citizenship (Individual Project)
Students who select this option will write a comprehensive paper between 810 pages that explores the changing meaning of race and citizenship for Latino/as in the United States. This paper will draw from lecturers, the assigned reading, and also two additional sources beyond the course materials. These papers will need to chart both the legal challenges and also social consequences of those changes.
PENALTIES FOR LATE WORK:
Late papers and projects incur severe grade penalties. A paper will receive an automatic grade reduction of one letter grade if it is not turned in at the start of class time on the date due. Each additional calendar day that passes will result in an additional grade reduction of one letter grade (the “late clock” includes days we do not meet for class sessions). For example, if a student turns in a paper on Wednesday that was originally due on Monday, their “A” would turn into a “C.”
Some latitude can be granted to students with approved excuses provided they contact me before the due date. I never accept computer/printer problems as legitimate excuses. Be sure to back up your work often, print it early, and save it in multiple locations (on your hard drive, a flash drive or cd, and keep a paper copy). If ever there is a question about a paper, I will ask students to present a new copy of the paper (failing to save it is not an excuse). There is no possibility of turning in the final project in late.
APPOINTMENTS:
I am always available and eager to meet with students to discuss concerns, their assignments,
4

or simply chat. I will have office hours from 12:00 to 1:00 pm every Monday and Wednesday in my office (3767 Haven Hall). If these times are not convenient, students should feel free to schedule an appointment by e-mailing me at apmora@umich.edu. Students can expect a reply to e-mail after twenty-four hours. AMERICANS WITH DISABILITIES ACT (ADA): The Americans with Disabilities Act (ADA) is a federal anti-discrimination statute that provides comprehensive civil rights protection for persons with disabilities. Among other things, this legislation requires that all students with disabilities be guaranteed a learning environment that provides for reasonable accommodation of their disabilities. If you believe you have a disability requiring accommodation, please contact the Office of Services for Students with Disabilities in G-664 Haven Hall, or call 734-763-3000. It is the student’s responsibility to contact the Department of Student Life and the professor. ACADEMIC DISHONESTY/PLAGIARISM/PROFESSIONALISM IN THE CLASSROOM: The University of Michigan presumes the integrity of its students. Students betray the Wolverine tradition by plagiarizing or participating in other forms of academic dishonesty. Plagiarism includes failing to credit sources used in your work and/or attempting to receive credit for work performed in part or in whole by another person. The Office of the Assistant Dean for Student Academic Affairs outlines the meaning of “Academic Dishonesty and Plagiarism” at http://www.lsa.umich.edu/academicintegrity/examples.html. Plagiarism is a serious offense and will result in receiving an “F” on the assignment and failing this course. Students might also face consequences from the Office of Academic Student Affairs. Every student has an opportunity to express her/his ideas about the course material during class discussions. Each student will listen to all of the ideas and respect his/her colleagues. When responding to other students or professors, students are always courteous and professional.
5

SCHEDULE: (THIS IS TENTATIVE AND MAYBE OPEN TO CHANGE OR REVISION)
WEEK ONE (September 9):
READING (Due September 14):
Rachel St. John, “A New Map for North America,” from Line in the Sand, (Princeton: Princeton University Press, 2011), pp. 12-38, Canvas.
Mora, “Preoccupied America,” in Border Dilemmas: Racial and National Uncertainties in New Mexico, 1848-1912, (Durham: Duke University Press, 2011), pp. 23-65
Introduction
September 9: Course Introduction, Requirements, Syllabus, Attendance Policy, Thinking About Latino/a Studies (Mora)
WEEK TWO (September 14-18):
READING (Due September 21):
Natalie Lira and Alexandra Stern, “Mexican Americans and Eugenic Sterilization: Resisting Reproductive Injustice in California, 1920-1950,” Aztlan: A Journal of Chicago Studies, 39:2, Fall 2014, pp. 9-34, Canvas.
Stephen Zuckerman, Timothy A. Waidmann, and Emily Lawton, “Undocumented Immigrants, left Out of Health reform, Likely to Continue to Grow as Share of the Uninsured,” Health Affairs, 30:10 (2011): 1997-2004, Canvas.
September 14: Mexico’s War for Independence (Mora)
September 16: U.S.-Mexican War (Mora)
WEEK THREE (September 21-25):
Reading (Due September 28):
Molina, How Race is Made, pp. 1-43
September 21: Mexican Americans in the Nineteenth Century: The First “Latinos” (Mora)
September 24: Latina/os and Health GUEST LECTURE: Alexandra Stern
6

WEEK FOUR (September 28- October 2): Reading (Due October 5): Molina, How Race is Made, pp. 44- 67
September 28: Spanish-American War, 1898 (Mora) September 30: Mexican Revolution, Part I (Mora) WEEK FIVE (October 5-9): READING (Due October 12):
Molina, How Race is Made, pp. 68-111 October 5: Mexican Revolution, Part II (Mora) October 7: Latinos in the Depression (Mora) WEEK SIX (October 12-16): READING (Due October 19):
Molina, How Race is Made, pp. 112-152 Díaz, Oscar Wao, pp. 1-76 October 12: Latino/as Abroad During World War II (Mora) October 14: Latino/as on the Homefront (Mora) WEEK SEVEN (October 19-23): READING (DUE October 26): “Cuba’s Refugees: Manifold Migrations,” Silvia Pedraza (Canvas) Deborah Cohen. “Border of Belonging, Border of Foreignness: Patriarchy, the Modern, and Making Transnational Mexicanness” in Braceros, (Chapel Hill: University of North Carolina Press, 2011), pp. 173-198. Díaz, Oscar Wao, pp. 77-118
7

October 19: FALL BREAK
October 21: EXAM I
WEEK EIGHT (October 26-30):
READING (DUE November 2):
“El Plan Espiritual de Aztlán,” Canvas
Corky González, “Yo Soy Joaquin,”Canvas
Mirta Vidal, “Chicanas Speak Out!”, Canvas
“Young Lords Party 13 Point Platform,” Canvas
Calvo-Quirós, William,”Sucking Vulnerability: Neo Liberalism, the Chupacabras, and the Post Cold-War Years,” in The Un/Making of Laitna/o Citizenship: Culture Politics & Aesthetics, Ellie D. Hernández and Eliza Rodríguez Gibson, eds, pp. 212232 Canvas.
Calvo-Quirós, William, “Chupacabras, the Strange Case of Carlos Salinas de Gortari and his Transformation into the Chupatodo,”in Crossing the Borders of the Imagination, María del Mar Ramón Torrijos, 95 108, Spring 2014, pp. 89-102, Canvas.
Díaz, Oscar Wao, pp. 119-201
October 26: Latinos at Midcentury (Mora)
October 28: Cuban Migrations GUEST LECTURE: Silvia Pedraza, Sociology
WEEK NINE (November 2 - November 6):
Reading (Due November 9)
Díaz, Oscar Wao, pp. 202-335
Lilia Fernández's Brown in the Windy City: "Mexican and Puerto Rican Labor Migration to Chicago" (33 pages) and "Putting Down Roots: Mexican and Puerto Rican Settlement on the Near West Side, 1940-1960" (43 pages)
8

Matthew Lassiter, “Pushers, Victims, and the Lost Innocence of White Suburbia: California’s War on Narcotics during the 1950s, Journal of Urban History, 1:21, 2015, pp. 1-21, Canvas.
November 2: Sixties and Latino Militancy (Mora)
November 4: Surprising Appearances of Chupacabras GUEST LECTURE: William Calvo-Quirós
WEEK TEN (November 9-13):
READING (Due November 16):
La Fountain, “Nuyorico and the Utopias of the Everyday,” Queer Ricans, pp. 131168, Canvas.
La Fountain, “Translocas: Migration, Homosexuality, and Transvestism in Recent Puerto Rican Performance,”in Hemispheric Institute E-Misférica, (http://hemisphericinstitute.org/hemi/en/e-misferica-81/lafountain), Canvas
Gonzalez, Fox, and Noriega, “Phantom Sightings: Art After the Chicano Movement,” Canvas
November 9: Latina/os and the Music Industry Guest Lecture: Jesse Hoffnung-Garskof
November 11: Latino/as and Policing Guest Lecture: Matthew Lassiter
WEEK ELEVEN (November 16-20):
READING (Due November 23):
Alma Garcia, “The Development of Chicana Feminist Discourse, 19701980,”Gender and Society, Vol. 3, No.2, 1989, pp. 217-238, Canvas.
Maria Cotera, “Practices of Chicana Memory Before and After the Digital Turn,” ¡Chicana! New Narratives of Chicana Activism and Feminism in the Chicano Movement Era, forthcoming, pp. 1-20, and Images (separate file), Canvas.
Maylei Blackwell, Chicana Power!: Contested Histories of Feminism in the Chicano Movement, pp.1-42, Canvas.
November 16: Latino/a and Queer Studies Intersections Guest Lecture: Lawrence La Fountain-Stokes
9

November 18: Chicano/a Art Guest Lecture: Colin Gunckel
WEEK TWELVE (November 23-25): READING (Due November 30): Díaz, Oscar Wao, pp. 202-335 November 23: Chicana Feminism Guest Lecture: Maria Cotera November 25: Thanksgiving Holiday
WEEK THIRTEEN (November 30– December 4): Reading (Due December 7): Frederick Wherry, The Philadelphia Barrio, pp. 1-71 November 30: Latino/as During the 1970s and 1980s (Mora) December 2: Visit to UMMA (Mora)
WEEK FOURTEEN (December 7-11): READING (Due December 14): Frederick Wherry, The Philadelphia Barrio, pp. 72-168 December 7: Militarization and the Millennium (Mora) December 9: EXAM II
WEEK FIFTEEN (December 14) READING: None December 14: Wrap Up
FINAL PROJECT DUE ON DECEMBER 16 BY 5:00 PM. NO LATE PROJECTS WILL BE ACCEPTED.
10

Latino/a Studies Major and Minor American Culture’s constituent Latina/o Studies Program offers highly motived students opportunities to major and minor. We seek students who are interested in socially committed scholarship and teaching. Latina/o studies majors have gone on to have successful careers in law, medicine, business, marketing, museum studies, or graduate school in the humanities and social sciences. A major or minor in Latina/o Studies shows that an individual has cross-cultural competencies and can engage meaningfully with the nation’s largest minority population. It makes an ideal second major that allows students a challenging and rewarding sense of community within the larger College and University experience.
Latina/o Studies Major Requirements (27 Credit Hours)
Prerequisites: AMCULT/LATINOAM 213 (3 credits) Language Requirement: All majors must satisfy 4th term proficiency in Spanish or another relevant language approved by the Program Advisor. Required Distribution Courses:
Students are required to take one 3-credit course that focuses on Latina/os in each of the following areas: History and Society; Gender and Sexuality; Language and the Arts; Media and Popular Culture; and Community Service Learning. Electives: Two 300 or 400-level courses in Latina/o Studies Cognates: Two courses outside the Latina/o Studies curriculum, one each from: A. Latin American culture, history, literature B. Asian/Pacific Islander American Studies; African American Studies; Native American Studies; or Arab and Muslim American Studies
11

