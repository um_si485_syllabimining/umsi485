SCIENCE AND OBJECTIVITY
Phil 381 Fall 2015 MoWe 2:30PM – 4:00PM, 142 Lorch
DESCRIPTION
It is a common view that science has special authority because it is based solely on objective empirical evidence and is therefore independent of any merely subjective social values. However, in his classic Structure of Scientific Revolutions, Thomas Kuhn has challenged this view, and his work has prompted a heated debate over the precise role of various kinds of values in science. In this course we will join this debate, considering not only Kuhn’s Structure but also recent work in the sociology of science and the philosophy of science. For the final sections of the course, we will focus in particular on Philip Kitcher’s Science, Truth, and Democracy and Helen Longino’s The Fate of Knowledge.
TEXTS
Kitcher, Science, Truth, and Democracy (Oxford, 2001) [STD] Kuhn, The Structure of Scientific Revolutions, 4th edition (Chicago, 2012) [SSR] Longino, The Fate of Knowledge (Princeton, 2002) [FK]
Other readings are available on the Resources section of the C-Tools course website
REQUIREMENTS 1. Class Participation
It is assumed that you will prepare by completing required readings before each lecture and discussion section meeting, and that you will participate in discussion particularly in your section. Lecture and section attendance and participation are worth 20% of your final grade.
Half of your participation grade is determined by attendance of and participation in your discussion section. Regular attendance without participation = B+ and 5 or more unexcused absences = F for that portion of your grade.
The other half of the participation grade is determined by your attendance of lecture and participation in class discussion, as well as by your weekly submission of reading responses that include questions about the readings and/or a short paragraph highlighting an aspect of the readings that you found interesting. To receive full credit, you must do all of the following: (1) Email your reading response to me and your section leader by 9AM of the day of the class in which the readings are covered; and (2) Retain one copy for your own possible use in class or section discussion. 5 or

2 more instances of failure to receive full credit for reading responses = F for that portion of your grade.
2. Papers There will be two papers, the first, a 750-1000 word paper on an assigned topic, due Mo 10/12, and the second, a 1500-2000 word paper on a topic you choose in consultation with your discussion section leader, due Mo 12/7. Both papers are due by the beginning of class, and can be submitted either electronically or in hard copy. The first short paper is 15% of your final grade, and the second 35%.
For tips concerning writing philosophy papers, I recommend the following website: http://www.jimpryor.net/teaching/guidelines/writing.html
3. Exams There will be in-class (closed book, closed note) midterm and final exams, each worth 15% of your final grade. The midterm will be Mo 11/2, and the final will be We 12/16, 1:30PM – 3:30PM.
For these exams, I will choose from a list of possible essay questions that I will distribute in advance.
CONTACT INFORMATION
The lecturer for this course is Tad Schmaltz. My contact information:
Email: tschmalt@umich.edu Office: 2231 Angell Hall
Please feel free to stop by any time during my office hours, Tu 10:00AM – 12:00PM. (If I am already speaking with someone, let me know that you're waiting.) I am happy to answer any questions you have about the course, talk about topics from the lectures, or simply discuss philosophy. If these hours are inconvenient, we can arrange to meet by appointment. Or you can email me. I read my email several times each day and usually respond right away.
I will lead discussion section 004 (We 10:00AM – 11:00AM, in 2347 Mason Hall). The Graduate Student Instructor for this course, Damian Wassel, will lead discussion section 003 (Tu 3:00PM – 4:00PM, also in 2347 Mason Hall).
Damian’s contact information:
Email: dwassel@umich.edu Office: 1156 Angell Hall

3 ACCOMMODATIONS If you think you need an accommodation for a disability, please let me or your discussion section leader know at your earliest convenience. Some aspects of this course, the assignments, the in-class activities, and the way the course is usually taught may be modified to facilitate your participation and progress. As soon as you make me aware of your needs, we can work with the Office of Services for Students with Disabilities (SSD) to help us determine appropriate academic accommodations. SSD (734-763-3000; ssd.umich.edu) typically recommends accommodation through a Verified Individualized Services and Accommodations (VISA) form. Any information you provide is private and confidential and will be treated as such.
SCHEDULE *This schedule is tentative and subject to revision*
I. FACTS/VALUES 9/14 Putnam, “The Empiricist Background,” and “The Entanglement of Fact
and Value” (on Resources) 9/16 Putnam (review), and Rorty, “Science as Solidarity” (on Resources)
II. KUHN’S STRUCTURE 9/21 Kuhn, SSR, chs I–IV; Hacking, “Introductory Essay,” vii–xvii 9/23 SSR, chs V–VIII; “Introductory Essay,” xvii–xxvii 9/28 SSR, chs IX–XI; “Introductory Essay,” xxvii–xxix 9/30 SSR, chs XII–XIII, Postscript; “Introductory Essay,” xxx–xxxvii 10/5 Kuhn, “The Trouble with the Historical Philosophy of Science,” and
“Objectivity, Value Judgment and Theory Choice” (both on Resources) 10/7 Kuhn (review)
10/12 FIRST PAPER DUE

4 III. THE SOCIAL TURN 10/12 Golinski, “An Outline of Constructivism,” 13-27, and Barnes and Bloor, “Relativism, Rationalism and the Sociology of Knowledge” (both on Resources) 10/14 Golinski, “An Outline of Constructivism,” 27-46 (on Resources) 10/19 FALL STUDY BREAK 10/21 Bloor, “Anti-Latour” (on Resources) 10/26 Latour, “For David Bloor : and Beyond: A Reply to David Bloor’s ‘Anti-Latour’” (on Resources) 10/28 Bloor vs. Latour (review) 11/2 MIDTERM EXAM
IV. KITCHER’S SCIENCE 11/4 Kitcher, STD, chs 1–3 11/9 STD, chs 4–6 11/11 STD, chs 7–8 11/16 STD, chs 9–11 11/18 STD, chs 12–14, Afterword 11/23 Longino, “Science and the Common Good,” and Kitcher, “Reply to
Longino” (both on Resources) 11/25 THANKSGIVING RECESS
V. LONGINO’S FATE 11/30 Longino, FK, chs 1–2 12/2 FK, chs 3–4 12/7 SECOND PAPER DUE 12/7 FK, chs 5–7 12/9 FK, chs 8–9

5 12/14 Kitcher, “The Third Way,” and Longino, “Reply to Kitcher” (both on
Resources) 12/16 FINAL EXAM, 1:30 PM – 3:30 PM

