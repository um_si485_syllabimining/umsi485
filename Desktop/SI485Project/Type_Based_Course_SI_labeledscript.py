__author__ = 'bryceeb'

import glob, csv, os, re, json

###CROWDFLOWER######

# f = open( 'CFfullreport452016.csv', 'rU' )
# emptydict = {}
# for line in f:
#     cells = line.split( "," )
#     cellsplit = cells[29].replace("https://webapps.lsa.umich.edu/open/syllabushandler.ashx/","")
#     cellsplit1 =cellsplit.replace("/","_")
#     emptydict[cellsplit1] = cells[14]
#
# #print emptydict
# f.close()

###END OF CROWDFLOWER######

###SYLLABI IN FULL####

emptylist= []
listforpreprocess = []
for file in glob.glob("SITEXT/*.txt"):


    with open(file) as f:
        content = f.read().replace('\n',' ')
        content1 = content.replace('\r',' ').replace("\\",'\\\\').replace('""','\\"').replace("\f","")
        listforpreprocess.append({'si_class': file[9:12], 'Syllabi': content1})

import json


with open('Type_Based_Course_SI_ready_for_preprocess.json', 'w') as outfile:
    json.dump(listforpreprocess, outfile, ensure_ascii=False)

