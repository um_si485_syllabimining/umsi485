__author__ = 'bryceeb'

import glob, csv, os, re, json

###CROWDFLOWER######

f = open( 'CFfullreport452016.csv', 'rU' )
emptydict = {}
for line in f:
    cells = line.split( "," )
    cellsplit = cells[29].replace("https://webapps.lsa.umich.edu/open/syllabushandler.ashx/","")
    cellsplit1 =cellsplit.replace("/","_")
    emptydict[cellsplit1] = cells[14]

#print emptydict
f.close()

###END OF CROWDFLOWER######

###SYLLABI IN FULL####

emptylist= []
listforpreprocess = []
for file in glob.glob("LSATEXT/*.txt"):
    # if re.search(r'720|725|735|747|750|755|756', file):
    #     continue

    with open(file) as f:
        content = f.read().replace('\n',' ')
        content1 = content.replace('\r',' ').replace("\\",'\\\\').replace('""','\\"').replace("\f","")
        project = re.search( r'project', content1, re.I)
        test = re.search( r'test', content1, re.I)
        exam = re.search( r'exam', content1, re.I)
        midterm = re.search( r'midterm', content1, re.I)
        final = re.search( r'final', content1, re.I)
        due_date = re.search( r'due date', content1, re.I)
        essay = re.search( r'essay', content1, re.I)
        group_project = re.search( r'group project', content1, re.I)
        split = file.split("/")
        replace = split[1].replace(".txt","")
        listforpreprocess.append({'lsa_class': replace, 'Syllabi': content1})
        if replace in emptydict:
            emptylist.append({'lsa_class': replace, 'Syllabi': content1, 'project': "1" if project else "0", 'test': "1" if test else "0",'exam': "1" if exam else "0", 'midterm': "1" if midterm else "0",'final': "1" if final else "0",'due_date': "1" if due_date else "0", 'essay': "1" if essay else "0", 'group_project': "1" if group_project else "0", "type_of_course": emptydict[replace]})


import json

with open('Type_Based_Course_LSA_cleanedCF_combined.json', 'w') as outfile:
    json.dump(emptylist, outfile, ensure_ascii=False)

with open('Type_Based_Course_LSA_ready_for_preprocess.json', 'w') as outfile:
    json.dump(listforpreprocess, outfile, ensure_ascii=False)

