__author__ = 'bryceeb'

import glob, csv, os, re, json

###CROWDFLOWER######
# output = []
# f = open( 'CFfullreport32216.csv', 'rU' ) #open the file in read universal mode
# firstline = True
# for line in f:
#     if firstline:
#         continue
#     firstline = False
#     cells = line.split( "," )
#     output.append( { cells[ 28 ] : cells[ 14 ] } ) #since we want the first, second and third column

f = open( 'CFfullreport32216.csv', 'rU' )
emptydict = {}
for line in f:
    cells = line.split( "," )
    emptydict[ cells[28]] = cells[14]


f.close()


import csv
# with open('cleanedCF_attendance.csv', 'w') as fp:
#     a = csv.writer(fp, delimiter=',')
#     a.writerows(output)
# with open('cleanedCF_attendance.json', 'w') as outfile:
#     json.dump(output, outfile, ensure_ascii=False)


###END OF CROWDFLOWER######

###SYLLABI IN FULL####
emptylist= []
listforpreprocess = []
for file in glob.glob("SITEXT/*.txt"):
    if re.search(r'666|519|110|301', file):
        continue

    with open(file) as f:
        content = f.read().replace('\n',' ')
        content1 = content.replace('\r',' ').replace("\\",'\\\\').replace('""','\\"').replace("\f","")
        attend = re.search( r'attend', content1, re.I)
        mandatory = re.search( r'mandatory', content1, re.I)
        listforpreprocess.append({'si_class': file[9:12], 'Syllabi': content1})
        if file[9:12] in emptydict:
            emptylist.append({'si_class': file[9:12], 'Syllabi': content1, 'attend': "1" if attend else "0", 'mandatory': "1" if mandatory else "0", "if_attendance_is_mandatory_how_is_attendance_recorded": emptydict[file[9:12]]})


import json
# with open('cleanedCF_sitext.csv', 'w') as fp:
#     a = csv.writer(fp, delimiter=',', quoting=csv.QUOTE_ALL)
#     a.writerows(emptylist)
with open('attendance_SI_cleanedCF_combined.json', 'w') as outfile:
    json.dump(emptylist, outfile, ensure_ascii=False)

with open('attendance_SI_ready_for_preprocess.json', 'w') as outfile:
    json.dump(listforpreprocess, outfile, ensure_ascii=False)


#######COMBINING THE TWO FILES

# There needs to be a si_class in the cleanedCF_sitext.csv file because that is how we would join the two.
# import pandas as pd
#
# a = pd.read_csv("cleanedCF_attendance.json")
# b = pd.read_csv("cleanedCF_sitext.json")
# b = b.dropna(axis=1)
# merged = a.merge(b, on='si_class')
# merged.to_csv("combined.json", index=False)