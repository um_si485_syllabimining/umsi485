BCOM 250 006 W15

https://ctools.umich.edu/portal/tool/91b0bb72-54cc-4cde-8a3f-bee631b3...

Syllabus BComm 250 Winter 2015
BCOM 250: INTRODUCTION TO BUSINESS COMMUNICATION
Stephen M. Ross School of Business Winter 2015
Section 006 and 009

INSTRUCTOR INFORMATION:

Instructor

Dr. Mary Hinesly

Office

R 4316

Phone

734 936 3183

Office Hours

Wednesday 10:30-11:30, Thursday 10:30-11:30 and by appointment

E-Mail

mhinesly@umich.edu

COURSE DESCRIPTION
Introduction to Business Communication presents frameworks for writing documents, conducting e‐mail correspondence, and presenting recommendations.  Cases and exercises emphasize informative communication and strategies for collaboration.  Methods for organizing ideas, analyzing data, understanding audience needs, formatting information, and developing a professional communication style are covered.

COURSE OBJECTIVES
By the end of the course, students will be able to:
Analyze receiver needs and business situations to implement eﬀective communication strategies Develop a repertoire of communication tools for common business situations Make an informed choice about message channel, including use of new and emerging technologies Select, organize, and present information clearly and concisely Produce professional informative and analytical documents, including emails, memos and reports Work as eﬀective members of a collaborative team to produce high‐quality deliverables Assess, edit, and revise their own and others’ business communication

COURSE READINGS The course requires the following text:

Guide to Managerial Communication: Effective Business Writing and Speaking, 10th ed., (2013), Munter, M. and Hamilton, L.

1 of 5

7/6/2015 2:27 PM

BCOM 250 006 W15

https://ctools.umich.edu/portal/tool/91b0bb72-54cc-4cde-8a3f-bee631b3...

Your instructor will post or distribute additional assigned readings listed in the syllabus.

GRADING
Final grades are determined by applying the Ross grading policy to class point totals. Individual assignments are scored using the evaluation criteria (noted on assignment descriptions posted on CTools) in a distribution that reflects school grading guidelines. A zero will be received for all work not completed as scheduled, unless special circumstances apply. In-class exercises generally cannot be made up. If you must be absent, please notify your instructor in advance.

COURSE THEMES AND MAJOR GRADED ASSIGNMENTS

Theme
Business Communication Foundations and Channels

# Assignment 01 Email Self-Introduction Assignment

Employment Communication

02 Cover Letter Assignment

Informative Communication 03

Recommendation Development and Presentation Section Assignment(s)

04

Accounting 301 Memo Assignment
Group Analysis Assignment Section Assignment(s) Participation and Exercises

Points

Due

Draft Class 2 20
Final Class 3
Draft Class 5 30
Final Class 6
40 Draft Class 8
Final Class 10

40 15 Up to 30

Class 11 or 12 Throughout Throughout

FEEDBACK
Feedback is an essential part of BCOM 250, as well as a management skill that will contribute to your future career success. You will receive written evaluations of all major assignments from your instructor. Your peers will also evaluate selected documents.

PARTICIPATION
To receive full benefit from the class, students must be actively engaged during each class. Students are expected to attend all class sessions. Throughout the course, students earn participation points for exercises completed both within and outside of class. Participation points are acquired based on the following activities:

§ Attendance and promptness § In-class activities and assignments

2 of 5

7/6/2015 2:27 PM

BCOM 250 006 W15

https://ctools.umich.edu/portal/tool/91b0bb72-54cc-4cde-8a3f-bee631b3...

§ Class involvement: leading critiques, contributing to discussion, etc. § Written and oral critiques of selected documents § Cooperation and attitude § Improvement in communication skills

SCHEDULE OF CONCEPTS, READINGS AND ASSIGNMENTS
Assignments should be completed before class and turned in at the start of class. Please note that even if no assignments are due, you are still required to attend. Readings should be completed prior to the listed class session.

Date CLASS 1
CLASS 2

Topics · How Is Business
Communication Different?
· Why Is Business Communication Important?
· Concise, Specific Writing Revision Strategies
· Assessing Audience
· Determining Objective
· Selecting Relevant Content
· Organizing Strategically
· Being Clear, Correct, and Specific

Readings Ch. I, Sections I, II, and III: Communication Strategy, Audience Strategy, and Message Strategy
· Ch. II: Writing: Composing Efficiently
· Chapter IV: Writing: Micro Issues
· OCD Handout on Networking Email
· Ferrazzi Video on Networking Email

Assignment/Exercise In-Class Exercise: Writing a Concise Summary
· In Class Exercise: Individual and Peer Review of Draft
· Assignment Due: Email Assignment Draft

CLASS 3

· Channel Selection and

· Ch. I, Section IV: Channel · In Class Exercise: Summary of

Communication Technologies Choice Strategy

Email Best Practices and Tone

Practice

· Effective Use of Tone

· Email Basics Video

· Assignment Due: Email · Email Subject Line Video Assignment Final

CLASS 4 Employment Communication CLASS 5 Organization and Format

· Self-Assessment Video
· OCD Strengths Assessment Tool

In Class Exercises: Critiquing Sample Cover Letters and Interview Practice

· OCD Resume Handout

· OCD Handout on Cover Letters
Ch. III: Writing: Macro Issues

· In Class Exercises: Improving Skim Value and Peer Review of Cover Letters

3 of 5

7/6/2015 2:27 PM

BCOM 250 006 W15

https://ctools.umich.edu/portal/tool/91b0bb72-54cc-4cde-8a3f-bee631b3...

CLASS 6

· Informative Content Development and Forms of Support
· Information Collection and Assessment
· Developing Effective Recommendations
· Presenting Negative Information

· Locker and Kienzler: Analyzing Data and Information for Reports
· Delivering Bad News Video
· Delivering Bad News to the Boss Video

· Assignment Due: Cover Letter Draft
· In Class Exercises: Selecting and Developing Content; Making Recommendations Persuasive, and Delivering Bad News
· Assignment Due: Cover Letter Final

CLASS 7

Visual Representation of Information

Ch. VI: Visual Aids (section on visuals only)

In Class Exercises: Developing Visual Representation of Data

CLASS 8

Collaborative Communication Challenges and Strategies

CLASS 9 Slide Design

CLASS 10 Presentation Delivery

Harrington: Guidelines for Collaborative Writing

· In-Class Exercise: Collaborative Communication
· Assignment Due: Accounting 301 Draft

· Ch. VI: Visual Aids
· Presentation Zen Meets slide:ology Video (long)
Ch. VII: Speaking: Nonverbal Skills

In Class Exercise: Critiquing Slides
· In Class Exercise: Presentation Delivery Practice

· Assignment Due: Accounting 301 Final

CLASS 11 Group Analysis Presentations CLASS 12 Group Analysis Presentations CLASS 13 Summary of Learnings

Assignment Due: Group Analysis Presentations
Assignment Due: Group Analysis Assignment

4 of 5

7/6/2015 2:27 PM

BCOM 250 006 W15

https://ctools.umich.edu/portal/tool/91b0bb72-54cc-4cde-8a3f-bee631b3...

DISABILITY ACCOMMODATIONS
If you need an accommodation for a disability, please let your instructor know early in the term. Some aspects of the course, the assignments, the in-class activities, and teaching style may be modified to facilitate your participation and progress. In addition to making your instructor aware of your needs, please work with the Office of Services for Students with Disabilities to determine appropriate accommodations. All information provided will be treated as private and confidential.
ACADEMIC HONOR CODE
Personal integrity and professionalism are fundamental values of the Ross School community. To help ensure that these values are upheld, and to maintain equitability in the evaluation of your work, this course will be conducted in strict conformity with the Academic Honor Code. The code and related procedures can be found at the following website: http://www.bus.umich.edu/Academics/Resources/communityvalues.htm. This site also contains comprehensive information on how to ensure you have not plagiarized the work of others. Any content in an assignment you submit that is based on the work of others must be properly cited. This includes any information found on the Internet. In addition, the use of materials prepared by students who have previously taken the course constitutes an honor code violation. With this clear understanding, we are highly confident you will uphold our mutual trust throughout the course.
COMMUNICATION CONSULTANTS
The Communication Program has a staff of consultants available to meet with students individually or in small groups to discuss any communication task. You may visit with a consultant for help with class assignments or job search materials. It is recommended you utilize consultants to get feedback on your written assignments, presentation organization and visuals, and presentation delivery. The consultants can view your recorded presentations with you and strategize how to improve delivery techniques. To sign up for a 30-minute appointment, follow this link: http://www.bus.umich.edu/CounselingCalendar/WritingProgram/.
The Communication Consultant offices are located in various places in Ross and Wyly. Please take note of the location of your meeting when you make your appointment and when you receive your confirmation email.
Syllabus 250 winter15.docx

5 of 5

7/6/2015 2:27 PM

