	  
	  
TO	  300	  Course	  Syllabus	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  Winter	  A	  2015	  
BUSINESS	  INFORMATION	  SYSTEMS	  

Instructors	  
	   Office	   Email	   Twitter	   Office	  Hours	  

Nigel	  Melville	   R5344	   npmelv@umich.edu	   @npmelville	   	  Wed	  4-­‐5,	  Winter	  Garden	  

Neil	  Nelson	   R3425	   njnels@umich.edu	   @NeilJNelson	   Wed	  10-­‐11,	  Winter	  Garden	  

Sections	  

Section	   1	   2	  

Class	  #	   001	   002	  

Day	   Tues/Thurs	   Mon/Wed	  

Time	   4	  -­‐	  5:30	  pm	   8:30	  -­‐	  10	  am	  

Class	  Location	   R1210	   R0210	  

Online	  Location	   Instructor	  

Canvas	  

Melville	  

Canvas	  

Nelson	  

Course	  Description	  	  
Digital	  initiatives,	  like	  Ford	  Sync’s	  onboard	  infotainment	  platform,	  are	  critical	  to	  organizational	   performance	  and	  competitiveness.	  And	  applications	  of	  information	  technology	  can	  disrupt,	  restructure,	   and	  redefine	  business	  and	  industry.	  As	  one	  example,	  Progressive	  Insurance	  analyzes	  7	  billion	  miles	  of	   driving	  data	  to	  enable	  its	  unique	  auto	  insurance	  business	  model.	  Progressive	  gets	  digital	  right,	  as	  do	   some	  other	  firms	  such	  as	  Accenture,	  Caesar’s	  Palace,	  Apple,	  Wells	  Fargo,	  Pfizer,	  Wal-­‐Mart,	  UPS,	  and	   LEGO1.	  But	  huge	  numbers	  of	  firms	  struggle	  to	  get	  digital	  right.	  Why?	  A	  key	  reason	  is	  that	  “digital	   leadership	  capabilities”	  –	  the	  ability	  of	  non-­‐technical	  managers	  to	  effectively	  lead	  digital	  projects	  –	  are	   not	  well	  developed.	  Developing	  these	  capabilities	  is	  thus	  a	  critical	  advantage	  in	  the	  workplace	  and	  the	   central	  focus	  of	  this	  class.	  Given	  this	  focus,	  the	  class	  is	  especially	  important	  to	  students	  considering	   careers	  in	  consulting,	  finance,	  marketing,	  manufacturing,	  and	  tech	  industries.	  

We	  will	  apply	  several	  conceptual	  frameworks	  to	  examine	  how	  senior	  management	  can	  generate	  value	   with	  information	  systems,	  how	  digital	  initiatives	  can	  be	  aligned	  with	  corporate	  strategy	  and	   organizational	  strategy,	  and	  how	  technology	  trajectories	  affect	  value	  long	  term.	  In	  so	  doing,	  this	  class	   will	  build	  on	  and	  complement	  other	  courses	  at	  Ross.	  We	  will	  also	  use	  a	  variety	  of	  learning	  approaches,	   including	  case	  discussions	  (to	  prepare	  you	  for	  making	  evidence-­‐based	  arguments	  in	  your	  careers),	  a	  
	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	   	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	 	   
1	  http://www.cio.com/cio100/2013#cio100_tbl	  	  

TO	  300	  Winter	  A	  2015	  /	  V	  121514	   	  

	  

	  1	  

team	  consulting	  project	  (to	  provide	  an	  action	  learning	  experience	  that	  mimics	  the	  real	  world	  and	   enables	  you	  to	  hone	  your	  collaboration	  skills),	  and	  an	  individual	  learning	  journal	  (to	  cultivate	  the	   reflective	  mindset	  of	  a	  successful	  business	  leader).	  In	  line	  with	  the	  2014	  Ross	  BBA	  reinvention,	  this	  class	   was	  completely	  redesigned	  to	  meet	  the	  needs	  of	  tomorrow’s	  leaders,	  to	  be	  integrative	  (e.g.,	   sometimes	  a	  case	  will	  include	  elements	  from	  strategy)	  and	  to	  include	  elements	  of	  how	  IT	  can	  be	  a	   positive	  force	  in	  society.	  Overall	  then,	  this	  class	  examines:	  	  

What	  types	  of	  benefits	  can	  information	  technology	  bring	  to	  business	  activity	  (and	   society),	  how	  can	  these	  benefits	  be	  successfully	  delivered,	  and	  what	  role	  do	  non-­‐ technical	  business	  managers	  play	  in	  the	  success	  of	  digital	  initiatives?	  

Learning	  Objectives	  
The	  learning	  objectives	  of	  the	  course	  are	  as	  follows:	  

1. Describe	  the	  Four	  Principles	  of	  Digital	  Fitness.	  	  

10. Describe	  technology	  trajectories	  and	  explain	  why	  they	  are	  

2. Explain	  the	  PRISM	  framework	  for	  IT	  business	  value	  and	  how	  

important.	  

it	  can	  be	  applied.	  

11. Explain	  key	  terms	  of	  digital	  business,	  such	  as	  

3. Describe	  how	  digital	  business	  can	  enable	  new	  

disintermediation,	  long	  tail,	  and	  big	  data.	  

products/services,	  possibly	  disruptive	  to	  industries.	  

12. Apply	  and	  integrate	  the	  above	  to	  make	  a	  compelling	  

4. Explain	  how	  digital	  can	  transform	  customer	  relationships.	  

business	  case	  for	  IS,	  including	  financial	  and	  intangible	  

5. Explain	  how	  digital	  enables	  redesign	  of	  business	  processes.	  	  

value	  generation,	  ROI,	  risks,	  etc.	  	  

6. Describe	  the	  value	  potential	  of	  social	  media	  to	  business	  and	   13. Demonstrate	  the	  role	  of	  a	  consultant	  given	  a	  specific	  

explain	  the	  challenges	  and	  risks.	  

objective	  and	  reporting	  to	  senior	  executives.	  

7. Explain	  ethical,	  privacy,	  risk,	  and	  sustainability	  issues	  as	  they	   14. Empathize	  with	  others	  regarding	  IT	  challenges.	  	  

relate	  to	  information	  systems.	  

15. Identify	  IS	  and	  business	  strategy	  misalignment.	  	  

8. Describe	  how	  IT	  can	  be	  a	  positive	  force	  in	  society.	  

16. Exemplify	  the	  mindset	  and	  actions	  of	  a	  digitally	  fit	  leader.	  

9. Illustrate	  how	  to	  exploit	  data	  for	  value	  generation.	  

	  

	  

Course	  Materials	  	  
• Managing	  &	  Using	  Information	  Systems:	  A	  Strategic	  Approach	  (5th	  edition),	  Pearlson,	  Keri	  E.	  and	   Saunders,	  Carol	  S.,	  2013,	  Wiley.	  978-­‐1118281734.	  
• Course	  Pack	  for	  TO	  300.	  	  You	  purchase	  the	  rights	  to	  use	  the	  materials	  through	  Kresge	  Library.	  	  The	   actual	  materials	  will	  be	  accessed	  online.	  	  Further	  details	  are	  on	  Canvas	  /	  Announcements.	  	  

Course	  Format	  
Classroom	  activities	  will	  consist	  of	  a	  mixture	  of	  lectures,	  case	  discussions,	  software	  demonstration,	  and	   student	  presentations.	  Class	  sessions	  will	  be	  dynamic	  and	  highly	  interactive.	  Also,	  working	  in	  teams,	   students	  will	  select	  a	  small	  fictional	  consulting	  engagement	  to	  complete	  that	  aligns	  to	  their	  career	   interests	  and	  will	  present	  their	  findings	  to	  the	  client	  at	  the	  end	  of	  the	  term.	  	  	  

Learning	  Management	  System	  	  
Both	  sections	  will	  use	  a	  new	  “learning	  management	  system”	  called	  Canvas	  instead	  of	  CTools.	  Why?	  We	   are	  Leaders	  and	  Best,	  and	  Canvas	  has	  numerous	  advantages	  over	  CTools.	  TO	  300	  students	  are	  part	  of	  an	   important	  digital	  initiative	  being	  rolled	  out	  by	  the	  University.	  We	  think	  this	  will	  provide	  an	  important	   additional	  learning	  component	  to	  the	  class,	  we	  look	  forward	  to	  feedback	  about	  Canvas,	  and	  we	   appreciate	  student	  flexibility	  and	  openness	  to	  change.	  

TO	  300	  Winter	  A	  2015	  /	  V	  121514	   	  

	  

	  2	  

Assignments	  and	  Grading	  
The	  table	  below	  lists	  all	  graded	  work	  for	  the	  half-­‐semester.	  

Graded	  Work	   	   Description	  

Team	  Deliverable	  	  

	  

Consulting	   	   Each	  team	  (85	  /	  7	  =	  12-­‐13	  teams)	  will	  

Project	  (50%)	   be	  grouped	  according	  to	  individual	  

backgrounds,	  experiences,	  and	  

preferences.	  Each	  team	  will	  be	  

allocated	  to	  consult	  for	  a	  specific	  c-­‐

level	  executive,	  in	  a	  specific	  company	  

and	  industry	  (e.g.,	  the	  CEO	  of	  a	  

consumer	  goods	  company).	  The	  

executive	  has	  identified	  a	  business	  

opportunity	  or	  challenge,	  and	  

expects	  your	  consulting	  team	  to	  

provide	  competitive	  insights,	  explore	  

options,	  suggest	  and	  analyze	  

alternatives,	  and	  provide	  a	  plan	  for	  

next	  steps.	  For	  details,	  see	  “Project	  

Statement	  of	  Work”	  document,	  

provided	  separately.	  	  

Individual	  Deliverables	   	  

Midterm	  

	   Midterm	  exam	  during	  Session	  9	  to	  

Exam	  (25%)	  

assess	  extent	  to	  which	  students	  are	  

learning	  the	  material	  presented	  in	  

the	  course.	  

Class	  

	   Individually,	  students	  are	  expected	  to	  

Participation	   prepare	  for	  each	  class,	  participate	  

(20%)	  

actively	  in	  the	  discussion	  of	  cases	  and	  

readings,	  and	  contribute	  to	  the	  

learning	  experience	  of	  the	  group.	  	  

The	  instructor	  will	  ask	  for	  volunteers	  

in	  class	  for	  role	  plays	  and	  other	  

activities	  which	  will	  contribute	  

towards	  this	  grade.	  	  	  	  	  

Learning	   Journal	  (5%)	  

	   The	  learning	  journal	  is	  your	  chance	  to	   capture	  what	  you	  are	  learning	   throughout	  the	  class,	  and	  reflect	  on	   your	  own	  insights,	  struggles,	  etc.	  	  

Evaluation	  
Deliverables	  are	  	   1. Executive	  update	  (Session	  7	  &	  8):	  2-­‐
minute	  verbal	  “elevator	  summary”	   2. Executive	  briefing	  report	  (8AM	  on	  team	  
presentation	  day):	  12	  pages	  or	  less	   (excluding	  tables,	  figures,	  etc.)	  with	   Font	  size:	  12;	  Spacing:	  2	  or	  1.5.	  (30%)	   3. Final	  presentation:	  8-­‐minute	   PowerPoint	  presentation	  (Session	  12	   and	  13).	  (20%)	   You	  will	  be	  evaluated	  on	  your	   thoroughness,	  the	  quality	  of	  insights	  and	   advice	  presented,	  the	  clarity	  with	  which	   you	  share	  the	  insights	  in	  written	  and	  verbal	   form,	  as	  well	  as	  your	  overall	   professionalism.	  
Mix	  of	  closed-­‐ended	  and	  open-­‐ended	  short	   answer	  questions.	  Will	  be	  done	  in-­‐class.	  
Class	  participation	  includes	  several	   components:	  punctual	  attendance	  (use	   sign-­‐in	  sheet	  at	  beginning	  of	  class	  and	  use	   your	  name	  card),	  in-­‐class	  activities	  and	   assignments,	  class	  involvement	   (contributing	  to	  discussions,	  etc.),	  and	   cooperation	  and	  attitude	  (follow	  class	   laptop	  policy	  below).	  Please	  let	  your	   instructor	  know	  if	  you	  will	  be	  absent	   (participation	  grade	  will	  be	  lowered,	   though	  emergencies,	  like	  a	  trip	  to	  the	  ER,	   are	  excused).	  Absences	  will	  affect	  your	   grade	  in	  a	  nonlinear	  fashion	  (i.e.,	  first	  miss	   is	  a	  small	  reduction,	  second	  miss	  costs	   more,	  etc.).	  Overall,	  we	  expect	  all	  students	   to	  be	  professional.	   You	  will	  be	  evaluated	  on	  depth,	   thoroughness,	  and	  self-­‐reflection.	  Due	  on	   Canvas	  BEFORE	  last	  day	  of	  class.	  

TO	  300	  Winter	  A	  2015	  /	  V	  121514	   	  

	  

	  3	  

Course	  Policies	  
• Laptops	  –	  You	  may	  use	  your	  laptop	  (or	  tablet	  or	  smart	  phone)	  during	  class	  time,	  but	  only	  for	  taking	   notes	  or	  retrieving	  material	  from	  Canvas.	  This	  is	  a	  discussion-­‐oriented	  class	  and	  you	  are	  expected	  to	   stay	  engaged	  with	  the	  class	  material.	  Anyone	  using	  their	  laptops	  or	  other	  devices	  to	  do	  email,	   check	  Facebook,	  or	  for	  any	  other	  non-­‐class	  purpose	  will	  be	  appropriately	  disciplined,	  which	  will	   count	  against	  your	  participation	  grade.	  If	  you	  struggle	  with	  information	  overload	  or	  related	  issues,	   we	  are	  happy	  to	  discuss	  positive	  strategies	  in	  office	  hours.	  
• Attendance	  –	  Class	  participation	  is	  important.	  There	  will	  be	  a	  sign-­‐in	  sheet	  for	  each	  class.	   • Required	  Readings	  –	  You	  should	  read	  the	  material	  listed	  in	  the	  schedule	  before	  the	  class	  in	  which	  
they	  will	  be	  discussed.	  	   • Grading	  –	  Grade	  distributions	  will	  adhere	  to	  the	  Ross	  School	  of	  Business	  grading	  policies.	   • Course	  LMS	  -­‐-­‐	  We	  will	  use	  Canvas	  extensively.	  Whenever	  possible,	  class	  materials	  will	  be	  distributed	  
electronically	  on	  Canvas	  rather	  than	  in	  paper	  form.	  	  Assignments	  will	  be	  submitted	  there,	  too.	  	  We	   will	  also	  use	  Canvas	  announcements	  to	  keep	  you	  informed	  of	  class	  plans	  or	  changes.	   • Communicating	  –	  The	  Forums	  in	  Canvas	  are	  the	  best	  way	  to	  get	  in	  touch	  (for	  all	  kinds	  of	  general	   questions	  that	  your	  peers	  probably	  also	  have),	  followed	  by	  email	  (for	  stuff	  you’d	  rather	  keep	   between	  you	  and	  the	  instructor)	   • Collaboration	  -­‐-­‐	  The	  RSB	  Honor	  Policy	  is	  to	  be	  observed	  in	  all	  respects.	  Individual	  assignments	  are	   meant	  to	  be	  individual.	  For	  team	  assignments,	  there	  should	  be	  no	  collaboration	  or	  communication	   between	  teams.	  Please	  review	  the	  material	  on	  the	  Community	  Values	  website:	  	   http://www.bus.umich.edu/Academics/Resources/communityvalues.htm	  	   Also	  read	  the	  "Important	  Message	  from	  the	  CVC"	  dated	  February	  2007:	   http://www.bus.umich.edu/Academics/Resources/ImptInfoFromCVC.pdf	   • Services	  for	  Students	  with	  Disabilities	  -­‐-­‐	  If	  you	  think	  you	  need	  an	  accommodation	  for	  a	  disability,	   please	  let	  us	  know	  at	  your	  earliest	  convenience.	  	  As	  soon	  as	  you	  make	  your	  professor	  aware	  of	  your	   needs,	  he	  can	  work	  with	  the	  Office	  of	  Services	  for	  Students	  with	  Disabilities	  (OSSD)	  to	  determine	   appropriate	  accommodations.	  We	  will	  treat	  information	  you	  provide	  as	  private	  and	  confidential.	   • Religious	  Holidays	  -­‐-­‐	  We	  are	  aware	  of	  the	  religious	  holidays	  scheduled	  to	  take	  place	  this	  semester.	   We	  are	  also	  aware	  of,	  and	  fully	  supportive	  of,	  the	  University's	  policies	  on	  religious	  holidays	  and	   academic	  conflict.	  However,	  we	  do	  have	  classes	  scheduled	  on	  some	  of	  these	  days;	  we	  encourage	   you	  to	  come	  to	  class	  on	  another	  day	  that	  week	  in	  order	  to	  make	  up	  that	  material.	   • Grading	  -­‐	  Final	  grades	  will	  be	  determined	  by	  applying	  the	  Ross	  grading	  policy	  to	  class	  point	  totals.	  	   No	  points	  will	  be	  received	  for	  work	  not	  completed	  as	  scheduled,	  unless	  special	  circumstances	  apply.	   In-­‐class	  exercises	  generally	  cannot	  be	  made	  up.	  If	  you	  must	  be	  absent,	  please	  notify	  instructor	  in	   advance.	  
	  
	  
	  

TO	  300	  Winter	  A	  2015	  /	  V	  121514	   	  

	  

	  4	  

Class	  Schedule	  (Section	  1,	  Section	  2)	  

Session	  

Topic	  

Reading	  Assignments	  

Module	  1:	  Introduction	  and	  Foundations	  	  

Session	  1	  	   Jan.	  12/13	  	   	  

Value	  Perspective	  -­‐	  IT	  &	  Business	  Value	  Generation	   How	  will	  this	  class	  advance	  your	  career?	  What	  are	  the	   four	  principals	  of	  digital	  fitness?	  How	  does	  IT	  add	   value	  to	  organizations?	  How	  can	  IT	  transform	   companies	  and	  industries?	  	  

• MUIS	  Book	  Introduction,	  pgs.	  1-­‐9,	  14-­‐18.	  
• N.	  Melville,	  “Digital	  Fitness:	  Four	  Principles	  for	   Successful	  Development	  of	  Digital	  Initiatives,”	   2014.	   	  

Session	  2	  	   Jan	  14/15	   	  

Strategic	  Perspective	  -­‐	  Alignment	  	   What	  is	  the	  IS	  Strategy	  Triangle	  and	  why	  is	  it	   important?	  How	  do	  IT	  alignment	  and	  misalignment	   occur?	  What	  are	  the	  implications?	  Examples	  of	   businesses	  that	  have	  leveraged	  IT	  as	  a	  key	  component	   of	  their	  business	  strategy?	  How	  does	  the	  strategy	   triangle	  aid	  in	  understanding	  the	  Lego	  case?	  	  	  

• MUIS	  Chap	  1,	  pgs.	  23-­‐36.	  
• Case	  Study:	  	  “Lego	  Products:	  	  Building	   Customer	  Communities	  Through	  Technology.”	  	   IMPORTANT:	  See	  Canvas	  for	  questions	  related	   to	  today’s	  case	  study.	   	  

Session	  3	   Jan	  20/21	   (NO	  CLASS	  ON	   1/19)	   	  

Technology	  Perspective	  -­‐	  Information	  Technology	  &	   Technology	  Trajectories	   What	  types	  of	  IT,	  IS,	  and	  IS	  services	  are	  employed	  in	   various	  industries	  and	  business	  functions?	  How	  does	   the	  IT	  function	  operate	  to	  provide	  these	  IS	  services?	  

• MUIS	  Chap	  7,	  pgs.	  222-­‐231.	   • Gartner	  -­‐	  Top	  10	  Strategic	  Technology	  Trends	  
for	  2015	  (see	  link	  on	  Canvas	  /	  Module	  1	  Session	   3	  Readings)	  

	   (funding	  models,	  outsourcing,	  etc.).	  What	  is	  the	  

“cloud”	  and	  its	  role?	  What	  are	  key	  technology	  

trajectories	  and	  how	  to	  leverage	  for	  business	  value?	  

Module	  2:	  Information	  Systems	  That	  Enforce	  Business	  Processes:	  ERP,	  CRM,	  etc.	  

Session	  4	   Jan	  22/26	   	  

ERP	  in	  Global	  Manufacturing	   What	  is	  ERP?	  What	  is	  the	  value	  of	  integrated	  systems	   in	  a	  traditional	  manufacturing	  company?	  What	  are	  the	   trade-­‐off's	  of	  a	  single	  integrated	  software	  platform	  vs.	   best-­‐of-­‐breed	  solutions?	  	  What	  are	  key	  information	   systems	  and	  technology	  demands	  in	  services-­‐based	   business	  models	  (CRM,	  digital	  currencies,	  Professional	   Services	  Automation,	  etc.)	  How	  to	  apply	  the	  PRISM	   model	  to	  Bombardier?	  

• CIO	  Magazine:	  	  ERP	  Definitions	  and	  Solutions.	   (Canvas	  /	  Module	  2	  Session	  4	  Readings)	  
• Case	  Study:	  	  “Successfully	  Navigating	  the	   Turbulent	  Skies	  of	  a	  Large-­‐Scale	  ERP	   Implementation.”	   IMPORTANT:	  See	  Canvas	  for	  questions	  related	   to	  today’s	  case	  study.	  
• MUIS	  Chap	  5,	  pgs	  148-­‐162.	   	  

	  

Session	  5	   Jan	  28/28	   	   	  

Demonstration	  of	  Enterprise	  Software	  SAP	  by	  Guest	   Speaker	  to	  all	  Sections	   Expert	  demonstration	  of	  SAP	  software	  to	  illustrate	   how	  a	  company	  leverages	  an	  integrated	  enterprise	   software	  application	  to	  run	  its	  business.	  Illustrate	   product	  flow	  through	  the	  value	  chain	  to	  optimize	   process	  efficiency	  and	  organizational	  performance.	  	  	  

• MUIS	  Chap	  5,	  pgs	  148-­‐162.	   • Review	  http://www.sap.com.	  (Canvas	  /	  Module	  
2	  Session	  5	  Readings)	  
• Both	  sections	  will	  meet	  on	  JANUARY	  28	  from	   6:00	  -­‐	  7:30pm.	  The	  Colloquium,	  Ross	  6th	  Floor	   	  

Module	  3:	  Information	  Systems	  That	  Enable	  Emergent	  Collaboration	  and	  Networking	  (social	  media,	  email,	  etc.)	  

Session	  6	  

Social	  Media	  	  

• Case	  Study:	  “Social	  Media	  at	  Outdoor	  World:	  

Jan/Feb	  29/2	  

What	  are	  the	  key	  economic	  characteristics	  /	  business	  

Possibilities	  and	  Pitfalls.”	  (see	  link	  for	  case	  and	  

	  

models	  underlying	  social	  networking	  sites	  such	  as	  

questions	  on	  Canvas	  /	  Module	  3	  Session	  6	  

Facebook,	  Twitter	  and	  LinkedIn.	  What	  are	  network	  

Readings)	  

Session	  7	  	   Feb	  3/4	   	  

effects,	  crowdsourcing,	  and	  hackathons?	  How	  can	   social	  media	  be	  used	  within	  the	  corporation	  to	   generate	  value?	  	  
Business	  Analytics	  and	  Big	  Data	  
What	  is	  big	  data?	  What	  is	  analytics?	  What	  is	  the	   impact	  of	  both	  on	  corporate	  decision-­‐making.	  How	   can	  big	  data	  be	  used	  as	  a	  positive	  force	  in	  society?	  

• MUIS	  Chap	  1,	  pg.	  34.	   	  
	  
• MUIS	  Chap	  11,	  pgs.	  340-­‐345.	   • Chui,	  Michael,	  “Competing	  through	  data:	  	  Three	  
experts	  offer	  their	  game	  plans,”	  McKinsey	   Quarterly,	  2011.	  

• Consulting	  Project	  Executive	  Update	  Due	  for	   Groups	  1-­‐7.	  See	  Canvas	  for	  details.	  

	  

TO	  300	  Winter	  A	  2015	  /	  V	  121514	   	  

	  

	  5	  

Module	  4:	  Information	  Systems	  That	  Support	  Individual	  Work	  and	  Productivity	  (Gdocs,	  Wearables,	  etc.)	  

Session	  8	  	   Feb	  5/9	  

What	  types	  of	  systems	  support	  individual	   productivity?	  What	  are	  wearables?	  Why	  are	  they	  

• Experiential	  Exercise:	  social	  media	  analytics	   (See	  Session	  7	  Slides	  for	  instructions).	  

	   important?	  What	  is	  3D	  printing?	  What	  impacts	  might	   • Consulting	  Project	  Executive	  Update	  Due	  for	  

it	  have	  on	  manufacturing?	  What	  is	  the	  future	  of	  

Groups	  8-­‐14.	  See	  Canvas	  for	  details.	  

individual	  productivity	  systems?	  	  

Session	  9	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  Midterm	  Examination	  (in	  class,	  on	  laptops)	  

Feb	  10/11	  

Module	  5:	  Innovation,	  Privacy,	  Ethics	  and	  What	  Can	  Go	  Wrong	  with	  IT	  Projects	  

Session	  10	   Feb	  12/16	   	  

Digital	  Innovation	   What	  is	  digital	  innovation?	  What	  does	  it	  mean	  for	   business?	  What	  is	  the	  long	  tail,	  collaborative	  filtering,	  

• Case	  Study:	  “Netflix	  in	  Two	  Acts:	  The	  Making	   of	  an	  E-­‐commerce	  Giant	  and	  the	  Uncertain	   Future	  of	  Atoms	  to	  Bits.”	  (see	  link	  on	  Canvas	  /	  

disintermediation,	  and	  Moore's	  Law?	  What	  are	  the	  

Module	  5	  Session	  10	  Readings)	  

challenges	  to	  digital	  initiatives	  (project	  management,	   IT	  governance,	  etc.)?	  

• MUIS	  Chap	  10,	  pgs.	  312-­‐319.	   • MUIS	  Chap	  5,	  pg	  161.	  
	  

Session	  11	   Feb	  17/18	  	   	  

Privacy,	  Security,	  Sustainability,	  Positive	  Business	  &	   Course	  Wrap-­‐Up	   What	  is	  privacy?	  What	  does	  it	  mean	  in	  the	  digital	  age?	   What	  is	  a	  security	  breach?	  Why	  do	  they	  happen?	   What	  sorts	  of	  impact	  do	  they	  have	  on	  companies	  such	   as	  Target?	  What	  impact	  does	  IT	  have	  on	  the	   environment?	  How	  does	  IT	  enable	  growth	  at	  the	  base	   of	  the	  pyramid?	  

• MUIS	  Chap	  9,	  pgs.	  359-­‐368.	  
• Case	  Study:	  “Host	  Europe	  -­‐	  Advancing	  CSR	  and	   Sustainability	  in	  a	  Medium-­‐sized	  IT	  Company”	   IMPORTANT:	  See	  Canvas	  for	  questions	  related	   to	  today’s	  case	  study.	   	   	  

Module	  6:	  Project	  Presentations	  

Session	  12	  	  

	  

Feb	  19/23	  	  

CONSULTING	  PROJECT	  PRESENTATIONS	  (1-­‐7)	  

[4	  @	  10	  then	  5	  minute	  break,	  3	  @	  10]	  

Session	  13	  

	  

Feb	  24/25	  	  

CONSULTING	  PROJECT	  PRESENTATIONS	  [8-­‐13]	  

[3	  @	  10	  then	  5	  minute	  break,	  3	  @	  10]	  

	   	  

TO	  300	  Winter	  A	  2015	  /	  V	  121514	   	  

	  

	  6	  

