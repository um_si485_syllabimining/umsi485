Management Accounting Teaching Philosophy Venky Nagar
venky@umich.edu “Vision without execution is hallucination.” Thomas Edison
I. Overview Management accounting explores how accounting information can help you manage physical and human resources efficiently to execute your strategy. One half of management accounting concentrates on physical resources, the other half on human resources. Execution is the key centerpiece of any successful strategy, because the biggest impediment to change is typically not some outside entity such as the competition, but the internal organization itself. To take an example, Microsoft’s biggest obstacle was not an organizational lack of Internet and mobile knowledge, but execution: it found it very hard to change itself to the new marketplace. The org-chart drawing on the next page explains the challenges in managing and changing humans in an organization.
II. Intended Audience There is no branch of business which does not require knowledge of execution: you cannot be a venture capitalist or an investment banker or private equity specialist if you have no idea how effective management will be at executing its strategy. You cannot be a consultant or a manager (pick any specialty --- auditing, marketing, ops, etc.) either, without this knowledge.
1

III. Pedagogy
There are individuals who believe that the best way to learn any subject is to first get a structured discussion of tools and frameworks, and then apply these tools and frameworks to settings. Alas, such individuals are unable to meet the needs of top organizations for a simple reason:
Frameworks and Tools cannot be deployed to a new organizational situation without a deep organizational awareness. One must first understand the specific organizational situation with a high degree of clarity. Only then does it become evident how to apply Tools and Frameworks. But this process fundamentally requires that you first develop your own point of view or perspective, which is precisely what this class will help you achieve. Once you develop perspective, you will be able recall exactly the right Tools and Frameworks to solve the organizational problems. Without perspective, all Tools you know will look ineffective for this new problem, and you will freeze.
The challenge in developing a perspective is that perspective is not the sum of tools. It must be learnt in a different manner than tools. In particular, the learning method in my class emphasizes analogies. You learn by trying to connect past experiences and past knowledge with what you are seeing in the classroom. As a result, when you see anything new like a new organization, you will
2

try to understand its situation by creating analogies to situations you already know. You will then have an aha moment, which is the key to crafting a solution to this new organization’s problems.
Always remember that organizations are not hiring you because they have forgotten Frameworks and Tools. Organizations are hiring you because business events have changed them to a point where they don’t recognize themselves, and they want you to bring clarity to their specific confusion, and help them forge a path forward. If you don’t have a perspective on the org setting, your Frameworks and Tools are pretty much useless, because you won’t be able to recall the correct Tool for the situation. And top companies can figure out in a flash whether you have a genuine individual point of view or you are just reciting some Tools and Frameworks you have memorized. And they know that you will blank out and not be able to recall the right Tool precisely when such a Tool is needed. All Tools will look useless to you, because you’ll be instantly blinded by the new situation.
Given that its pedagogical objective and learning methods are different than a traditional class, both the role of the instructor and the student in this class will change in the following manner.

Perspective is not the Sum of Tools

Learning in a Traditional Class

Learning in My Class

Student learns various abstract concepts for their Student learns by analogies. She builds and

own sake through problems, frameworks, and solidifies connections between her past

memorization.

experiences and the classroom.

Instructor is an absolute authority who dispenses clear knowledge. The role of the student is primarily to absorb the instructor’s point of view.

Instructor is a facilitator who guides your thinking. The role of the student is to develop her own individual perspective.

Student’s prior experience is of little relevance, because the instructor is giving absolute knowledge.

Student’s prior experience is crucial, because it sets the foundation for building new connections on organizational knowledge and deeper points of view.

Student’s job is to take notes and do assigned problem sets and exams.

Student’s job is to integrate the new material in class with his or her prior experience.

Student’s performance is judged by clear, tangible problem sets and exams. Classroom conversation is a sideshow. The class setting is far removed from the daily experience at top

The classroom conversation is central. Student is judged by how she articulates and navigates through open-ended settings in the classroom. The class setting is similar to daily experience at

3

companies.
Student receives clear closure, with correct answers and takeaways.
Student knows all the Tools, but cannot recall them in stressful business situations: all Tools will look inapplicable because the student will be blinded.

top companies.
Student receives intangible deeper organizational understanding.
Student can assess any situation and immediately figure which Tool to apply, because the student will have their wits around them.

IV. Success on the Job
Why do Wall Street firms love Princeton undergrads even though Princeton has no business school? Many business school students across the country are like obedient donkeys, waiting to be commanded by the someone --- the instructor, the manager, somebody, anybody. Such students are best suited for repetitive jobs in mediocre firms with no hope of advancement.
A student with perspective has the guts, the instinct, and the skill to face new ambiguous problems in stressful situations by recalling crucial concepts and frameworks in real time and make sense of the chaos he is she is facing. This recall skill gets better and better with practice, and once you get to a top firm, it will give you a land of opportunity, and you will rapidly pull away from someone in a mediocre firm. Its ability to develop student perspective is the only secret to Princeton’s success.
V. Role of Math
Math, while crucial to this course, is subservient to the business problem and the people in the business. Math’s only role is to help us understand what people in the business are doing. Contrast this to physics, where math’s role is central to understand the subject. People are there in physics too, but only as a diversion (watch a play to clear your head) or to play politics with your physics career.
VI. Grading
A class such as this cannot be graded on numerical exams, for they do not display any learning that the class values. Instead, students will be graded on classroom participation, joint submissions of case assignments, and individual write-ups of material that will enable the instructor to assess student maturity and development.
A consequence of this style of teaching is that grades will be very noisy indicators of performance. Is A > B? That is not the question. If you are super-anxious about your grades as opposed to landing a job at a top firm and doing well, this class is not for you.
4

The grading key is as follows:
Individual Online Surveys Group Case Analyses Class Participation Individual Writeups Total
5

20% 30% 30% 20% 100%

Online Surveys: The surveys that are to be individually completed by 6AM on the day of the case. Their link will be posted as C-Tools announcements. These surveys give the instructor a preliminary feedback on students’ initial read of the key issues in the case.
Case Analyses: All cases where noted are due. See the case questions section for the case analyses. Bring a printed copy to class, one per group or individual as the case may be. Groups will change with every case. I will assign all groups and post the group assignments online.
Class Participation: After every class, the instructor will review your participation and attendance and assign you a score based on both aspects. Your scores are available from the instructor upon request.

Individual Writeups:
1. Zero to One by Peter Thiel --- Due Individually on Feb 2
The author is a famous venture capitalist. Buy this book on Amazon, and produce a 3-5 page essay on what you think of Thiel’s point based on this class.
2. Bryce Hoffman’s Book --- Due Individually on Feb 23
Read Chs 1-14 of Bryce Hoffman’s book “American Icon: Alan Mulally and the Fight to Save Ford Motor Company” (you can buy it on Amazon), and write an essay of how the inside look in that book meshes with this class, and your work experience. Try to limit the essay to 5 or 6 pages max. What I am looking for here is how you are able to think through what we’ve done in class.
3. Ben Horowitz’s book “The Hard Things About Hard Things” --- Due Individually on April 20
The author is a noted venture capitalist, and you can buy his book on Amazon. Write a similar essay as the one above, based on the 2nd half of the class.

Schedule and Questions for ACC315 (Unless otherwise noted, the cases are on Studynet)

Lecture 1 (Jan 12): No case.

6

Lecture 2 (Jan 14): Forrestor Carpet (DUE in GROUPS, Case on ctools, not Studynet) Questions in the case.
Lecture 3 (Jan 21): Introduction to ABC and Cambridge Hospital No questions.
Lecture 4 (Jan 26): Cambridge Hospital (DUE in GROUPS)
1. Describe the old cost and billing system at Cambridge Hospital 2. Was PCU the best place to conduct the ABC study? 3. Describe the ABC system at PCU. What does it tell you that the old system didn’t? 4. What kind of organizational challenges do you see in putting ABC into an organization?
Lecture 5 (Jan 28): Seligram (DUE in GROUPS)
1. What caused the existing system at ETO to fail? 2. Calculate the costs of the five components described in Exhibit 6 as reported by:
a. the existing system; b. the system proposed by the accounting manager; c. the system proposed by the consultant. 3. Which system do you believe is best? Why? 4. Would you treat the new machine as a separate cost center or as part of the Main Test Room? Explain.
Lecture 6 (Feb 2): Lehigh Steel (DUE in GROUPS, Thiel Review due Individually) Mandatory Supplementary Reading: “Time-driven ABC”
1. How many overhead categories do you see in Exhibit 5? (The answer is two.) One of the overheads is called Direct Manufacturing expense. Overhead is supposed to be an indirect cost. Why is it then called Direct?
2. What are the main steps in the process flow? See Exhibit 3. 3. How much does each step cost (by minutes or orders or SKUs? See Exhibit 6 4. Based on Exhibits 4 and 5, how much does it cost under the ABC system to make 478,679
lbs of Alloy: Condition Round? Do not forget to include direct costs such as material and labor. 5. What exactly is going on in the TOC discussion?
Lecture 7 (Feb 4): Micro Devices Mandatory Supplementary Reading: “ABC and Capacity”
1. The “excess capacity of the facility is associated with four causes: a. Long-term yield variation b. Short-term yield fluctuation c. Changes in product mix
7

d. Lumpy capacity acquisition Should the cost associated with any of these causes of excess capacities be included in product costs or transfer prices?
2. Supposed machines cost $12 Million and you planning to make 100,000 units. a. What is your expected cost per unit? b. Suppose you only made 75,000 units. How would you report the costs?
Lecture 8 (Feb 9): Data Services (DUE in GROUPS)
1. What are the main steps in the customer acquisition and sales process? Draw a flowchart akin to Lehigh Exhibit 3.
2. Use Exhibit 4 of Data Services to create an Exhibit akin to Exhibit 6 of Lehigh. Compute the rates as well.
3. Why is this exercise useful?
Lecture 9 (Feb 11): Regression Lecture No case.
1. Add the Analysis Toolpack to your Excel. Go to Windows  Excel Options  Add ins  Add Analysis Toolpack. Contact Ross IT if you have trouble.
2. Download the MBA mutual fund data from Ctools and play with it. We will go over that dataset in class.
Lecture 10 (Feb 16): Texas Instruments (DUE in GROUPS)
1. Describe the COQ system. What are your opinions about this system? 2. What is the main difference between TI (A) and TI (B)? 3. What implications does this case have for modern tech companies? 4. We will spend about 15 minutes answering student questions about the Class Presentations.
Lecture 11 (Feb 18): Class Presentations (DUE in GROUPS)
1. Read http://blogs.hbr.org/2012/10/how-to-pick-the-right-metrics/ 2. Download the Bank data set from Ctools. 3. Create a statistical model to predict the banks’ 1995 profits based on 1994 information.
a. How would such a model help a bank’s management team? (Think “cause-effect”). b. How would such a model help a bank financial analyst? 4. Create a powerpoint of your Group’s analysis. The first slide should contain the group number. Make sure you don’t dump the data analysis on the powerpoint, but create a meaningful presentation keeping the HBR article in mind above. 5. Plan for presenting for about 7 minutes each. Be prepared for interruptions and questions from the audience. 6. Make sure to UPLOAD into ctools a .PDF version (not .PPT) by 7AM Feb 18. Label it GroupXXX.PDF. Only one upload per group please.
8

7. I will merge all the .PDFs into one big .PDF. Groups can then present their findings to class one by one.
Lecture 12 (Feb 23): Left over presentations and discussion of Individual writeups
Lecture 13 (Feb 25): Summary of the first half of the course
Lecture 14 (March 9): Forrest Gump (DUE in GROUPS)
1. Compute the accounting profit for Forrest Gump. 2. How much money did Winston Groom make based on his profit-based contract (see Q4
in the case)? 3. How much money did Tom Hanks make based on his revenue-based contract? 4. How would you defend the definition of Paramount’s accounting profits to a judge?
Lecture 15 (March 11): Codman and Shurtleff (DUE in GROUPS)
1. Evaluate the planning and control system in use at Johnson & Johnson. What are its strengths and weaknesses?
2. Johnson & Johnson is widely viewed as one of the most innovative and well-managed firms in its industry. What role, if any, do you believe that J&J’s management planning and control systems play in achieving (or hindering) this innovation?
3. Roy Black states that decentralized management is “unequivocal accountability for what you do” (last paragraph, p. 12). Do you agree with his statement?
Lecture 16 (March 16): Purdue Pharma Budgeting (DUE in GROUPS) Mandatory Reading: How Google sold its Employees on Management
1. Develop, as best as you can, the budget for Purdue Pharma. See the case for details.
Lecture 17 (March 18): Borealis (DUE in GROUPS) Mandatory Reading: Control in the Age of Empowerment Mandatory Reading: https://hbr.org/2008/09/how-pixar-fosters-collective-creativity
1. In light of Purdue Pharma, what do you think of Borealis’s decision to abandon budgets? 2. What did Borealis replace its budgets with?
Lecture 18 (March 23): Purity Steel and Ford Motor (DUE in GROUPS)
1. Produce an INDIVIDUAL handwritten solution of the Purdue Pharma case using Taccounts.
2. What actions should Higgins take in response to Larry Hoffman’s concerns? 3. Probability tree: It is very important that you know this technique. It a standard staple on
consulting interviews (e.g., McKinsey), and Charlie Munger swears by it. Suppose a
9

project requires an upfront initial investment of $100 on Jan 1. On Feb 1, the first payoff occurs. The possible cash flows are either $40 or $80, with 50% probability each. If the Feb 1 payoff was $80, the March 1 payoffs are either $70 or $100, with probability 50% each. If the Feb 1 payoff was $40, the March 1 payoffs are either $20 or $-90, with probability 50% each.
a. Draw the probability tree of the payoffs. Don’t forget the initial investment of $100.
b. Should the company accept this project? Assume there is no time value of money. c. Suppose the company can stop the project on Feb 2. Should it stop the project,
and when? d. Suppose the owner has no time to run the company, but must hire a manager. How
can the owner provide incentives to the manager that she takes the correct decision (and doesn’t gamble the firm away to make some small bonus)? 4. Go to http://corporate.ford.com/content/dam/corporate/en_us/investors/financialreports/proxy-statements/ir_2014_proxy.pdf What do you think of the process by which Ford decides compensation for its executives (Proposal 3)?
Lecture 19 (March 25): HCC Industries (DUE in GROUPS)
1. What are the advantages and disadvantages of the “minimum performance standard (MPS) targets relative to "stretch targets".
2. Which one of the four divisions is least suited for the MPS system? Why? 3. Should budgets be developed from the "bottom-up" or from the "top-down?" Identify one
advantage and one disadvantage of each. 4. Flowchart the process used to develop the annual budgets. 5. Continue the Ford compensation contract from the previous case:
a. What is the performance metric on which Ford bases its incentive compensation? b. What are the various forms of incentive compensation for Ford executives?
Lecture 20 (March 30): Lecture on Variances and Transfer Pricing and Executive Compensation
Lecture 21 (April 1): Software Associates (DUE in GROUPS)
1. Assignment Question 1 in the case 2. Assignment Question 2 in the case 3. Assignment Question 3 in the case 4. Assignment Question 4 in the case 5. Instead of Assignment Question 5, please answer the following questions. Be c
oncise and reason through your recommendations in writing:
10

a. You are on the Board of Directors of Software Associates. Advise Norton on how he should proceed next.
b. You are on the Board of Directors of Software Associates. Evaluate Norton’s performance. Does he deserve a discretionary bonus from the Board?
Lecture 22 (April 6): Birch Paper (DUE in GROUPS)
1. Flowchart the flow of the raw inputs and the product through the various divisions (by hand is okay).
2. How should Birch paper set the transfer price?
Lecture 23 (April 8): Polysar (DUE in GROUPS) Do the following questions in order. Each question feeds into the next:
Look at Exhibit 1. What are the total budgeted fixed costs for production for 9 months? What are the budgeted fixed costs for production for 12 months (multiply the above answer
by …)? What is the formula for the rate at which budgeted fixed costs are allocated per tonne on p5?
Write the formula down. What is the annual demonstrated capacity of NASA Sarnia 2 on p.6? Compute the standard allocation rate in question 3. What is formula for the volume variance on p.5? Write the formula down. How many tonnes were actually produced in the nine months in Exhibit 1? What is the demonstrated capacity for nine months (multiply the answer for question 4 by
…)? Compute the volume variance. Did you get the $(11,375) as in Exhibit 2? Can you figure out how they got budgeted volume variance of $(6125)? What changed? What is going on in Polysar?
Lecture 24 (April 13): Kidder Peabody (DUE in GROUPS) Supplementary Reading: How free are free agents?
1. Suppose that an asteroid is bearing down on our planet. If nothing is done, it will strike in 2019, inflicting $20 trillion in losses. At a nominal interest rate of 5 percent, that’s a present value of $10 trillion.
If we do nothing about the asteroid, by next year the present value of the future losses from the asteroid strike will be $10.5 trillion. So the “unfunded liability” from the asteroid strike rises by $500 billion a year.
Suppose that there is a way to fix the problem: we can send Bruce Willis into space to blow up the asteroid. So here’s the question: if we wait a year to send Bruce Willis into space, does that cost $500 billion?
11

Of course not: it could cost either more or less. If waiting a year means that we’ve lost our last chance to stop the asteroid, it costs $10 trillion – the full present value of the avoidable losses the asteroid would inflict. On the other hand, if Bruce Willis can still blow up the asteroid next year (or any year before 2019), there is no cost at all to waiting. In fact, if waiting increases the Willis expedition’s chances of success, there’s a benefit to delay. In other words, the $500 billion increase in the present value of the future costs from the asteroid says nothing about the costs of delaying action. All it says is that the future is getting closer. Question: What does this story have to do with Kidder Peabody? 2. How difficult was it to understand the accounting for strips and recons? 3. Why was the nature of this profit not understood earlier? 4. Who was to blame? 5. How does this case inform you about the recent US financial crisis? Lecture 25 (April 15): Summary of the Course Lecture 26 (April 20): Ask Me Anything (Individual Assignment Due)
12

