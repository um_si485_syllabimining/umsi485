PROFESSOR

BL 306: Legal Aspects of Management and Finance
Ross School of Business, University of Michigan Section 001 (M & W 11:30 AM to 1:00 PM) Section 002 (M & W 1:00 PM to 2:30 PM) Room 1240
Peter Shedd

OFFICE:

R4461 S

EMAIL:

pshedd@umich.edu

TELEPHONE: 734-647-4606

OFFICE HOURS: Monday 2:45 to 4:00 pm and Wednesday 10:15 to 11:30 am. Please feel free to set up an appointment at another time (to set up a time, please email me or see me after/before class).

EXAMS:

Midterm Exam: Tuesday, February 24, 6:30-8:30 pm
Final Exam: Thursday, April 23, 10:30 am-12:30 pm
If you have a class or exam conflict with either exam and intend to ask for an alternate exam time, you MUST do so before January 31, 2012. Otherwise, you need to plan to take the exams at the scheduled times or take a zero for an exam you miss.

REQUIRED MATERIALS

 Mann & Roberts, LHC 306: Legal Aspects of Management and Finance; ISBN: 1-28510995-3 published by Cengage Learning. NOTE: This is a special edition soft cover printed for this course and currently is only available through the local bookstores, from the cengagebrain microsite - www.cengagebrain.com/course/1-1OZXWTV, or from students who took the course last year. This contains selected chapters from the version of the full hardcover book listed below.  If you prefer to buy the full hardcover book you can buy: Richard A. Mann and Barry S. Roberts, ESSENTIALS OF BUSINESS LAW & THE LEGAL ENVIRONMENT, 11th Edition (2013); ISBN: 9781133188636. Make sure it is the 11th edition.  NOTE: It is important that you bring the reading materials to class with you. We will review the cases in class. Whether you have the materials with you is among the factors I consider in assigning class participation points.
 The various items posted within our CTools course.

WEBSITE OPTIONAL
MATERIALS
COURSE DESCRIPTION AND FORMAT

 Please check the CTools website for announcements and additional materials.
 Irvin Gleim and Jordan Ray, BUSINESS LAW/LEGAL STUDIES: EXAM QUESTIONS AND EXPLANATIONS (9TH ED); ISBN: 978-1-58194-919-3. This test preparation book includes practice multiple choice questions on business law topics. A few copies of earlier editions will be on reserve in Shapiro Library and I will give you a list of the relevant questions in those editions. Relatively few pages in the book are relevant to the class so most students do not buy the entire book, but it can be purchased online if you wish. However, do remember that because there are only a few copies on reserve, there will be a rush to look at it right before the midterm and final exams.
The Ross School online course catalog describes the class this way:
Legal Aspects of Management and Finance - A study of the legal aspects of organizing, financing, and operating a business enterprise, with emphasis on the law of agency, partnerships, corporations, secured credit financing, and managing a workforce. The course includes coverage of business ethics and corporate social responsibilities. It also examines securities law (including securities fraud and

1

GRADING

insider trading) and the law of mergers and acquisitions. Cases are used to illustrate the application of legal principles to actual business problems.
BL 306 satisfies the BBA core law requirement. It is also offered as an elective for students who may have taken BL 305.
This course will introduce you to the fundamental principles of how ethics, legal rules, and public policy interact to regulate business activities and relationships, including agency and government oversight of corporations. During the semester we will be learning many of the business-relevant rules of the American legal system and we will also focus on the reasoning, policy, and ethical standards inherent in those rules. This class will help to strengthen your ability to reason through complex legal material and articulate difficult concepts to your peers
 Your final grade will be based on the following:
o Midterm Exam………..100 points o Final Exam……………120 points o Class Participation……..30 points o In-class problem set #1…25 points o In-class problem set #2…25 points
Total….300 points
 Neither bonus points nor extra credit are available.  The final grades will conform to the Ross School’s elective grade curve. Because I teach
two sections of this course and both sit for the same exams at the same time, I expect to curve the two sections together.  EXAMS: The exams will be closed book and closed notes, and will be made up entirely of multiple choice questions. The final exam will not be cumulative. However, basic knowledge gained in the first half of the course may be necessary to answer questions on the final. o Though all assignments are important, typically much of the differentiation in grades,
particularly in the top half of the class, is attributable to the midterm and final exam. o HIGHLY IMPORTANT: All sections of BL 306 will take the midterm and final
exams at the same time. You must be in class on the days assigned for those exams. If you have an exceptional reason to miss either exam (e.g., conflicting exam from other course) you must make arrangements with me by January 31. Otherwise, if you cannot take the exams on the listed dates and times, then you should not take this course. The past policy for all sections of BL 306 is that in the case of a last minute medical emergency for the midterm, you will take an exam over the first half of the course material the day following the final exam. If the emergency causes you to miss the final exam, you will be required to take an alternative exam after the professor has finished grading the other final exams. This may require you to return to Ann Arbor in May and accept an interim grade of incomplete.
o Midterm Exam: Tuesday, February 24 at 6:30 to 8:30 pm
o Final Exam: Thursday, April 23, 10:30 am-12:30 pm
 PARTICIPATION: Participation points are based on the following components: o Class attendance: Arriving late for class or departing early may count as an absence at my discretion. A seating chart (to be distributed during the second class session) and/or sign-in sheet will be used to determine class attendance. It is a violation of the Ross Community Values to sign the sign-in sheet for someone else, ask someone else to sign it for you, or to sign it when you know you haven’t attended or will not attend the vast majority of the class session.
There are no ‘excused’ absences except in the very unusual case of someone with a lengthy serious illness, etc. Three absences over the course of the term will not affect your participation grade by itself. Three absences should be enough to cover the
2

random illness, conflict with another obligation, etc. Use them wisely. As a general guideline, if you miss more than 5 class sessions or otherwise have serious participation issues, you cannot earn more than 15 points of participation.
o Quality verbal participation: What matters is the quality of one's contributions to the discussion, not the number of times one speaks. Participation will be graded on evidence of thoughtful analysis of the course materials, and insightful responses to questions and comments of other students. Thoughtful answers and comments that give what is technically the “wrong” answer are part of the learning process and are considered quality participation. You are expected to be prepared for class (i.e., have done all reading assigned for that day) and I may call on students.
o Engagement: Engage in meaningful debate but be respectful of the comments and ideas of others. Listen attentively to lectures and student comments—having side conversations with others, reading the newspaper, texting, failing to have the required course materials with you, etc., will be problematic for your class participation score.
 IN-CLASS PROBLEM SETS: You must be in class on the days assigned for the problem sets in order to receive credit. If you have an exceptional reason to miss those classes you must make arrangements with the professor immediately. Do not schedule conflicts with these dates. The first assignment is designed to give you practice in answering the types of questions you will see on the exam. The second assignment will focus on the content of the secured transactions material in the second half of the class.

ELECTRONIC DEVICES
HONOR CODE & ACADEMIC INTEGRITY

 Please silence all the electronic devices that you have with you. Each time your cell phone rings aloud in class, you will lose 5 points of class participation. I expect our class to engage your full attention; therefore, any use of electronic devices should be focused on class-related matters.
 Personal integrity and professionalism are fundamental values of the Ross Business School community. This course will be conducted in strict conformity with the Academic Honor Code. The Code and related procedures can be found at www.bus.umich.edu/Academics/Resources/communityvalues.htm. The site also contains comprehensive information on how to be sure that you have not plagiarized the work of others. Claimed ignorance of the Code and related information appearing on the site will be viewed as irrelevant should a violation take place. Non-Ross Business School students taking the course should also familiarize themselves with the Code as they will be subject to the Code as well while in this course.

STUDENTS
WITH
DISABILITIES



COURSE SCHEDULE

If you think you need an accommodation for a disability, please let me know at your earliest convenience. Some aspects of the course, the assignments, the in-class activities, and the way I teach may be modified to facilitate your participation and progress. As soon as you make me aware of your needs, we can work with the Office of Services for Students with Disabilities to help us determine appropriate accommodations. I will treat information you provide as private and confidential.

Note: 1. You are expected to do all readings before class and be prepared to discuss and apply those materials. There are many cases in the book that we will skip, and we often only cover parts of a chapter at a time—so please pay careful attention to what the assignment listings below are telling you to read. 2. Unless otherwise noted, the reading assignments refer to the textbook. Supplemental readings and additional cases generally will be posted within our CTools course. 3. This schedule is subject to adjustment as needed. Please regularly check the CTools announcements section for announcements on any additional in-class problem sets that may be posted but are not listed below.

3

# Date

Topic

Reading assignment (read before class that day)

1 M. Jan. 12 Course Introduction & Overview Syllabus and note important dates on your calendar.

2 W. Jan. 14 Business Ethics & CSR

Chapter 2. Ethics: read pp. 13-22 and the Oliver Winery, Inc. case on pages 31-32. Be prepared to discuss the various perspectives in the case.

M. Jan. 19 No Class

MLK Holiday

3 W. Jan. 21 The Court Systems

Chapter 3. Civil Dispute Resolution: read pp. 44-50.

4 M. Jan. 26 The Litigation Process & ADR Chapter 3. Civil Dispute Resolution: read pp. 51-64. Please give careful consideration to the World-Wide Volkswagen v. Woodson case on p. 51.

5 W. Jan 28 Forming a General Partnership: Chapter 30. Formation and Internal Relations of

Intentionally & Unintentionally

General Partnerships: Read pp. 546-547 (stopping at “Factors Affecting the Choice”) and 552 (starting at “Formation of General Partnerships) – end of chapter.

Note, omit Thomas v Lloyd on p. 558.

6 M. Feb. 2

Agents of the Partnership (& other Business Organizations)

Chapter 28. Relationship of Principal and Agent: Read the chapter, but omit: Miller v. McDonald’s Corp.,
p. 505; Detroit Lions, Inc. v. Argovitz, p. 510; Gaddy v.
Douglass, p. 514.

7 W. Feb. 4 8 M. Feb. 9

Chapter 29. Relationship with Third Parties: pp. 530 (begin at Tort Liability of the Principal) - 534 (end at Relationship of Agent and Third Persons), but omit Connes v. Molalla Transport System Inc. p. 532.

Agents of the Partnership (& other Business Organizations) & Third Parties
Leaving the Partnership and Ending the General Partnership

Chapter 29. Relationship with Third Parties: pp. 521530.
Chapter 31. Operation of General Partnerships: pp. 570-576 (omit Conklin Farm v. Leibowitz p. 576).
In Class Group Problem Set #1 NOTE: Be prepared for graded group work in this class session. The projects will be turned in at the end of the class. Attendance Required for Assignment Credit.
Chapter 31. Operation and Dissolution of General Partnerships: Read pp. 577-590, except omit Warnick, p. 583

9 W. Feb 11 Beyond Partnerships: Limited Chapter 30. Formation and Internal Relations of

Partnerships & Limited Liability General Partnerships: Read pp. 548 (starting at “Forms

Companies

of Business Associations)-550.

Chapter 32. Limited Partnerships and Limited
Liability Companies: Read the chapter, but omit: Wyler
v. Feur on p.600; Taghipour v. Jerez on p. 604; Estate of Coutnryman v. Farmers Coop. Ass’n. on p.606.

10 M. Feb. 16 Forming a Corporation

Chapter 33. Nature and Formation of Corporations: Read the chapter, but omit Harris v. Looney, p. 626.

4

11 W. Feb. 18 Issues in Managing a Corporation
12 M. Feb. 23 Review

Chapter 35. Management Structure of Corporations: Read pp. 656-end but omit the cases Compaq Computer v. Horton, p. 664 and Donahue v. Rodd Electrotype Co., Inc, p. 667.
Midterm Review

M. Feb. 23 Mid-term Exam

6:30-8:30 pm (rooms to be announced)

13 W. Feb. 25 ADR reviewed - Negotiation

This class will consist of a small group activity project. Attendance and participation are expected as part of class participation. Unlike the other two in-class group projects, these projects will not be graded.

Mar. 2 & 4 No classes

Winter Break

14 M. Mar. 9 Ethics and CSR Revisited

Please review the material from Chapter 2 (except the Oliver Winery case). A scenario for discussion will be distributed in class.

15 W. Mar. 11 The Employment Relationship
16 M. Mar. 16 The Employment Relationship: Discrimination Issues

Chapter 41. Employment Law: Skim pages 811-813; Read pages 824-829.
Chapter 41. Employment Law: Read pages 813 (starting at “Employment Discrimination Law”) – 824.

17 W. Mar. 18 Borrowing Money: Basic Legal Concepts of a Secured Loan

● Chapter 37. Secured Transactions: Read pages 702713 (stop before “Priorities Among Competing Interests”); omit the Border State Bank case, pp. 707-

708. ● CTools: Rules for Secured Transactions and Problem

Set #1 for Secured Transactions.

18 M. Mar. 23 Borrowing Money: Competing Creditors

● Chapter 37. Secured Transactions: Read pages 713(begin with “Priorities Among Competing Interests”) -717 (stop before “Against Lien Creditors”) and 718 (start with “Default”) -722 (stop before “Suretyship”).

19 W. Mar. 25 Borrowing Money & Secured Loans – Review & Problems

● Chapter 37. Secured Transactions: Review material from prior 2 classes.

In Class Group Problem Set #2

NOTE: Be prepared for graded group work in this class

session. The projects will be turned in at the end of the

class. Attendance Required for Assignment Credit.

20 M. Mar 30 21 W. Apr. 1

IPOs and the Basics of Securities Law
Guest Lecturer

Chapter 39. Securities Regulation: Read pages 762767 (stop at “Securities Exchange Act of 1934”). You can quickly SKIM the pages from 767-772 (the material on “Exempt Securities”) and Read pages 772 (starting at “Liability”) - 774 (stopping before “The Securities Exchange Act of 1934”).
Mr. Gary Tidwell, International Organization of Securities Commissions (IOSCO), formerly with Financial Industry Regulatory Authority (FINRA) and the Securities and Exchange Commission (SEC)

5

22 M. Apr. 6 23 W. Apr. 8
24 M. Apr. 13 25 W. Apr. 15

The SEC Act of 1934 & Securities Fraud. Short-swing trading and Insider trading
FCPA, & Tender Offers (part I)
Tender Offers (part II):

● Chapter 39 (Securities Regulation con’t): Read pages 775-782 (stop before “Insider Trading”). ● CTools: Daou Case ● Chapter 39 (Securities Regulation con’t): Read pp. 783-785 (stop before “Misleading Proxy Statements”),
and review prior class material, including problem set
questions.
● CTools: Problem Set ● Chapter 39 (Securities Regulation con’t): Read pp. 778 (starting at “Tender Offers) – 780 and 786 (starting at “Fraudulent Tender Offers) -788. ● CTools: Posted Reading
● CTools: Posted reading

26 M. Ap. 20 Review

Final Exam Review

Th. Ap. 23 Final Exam

10:30am-12:30pm (rooms will be announced)

6

