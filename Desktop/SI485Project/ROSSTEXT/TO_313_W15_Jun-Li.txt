Instructor:
Sections: Office: Phone: Office Hours:
E-mail (@umich.edu)

TO 313: OPERATIONS MANAGEMENT Winter 2015

(March 1st, 2015)

Jun Li

Jacob Chestnut

Murray(Yanzhe) Lei Evgeny Kagan

1, 4, 5 R6372 763-4612 Mon & Tue 3:30-5pm

6 R4425 763-0450 Fri 1pm-3pm in R2130

2 R5421 276-3204 Fri 12pm-2pm in R2130

3 R5425 604-7152 Fri 12pm-2pm in R2130

junwli

jacobpc

leiyz

ekagan

Class Schedule:
Section 4 Section 5 Section 1 Section 3 Section 6 Section 2

MW 8:30 am – 10:00 am MW 10:00 am – 11:30 am MW 11:30 am – 1:00 pm MW 1:00 pm – 2:30 pm MW 2:30 pm – 4:00 pm MW 4:00 pm – 5:30 pm

R2210 R2210 R2210 R2210 R2210 R2210

Li Li Li Kagan Chestnut Lei

Course Description:
Overview: Operations Management studies the processes by which inputs of materials, labor, capital, and information are transformed into products and services which customers want and are willing to pay for. These processes can be managed well or poorly. Knowledge introduced in this course will help you understand the reasons for both.
Objective: This course will provide students with the managerial tools needed to understand and articulate the impact of an organization’s business processes, and the ability to analyze and continuously improve these business processes to make things work better, faster and cheaper.
Goal of OM: Create value to be shared by managers, workers, customers, and shareholders.
Course Structure:

1

Course Topics Process Analysis
Waiting Time Analysis
Inventory Management
Quality Management
Project Management Optimization
World-Class Operations World-Class Supply Chains

Selective Concepts Flow Rate, Flow Time, Inventory, Capacity, Bottleneck, Throughput, Variability, Buffer Arrival Rate, Service Rate, Waiting Time, Queue Length, Queue Pooling
Critical Ratio, Service Level, Safety Stock, Pipeline Inventory, Cycle Inventory, Exposure Period, Lead time
Waste Total Quality Management Capability Six Sigma
Critical path

Analytical Tools 1. Process Diagram 2. Gantt Chart 3. Bottleneck Analysis 4. Little's Law 1. Inventory Diagram 2. VUT equation 3. Little's Law
1. Newsvendor Model 2. Revenue Mgmt. Model 3. Periodic Review Model 4. Continuous Review Model 5. EOQ Statistical Process Control
Critical Path Analysis Time Cost Model

Cases 1.Kristen's Cookie 2.National Cranberry 1. Logan Airport 2. REI Rentals 1. REI Rentals 2. Jamie Chang Amazon
Toys City

Shadow Price, Binding/Nonbinding Constraint, Allowable Increase/Decrease Toyota Production System Lean
Bullwhip Effect Centralized and Decentralized Supply Chains

Linear Programming Integer Programming

Mihocko Betapharm

In-class Games
House Building Game
Beer Game

Required Materials:
1. Customized Text: Contains selected chapters from: Matching Supply with Demand: An Introduction to Operations Management (3rd edition) by Cachon and Terwiesch Operations and Supply Management (14th edition) by Jacobs, Chase, and Aquilano Ebook available from McGraw Hill Education. See Announcements on Ctools for instructions of purchasing the book. See the end of this syllabus for textbook table of contents.
2. Course Pack: Contains supplemental readings and cases to be used throughout the course. Available at the Kresge Library. See the end of this syllabus for course pack table of contents.
3. The Toyota Production System Booklet: Available at the Kresge Library.

Grading:

Per standards of Ross BBA program. Student grades will be based on:

Required case reports (3 @ 5%)

15%

Selective case reports (best 4 out of 6 @ 2.5%)

10%

Class participation

10%

Midterm exam

20%

Group project

20%

2

Final exam

25%

Exams:
The midterm and final exams will consist of both quantitative and qualitative questions related to the readings, lectures, and discussions of the course. Both exams will be closed book. Students are responsible for making sure they appear for the exams on time. No latecomers will be admitted. Students who fail to write any of the exams, without prior permission from their instructor, will not be given any make-up exams. There is an alternate exam time for both exams for those with legitimate conflicts.

Assignment Preparation:
You should form a case group (no more than 3 members) and complete 3 required group case studies (National Cranberry, REI Rentals, and Amazon.com). Each group must consist of team members from the same section and must stay together for all 3 submissions.

There are other 6 selective cases studies (see the schedule on p.14). You should analyze at least 4 of them, and the best 4 will be counted for the final grade, but it is recommended that you try all 6 assignments and prepare for case discussions in class. For each assignment, you could choose to work individually (allowed only for the 6 selective cases) or work with a group of no more than 3 members (can be a different group from the required cases). You should let your team members know one week before the due date whether you choose to do the case or not.

For each case, you should do the following: – Read the case. Then ask yourself, “What are the issues here?” That is, what is the controversy to be
resolved and/or what are the decisions or evaluation to be made? – Answer the questions developed for the case. Go back through the case and develop analysis needed to
respond to the questions. Write your case report using the report templates on CTools. – After answering the questions, ask yourself, “Have I resolved the issues in the case?” In many cases,
the answers are not the end in themselves, but merely the means to help give you insights into
resolving the issues. You may then come to some meaningful conclusion or recommendation. – Make notes that would be helpful to you if you are called upon in class to give a brief synopsis of the
case, discuss the case situations, and answer the case questions.

Community Values and Academic Honor Code
As members of the Ross School community, we commit ourselves to performing our work and fulfilling our responsibilities honestly and professionally. In particular, we will not tolerate cheating of any sort by any member of our community in any situation. This course will be conducted in strict conformity with the Ross Community Values. The Statement of Community Values, Academic Honor Code, and related resources can be found at the following website: http://www.bus.umich.edu/Academics/ Resources/communityvalues.htm and will not be repeated here. Claimed ignorance of this information is irrelevant should a violation take place.

No discussion of an assignment is permitted with anyone outside your team until after the deadline for the assignment submission has expired. If you have questions in analyzing the cases, you should come to see the instructor or tutors during their office hours. You should not seek for help on the assignments from any other channels (including other case groups, senior students, and other websites).

What is considered a violation? – Rephrasing other groups’ work and including it in your submission. – Modifying other groups’ supportive documents, including Excel worksheets, tables and exhibits and
including them in your submission.
3

– Sending (or receiving) a draft or final version of any portion of work to (from) other groups. – Discussing solution approaches to assignment questions. – Switching from one group to another in the middle of the assignment, thereby bringing the work across
groups.
Assignment Submission Guidelines: Instructions for each assignment will be given on CTools. All assignments are due by the specified deadlines. No late assignments will be accepted. The assignments should be submitted on CTools. In case of problems with CTools servers, you can email the instructor a copy of your report by the specified deadline. When preparing the write-ups, please do not store your documents in public domains on the network. If you have to do so, you should password-protect your work, and remove the password when submitting the document.
Term Project: Active learning is a critical component of one’s education at the Ross School of Business. In this course, each group of 6 students will work on the Term Project, and apply Operations Management analysis to a company of their choice.
Class Participation: The instructor will come to class fully prepared each day, and students are expected to do the same. “Prepared” for students means that you have carefully read the assigned materials, have seriously attempted to complete exercises or answer assigned questions, and are ready and willing to actively engage in the classroom learning experience. A number of students will be asked to initiate the case discussion. The implicit assumption is that we all have something to contribute to the collective learning experience each day, and we all want to benefit from it. Coming prepared will maximize the benefits for everyone. Class participation will be evaluated based on each student’s comments and contributions to case and lecture discussions. The relatively high percentage placed on class participation is based on the above assumption that learning will be enhanced if well-prepared individuals contribute. The instructor will systematically record data on class participation. “Good” participation is that which enhances group learning: it could be a question, an observation, a shared experience, or an answer to a question. Students are also encouraged to submit relevant news clippings to the instructor for discussion in class.
Practice Problems: A list of suggested questions and problems (either at the back of some chapters in the text or additional problems from the instructor) will be posted on CTools periodically. Students are strongly advised to work on these questions and problems. This gives students an opportunity to practice their problem-solving skills on small, well-defined problems, and will be useful in tackling cases and exam questions.
4

TO 313 Detailed Daily Schedule

SESSION #1 PROCESS ANALYSIS I: INTRODUCTION AND OVERVIEW

Jan. 12

Learning Objectives:  Understand course structure and expectations  Understand what Operations Management (OM) is about and the central role of operational
decisions in firms’ competitiveness  Introduce the terminology and tools used to describe, analyze, and evaluate processes
Preparation:  Purchase coursepack and custom textbook
After Class:  Form case groups (no more than 3 students)  Read chapter 1(t) sections 1 and 2, chapter 2(t) sections 1, 2, and 3.  Read Kristen’s Cookie Company case, coursepack item 1(cp). Submit a 2-page case report
(selective) for grading by midnight of January 13.

SESSION #2 PROCESS ANALYSIS II: CASE KRISTEN’S COOKIE CO.

Jan. 14

Learning Objectives:  Continue discussions of process analysis techniques  Describe levers for improving throughput rate and flow time  Apply process analysis as a tool to Kristen’s Cookie Co. case
Preparation:  Write a 2-page report analyzing Kristen’s Cookie Company (A) case, and submit for grading. Be
prepared to discuss in class.

SESSION #3

Jan. 21

PROCESS ANALYSIS III: TAXONOMY OF PROCESS TYPES, INVENTORY BUILD-UP, LITTLE’S LAW,

VARIABILITY AND BUFFERS

Learning Objectives:  Understand generic process types and their implications  Understand how inventory builds up and learn to construct inventory build-up diagrams  Understand Little’s Law  Understand the effects of variability and utilization on congestions and delays  Introduce the “OM Triangle”: relation between capacity, inventory and information
Preparation:  Read chapter 1(t) sections 3, 4, 5, and 6, chapter 2(t), chapter 3(t) section 1 and 2. Also coursepack
items 2(cp) and 3(cp): “Variability, Buffers, and Inventory” and “Making Supply Meet Demand in an
Uncertain World”. Be prepared to discuss in class. Assignment will be handed out in class.
After Class:  Start preparing National Cranberry Cooperative case, course pack item 4(cp). A required case
report is due in one week.

5

SESSION #4 PROCESS ANALYSIS IV: CASE: NATIONAL CRANBERRY

Jan. 26

Learning Objectives:  Apply process analysis in a manufacturing setting  Use inventory buildup diagram to perform bottleneck analysis  Discuss capacity investment
Preparation:  Write a 5-page report analyzing National Cranberry Cooperative case, and submit for grading. Be
prepared to discuss in class.

SESSION #5 WAITING TIME ANALYSIS I: QUEUEING ANALYSIS

Jan. 28

Learning Objectives:  Understand why queues build up in service  Understand the structure and performance characteristics of basic queueing systems  Learn how to make capacity decisions using queueing analysis
Preparation:  Read coursepack item 5(cp). Textbook Chapter 8.
After Class:  Read Delays at Logan Airport case, coursepack item 6(cp). A selective case report is due in one
week.

SESSION #6 School Cancelled due to Snow

Feb. 2

SESSION #7 WAITING TIME ANALYSIS II: MANAGING CONGESTIONS: DELAYS AT LOGAN AIRPORT

Feb. 4

Learning Objectives:  Realize the power of simple queueing models in realistically estimating wait times and evaluating
options to manage congestion  Understand peak hour pricing as a method of demand management  Review linear programming
Preparation:  Read and analyze Delays at Logan Airport case. Submit a 3-page case report (selective) for grading.
Be prepared to discuss in class.
After class:  Read REI Rentals case, coursepack 11(cp). This case integrates waiting time analysis and inventory
management (lectured in the next class). Start from the analysis of ski rentals (waiting time
analysis), and then analyze snow shoe rentals (inventory management) after the next class. A 5page required case report is due by midnight Feb. 10th.

6

SESSION #8 INVENTORY MANAGEMENT I: BALANCING UNDERAGE AND OVERAGE COSTS

Feb. 9

Learning Objectives:  Recognize the power of Newsvendor logic  Practice the use of Underage and Overage Costs in contexts of inventory, and capacity
Preparation:  Read coursepack item 9(cp): “Turning the Supply Chain into a Revenue Chain” and 10(cp)
“Managing Inventories”.  Read textbook 7(t): “A Single-Period Inventory Model”.
After Class:  Read REI case, coursepack item 11(cp). Finish the analysis of snow shoe rentals (inventory
management).  Review linear programming, coursepack item 7-8(cp).

SESSION #9 RESPONSIVENESS + INVENTORY MANAGEMENT: REI RENTALS

Feb. 11

Learning Objectives:  Illustrate the sources and impact of variability  Understand when and how response time is important, and the role of operations in determining
market competitiveness
Preparation:  Write a 5-page group report analyzing REI Rentals case, and submit for grading. Be prepared to
discuss in class.  Read coursepack items 7(cp) & 8(cp): “Linear Programming: A Brief Overview” and “Note on the
Use of Solver in Excel”.

SESSION #10 QUALITY MANAGEMENT: HOUSE BUILDING GAME, QUALITY AND CAPABILITY

Feb. 16

Learning Objectives:  Understand the basics of lean operations and the role of quality management  Master fundamentals of quality management: dimensions, costs, and “Six Sigma” tools  Understand basic concepts of Statistical Process Control  Understand capability, and distinguish between process being “in control” and being “capable”
Preparation:  It is very important to start this game on time, so please be a few minutes early.  Read textbook item 5(t) and 6(t).
After Class:  Read Mihocko case, coursepack item 12(cp). A selective case report is due by midnight Feb. 17th.

SESSION #11 OPTIMIZATION: LINEAR PROGRAMMING APPLICATION: MIHOCKO INC. & MIDTERM REVIEW

Feb. 18

Learning Objectives:  Apply LP to analyze resource allocation when environmental concerns are paramount
Preparation:
7

 Read and analyze Mihocko case. Submit a 2-page case report (selective) for grading. Be prepared to
discuss in class.
After Class:  Submit one-page Project Proposal by midnight of Feb 22, Sunday.

SESSION #12 REVIEW: MIDTERM REVIEW

Feb. 23

MIDTERM EXAM

Wednesday 7:30pm-9:30pm

Feb. 25

No Class. SPRING BREAK

Feb. 26 – Mar. 8

SESSION #13 PROJECT MANAGEMENT I: CRITICAL PATH, TIME COST MODEL, DISTRUPTIVE FLEXIBILITY

Mar. 9

Learning Objectives:  Introduction to Project Management  Understand key concepts: Critical Path methods, time and cost management of projects
Preparation:  Read textbook item 4(t): “Projects”.
After Class:  Read Toys City case, coursepack item 13(cp). A selective case report due by midnight March 15th.

SESSION #14 INVENTORY MANAGEMENT II: REVENUE MANAGEMENT, EOQ, PERIODIC REVIEW

Mar. 11

Learning Objectives:  Practice the use of Underage and Overage Costs in contexts of revenue management.  Understand the Economic Order Quantity (EOQ) model.  Understand the basic logic of periodic review inventory model
Preparation:  Read “Managing Inventories” Notes, coursepack item 10(cp). Read textbook item 7(t) (skip price
break model) as a supplement.
After Class:  Read Jamie Chang case, coursepack item 14(cp). A selective case report due by midnight March 17th.

SESSION #15

Mar. 16

PROJECT MANAGEMENT II AND INVENTORY MANAGEMENT III: TOYS CITY / PERIODIC REVIEW (CONT.)

Learning Objectives:  Apply project management techniques to a simple case involving an auditing company  Continue the discussion of periodic review inventory management model

8

Preparation:  Read and analyze Toys City case. Submit a 3-page case report (selective) for grading. Be prepared
to discuss in class.
After Class:  Read Jamie Chang case, coursepack item 14(cp). A selective case report due by midnight March 17th.

SESSION #16 INVENTORY MANAGEMENT IV: CONTINOUS REVIEW & JAMIE CHANG

Mar. 18

Learning Objectives:  Apply EOQ model.  Understand continuous review inventory management model.
Preparation:  Read “Managing Inventories” Notes, coursepack item 10(cp). Read textbook item 7(t) (skip price
break model) as a supplement.  Read and analyze Jamie Chang case. Submit a 2-page case report (selective) for grading. Be
prepared to discuss in class.
After Class:  Start preparing Amazon.com’s European Distribution Strategy case, coursepack item 16(cp). A required case report is due by midnight March 22nd.

SESSION #17 INVENTORY MANAGEMENT V: AMAZON’S DISTRIBUTION STRATEGY

Mar. 23

Learning Objectives:  Understand the importance of managing supply chains and distribution networks in a global
context  Use inventory models to analyze benefits of “delayed differentiation”  Understand strategies in designing supply chains and the impact of mass customization on supply
chain management
Preparation:  Read coursepack item 15(cp): “Managing Supply Chain Inventory: Pitfalls and Opportunities.”  Write a 5-page group report analyzing Amazon.com’s European Distribution Strategy case, and
submit for grading. Be prepared to discuss in class.

SESSION #18

Mar. 25

INVENTORY MANAGEMENT + QUALITY MANAGEMENT + FLXIBILITY: TOYOTA PRODUCTION SYSTEMS

Learning Objectives:  Understand how the Toyota Production System works in practice, especially the coordination of
material flows with information flows within and across organizational lines  Understand the basic concepts of Kaizen, Jidoka, Heijunka, Kanban systems, etc.  Become familiar with basic inventory concepts: Reasons for holding inventory, inventory holding
cost, ordering cost, and tradeoff between the two costs.

9

Preparation:  Read Toyota Production System Booklet and coursepack item 17(cp). Be prepared to discuss in class the following questions: 1. What are the fundamental elements of the Toyota Production System? 2. Does Toyota respond just-in-time to customer orders? 3. What are the pros and cons of the Andon system? How much might stopping the line cost Toyota? 4. What are the pros and cons of operating mixed model assembly (i.e., having a mix of products on the line at one time)? 5. How can the Toyota Production System be applied in non-manufacturing settings? 6. How do the 4 TPS rules-in-use support Toyota’s commitment to quality?  Also refer to textbook item 7(t), “Inventory Control”. Familiarize yourself with the basic ideas of inventory management.  Submit the Midterm Project Report by midnight Wednesday March 25. See CTools for the guidelines. Each group will be asked to schedule a meeting with the instructor within the next week to discuss their projects.

SESSION #19 SUPPLY CHAIN MANAGEMENT I: THE BEER GAME

Mar. 30

Learning Objectives:  Understand the sources of variability that bedevil Supply Chain Management
Preparation:  Read carefully the “Beer Game” handout and instructions for playing the game. You will need to
understand all the details to be able to complete the tasks during class time.

SESSION #20

Apr. 1

SUPPLY CHAIN MANAGEMENT II: BULLWHIP EFFECT, SUPPLY CHAIN COORDINATION, AND E-BUSINESS

Learning Objectives:  Understand the Bullwhip Effect in supply chains  Translate strategic E-Business and IT choices into operational decisions  Understand “virtual integration” supply chain model
Preparation:  Read additional readings to be posted on CTools. Be prepared to discuss in class.
After Class:  Read Betapharm Corp. case, coursepack item 18(cp), a selective case due by midnight Apr. 5th.

SESSION #21 SUPPLY CHAIN MANAGEMENT III: BETAPHARM CASE + SUPPLY CHAIN SUSTAINABILITY

Apr. 6

Learning Objectives:  Understand the differences between and attendant challenges of indirect and direct materials sourcing  Understand the role of online auctions in competitive sourcing  Introduce the concept of “total cost of contract ownership” and how to evaluate it using OM toolset
10

Preparation:  Read and analyze Betapharm Corp. case. Submit a 3-page case report (selective) for grading. Be
prepared to discuss in class.

SESSIONS #22, 23, AND 24 TERM PROJECT PRESENTATIONS
See guidelines on the next page for details. Presentation schedule will be announced in class.

Apr. 8, 13, 15

SESSION #25 COURSE WRAP-UP AND REVIEW
Preparation:  Come prepared to ask questions on any concepts that are not clear.  Submit Final Project Report in class.

Apr. 20

FINAL EXAM

1:30 AM – 3:30 PM

April 27

11

Term Project Guidelines
Objectives  To apply the principles of Operations Management to business situations.  To develop business problem analysis skills (in both quantitative and qualitative analytics).  To learn teamwork and presentation skills.
Overview  Team Members: 5 or 6 TO313 students, all from the same section.  Project Context: Any business operation.
Deliverables  1-page Project Proposal is due by Sunday February 22 midnight on CTools.  Midterm Project Report is due by Wednesday March 25 midnight on CTools.  Class presentations on one of the dates: Apr. 8, 13, 15.  Final Report is due in class on Monday April 20.
Project Proposal Guidelines Please make sure the following information is included in the project proposal:
 name of group members  name of the company to be studied & people contacted there  basic description of what the company does, the customers and markets served  basic description of the process  brief outline of what the group intends to study at that company There is no hard constraint on the length of your proposal. The more details you can provide, the better idea the instructor will have of what you are going to study. If your project seems to be too thin or too complex, the instructor will get back to you with comments. Please make sure that you will have access to people at that company who can give you details on the processes, the data on demand, etc. that you may need to do your project well.
Midterm Project Report Guidelines See CTools Web site.
Final Report Format The written report should be 1.5 line-spaced and 11pt font or larger. The length of the body of the report is limited to 15 pages. Up to additional 10 pages of exhibits may be appended. Please use the following framework to organize your paper.
 Executive Summary. State briefly (not exceeding 1 page, single spacing OK) the goal of the project, the problem, your recommendation, and what value this project may bring to the company/organization. If your analysis is about why a company/organization is so successful, you should describe the reasons for their success and your recommendations on how they could continue to remain successful.
 Introduction and Background Information: This section should not exceed 3 pages in length. You can divide this section into following subsections. i) Industry Background: This section should provide the reader some information about the industry in which this firm is competing and provide a few vital details pertaining to the size of the industry, the major competitors in the marketplace, and the relative shares of the market that these firms currently command.
12

ii) Company Background: This section should focus on background information regarding the firm and its size, annual sales, and details about its product portfolio. This section should also include information about the location of the firm’s facilities, and the products/clients these facilities may be serving.
iii) Facility Background: This section should specifically address details pertaining to the facility that you investigated.
 Current Process and Problem Definition: i) Characterization of Current Process: In this section, a clear description of the current process should be provided and a detailed process flow diagram should be attached to characterize the various states in the production or service process. ii) Problem Statement: In this section you must describe what may in fact be the problems associated with the existing system. This sub-section sets the stage for why you have undertaken this project and how your analyses and recommendations will improve the performance of the system. iii) Primary Goals of the project and the flow of rest of the report: Here you must briefly describe the goals of the project and also detail how the remainder of the report will flow.
 Analyses and Recommendations. This section should contain the various recommendations you have made for improving the current process and detailed analysis for each of the recommendations. The analysis can be either quantitative or qualitative. An integral part of the analysis is to estimate chances for success and impact on performance if your recommendations are implemented. In case your project is about why a company/organization is successful, this section should contain your analysis for the recommendations on how the company can continue to remain successful.
 Appendices: Please attach relevant exhibits, tables and figures (not to exceed 10 pages).
Presentation Your group must present your project to the class during the regular class meeting times near the end of the term. Each group member must be actively involved in the oral presentation. Presentations will be limited to 18 minutes, plus a 2 minute question-and-answer period moderated by the presenting group (total of 20 minutes per group). Each group member must be actively involved in the presentation.
Grading Criteria  Paper: Structure, Content, Exhibits. The main emphasis of the grading will be on clarity and depth of your analysis, demonstration of a good understanding of OM concepts and tools and their application to the operation studied and the applicability/feasibility of your recommendations.  Presentation: Clarity, Content, Delivery, Visual Aids, Time Management, Discussion Management.  Self and Peer Evaluation: All team members need to actively participate in the term project. In extreme cases, individual grades may be adjusted up or down based on self and peer evaluations.
Guidance  Sample projects will be e-mailed to you on CTools.  Teams must meet with the instructor at least once to report progress and get advice.  Teams can also meet with the teaching assistants (senior BBAs) to get advice.
13

No Date
1 Jan 12(M) 2 Jan 14(W) 3 Jan 21(W) 4 Jan 26(M) 5 Jan 28(W)

TO 313: Winter 2015 Course Outline

Topic
Process Analysis I: Lecture: Introduction and Overview Process Analysis II: Case: Kristen’s Cookie Co. Process Analysis III: Lecture: Taxonomy of Process Types, Inventory Build-up, Little’s Law, Variability and Buffers Process Analysis IV: Case: National Cranberry Waiting Time Analysis I: Lecture: Waiting Time Analysis

Required Readings (see detailed reading list on pp.5-11)
1(t)

Submissions

Case Group

Project Group

1(t) 2(t) 1(cp)

2-page

1-3(t) 2-3(cp) 1(t) 4(cp) 5(cp)

5-page [required]

6 Feb 2(M) School Cancelled due to Snow

7 Feb 4(W) 8 Feb 9(M) 9 Feb 11(W) 10 Feb 16(M)
11 Feb 18(W)

Waiting Time Analysis II: Case: Delays at Logan Airport Review: Linear Programming
Inventory Management I: Lecture: Balancing Underage and Overage Costs Waiting Time Analysis + Inventory Management Integrated Case: REI Rentals
Quality Management Game: House Building Lecture: Quality and Capability
Optimization: Linear Programming Case: Mihocko Inc. Review: Midterm Review I

3(t), 6(cp) 7(t), 9-10(cp)
11(cp) 7-8(cp)
7(t), 12(cp)

12 Feb 23(M) Review: Midterm Review II

Mid-Term Exam Feb 25 (W) 7:30pm – 9:30pm

Feb 26 – Mar 8

No Class. Spring Break.

13 Mar 9(M)

Project Management I: Lecture: Critical Path, Time-Cost Model, Disruptive Flexibility

4(t)

Inventory Management II: 14 Mar 11(W) Lecture: Revenue Management, EOQ,
Periodic Review Project Management II: Case: Toys City 15 Mar 16(M) Inventory Management III: Lecture: Periodic Review (cont.) Inventory Management IV: 16 Mar 18(W) Lecture: Continuous Review Inventory Case: Jamie Chang

7(t), 10(cp) 13(cp)
14(cp)

3-page
5-page [required]

2-page

Project Proposal

3-page 2-page

14

17 Mar 23(M)

Inventory Management V Case: Amazon’s Distribution Strategy

Inventory Management + Quality

18 Mar 25(W) Management + Flexibility Lecture: Toyota Production System

19 Mar 30(M)

Supply Chain Management I Game: The Beer Game

Supply Chain Management II

20 Apr 1(W)

Lecture: Bullwhip Effect, Supply Chain Coordination, and E-Business

21 Apr 6(M)

Supply Chain Management III Lecture: Strategic Global Sourcing & Supply Chain Sustainability

Case: Betapharm Corp.

22 Apr 8(W) Project Presentations: I

23 Apr 13(M) Project Presentations: II

24 Apr 15(W) Project Presentations: III

15-16(cp)

5-page [required]

17(cp), TPS booklet

18(cp)

3-page

25 Apr 20(M) Review: Final Exam Review

FINAL EXAM Apr 27 Monday 1:30-3:30pm

MidProject Report
Final Project Report

15

Textbook (t) Table of Contents

Chapters in the Customized Text

Source

1(t). The Process View of the Organization

CT Ch2

2(t). Understanding the Supply Process: Evaluating Process Capacity

CT Ch3

3(t). Variability and Its Impact on Process Performance: Waiting Time Problems

CT Ch8

4(t). Project Management

JCA Ch4

5(t). Six-Sigma Quality

JCA Ch12

6(t). Statistical Quality Control

JCA Ch13

7(t). Inventory Management

JCA Ch20

Note: Textbook references will be given in terms of the chapter number and pages in the custom text, not the source text.

Course Pack (cp) Table of Contents

Course Pack Materials 1(cp). Kristen’s Cookie Company (A) 2(cp). Variability, Buffers and Inventory 3(cp). Making Supply Meet Demand in an Uncertain World 4(cp). National Cranberry Cooperative 5(cp). There’s More to a Line Than Its Wait 6(cp). Delays at Logan Airport 7(cp). Linear Programming: A Brief Overview 8(cp). Note on the Use of Solver in Excel 9(cp). Turning the Supply Chain into a Revenue Chain
10(cp). Managing Inventories
11(cp). REI Rentals Case 12(cp). Mihocko Inc. 13(cp). Toys City 14(cp). Jamie Chang 15(cp). Managing Supply Chain Inventory: Pitfalls and Opportunities 16(cp). Amazon.com’s European Distribution Strategy 17(cp). A Brief Note on the Toyota Production System 18(cp). Procurement at Betapharm Corp. (A) (B) (C)

Comment Class 2 Case Class 3 Reading Class 3 Reading Class 4 Case Class 5 Reading Class 6 Case Class 6 Reading Class 6 Reading Class 7 Reading Class 7, 14, 15, 16 Reading Class 8 Case Class 11 Case Class 15 Case Class 16 Case Class 17 Reading Class 17 Case Class 18 Reading Class 21 Case

16

