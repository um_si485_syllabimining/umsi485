New Product and Innovation Management
Winter 2015

MKT 425

Instructor: S. Sriram

Office: R5474

Phone: 734-647-6413

E-mail: ssrira@umich.edu

Office Hours: Tuesday/Thursday 1.00-2.00 PM or by appointment

Course Web Page: on C-Tools

COURSE DESCRIPTION Innovation and development of new products and services are essential for the success of any organization. At the same time, designing and launching new products is risky. Managing the new product development process therefore involves identifying new product ideas that have great potential and lowering the risk of their failure. The course will expose students to (a) creative techniques for idea generation, (b) fine-tuning these ideas to develop products and services that meet specific consumer needs, and (c) testing the feasibility of these ideas. The course uses lectures, cases, and outside speakers. Moreover, the course includes a project wherein student teams need to use the creativity techniques covered in this class to come up with new product ideas and perform a concept test to evaluate their feasibility. The course will be useful to students interested in product/brand management, management consulting, and entrepreneurship. Course objectives: The goal of this course is to develop skills in carrying out marketing analysis and preparing marketing plans for a company's product line. The specific objectives of the course are:

• To familiarize students with applications of new product management, planning and policy techniques
• To provide an understanding of the concepts and processes involved in new product development
• To develop skills in conducting business analyses for new products • To provide an understanding of test marketing and product introduction
Course Materials: Course Packet:
a. Break Free from the Product Life Cycle, Harvard Business Review, May 2005 b. Mountain Man Brewing Company, Harvard Business School brief case # 2069 c. Finding Your Innovation Sweet Spot, Harvard Business Review, March 2003 d. Hulu: An Evil Plot to Destroy the World?, Harvard Business School case # 9-510-005 e. The Four Faces of Mass Customization, Harvard Business Review, January-February 1997 f. Montreaux Chocolates USA: Are Americans Ready for Healthy Dark Chocolate?, Harvard
Business School case # 9-914-501 g. The Promise of Prediction Markets: A Roundtable, Mckinsey Quarterly, 2008, Number 2 h. Aqualisa Quartz: Simply a Better Shower, Harvard Business School case # 9-502-030

1

MKT 425
Additional Readings (Not Required) a. Goldenberg, Jacob and David Mazursky (2002), Creativity in Product Development, Cambridge. b. Kelley, Tom (2001), The Art of Innovation, Doubleday. c. Kim, W. Chan and Renee Mauborgne (2005), Blue Ocean Strategy, Harvard Business Press. d. Michalko, Michael (2006). Thinkertoys, Ten Speed Press. e. Schwartz, David (1987), Concept Testing, AMACOM. f. Solverstein, David, Philip Samuel and Neil DeCarlo (2009), The Innovator’s Toolkit, Wiley. g. Surowiecki, James (2004), Wisdom of the Crowds, Random House. h. Thomas, Robert (1995), New Product Success Stories, Wiley.

EVALUATION AND GRADING

Grading:

Class Participation: Exams: Assignments & Written Case Analyses: Project Report & Presentation:

20% 20% 30% 30%

Class Participation: Students’ participation in the class discussion is an important part of the learning process. Each student is expected to contribute regularly to class discussion, as called upon by the instructor and on a voluntary basis. Each class, students should be prepared to discuss the assigned readings and also relate them to previous materials discussed in class. This will help to build bridges between and among topics. While some parts of the class discussions will be devoted to understanding concepts described in the assigned readings, we will also criticize some conclusions of the assigned readings.

To a great extent, the benefits that you would derive out of class discussions and assignments are related to your willingness to express your own viewpoints and conclusions to critical judgment of the class, and by building upon or critically evaluating the judgment of others.

Exams: There will be two exams during the course of the semester. Both exams will be held in class.

Assignments & Written Case Analyses: During the course of the semester, you need to submit three written case reports. One of these is mandatory. You can submit any two of the remaining three cases. In case you submit all three optional case write ups, I will pick the best two for your final grade.

Mandatory case: Mountain Man Brewing Company Optional cases (pick two):
Hulu: An Evil Plot to Destroy the World? Montreaux Chocolates USA: Are Americans Ready for Healthy Dark Chocolate? Aqualisa Quartz: Simply a Better Shower

These reports are to be submitted in groups of 4-6 members. You may retain the same teams for the project. In writing the case analyses, use the discussion questions at the end of the syllabus as the guideline. Pay special attention to any suggested analyses. Remember that some cases require more in-

2

MKT 425
depth analysis than others. All write-ups are due at the beginning of the class in which we plan to discuss the case.

In addition to these case analyses, there will be two assignments during the course of the semester. One of these will be an individual assignment, while the other can be done with your group. Depending on availability of time, you may need to make a presentation of the contents of your assignment.

New Product Development Project The objective of the new product development project is to provide the students an opportunity to use the concepts learned in the class in order to come up with a new product/service idea and test its feasibility. The students will work in teams of 4-6 members. At the end of the semester, you will submit a report that describes your new product concept including how you arrived at it as well as how it fared in the concept test. You will also make a presentation that describes how you implemented the four phases above, the key findings, and what you learned from the whole exercise. Please refer to the Project Guidelines handout for more details on the project.

The following are some key dates for the project.

Date Feb 3
Mar 19 April 14, 16 & 21
April 21

Deliverable Preliminary Report (1-2 pages) with
• Your choice of “product” category • Preliminary secondary data for the category
(market size, trends, competition etc.) Mid-Term report with
• Focus group results
• Ideas for new “product” • Concept Testing Questionnaire • Mid-term peer evaluation Project presentations Project Report • Submit peer evaluation

Project Grading: Your project is worth 30% of the course grade. Of this, 25% will be based on the final report and the remaining 5% will be based on the project presentation. The main objective of this course is to get you thinking in a structured fashion while identifying and evaluating new product ideas. Therefore, I will be grading your project report based on the quality of your analysis as well as on the creativity/feasibility of the new product idea. The broad categories I will be evaluating you on are:
• Quality of the secondary data analysis • Insights from the focus group study • Identification of alternative concepts from the focus group • Design of the concept testing questionnaire • Data analysis • Feasibility of the chosen concept

I will be grading your presentations based on the following criteria: • Clarity of presenters

3

• Professionalism of presenters • Quality of visual aids • Organization/time allocation. • How interesting was the presentation? • How actionable and useful was this study? • Quality of Analysis

MKT 425

Peer Evaluation: The objective of the peer evaluation is to ensure that there are no free riders. While it is acceptable to have marginal differences in contribution across team mates, it would be unfair for a few members of the team to pull the project along. In order to ensure this, you will submit peer evaluations along with your mid-term and final reports. I will be using the final peer evaluation submitted along with the final report to apportion the final project grade across teammates. For a template of the peer evaluation form, please see the last page of the syllabus.

Re-grading Policy Errors in grading do happen from time to time (e.g., an appendix gets overlooked). The re-grading policy is designed only to rectify these errors. Therefore, please consider your re-grading requests carefully before submitting them. The requests must be submitted in writing with a detailed explanation of why you think the request is legitimate. Requests should be submitted within seven days of receipt of the graded paper.

Laptop Policy Laptop and PDA/mobile phone use is prohibited during lectures and case discussions. The only exception is during specific class sessions which require laptop use.

Academic Honor Code Personal integrity and professionalism are fundamental values of the Ross Business School community. This course will be conducted in strict conformity with the Academic Honor Code. The Code and related procedures can be found at www.bus.umich.edu/Academics/Resources/communityvalues.htm. The site also contains comprehensive information on how to be sure that you have not plagiarized the work of others. Claimed ignorance of the Code and related information appearing on the site will be viewed as irrelevant should a violation take place. Non-Ross Business School students taking the course should also familiarize themselves with the Code as they will be subject to the Code as well while in this course.

Students with Disabilities If you need an accommodation for a disability, please let me know at your earliest convenience. Some aspects of the course, the assignments, and the in-class activities may be modified to facilitate your participation and progress. As soon as you make the instructor aware of your needs, we can work with the Office of Services for Students with Disabilities to help us determine appropriate accommodations. I will treat information you provide as private and confidential.

4

Session 1

Day Tue, Jan 13

2 Thu, Jan 15
3 Tue, Jan 20
4 Thu, Jan 22
5 Tue, Jan 27
6 Thu, Jan 29
7 Tue, Feb 3 8 Thu, Feb 5 9 Tue, Feb 10 10 Thu, Feb 12 11 Tue, Feb 17
12 Thu, Feb 19
13 Tue, Feb 24 14 Thu, Feb 26 15 Tue, Mar 10
16 Thu, Mar 12

17 Tue, Mar 17

18 Thu, Mar 19

19 Tue, Mar 24

20 Thu, Mar 26 21 Tue, Mar 31 22 Thu, Apr 2 23 Tue, Apr 7 24 Thu, Apr 9 25 Tue, Apr 14

MKT 425: Tentative Schedule of Classes

MKT 425

Topics • Introduction to New Product
Management • New Product Development Strategies • Opportunity Identification I
• Opportunity Identification II
• Opportunity Identification III • Mountain Man Brewing Company
Case
• Idea Generation I
• Idea Generation II (Guest Speaker) • Idea Generation III • Design (Guest Speaker) • Perceptual Mapping • Preference Analysis • Benefit Segmentation & Product
Positioning • Mass Customization • Hulu Case
Exam • Concept Testing
• Sales Forecasting Techniques I
• No Class instead of Project Meeting with the Instructor during the week of March 24
• Sales Forecasting Techniques II
• Advertising and Product Testing • Pre-test Market Forecasting/Test
Marketing • Test Marketing/Prediction Markets
• Testing (Guest Speaker)
• Montreaux Chocolates case
• Marketing Mix Decisions for New Products
• Aqualisa Quartz case

Readings/Assignment
Read and be prepared to discuss “Break Free from the Product Lifecycle” article Submit case write up Read the “Finding your Innovation Sweetspot Article” Submit Preliminary Project Report Submit Idea Generation Assignment Read and be prepared to discuss “The Four Faces of Mass Customization” article Submit Case Write Up
Submit Mid-term project report Read and be prepared to discuss “The Promise of Prediction Markets” article Submit Forecasting Assignment Submit Case Write Up
Submit Case Write Up

Exam

Project Presentations

5

26 Thu, Apr 16 27 Tue, Apr 21

Project Presentations Project Presentations

MKT 425 Submit Final Report

Preparation Guide for Cases Mountain Man Brewing Company 1. What has made MMBC successful? What distinguishes it from competition? 2. How did MMBC create such a strong brand? What has caused MMBC’s decline despite the strength
of its brand? 3. Should MMBC introduce the light beer? In answering this question, pay attention to the following:
a. What would happen under status quo? b. What is required for Mountain Man Light to break even in two years? Note that
breakeven implies making the same amount of total profit with both Mountain Man Light and Mountain Man Lager as they would have made with just Mountain Man Lager. You can work this out under the assumption that MM Lager would lose between 5% and 20% of its sales due to MM Light. Hint: There are two ways to answer this question: (i) assume a reasonable % loss in lager sales and then calculate how many barrels of light beer need to be sold in order to breakeven or (b) use the projections of light beer sales in the case and calculate the % loss of lager sales at which they will breakeven. c. What are the risks associated with the launch? d. What cannibalization rate do you believe is reasonable? How can we infer this prior to the launch? e. What other strategic options exist if Mountain Man Light is not introduced? Hulu: An Evil Plot to Destroy the World? 1. In your view, should Kilar move Hulu away from a pure advertising-supported model? If so, what is the preferred new model? 2. How does Hulu’s value proposition differ from that of traditional broadcast and cable television? How does it differ from that of Youtube? What explains its success to date? 3. How does Hulu serve content owners, users, and advertisers? Do the needs of these customer groups converge? 4. What are the underlying economics of Hulu vs. broadcast and cable television? Specifically, how do the revenues per hour-long program compare across television and Hulu? 5. The different broadcasters – ABC, CBS, NBC, FOX, and CW – have each chosen a different approach to the internet. Which strategy is preferred? Montreaux Chocolates 1. What are the key challenges and marketing issues that Andrea Torres must address at this time? Why do you feel that these issues and challenges are key to the success of the new product line? 2. How well do the BASES concept test, BASES II, and test marketing help in assessing awareness, trial, availability, and repeat purchase rate (i.e., ATAR)? 3. What are the roles of: a. Product
6

MKT 425
b. Advertising copy c. Advertising budget d. Price in determining sales volume in the market, i.e., which aspects of ATAR do each of these affect? How well do the BASES tests measure the impact of each of these marketing program elements? 4. Torres wants to know the concepts obtained only average purchase intent scores of 23% top-box and 40% second-box. Where do you think his problems lie? What can she do to improve the purchase intent scores? 5. What is your recommendation for Andrea Torres? 6. What are the pros and cons of the BASES II methodology vis-à-vis (a) the BASES concept test and (b) test marketing? Aqualisa Quartz: Simply a Better Shower 1. What is the Quartz value proposition to plumbers? 2. How much does an average plumber contribute to Aqualisa’s profitability? 3. What is the Quartz value proposition to consumers? Suggested Analyses: • What is the installation cost to consumers with Quartz? • What is the installation cost without Quartz? • How much do they save? 4. Why is the Quartz shower not selling? 5. Aqualisa spent three years and €5.8 million developing the Quartz. Was the product worth the investment? Is Quartz a niche product or a mainstream product? 6. Aqualisa currently has three brands: Aqualisa, Gainsborough, and ShowerMax. What is the rationale behind this multiple brand strategy? Does it make sense? 7. What should Rawlinson do to generate sales momentum for the Quartz product? Should he change his marketing strategy to target consumers directly, target the DIY market, or target developers? Should he lower the price of Quartz? Or should he do something different altogether?
7

SAMPLE PROJECT -PEER EVALUATION FORM

MKT 425

The purpose of this form is to allow you to evaluate the relative contribution of the members of your group to the group project. In making your evaluation, you should divide 100 points among the members of the group, other than yourself. Thus, the total in each column should be 100.

TEAM MEMBERS Your Name: Other Team members:

OVERALL CONTRIBUTION Please do not evaluate yourself.

Total Please check that the total adds up to 100. Comments:

100

8

