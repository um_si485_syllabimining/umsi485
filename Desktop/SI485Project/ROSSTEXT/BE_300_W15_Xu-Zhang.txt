BE300: Applied Microeconomics Winter 2015

University of Michigan Ross School of Business

BE 300: APPLIED MICROECONOMICS

Winter 2015

All Sections Meet Tuesdays & Thursdays

Instructors: Kyle Handley Room: R0220
Sections: Sec. 3 @ 8:30am

Ach Adhvaryu R0210 Sec. 5 @ 8:30am

Xu Zhang

Tammy Feldman Sarah Miller

R0210

R0210

R0210

Sec. 2 @ 10:00am Sec. 7 @ 11:30am Sec. 1 @ 2:30pm

Sec. 6 @ 1:00pm Sec. 4 @ 4:00pm

e-mails: handleyk@umich.edu xuzh@umich.edu mille@umich.edu

adhvaryu@umich.edu trfeld@umich.edu

office hrs: Refer to your Section’s CTools site for professor office hours info and updates. Tutor/TA office hours schedules will also be posted on CTools.

Check email daily for course updates and new materials.

Unless specifically announced for a particular class session, electronics (e.g., laptops, tablets, and smartphones) should not be used during class sessions. Use of electronics during class sessions will affect class participation grades.

COURSE OBJECTIVE:

Applying fundamental microeconomic concepts to a wide range of managerial and strategic decisions in a business setting.

COURSE MATERIALS:

Required:

Jeffrey M. Perloff & James A. Brander, Managerial Economics and Strategy (Pearson 2014). Please see CTools announcement for text purchase options.

Other required materials, including cases, problem sets, readings, practice problems, and practice exams, will be posted on the course website (at ctools.umich.edu).

Optional:

MyEconLab (online tutorial program covering relevant microecon topics)

BE300: Applied Microeconomics Winter 2015

University of Michigan Ross School of Business

COURSE REQUIREMENTS AND EVALUATION:

Class participation Homework (problem sets & cases) On-line Ctools Quiz Midterm Final Exam

10% 15%
5% 30% 40%

(available Feb. 13; complete by 8:00 am Feb. 16) (Wed., March 11, 6:30 pm – 8:30 pm) (Wed., Apr. 29, 10:30am – 12:30pm)

The emphasis of this course is on applying economic concepts and principles to real world managerial problems. Although most concepts listed on the syllabus will be covered in class, every student will be responsible for understanding each of the listed economic concepts even if not specifically discussed in class, so that class time can be devoted principally to applying, rather than learning, the concepts. An important part of mastering the application of economic principles is the experience you will gain setting up and solving problems contained in the cases and exercises. These are very important and are second only to class attendance and attention in developing your ability to apply the economic concepts in courses, as well as in your careers.

QUIZ: The primary purpose of the on-line quiz is to give you some experience with the types of questions you will be asked on the midterm and final, and to alert you if you are falling behind in the course. Note that the questions on the midterm and final will be harder than those on the quiz. Actual quizzes, midterms, and final exams from previous years are posted on the course website for you to look at and practice with.

MIDTERM & FINAL EXAMS: The midterm and final exams are cumulative—that is, you may be tested on all material up to that point in the course. Please note the date and time of the exams carefully. This is a core class; no make-up exams will be given except for valid medical excuses.

HOMEWORK: Homework (i.e., case write-ups and problem sets) will be graded on a “check” (2 points)/“check-minus” (1 point) basis. Answers are due at the beginning of class on the day the homework is scheduled to be discussed. (A schedule of class topics will be posted and updated regularly on the course website.) Because we will often be covering topics and material for the first time as we work through the cases
in class, your written answers to the cases are not expected to be perfect. But they should reveal a level of thought and effort to understand and answer the questions.

CLASS PARTICIPATION: Class participation is a function of attendance, contributions to in-class discussions (including questions and volunteering of answers), and attentiveness during class sessions (i.e., refraining from using electronic devices when not required for an in-class exercise). Submitting current news stories and/or articles relevant to the course material (and that are subsequently the basis for an in-class discussion) will also count as class participation; send suggested news articles/stories by email to your section instructor or grader. All students start the semester with a mean score (B+) for class participation, and your class participation grade will increase or decrease as a factor of the elements listed above.

ABSENCES: Each student can have two “wild card” absences without penalty; this allows for
situations such as an internship interview that cannot be rescheduled, a family emergency, etc. Any absences beyond the two will result in a lower class participation grade, but may be “excused” (i.e., no penalty) if you
submit a written explanation to your section grader before the class session.

All readings listed on the syllabus are required, unless noted otherwise. This class builds on material from the course prerequisite principles of microeconomics. Class time will be spent on discussion, solving business world problems and examples, cases, and simulations. To get the most out of lectures it is required and expected that students will read the textbook chapters and related reading material before the date of the class session for which that material is listed.

BE300: Applied Microeconomics Winter 2015

University of Michigan Ross School of Business

Accommodations for Students with Disability Statement: Students who require accommodation for a disability should let me know as soon as possible. Once aware of your needs, the Office of Services for Students with Disabilities will determine appropriate accommodations for your needs. Any information on disability you provide will be treated as private and confidential.

Academic Honor Code
Personal integrity and professionalism are fundamental values of the Ross School community. To help ensure that these values are upheld, this course will be conducted in strict conformity with the Academic Honor Code. The code and related procedures can be found at the following website: http://www.bus.umich.edu/Academics/Resources/communityvalues.htm.
Ignorance of the code and related information appearing on the site will not excuse a violation.

BE 300 Rules regarding Student Collaboration Students may work together on answering the cases but must (i) write up and submit their own answers in their own words, and (ii) provide the names of students with whom they worked.
Students may NOT collaborate on exams or the take-home quiz.

COURSE OUTLINE & SYLLABUS
1. PRELIMINARIES 1. Course policies and overview 2. Intro to BE300
Readings: Introduction: Ch. 1, Ch. 7.1 & 7.5

2. INDIVIDUAL AND MARKET DEMAND

1. Decision making on the margin 2. Consumer surplus 3. Demand estimation, elasticity, and revenue

Cases: Readings:

“Electric Power Purchasing” (write-up) “O2, Brute” (for class discussion only) “Estimating the Demand for Tagamet” (write-up)
“In the Dark” (Wall St. Journal) “Selected Estimates of Own-Price Elasticities”

Textbook:

Demand and supply: Ch. 2.1 – 2.2 Consumer surplus: pp. 255–58
Elasticities: Ch. 3 intro & Ch. 3.1 Regression Analysis: Ch. 3.2 – 3.5

BE300: Applied Microeconomics Winter 2015

University of Michigan Ross School of Business

3. COSTS AND PROFIT MAXIMIZATION

1. Economic and accounting costs 2. Cost concepts:
a. Opportunity, avoidable, and sunk b. Fixed and variable; total, average, and marginal c. Short-run and long-run 3. Economies of scale and scope; learning curves 4. Capacity decisions (long-run horizon): Planned output 5. Operating decisions (short-run horizon): Production and price setting

Cases:

“Franchisee McCosts” (for class discussion only) “Moo’ve on Out? Should Households Still Own Cows and Buffalo?” (write-up)

Readings: Textbook:

“Cattle look good on gift cards, but look like poor investments” (Brittan, FT.com) “Why Don’t Airlines Just Add More Flights at the Holidays?” (Phil LeBeau, CNBC) “Outsource your way to success” (Rampell, NYT Magazine) “How to Walk Away” (Halvorsen, The Atlantic)
Costs: Ch. 5 intro, Ch. 6 intro & Ch. 6.1 – 6.3 Diminishing Marginal Returns pp. 132 – 133
Returns to scale: Ch. 5.4
Technological change: Ch. 5.5 Economies of Scale: pp. 176 – 179 Learning Curves & Economies of scope : Ch. 6.4 – 6.5
Profit maximization: Ch. 7.2

4. COMPETITIVE SUPPLY AND MARKET ANALYSIS

1. Price-taking, competitive short-run supply, and market equilibrium 2. Entry, competitive long-run supply, and long-run market equilibrium 3. Producer surplus (economic rents) 4. Efficiency, deadweight loss, and distributional analysis 5. Applications
(a) Tax incidence (b) Import quotas and tariffs 6. Rent seeking and government policy

Cases: Readings:

“U.S. Sugar Import Quotas” (write-up) “The Politics of Sugar: Sugar’s Iron Triangle” (Center for Responsive Politics)

Textbook:

Market equilibrium: Ch. 2 Intro & Ch. 2.3 – 2.4
Market Structure: Ch. 2.6 & 7.5 Competitive firms and markets: Ch. 8 intro & Ch. 8.1 – 8.3 Monopoly: Ch. 9 intro & Ch. 9.1 – 9.4
Efficiency / Economic Well-Being: Ch. 8.4 Government interventions: Ch. 2.5, pp. 265–268, Ch. 16 intro & Ch. 16.1
Quotas, tariffs, and rent seeking: Ch. 17.3

BE300: Applied Microeconomics Winter 2015

University of Michigan Ross School of Business

5. GLOBAL BUSINESSES AND THE FIRM
1. Transaction costs, “make vs. buy”, and vertical integration 2. Relationship-specific investments, opportunism, and credible commitments 3. Hold-up problems 4. Multinational firms and outsourcing

Textbook:

Vertical integration and transaction costs: Ch. 7.4
The hold-up problem: Ch. 13.5
Comparative advantage: Ch. 17 intro & Ch. 17.1 Multinational Firms and Outsourcing: Ch. 17.4 – 17.5

6. MANAGERIAL DECISIONS UNDER RISK AND UNCERTAINTY

1. Commodities, currency, and risk management 2. Individual choice and attitudes toward risk 3. Market equilibrium with risk

Cases:

“When Oil Prices Break Bad: A Crude Case Study” (for class discussion only) “Failures in Franchising” (for class discussion only)

Readings: “A Marketer’s Guide to Behavioral Economics” (Welch, McKinsey Quarterly)

Textbook:

Exchange rates and hedging: pp. 582–583 Risk: Ch. 14 intro, Ch. 14.1 – 14.4 Oil drilling and shutdown: pp. 242–243 & p. 494
Behavioral economics of risk: Ch. 4.6 & 14.5
Present/Future Value: Appendix 7

7. INFORMATION, NETWORKS, AND THE INTERNET
1. Asymmetric information and incentives: a. Moral hazard b. Adverse selection and signaling
2. Information and networks 3. The Internet and advertising 4. Externalities 5. Property rights, knowledge goods and intellectual property

Case:

“Zombie Apps” (write-up)

Readings:

“Why free parking is bad for everyone” (Stromberg, Vox.com) “Websites Vary Prices, Deals Based on Users’ Information” (Valentino-Devries et al.,
Wall St.Journal) “Get Paying Users by Giving Your App Away” (Smith, The Accelerators, WSJ.com) “The Economics of Freemium” (Holmes, The Accelerators, WSJ.com)

Textbook:

Adverse selection: Ch. 15 intro, Ch. 15.1 – 15.2 Moral hazard: Ch. 7 intro, Ch. 7.3 & 15.3 – 15.5
Networks: Ch. 9.6
Advertising: Ch. 9.5
Externalities: Ch 16.4 Property rights & Intellectual property: Ch. 16.5 – 16.6

BE300: Applied Microeconomics Winter 2015

University of Michigan Ross School of Business

8. ADVANCED TOPICS IN BUSINESS STRATEGY

A. SINGLE-PRICE STRATEGIES

1. Strategic interactions 2. Collusion 3. Predatory pricing and entry deterrence 4. Repeat interactions 5. Auction types and bidding strategies 6. Antitrust policy

Case:

“Sony Playstation and Microsoft Xbox” (write-up)

Readings:

“Game Theory: How to Make it Pay” (Garicano, Financial Times) “Are We in Danger of a Beer Monopoly?” (Davidson, N.Y. Times)

Textbook:

Market structure: Ch.7.5 & Ch. 11 intro Oligopoly & Collusion—Cartels: Ch. 11.1 Game theory and strategic interactions: Ch. 12 intro & Ch. 12.1 – 12.3
Auctions: Ch. 12.5
Repeated games: Ch. 13 intro & Ch. 13.1 Sequential games: Ch. 13.2 & 13.5 – 13.6 Deterring entry: Ch. 13.3 – 13.4 Antitrust Policy: pp. 372 –374 & Ch. 16.3

B. MULTI-PART PRICING AND NON-PRICE STRATEGIES
1. Price discrimination (1st, 2nd, 3rd degree) 2. Bundling and tying 3. Peak-load pricing 4. Antitrust policy

Cases:

“A Threat to Scholarly Communication” (write-up) “Bundles of Joy” (write-up)

Readings:

“Uber’s Surge Pricing is Totally Logical and Fair. So Why Do People Hate it So Much?” (Neil Irwin, The Washington Post Wonkblog) “When a $65 Cab Ride Costs $192” (Chow, NPR/Planet Money) “A Marketer’s Guide to Behavioral Economics” (Welch, McKinsey Quarterly)

Textbook:

Price discrimination: Ch. 10 intro & Ch. 10.1 – 10.5 Bundling: Ch. 10.6 Peak-Load Pricing: Ch. 10.7 Antitrust policy: Ch. 16.3

BE300: Applied Microeconomics Winter 2015

University of Michigan Ross School of Business

TENTATIVE SCHEDULE OF CLASSES (AS OF JAN. 8, 2015)*

Week Dates
1 Tu 1/13 Th 1/15
2 Tu 1/20 Th 1/22
3 Tu 1/27 Th 1/29
4 Tu 2/3 Th 2/5
5 Tu 2/10 Th 2/12 Fri 2/13 – Mon 2/16
6 Tu 2/17 Th 2/19

Topic
1. Preliminaries 2. Individual & Market Demand 2. Individual & Market Demand 2. Individual & Market Demand
3. Costs & Profit Maximization 3. Costs & Profit Maximization
4. Competitive Supply & Market Analysis 4. Competitive Supply & Market Analysis
4. Competitive Supply & Market Analysis 5. Global Business FR 2/13: ON-LINE QUIZ OPENS MON 2/16: ON-LINE QUIZ DUE AT 8AM 5. Global Business 6. Managerial Decisions under Risk

7 Tu 2/24 Th 2/26
8 Tu 3/10

6. Managerial Decisions under Risk 6. Managerial Decisions under Risk Midterm Exam Review

Cases
Electric Power Purchasing O2, Brute **
Demand 4 Tagamet Franchisee McCosts** Continued Existence of Cows
Sugar Import Quotas Quiz
When Oil Breaks Bad: A Crude Case Study**
Failures in Franchising**

Wed 3/11 MIDTERM EXAM 6:30 – 8:30 pm

Th 3/12

7. Information, networks, and the Internet

9 Tu 3/17 Th 3/19

7. Information, networks, and the Internet 7. Information, networks, and the Internet

Zombie Apps

10 Tu 3/24 Th 3/26

7. Information, networks, and the Internet 8.A Adv. Topics: Single-Price Strategies

11 Tu 3/31 Th 4/2

8.A Adv. Topics: Single-Price Strategies 8.A Adv. Topics: Single-Price Strategies

Sony Playstation & MS Xbox

12 Tu 4/7 Th 4/9

8.A. Adv. Topics: Single-Price Strategies 8.B Adv. Topics: Multi-Part Pricing/Non-Price Strategies Scholarly Communication

13 Tu 4/14 Th 4/16

8.B Adv. Topics: Multi-Part Pricing/Non-Price Strategies 8.B Adv. Topics: Multi-Part Pricing/Non-Price Strategies

Bundles of Joy

14 Tu 4/21

Review & Wrap-up

Last Class

Wed 4/29 FINAL EXAM 10:30am – 12:30pm

*This schedule will be posted and regularly updated on the course website. ** For class discussion only, NOT to be turned in.

