STEPHEN M. ROSS SCHOOL OF BUSINESS

MKT 300: MARKETING MANAGEMENT WINTER 2015

Professor: Carolyn Yoon

E-mail:

yoonc@umich.edu

Office:

R5374

Office Hours: Wednesdays 1 - 3 pm, or by appointment

Class Hours: Section 1: TuTh 10am – 11:30am, R2230 Section 2: TuTh 1pm – 2:30pm, R2220 Section 3: TuTh 4pm – 5:30pm, R2220

Readings: There is no textbook for this course. Instead, required readings are available via Study.net. (Please see instructions in CTools on how to purchase course materials.)

COURSE OVERVIEW AND OBJECTIVES
The definition of marketing is often debated, but it is most frequently conceptualized as the art and science of meeting customers’ needs profitably. To do so, the marketing manager must acquire and retain customers by creating, delivering, and communicating superior customer value. This survey course will examine the challenges and opportunities that marketing managers face when trying to meet needs profitably.
From this course, you will gain an appreciation of the broad array of decisions facing marketing managers, ranging from the high-level (e.g., should we focus on acquiring or retaining customers?) to the more prosaic (e.g., which pricing tactic should we implement?). In class, we will consider and utilize a structured “Big Picture” model as a framework that will help us understand how to make each of these decisions. Our readings, completed outside of class, will come from a number of different sources. By the end of the course, you will possess the analytical tools necessary to act on a wide range of marketing opportunities.

COURSE COMPONENTS
Your semester grade is based on your class participation (10%), a midterm exam (25%), a final exam (35%), and a group project (30%). There is also a research requirement that you must complete to receive a grade for the course. These components are described below. Please read through the entire syllabus carefully.
Class Participation (10%)
Your class participation grade is based partly on your attendance and partly on your in-class engagement.
Attendance will be taken each class. 100% attendance is expected. The more class you miss, the lower your class participation grade will be. Absences based on religious holidays or medical emergencies are excused – just give me as much advance notice as possible.

1

Comments will be evaluated on two dimensions: quality and quantity. Students who participate frequently in class, but whose comments reveal a lack of preparation or thoughtfulness, will not do as well as students whose comments are more thoughtful but less frequent.
Finally, note that this course is “unplugged.” You are not allowed to use laptop computers, cell phones, or any other electronic equipment in class. Recent research indicates that taking notes by hand leads to deeper conceptual learning than taking notes on a laptop, even when the laptop is disconnected from the Internet (Mueller and Oppenheimer 2014, Psychological Science).
If you have a documented disability that requires the use of electronic equipment, speak with me at the beginning of the semester. Misusing an electronic device (e.g., texting during class) will adversely affect your class participation grade.
Exams (60%)
We will have a midterm exam that constitutes 25% of your semester grade, and a final exam that constitutes 35% of your semester grade. The final exam will be comprehensive, though mainly focused on material covered after the midterm.
The exams are closed-readings / closed-notes, and you may not use any other class-related materials during the exam. You may use a calculator if it is non-programmable. We will use Scantron forms for the exams, so be sure to bring a Number 2 pencil to the exams.
Note that the final exam has been scheduled (by Ross) for Tuesday, April 28, 10:30am –12:30pm. The location will be announced later. If you have a documented time conflict, you can take the exam during the Conflict Exam Time: Tuesday, April 28, 8am – 10am. If applicable, please submit the documentation to take the final exam during the Conflict Exam Time by no later than April 14, so that exam proctors can be arranged.
Review documents will be posted on our CTools site before the midterm and before the final. These will give you a general sense of what will be covered on the exams. However, note that everything covered in class and in the readings is fair game for the exams, unless explicitly noted otherwise.
Group Project (30%)
The group project is the capstone experience of this course. The objective of the group project is to use the Big Picture framework to analyze the marketing challenges and opportunities facing a firm. After researching the firm, you will prepare a write-up that provides a brief overview of the firm’s background and illustrates (with evidence) the firm’s current Big Picture. Your group will then make a presentation that briefly recaps the firm’s current Big Picture, discusses the firm’s key marketing impediments, and proposes revisions to the firm’s Big Picture.
I will randomly assign you to a group. Groups will be announced in class on Tuesday, February 3 (one week after the add/drop deadline). You will have until Friday, February 13 to select a topic (a firm). If you later discover that this topic is not viable, I encourage you to change your topic – just let me know. Note that only one group per section may select any particular firm. A public list of topics will be maintained.
The write-up constitutes 15% of your overall semester grade, and the presentation constitutes 15% of your overall semester grade. Each component of the group project is described further below, followed by Grading Rubrics for the write-up and the presentation.
2

Topic Selection
Select a company that clearly could be performing better from a marketing perspective. You’ll want to ensure that their impediments are non-trivial and not easily remedied. If the impediments are easily repaired (e.g., their prices are too low), you will not have enough substance for your presentation. If your team is having a hard time selecting a topic, talk with me.
You are free to select either a local or national company. Local companies can provide novel sources of data (e.g., conversations with the owner), and maybe even opportunities for intervention (e.g., perhaps they implement one of your recommendations). But beware of local businesses that do little advertising (e.g., Mr Spots). It is often difficult to infer these companies’ current marketing strategy, much less illustrate it. Note that “local” here does not refer to an Ann Arbor location of a larger franchise (e.g., the Subway on State). Instead, “local” refers to companies that only operate in Ann Arbor (e.g., Sava’s) or in another single location.
If you’ve selected a topic that is not viable (e.g., because the company doesn’t have a sufficient number of problems that need repair, or because there is insufficient data), you are encouraged to change topics. You’ll have to update your topic on the topic list, and make sure another group is not already pursuing that topic.
The Write-Up
The write-up is intended to provide a brief overview of your firm’s background and illustrate (with evidence) the firm’s current Big Picture. Write-ups will be posted on CTools, and the expectation is that all students will have read your write-up before attending your presentation, so that they can ask informed questions.
The write-up should begin with a cover page that includes the name of your firm, your section number, your group number, and the names of the members of your group. Then, your write-up should include no more than 3 pages of text, single-spaced. Finally, on pages that follow your text, you should include exhibits of both a qualitative (e.g., advertising samples) and quantitative (e.g., recent sales figures or survey results) nature. There is a limit of six figures. Redundant or otherwise unnecessary figures will reduce your grade.
Your group’s write-up is due by 8am on Thursday, April 2. One member of your group should email me a PDF version of your write-up by that time. Be sure to CC your teammates when submitting the PDF. These write-ups will be posted on CTools, so that your classmates can read them in advance and be prepared for discussion. The PDF must be 10 MB or less. If your PDF is over 10 MB, re-save the images as smaller files (e.g., using Paint or Photoshop), paste in the new smaller images, and re-create the PDF.
The Presentation
Your group will make a presentation that briefly recaps the firm’s current Big Picture, discusses the firm’s key marketing impediments, and proposes revisions to the firm’s Big Picture. Since the class will have read your write-up before the presentation, the majority of your presentation should be focused on the firm’s key impediments and your proposed solutions.
3

Plan on 18 minutes for your presentation and 5 minutes for Q&A. We will have three groups present each day. Be sure to arrive to class early on the day you present to upload your slides and ensure everything is working.
On the day you present, find me at the beginning of class and give me a printed handout of your slides. Also, email me your final slides (and any videos you created) no later than 6pm on the day of your presentation.
Note that not all members of your group must actually speak during your presentation. Some groups are more comfortable having each member speak for short amounts of time (e.g., “Now Emily will discuss the firm’s impediments…Now Jonah will discuss our first proposed solution…”). Other groups are more comfortable having only a small number of members speak. I’ve seen both ways work well. You should use whichever format you feel will make your presentation most engaging.
Some General Advice about the Project
Read the Group Project Grading Rubrics (in this syllabus) carefully before embarking on your project. Many common errors can be avoided by keeping these criteria in mind.
Helpful data resources can be found here: http://webservices.itcs.umich.edu/mediawiki/KresgeLibrary/index.php/Marketing_300_Assignment
Examples of good write-ups and presentations from previous years will be posted on CTools. However, please note that grading requirements for group projects – both the write-up and the presentation – are refined from year-to-year, so the samples posted online may not fare as well under the current guidelines. They are there to give you a general idea of what a group project should look like.
Consider whether you’re including any information that actually undermines your arguments. Be mindful of potential counterarguments when deciding which data to include. If you cannot avoid including data that will elicit counterarguments, have a response prepared.
Think about how you can collect relevant data. Typically a survey will be most informative, but ethnographic methods (e.g., observation of consumer behavior, interviews with consumers) may also provide valuable information. (Check with me first if your ethnographic efforts may be considered invasive by some consumers.) For some groups, a taste test might provide valuable data. Be creative and adventurous, but make sure your adventures are aimed at collecting relevant data.
If you conduct a survey, include some evidence of your efforts (in addition to any charts, graphs, and tables that you make based on your data). This evidence can be a copy of a completed survey, if you are conducting a paper-and-pencil survey. Or, if you are conducting your survey online, include a screenshot of your raw data reports. This evidence can be presented as an Appendix in your writeup, and will not count against your six-figures-maximum.
Also, if you conduct a survey, design it carefully. For example, if you’re asking people how often they visit a restaurant that gets hardly any business, be sure to include a “Never” option. Think about it from the respondent’s perspective.
4

Your slides should be visually digestible. Try to avoid having too much text on a slide, but if you must have a lot of text, display the various components sequentially to facilitate comprehension.
If you can illustrate your proposed solutions with a new commercial, feel free to film one and include it in your talk.
Finally, try to have fun with it! It is often apparent in both the write-ups and the presentations which groups really embraced the project (enthusiasm reveals itself in all kinds of subtle and obvious ways), and these projects generate more goodwill among the audience. Energy can be a competitive advantage.
Evaluating Your Teammates
After your group completes the presentation, you will have an opportunity to evaluate your teammates’ contribution to the project. For each teammate, you will be asked to judge the extent to which your teammates contributed in a positive way to the project (e.g., participating constructively during meetings, performing their tasks in a timely and competent manner). Judgments will be made on a 0-100% scale, where 0% indicates no positive contribution and 100% indicates the expected (high) level of positive contribution. Most students will contribute positively, as expected, and receive a 100% rating. Thus, the evaluation survey will only ask you to elaborate upon responses below 100%.
Evaluations will be individually completed online, outside of class. Your evaluations of your teammates will remain completely confidential. See The Rule of Two Unknowns policy for more information on confidentiality. Information about accessing the evaluation survey will be given later in the semester.
Your teammates’ evaluations of you will be averaged to determine your “group project multiplier.” This multiplier will range from 0 to 1, and will be multiplied by your group’s overall project grade (write-up and presentation averaged) to determine your individual project grade. Almost all students receive a group project multiplier of 1, meaning that their individual project grade will not be reduced. Minor disagreements between teammates are typically interpreted in favor of the student being evaluated (e.g., if two teammates give you a 100% positive contribution rating, and two give you a 95% positive contribution rating, your multiplier will be 1). Other obvious response irregularities (e.g., students who give all teammates positive contribution ratings of 25%) are rare and controlled for statistically. If you evaluate a teammate’s contributions negatively, the survey will ask you to elaborate upon and justify your numerical response. Non-justified negative evaluations are discounted.
For example, imagine that Jacob, Michelle, and Jordan are in a group. The group received a 90% on their write-up, and a 70% on their presentation. Their overall group project grade is therefore 80%. In the peer evaluation survey, Michelle and Jordan each gave Jacob a 100% rating. Jacob’s multiplier is therefore 1. Jacob and Jordan each gave Michelle a 100% rating. Michelle’s multiplier is therefore 1. Jacob and Michelle each gave Jordan a 50% rating, and provided compelling justification for their very negative evaluations. Jordan’s multiplier’s is therefore 0.5. Thus, in the grade book, Jacob’s individual project grade will be 80, Michelle’s individual project grade will be 80, and Jordan’s individual project grade will be 40.
5

Group Project Write-Up Grading Rubric (Full credit is achieved by fulfilling the criteria listed below.)
Administrative (10%)
Submitted on time.
Submitted in proper format (e.g., includes cover page; submitted as PDF; no more than 3 pages of text).
Writing (65%)
Write-up is carefully done, interesting, and engaging.
Current Big Picture can be fully inferred from write-up. If aspects of the current Big Picture are ambiguous, acknowledge this in your write-up (e.g., some evidence suggests a focus on acquisition, but other evidence suggests a focus on retention). If there is compelling evidence that points in opposite directions, it is fine to conclude that a particular aspect of the current Big Picture is unclear.
Any inconsistencies within the Current Big Picture are acknowledged. (Save any proposed remedies for the presentation.) Claims are clearly supported by data whenever possible. Make it as clear as possible why you’re claiming what you’re claiming (e.g., “Because Company 1 is doing X, Y, and Z, it appears that they are focusing their efforts on retaining current customers.”).
Any potentially unfamiliar industry-specific or company-specific jargon is defined. Claims that the reader might find surprising or counterintuitive are explained.
Exhibits / Appendix (25%) Each exhibit presents unique information (e.g., don’t include a bar graph and a pie chart that both make the same point).
Each exhibit presents relevant information. (If the exhibit is not cited in the text to make a point, that might suggest that it’s not relevant.) Exhibits are easy to read and interpret (e.g., no weird abbreviations that can’t be interpreted).
Informative qualitative exhibits (e.g., ads, commercial screenshots) are included.
Evidence of survey efforts (e.g., screenshots of raw data) is provided as an Appendix (if applicable). Information about who took the survey should also be provided (e.g., “50 male and 50 female BBA students”).
Total: ____ / 100
6

Group Project Presentation Grading Rubric (Full credit is achieved by fulfilling the criteria listed below.)
Administrative (10%) A handout of the final slides is given to the professor at the beginning of class in proper format (print as “Handouts” instead of “Slides”). This is important – I use these to take notes on the presentation. Final slides emailed to professor by 6 pm on day of presentation. Content (60%) Current Big Picture (BP) is clearly articulated. Revised BP is clearly articulated and internally consistent. Deviations from internal consistency (in either current or revised BP) are acknowledged and explained. Evidence/data presented supports and does not undermine revised BP. Any inconsistencies between your recommendations and your collected data are explained. Impediments identified are substantive enough to warrant our attention (i.e., the current BP and revised BP differ in important ways). Proposed solution(s) are logically justified and feasible. An estimate of the costs of the proposed solution(s) is provided. Assumptions are clearly justified by data whenever possible. Details about data collection are provided (e.g., how many people took the survey? Were they students, customers, managers, or someone else?). Q&A (15%) Responses to counterarguments are thoughtful and reflect preparation. Presenters listen and respond carefully to audience feedback. Delivery (15%) Slides are visually digestible. Proposed remedies are illustrated whenever possible (e.g., here’s what a new commercial / punch-card / logo / package might look like). Presenters are enthusiastic and doing their best to stimulate/maintain audience interest.
Total: ____ / 100
7

IMPORTANT POLICIES
STUDENTS WITH DISABILITIES
If you have a disability that requires accommodations, please let me know as soon as possible. This is the only way I can guarantee exam accommodations, should you need them. I will treat any information you provide as private and confidential.
HONOR CODE
As is true of all Ross courses, the Academic Honor Code and Community Values Committee policies and procedures apply to this course. For more information, go to: http://www.bus.umich.edu/Academics/Resources/communityvalues.htm
Cheating of any degree will not be tolerated. In particular, note that the exams are closed-book. You will not be allowed to have access to any class-related materials, or any programmable calculators, during the exams. Failure to abide by these guidelines may result in failure or expulsion.
RESEARCH REQUIREMENT
Much of what is taught in business schools is informed by academic research. Students at researchactive schools tend to learn cutting-edge insights sooner, and accordingly the net present value of a business school degree is highly correlated with the research impact of the business school (indexed by number of publications, citations, and peer-ratings; see Armstrong 1995, Journal of Marketing).
Many faculty members and Ph.D. students in our marketing department utilize experiments in their research. To support this important work, we require you to participate in three different marketing experiments this semester. The experiments will be conducted in the Ross Behavioral Lab. Each experiment will last about 60 minutes.
The experiments will be posted online at http://RossStudentPool.bus.umich.edu. Experiments will be posted periodically throughout the semester, so be sure to check the website often. I will keep you posted whenever I learn about upcoming experiments.
If your schedule makes it impossible to complete three experiments, you will need to complete an alternative assignment. This alternative assignment requires reading academic marketing research articles and writing reports summarizing the articles and proposing new directions for future research. You will read and report on one research article for each experiment you miss. Reports will be judged on a pass/fail basis. If you fail any report, you will have to either re-write it or take an Incomplete for the course. Students generally find the alternative assignment to be much more timeconsuming than completing three experiments. You should treat this option as an emergency backup. Historically, fewer than 5% of students end up completing the alternative assignment. Discuss this option with me before April 7 if it looks like you will not be able to complete the three required experiments.
THE RULE OF TWO UNKNOWNS
To protect the confidentiality of the group project peer evaluations, neither your group project multiplier nor your class participation grade will be revealed to you.
8

ROSS POLICY ON SEMESTER GRADES The Ross School of Business implements a strict grading policy for core courses to prevent grade inflation. The Ross policy imposes the following restrictions on the semester grade distribution: 1. Less than 40% of students can receive an A- or better. 2. Less than 80% of students can receive a B or better. 3. More than 20% of students must receive a B- or worse. There will be many outstanding students in this class, and the grading is very competitive. Due to the Ross policy, your grade will not only depend on how well you perform, but also on how well you perform relative to your peers. Small absolute numerical grade differences will likely distinguish between different letter-grades. This means that some of you may receive semester grades lower than those you are accustomed to receiving. GUEST LECTURE We will have a guest lecture this year, scheduled for Wednesday, February 25 at 6:00pm in the Blau Auditorium. Russell Weiner (CMO of Domino’s) will be talking with us. He will discuss the marketing challenges that Domino’s has faced, and the marketing innovations Domino’s has implemented to restore the brand. If the guest lecture time conflicts with another class you are taking, you may watch a video recording of the guest lecture online later. There will be exam questions based on the guest lecture. SYLLABUS SUBJECT TO REVISION No changes to the syllabus are anticipated. However, if there are any changes to the syllabus, they will be announced in class, with as much advance notice as possible.
9

MKT 300: WINTER 2015 COURSE CALENDAR

Week Date 1 1/13 2 1/20 3 1/27 4 2/3
5 2/10 6 2/17 7 2/24 8 3/10
9 3/17 10 3/24 11 3/31 12 4/7 13 4/14 14 4/21

Tuesday
Course Overview
Marketing Objective Reading: “Module 3, Marketing Objective”
Test Marketing Reading: “Understanding Conjoint Analysis”
Targeting & Positioning Readings: “The Curse of Knowledge”; “Why You Aren’t Buying Venezuelan Chocolate”; “Make the Most of a Polarizing Brand”
Product Reading: “Break Free from the Product Life Cycle”
Marketing Research Methods “A Step-by-Step Guide to Smart Business Experiments”
Midterm Exam (in class)
Service Marketing Reading: “The Four Service Marketing Myths. Remnants of a Goods-Based, Manufacturing Model”
Media Planning Reading: “Branding in the Digital Age”
Place Reading: “Aligning Incentives in Supply Chains”
Managerial & Consumer Biases Reading: “The Hidden Traps in Decision Making”
Group Project Presentations
Group Project Presentations
Review Session

Date 1/15 1/22 1/29 2/5
2/12 2/19 2/26 3/12
3/19 3/26 4/2 4/9 4/16

Thursday Business Objective Reading: “Marketing Myopia” Source of Volume Reading: “Module 4, Source of Volume” Segmentation Reading: “Customer-Centered Brand Management” Connecting Strategy & Execution Reading: “Ending the War Between Sales and Marketing”
New Product Development Reading: “Creativity in Product Innovation” Review Session
No Class (this session replaced by guest lecture on 2/25) Promotion Reading: “Beyond Paid Media: Marketing’s New Vocabulary”
Price Reading: “How to Fight a Price War”
Ethical Concerns Reading: “What’s Wrong with Advertising” Buzz & Viral Marketing
Group Project Presentations Group Project Presentations

Final Exam: Tuesday, April 28, 10:30am–12:30pm (Location TBA).

Domino’s Guest Lecture: Wednesday, February 25, 6:00pm in Blau Auditorium.

Important Group Project Dates: Groups assigned Tuesday, February 3. Select topic by Friday, February 13. Write-Up due (via email) by 8am on Thursday, April 2.

10

MKT 300: WINTER 2015 REVISED COURSE CALENDAR

As of March 17, 2015

Week Date Tuesday

Date Thursday

9 3/17 Media Planning
Reading: “Branding in the Digital Age”

3/19 Price Reading: “How to Fight a Price War”

10 3/24 Place
Reading: “Aligning Incentives in Supply Chains”

3/26 Ethical Concerns
Reading: “What’s Wrong with Advertising”

Managerial & Consumer Biases
Reading: “The Hidden Traps in Decision Making”

11 3/31 No Class (this session replaced by 4/2 guest lecture on 3/30).

Buzz & Viral Marketing

12 4/7 Group Project Presentations

4/9 Group Project Presentations

13 4/14 Group Project Presentations

4/16 Group Project Presentations

14 4/21 Review Session

Final Exam: Tuesday, April 28, 10:30am–12:30pm (Location TBA).
SC Johnson’s Guest Lecture: Monday, March 30, 6-7 pm in Blau Auditorium. (Followed by Reception 7-8 pm – Optional)
Important Group Project Date: Write-Up due (via email) by 8am on Thursday, April 2.

1

