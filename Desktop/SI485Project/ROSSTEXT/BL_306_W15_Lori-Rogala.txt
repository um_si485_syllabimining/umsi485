Section 03

Legal Aspects of Management and Finance BL 306
Winter 2015

Days

Time

Location

TuTh

11:30-1:00

R2220

Instructor: Phone: Office: Email: Office hours:

Lori Rogala 734.764.9674 R3460 lrogala@umich.edu Thursdays from 9:00 - 11:00 and by appointment

COURSE OBJECTIVES
The Ross School online course catalog describes the class this way:
A study of the legal aspects of organizing, financing, and operating a business enterprise, with emphasis on the law of agency, partnerships, corporations, secured credit financing, and managing a workforce. The course includes coverage of business ethics and corporate social responsibilities. It also examines securities law (including securities fraud and insider trading) and the law of mergers and acquisitions. Cases are used to illustrate the application of legal principles to actual business problems.
This course will introduce you to the fundamental principles of how legal rules, ethics, and public policy interact to regulate business activities and relationships, including agency and government oversight of corporations. During the semester we will be learning many of the business-relevant rules of the American legal system and we will also focus on the reasoning, policy, and ethical standards inherent in those rules. This class will help to strengthen your ability to reason through complex legal material and articulate difficult concepts to your peers.
COURSE MATERIALS
Required Text Mann & Roberts, LHC 306: Legal Aspects of Management and Finance; ISBN-10: 1285-10995-3 or ISBN-13: 978-1-285-10995-4, published by Cengage Learning. NOTE: This is a special edition soft cover printed for this course and currently is only available through the local bookstores, from the Cengagebrain website (http://www.cengagebrain.com/course/site.html?id=11OZXWT), or from students who took the course last year.
 If you prefer to purchase the full hardcover book, you can buy: Mann & Roberts, Essentials of Business Law & The Legal Environment, 11th edition (2013) (full text ISBN-10: 113318863X ; or ISBN-13: 9781133188636).

1

 The full text or e-book/e-chapters may be available at:
http://www.cengage.com/search/productOverview.do?Ntt=mann||14580610493547864402067332
9202072780458&N=16+4294922239+4294966221+4294950657&Ntk=APG%7C%7CP_EPI.
Important Class Handouts I will distribute copies of my lecture slides before each class meeting via the class CTools website. Additional handouts will also be posted on CTools or distributed in class.
Optional Supplemental Texts Irvin Gleim and Jordan Ray, BUSINESS LAW/LEGAL STUDIES: EXAM QUESTIONS AND EXPLANATIONS. This test preparation book includes practice questions on business law topics and will be on reserve at Shapiro Library. Relatively few pages of the book are relevant to this class, so most students do not purchase this book.

EVALUATION AND COURSE GRADE
This class will be graded according to the Ross School’s grading curve for elective classes. The breakdown of your grade allocation is as follows:

Midterm Exam (February 24, 6:30-8:30 pm) Final Exam (April 23, 10:30am - 12:30pm) In-Class Problem Set #1 (February 3) In-Class Problem Set #2 (March 26) Class Participation

30% 40% 10% 10% 10% 100%

Exams (70%) Examinations will be closed note and closed book. The exams are designed to evaluate your understanding of the material and your ability to engage in a level of sophisticated analysis demonstrating that you can apply what you have learned to relevant situations.
Both exams will likely follow the same format that includes an emphasis on extensive and detailed multiple choice questions. The final exam will focus primarily on the material discussed after the midterm exam. However, the nature of business law dictates that all the topics covered are relevant throughout the course and a continued understanding of the early material is essential to success on the final exam.
All sections of BL 306 will take the midterm and final exams at the same time. If you have an exceptional reason to take the exam at a different time (e.g., conflicting exam from another course, a university recognized disability that will impact exam attendance), you must make arrangements with me by January 29.
 The midterm exam is scheduled for February 24 from 6:30 pm to 8:30 pm.  The final exam is scheduled for April 23 from 10:30 am to 12:30 pm.
In-Class Problem Sets (20%) You must be in class on the days assigned for the problem sets to receive credit. If you have an exceptional reason to miss those classes you must make arrangements with me immediately. Do not schedule conflicts with these dates. The first assignment is designed to give you practice in answering the types of questions you will see on the exam. The second assignment will focus on the secured transactions material.
2

 In-Class Problem Set #1 is scheduled for February 3.  In-Class Problem Set #2 is scheduled for March 26.
Class Participation (10%): Participation is based on the following components:
1. Attendance: Arriving late for class or departing early may count as an absence at my discretion. A seating chart and sign-in sheet will be used to track attendance. It is a violation of the Ross Community Values to sign the sign-in sheet for someone else, ask someone to sign it for you, or to sign it when you know that you haven’t attended or will not attend a majority of the class session.
There are no “excused” absences except in the very unusual cases, such as a lengthy serious illness. Three absences over the course of the term will not affect your participation grade by itself and should be enough to cover the random illness, conflict with a job interview, etc. Use them wisely.
2. Quality substantive participation/Class Experts: Quality contributions from all students is essential to a successful learning experience in this course and students are encouraged to ask questions and participate in every discussion, particularly when we discuss the cases or work on problems in class. To facilitate discussions, students will be required to be a “class expert” for one class session. On that day, those students will act as class “experts” on the assigned readings. The details of this assignment will be discussed on the first day of class. A sign-up sheet for being “on call” will be distributed on the second day of class.
3. Engagement: Engage in meaningful debate but be respectful of the comments and ideas of others. Listen attentively to lectures and student comments – having side conversations with others, texting, failing to have the required course materials with you, etc. will be problemactic for your class participation score.
INSTRUCTOR EXPECTATIONS
Readings Students are expected to complete all of the assigned readings before each session. With advance warning additional reading materials may be posted to the CTools website or distributed in class throughout the semester. Having read and contemplated the material is essential to student learning and productive class discussions. Accordingly, I will conduct class discussion with the understanding that all students have read the material and are prepared to discuss it, in detail, each day. You must bring the textbook to class every day. If you opt for an electronic version of the text and wish to use that version in class, you must decide to do so by the end of the second week of the semester and sit in one of the first two rows of the classroom for the duration of the course.
Academic Honor Code Compliance: Personal integrity and professionalism are fundamental values of the Ross School community. You have a duty to be familiar with the Ross Community Values Code and related procedures, which can be found at the following website: http://www.bus.umich.edu/Academics/Resources/communityvalues.htm and will not be repeated here. The site also contains comprehensive information on how to be sure that you have not plagiarized the work of others. Claimed ignorance of the Code and related information appearing on the site will be viewed
3

as irrelevant should a violation take place. Non-Ross School students are subject to the Code while in this course and should familiarize themselves with its contents.
Simply put: any violation of the Ross School Honor Code such as plagiarism, otherwise passing off anyone else’s work as your own, unauthorized collaboration, use of materials generated for use during past offerings of this course, or any form of cheating will be referred to the Community Values Committee. Possible penalties include course failure with a permanent notation of an honor policy violation on your transcript and even expulsion.
Students with Disabilities: If you need an accommodation for a disability, please let me know at your earliest convenience. Some aspects of the course, the assignments, and the in-class activities may be modified to facilitate your participation and progress. As soon as you make me aware of your needs, we can work with the Office of Services for Students with Disabilities to help us determine appropriate accommodations. Any information you provide related to a recognized disability will be treated as private and confidential.
Policy Related to the Use of Class Materials: All materials generated for class, including slides, handouts, review packets, quizzes and exams (and answers to these materials), any written review materials, class problem sets or class problem answers generated for this class, or any other materials prepared by you or the professor for this course are intended for use by current students in this class. You are not permitted to use materials related to the class that were generated by the professor or students in previous versions of this course. A violation of this policy may be a violation of the Ross Community Values Code and may result in a student being referred to the Community Values Committee for disciplinary action. It is also a violation of this policy to participate in the collection, distribution, online posting, or otherwise sharing of class materials that are intended for use by current students in BL 306.
Electronic Device Policy: You are not allowed to use a laptop computer, tablet, cell phone, or other electronic devices during class. The only exception to this policy is for students who opt to use an electronic version of the text during class. If you choose this option you must decide to do so by the end of the second week of class and sit in one of the first two rows of the classroom. In no other instance may you use your electronic devices during class time. Your failure to abide by this policy will negatively affect your participation grade.
CLASS SCHEDULE, DUE DATES, AND READING ASSIGNMENTS
1. You are expected to do all readings before class and be prepared to discuss and apply those materials. There are many cases in the book that we will skip, and we often only cover parts of a chapter at a time – so please pay careful attention to what the assignment listings below are telling you to read.
2. This schedule is subject to adjustment as needed.
3. The terminology associated with any law class is new to most students and can often be confusing. When you encounter terms or concepts that you do not understand you should look them up in Appendix C of the text or through a basic online legal dictionary such as http://dictionary.law.com/ or http://dictionary.lp.findlaw.com/.
4

# Date 1 T. Jan. 13 2 R. Jan. 15
3 T. Jan. 20 4 R. Jan. 22 5 T. Jan. 27 6 R. Jan. 29
7 T. Feb. 3

Topic

Reading assignment (read before class that day)

Course Introduction & Overview Syllabus and note important dates on your calendar.

Business Ethics & CSR

Chapter 2. Ethics: read pp. 13-22 and the Sword Technology, Inc. case on pages 35-38. Come to class with written notes prepared to discuss this question: Should Sword follow the legal and ethical practices of its home country or the legal and ethical practices of the host country? Consider the question for each issue raised in the case (such as transfer pricing, cash gifts, etc.). Be sure you can justify your position.

Understanding the US Legal System and ADR

Chapter 3. Civil Dispute Resolution: skim pp. 44-47 and 53-59. Read carefully pp. 47 (starting with “Jurisdiction”) - 53 and 59 (starting with “Alternative Dispute Resolution”) - 64.

Forming a General Partnership: Intentionally & Unintentionally
Agents of the Partnership (& other Business Organizations)

Chapter 30. Formation and Internal Relations of General Partnerships: Read pp. 546-547 (stopping at “Factors Affecting the Choice”) and 550 (starting at “Formation of General Partnerships) – end of chapter. Note, omit Thomas v Lloyd on p. 558.
Chapter 28. Relationship of Principal and Agent: Read the chapter, but omit: Detroit Lions, Inc. v. Argovitz, p. 510; Gaddy v. Douglass, p. 514.

Due Date for Exam Conflicts

Chapter 29. Relationship with Third Parties: Read the chapter but omit Parlato v. Equitable, p. 528; Connes v. Molalla Transport System Inc. p. 532; and Plain Dealer Publishing Co. v. Worrell.

In Class Group Problem Set #1

NOTE: The answers to the Problem Set will be turned in at the end of the class for grading. Attendance Required for Assignment Credit.

8 R. Feb. 5

Ending a General Partnership

Chapter 31. Operation and Dissolution of General Partnerships: pp. 570-588, but omit Conklin Farm v. Leibowitz p. 576; Warnick, p. 583-584 (stop before Dissolution under the UPA which will not be assigned). Prepare written notes for end of chapter question number 6 and come to class prepared to discuss it.

5

9 T. Feb 10
10 R. Feb. 12 11 T. Feb. 17
12 R. Feb. 19 T. Feb. 24
13 R. Feb. 26

Beyond Partnerships: Limited Partnerships & Limited Liability Companies

Chapter 30. Formation and Internal Relations of General Partnerships: Read pp. 548 (starting at “Forms of Business Associations”)-550.
Chapter 32. Limited Partnerships and Limited Liability Companies: Read the chapter, but omit: Wyler v. Feur on p.600; Taghipour v. Jerez on p. 604; Estate of Coutnryman v. Farmers Coop. Ass’n. on p.606.

Forming a Corporation
Issues in Managing a Corporation

Chapter 33. Nature and Formation of Corporations: Read the chapter, but omit Harris v. Looney, p. 626.
Chapter 35. Management Structure of Corporations: Read pp. 656-end but omit the cases Compaq Computer v. Horton, p. 664 and Donahue v. Rodd Electrotype Co., Inc, p. 667.

Catch-up and Review Midterm Exam ADR Revisited - Negotiation

Midterm Review
6:30-8:30 pm (rooms to be announced)
This class will consist of a small group activity project that will be distributed during class. Attendance and participation will count toward your class participation grade.

Mar. 3 Mar. 5 14 T. Mar. 10
15 R. Mar. 12 16 T. Mar. 17
17 R. Mar. 19

No classes Ethics and CSR Revisited
The Employment Relationship The Employment Relationship: Discrimination Issues
Borrowing Money: Basic Legal Concepts of a Secured Loan

Winter Break
Please review the material from Class 2 (except the Sword Technology, Inc. case). A scenario for discussion will be distributed in class.
Chapter 41. Employment Law: Skim pages 811-813; Read pages 824-end.
Chapter 41. Employment Law: Read pages 813 (starting at “Employment Discrimination Law”) – 824, but omit Ricci v. Destefano, p. 817 and Toyota Motor Mfg., Inc. v. Williams, p. 822.
Chapter 37. Secured Transactions: Read pages 702713 (stop before “Priorities Among Competing Interests”); omit the Border State Bank case, pp. 707708.
CTools:Read Rules for Secured Transactions 9-102, 9103, 9-201, 9-203, and 9-204 and prepare written

6

18 T. Mar. 24
19 R. Mar. 26
20 T. Mar 31 21 R. Apr. 2 22 T. Apr. 7 23 R. Apr. 9 24 T. Apr. 14

answers to Problem Set #1 for Secured Transactions.

Borrowing Money: Competing Creditors

Chapter 37. Secured Transactions: Read pages 713(begin with “Priorities Among Competing Interests”) -717 (stop before “Against Lien Creditors”) and 718 (start with “Default”) -722 (stop before “Suretyship”).

CTools: Carefully read the remaining rules in Rules for Secured Transactions and prepare written answers to Problem Set #2 for Secured Transactions.

Borrowing Money & Secured Loans – Review & Problems

Chapter 37. Secured Transactions: Review material from prior 2 classes.
In Class Group Problem Set #2

NOTE: The answers to this Problem Set will be turned in at the end of class for grading. Attendance Required for Assignment Credit.

IPOs and the Basics of Securities Law
Guest Lecturer

Chapter 39. Securities Regulation: Read pages 762-774 (stop at “Securities Exchange Act of 1934”). You can quickly SKIM the pages from 767-772 (the material on “Exempt Securities”).
Mr. Gary Tidwell
International Organization of Securities Commissions (IOSCO), formerly with Financial Industry Regulatory Authority (FINRA) and the Securities and Exchange Commission (SEC)

The SEC Act of 1934 & Securities Fraud.

Chapter 39 (Securities Regulation con’t): Read pages 775-782 (stop before “Insider Trading”).
CTools: Daou Case

Short-swing trading and Insider trading

Chapter 39 (Securities Regulation con’t): Read pp. 783-785 (stop before “Misleading Proxy Statements”), and review prior class material, including problem set questions.
CTools: Problem Set

FCPA, & Tender Offers (part I)

Chapter 39 (Securities Regulation con’t): Read pp. 778 (starting at “Tender Offers) – 780 and 786 (starting at “Fraudulent Tender Offers) -788.

CTools:Posted Reading

7

25 R. Apr. 16 26 T. Apr. 21
R. Apr. 23

Tender Offers (part II): Catch-Up and Review Final Exam

CTools: Posted reading 10:30 am to 12:30 pm (rooms to be announced)

8

