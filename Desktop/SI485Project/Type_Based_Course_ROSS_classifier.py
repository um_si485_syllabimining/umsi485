__author__ = 'bryceeb'

#####PUT CSV INTO JSON FILE###############


############CLEAN_DATA#####################

import tweepy,nltk,pandas,vincent,time, json, io, re, csv

stemmer = nltk.stem.PorterStemmer()

def preprocess(text):
    clean = re.sub('(https://t.co/[A-Za-z0-9]+)',' URL ', text)
    clean = re.sub('(https:/)',' URL ', clean)
    clean = re.sub('(https)',' URL ', clean)
    clean = re.sub('[^A-Za-z0-9#\?\!\'\"\@\(\)\:\$\%]+',' ', clean)
    clean = re.sub('\?',' ? ', clean)
    clean = re.sub('\f','  ', clean)
    clean = re.sub('\@[A-Za-z0-9_]*',' at_user ', clean)
    #clean = re.sub('(https://t.co/[A-Za-z0-9]+)',' URL ', clean)
    clean = re.sub('\!','  ', clean)
    clean = re.sub('\?','  ', clean)
    clean = re.sub('\(','  ', clean)
    clean = re.sub('\)','  ', clean)
    clean = re.sub('\"','  ', clean)
    clean = re.sub('\:',' : ', clean)
    clean = re.sub('\$','  ', clean)
    clean = re.sub('%','  ', clean)
    clean = re.sub(' [ ]*', '  ', clean)
    clean = re.sub('(#)', '  ', clean)
    clean = clean.lower()
    #pre_text = pre_text.strip('#')
    out_text = stemmer.stem(clean)
    #print out_text
    return out_text

#should this be in a JSON format? Does using JSON make it easier to read in labels?
json_data = open('Type_Based_Course_ROSS_ready_for_preprocess.json', 'r') #replace with file to SI Syllabi
json_file_write = open('Type_Based_Course_ROSS_preprocessed_syllabi.json', 'w') #replace with file of preprocessed SI syllabi
data = json_data.read()
#print data
data1 = json.loads(data)

for x in data1:
    #l = json.loads(x)
    #print l
    ross_class = x["ross_class"]
    data_write =json.dumps({'ross_class':ross_class,'Syllabi': preprocess(x["Syllabi"])}).encode('utf-8')
    json_file_write.write(data_write)
    json_file_write.write("\n")


############ExtractFeatures#################

#Create higher unigrams...4, 5, 6

def extract_feature(text, size= 1):
    extra = text.split('  ')
    features = {}
    for i in range(0, extra.__len__()-size+1): #UNIGRAM FOR LOOP
        feat = ' '.join(extra[i:i+size])
        if features.has_key(feat):
            features[feat] = features[feat] + 1
        else:
            features[feat] = 1
    for i in range(0, extra.__len__()-size+1): #BIGRAM FOR LOOP
        feat = ' '.join(extra[i:i+size+1])
        if features.has_key(feat):
            features[feat] = features[feat] + 1
        else:
            features[feat] = 1
    for i in range(0, extra.__len__()-size+1): #TRIGRAM FOR LOOP
        feat = ' '.join(extra[i:i+size+2])
        if features.has_key(feat):
            features[feat] = features[feat] + 1
        else:
            features[feat] = 1
    #print features
    return features

def load_training_data():
    with open('Type_Based_Course_LSA_cleanedCF_combined.json') as data_file:
        data = json.loads(data_file.read())
        #print data
        empty_list=[]
        for syllabi in data: #data
            feature = extract_feature(preprocess(syllabi['Syllabi'])) ####didnt work for SI bc too limited of a dataset
            #the hints worked the best for a small dataset
            #feature = {}
            #print feature
            if feature != None:
                feature['_PROJECT'] = int(syllabi['project'])
                feature['_TEST'] = int(syllabi['test'])
                feature['_EXAM'] = int(syllabi['exam'])
                feature['_MIDTERM'] = int(syllabi['midterm'])
                feature['_FINAL'] = int(syllabi['final'])
                feature['_DUE_DATE'] = int(syllabi['due_date'])
                feature['_ESSAY'] = int(syllabi['essay'])
                feature['_GROUP_PROJECT'] = int(syllabi['group_project'])
                #print feature
                empty_list.append((feature, syllabi['type_of_course']))
        #print empty_list
        train_set = empty_list[:175]
        test_set = empty_list[175:]
        print "NLTK Accuracy:"
        print nltk.classify.accuracy(nltk.NaiveBayesClassifier.train(train_set), test_set)
        classifier = nltk.NaiveBayesClassifier.train(empty_list)
        print classifier.show_most_informative_features()
        # print classifier.labels()
        print [x[1] for x in test_set]
        test_set_nl = [x[0] for x in test_set]
        print classifier.classify_many(test_set_nl)
        # for pdist in classifier.prob_classify_many(test_set_nl):
        #     print ('%.4f %.4f %.4f %.4f' % (pdist.prob('syllabus_says_mandatory_but_doesnt_describe_process_for_monitoring_attendance'), pdist.prob('in_person_checkin'), pdist.prob('in_class_roll_call'), pdist.prob('in_class_reading_quiz')))
        #print classifier
        #print empty_list
        return classifier


step3 = load_training_data()
#print step3


#####APPLYING THE CLASSIFIER####

def sentiment_analysis(data, classifier):
    label = extract_feature(data)
    #print label
    classy = classifier.classify(label)
    #print classy
    return classy


json_data = open('Type_Based_Course_ROSS_preprocessed_syllabi.json', 'r')
data = json_data.readlines()
json_file_write = open('Type_Based_Course_ROSS_labelled_syllabi.json', 'w')
for x in data:
    l = json.loads(x)
    ross_class = l["ross_class"]
    data_write =json.dumps({'ross_class':ross_class,'Syllabi': l["Syllabi"], 'Type_Based_Course_Label': sentiment_analysis(l["Syllabi"], step3)}).encode('utf-8')
    json_file_write.write(data_write)
    json_file_write.write("\n")