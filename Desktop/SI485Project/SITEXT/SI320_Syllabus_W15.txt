Term: Winter 2015
Class Time: Tuesdays, 5:00 p.m. - 8:00 p.m. 1570 CCL - Clarence Cook Little Building
Instructors: Vadim Besprozvany | vbesproz@umich.edu Elena Godin | yelena@umich.edu
Daytime Phone: 734.945.1940 (emergencies only)
Office Hours: Wednesday - 10 a.m. - 1 p.m. | Room 3339, North Quad Friday, 4 p.m. - 7 p.m. | Room 2124, Shapiro Library
Refer to the Google calendar for classes and office hours locations and possible changes: goo.gl/yhJFB
Course Description
Modern approach to design made this discipline not “a sole territory” of artists or designers. This course offers a universe skills set that is mandatory for variety of career and concentrations paths (“remixability”). These skills will be used in the range of professional and learning projects.
The course supports students’ professional development by exploring the art and science of visual communications. The conceptual part of the course covers the graphic design theory discourse. Students will reinforce the practical application of critical thinking, analyze and process of making and conceptualizing, learn how to “read” designs and how to apply them into a broader cultural, technological, and social context. In the hands-on part of the course, students will gain necessary software skills, learn how to follow or establish brand/identity guidelines, prepare work for print or web production, and select the right tools, media, and budget for their projects.
Students will improve their aesthetic sensibilities and design skills through mastering visual language vocabularies, understanding principles of effective visual communications, solving creative problems, and developing analytical thinking, promoting thus their ability to respond professionally to rapidly changing needs of the modern world. While studying the elements of layout, typography, illustration, color, and web design, students will build their own designer’s portfolio.
BSI students will benefit form having graphic skills and solid theory knowledge of graphic arts to be competitive on a job market. Theoretical knowledge in the area will allow them to be both active and valuable team members and sole players. Adobe Creative Suite a key tool in the arsenal of any UI/UX designer, front-end developer, web administrator, data visualization specialist, social media specialist, etc. Many of the BSI students will be employed in one of these positions. That will require them to not only to work with the graphics on professional level, with the skills ranging from basic image manipulations to navigating and building multilayered website or app mockups, preparing graphics for development, but also solve creative tasks from selecting color palette to developing the modular grids or even making a comprehensive choice regarding visual message(s) that the product have to send.
Course Objectives
 Students will be able to define, investigate and evaluate visual design concepts and practices by: o Exploring graphic design basics (color, composition, typography) and modern design trends and movements. o Practicing these intellectual skills by applying these to their own work

o Evaluating the deign projects (done by both professional designers and students peers in class)
o Learning design terminology, concepts, guidelines for web and mobile devices. o Measuring their progress by assessing their use of graphic design vocabulary during in-
class discussions and by in-class quiz.  Students will know how cultural and social factors play a vital role in shaping visual design
solutions o This goal will be completed by active students participation in lectures, in-class discussions for projects, and case studies demonstrations and critiques. o Students will consider these factors when working on homework assignments, and midterm and final projects. Students will solve both formal (color, composition) and informal (conceptual) visual tasks.
 Students will understand the graphic design process by o Exploring production cycle for both fully staffed web team (assuming that here the students accept the role of the designer team members) and smaller teams, where one person is responsible for planning, visual strategy, and implementation of visual solution for a project based on ”client” brief (accepting roles of informational architect, art director, and graphic designer who will transfer the deliverables to developers).
 Students will gain graphic design skills to effectively visualize and present design solutions and concepts in a clear and concise manner. o Demonstrated by hands-on assignments (both conceptual and software-based). o Tried on in labs – small in-class projects that will help students to understand the process better and reinforce the knowledge of the software or technical skills needed to complete the tasks; o Applied in homework assignments that will allow students to practice skills and knowledge gained in class o Exaggerated in midterm and final projects that will allow students to demonstrate conceptual and technical knowledge and, in addition, reveal they creative skills (the later is a “stretch” goal, since not everyone can reveal his/her creative side while working the deadline-based project)
 Students will build professional portfolio that: o will be a collection of diverse graphic design project completed during the class, and will include results of the homework assignments, midterm and final projects, and best of the lab exercises. o Each of portfolio components will come with the set of the supplementary materials that will show student’s approach to a task:  sketching,  planning,  definition of intended audience,  design message,  design process with the explanation for decision-making,  finalized work
 During and after completion of this class, students will appreciate the value of high quality design solutions and will know how to evaluate and apply these in the work realm.
 Students will know how to effectively critique design work based on set of criteria, and not on a personal/subjective preference
 Student will continue to use their graphic design skills in their other SI classes.  Students will develop a solid foundation in theoretical knowledge and technical skills as the basis
for growth over a professional career.
Software
Adobe Acrobat (Professional version preferred) Adobe Illustrator Adobe InDesign (TBA)

Adobe Photoshop Note: you can use Adobe CS 5 or 6. CS 6 is preferred. Creative Cloud is also an option.
Sketches & Process Notepad
Sketches are a required part of concept development, discussion and brainstorming sessions. Please use them for all projects. You will be asked to attach your sketches to home work submission package
Textbooks
There is no textbook required for this class. You may use Visual Quick Start Guides (available via ProQuest) or other resources (like Adobe Help center) for your reference while mastering the software.
Required reading materials (articles, manuals, etc.) will be available online.
Lab and Homework Submission
Homework
 Homework is due immediately after discussion session.  Submit printed copy and upload the optimized version to M+Box.  Late homework are accepted, however, late submission will result in lower grade (-10%).  Homework containing inappropriate images or language will receive no credit.  Make corrections on time and resubmit homework as new package. Re-upload the files to
M+Box.  Homework graded on the following criteria: following instructions, technical accuracy, and
craftsmanship, effective use of skills developed in labs, and fulfillment of the assignment objectives. Refer to the check lists.
Labs
 Labs are graded on the following criteria: following instructions, technical accuracy, and craftsmanship, and fulfillment of the assignment objectives.
 Labs are uploaded to M+Box, their completion is monitored throughout the term and they count toward your final class grade. Instructor may comment on the labs via M+Box.
 Labs are due before the next class. Late labs are accepted, however, late submission will result in lower grade (-10%).
Naming Files
Use the following pattern: week_assignment_variant (for example: week_1_facebookad.pdf) Working files (.ai, .indd, .psd) should be uploaded to M+Boxs (only if indicated on a check list).
Group Presentation
Meet early, discuss and select the most informative materials, put together Google Doc presentation for class. You may decide to present as a team or to have one of the group members to be a presenter. Grade will be based both on the quality of the content and visual appearance of the presentation. Send your presentation no later than 2 hours prior the class to vbesproz@umich.edu in .pdf format. Note: presentation missed will result in 0 grade. Team is responsible for arranging the meeting with instructor: at least 50% of team members must be present at this meeting.

Revisions
 Revisions are essential part of your creative process. There are optional and mandatory revisions (will be indicated on the checklist or int he comments).
 Labs also can be revised; upload revised file (don't remove older version, just post the updated one) to M+box, comment on revisions you have completed and email <yelena@umich.edu> the link to revised lab. Labs without comments will not be re-graded.
How to submit revisions to homework assignments:
 Upload to M+Box - don't remove older version, just post the updated one  Print your updated design and attach it to the previous version of your work. Reuse
the checklist. Add the notes from in-class discussions.  Submit revisions with your other homework in class. Do not print new checklist - use the one with
my comments.  There is no deadline for revisions - however keep in mind that your design may need another
round of corrections - try to resubmit your work in a week after it was returned to you for revisions.  NOTE: do not resubmit if the only problem with your work was related to M+Box. Email the link to your M+Box upload instead.  Homework revisions without completed Corrections checklist (available in the Resources) are not accepted
Projects & Examinations
 Final project – refer to the gradebook  Quiz – refer to the gradebook
Assessment & Grades
Students will be evaluated based upon the successful completion of all projects and creative group work and participation. Attendance and participation grades are based on your presence in class, discussion involvement, and comments you leave for your classmates work on M+Box.
Grade
 Final grade is based on the following:  Attendance and participation – 15%  Homework – 30%  Labs – 30%  Final Project – 10%  Group Presentation – 10%  Vocabulary quiz / essay - 5%
Attendance and Participation
 Attendance is 15% of the final grade.  Valid reasons for not attending the class are: illness, conference participation, job interview,
family emergency. Notify instructors before the class via email.  Students arriving after 5:25 p.m. are considered late (-10% of the grade for a scheduled class).

 Students who are not in attendance for at least 75 percent of any scheduled class may be considered absent for that class. Students should discuss missing portions of a class with their instructor to determine how their grade may be affected.
 Sign up via CTools "Attendance" menu at the beginning of class.  Participation is monitored via in-class discussion involvement and M+Box comments posts
(students are expected to leave valuable comments for the work of their discussion peers). Each discussion comment is 10% of the grade for a scheduled class.
Grading
("=" - "equal or more...", no rounding)
98 –100 = A + 94 – 97 = A 90 – 93 = A – 87 – 89 = B + 86 – 84 = B 80 – 83 = B – 77 – 79 = C + 74 – 76 = C 70 – 73 = C –
Accommodations for Students with Disabilities
If you have a documented disability (Learning, Psychological, Sensory, Physical, or Medical) you may be eligible to request reasonable accommodations. In general, it is the responsibility of the student to make their disability status and subsequent need for an accommodation known to the appropriate University official. Refer to these U-M resources.
If you think you need an accommodation for a disability, please let us know at your earliest convenience. Some aspects of this course, the assignments, the in-class activities, and the way we teach may be modified to facilitate your participation and progress. As soon as you make us aware of your needs, we can work with the Oﬃce of Services for Students with Disabilities (SSD) to help us determine appropriate accommodations. SSD (734.763.3000; ssd.umich.edu) typically recommends accommodations through a Verified Individualized Services and Accommodations (VISA) form. We will treat any information that you provide in as confidential a manner as possible.
Academic Misconduct
You are expected to participate in the class by contributing your own ideas and understanding to each assignment. All projects and homeworks submitted for grade must be your own original work created for a specific assignment. In instances of cheating or plagiarism (or other breaches of academic integrity) students receive 0 points for the assignment. In addition, we are required to report cases of academic misconduct to the office of Academic and Student Affairs. This office will determine any additional sanctions: academic probation, educational activities, or for very serious or repeated instances, dismissal from the program. Learn more here - www.lsa.umich.edu/academicintegrity/
Social Media
We will be happy to be your LinkedIn connection once the semester is over.

Schedule
Tentative Schedule

Date Lecture Topic

Student Presentation

Wee Class Objectives and k 01 Requirements. Using
cTools. 01/1 3 Introduction to
Graphic Design and Visual Communications. What is Graphic Design: Communicative Aspects. Who are Graphic Designers: Social Aspect.
Wee Principles and k 02 Elements of Design.
Color. Color and 01/2 Spectrum. Color 0 Models. Pantone
Matching System. Properties of Color.

Labs, Hands-on exercises

@Home

Lab 01: “Quick facts” slide

Select presentation topics
Set up and explore M+Box. Your M+Box links will be collected next class

Finalize and upload your lab to M+Box

Photoshop. Workspace basics. Tools Panel. Color and hue / saturation adjustments . Basic selection techniques. Understand ing layers. Transform tool. Move tool. Crop tool.
Lab 02:

Finalize the lab
Review: Photoshop workspace basics Adjusting crop, rotation, and canvas Tools panel Kuler extension.
Take Online Color Challenge!
Find 3 large images (1024 px wide or more) of your favorite fruit on white/light, dark, and color

Wee Principles and k 03 Elements of Design.
Color Wheel. Color 01/2 Harmonies and Color 7 Schemes (Palettes)

Layered layout

backgrounds. Save the link / credits for these images, too.

Color Symbolism student presentation 1
 Cultural and historical aspects
 Sociodemographic aspect
 Shift in the meaning
 What is the relevance of Color Symbolism to graphic design?

Group activity: Kuler.
Photoshop. Kuler Extension. Understand ing layers groups and layers effects. Colors and Gradients.
Lab 03: Color solutions for HTML invite.

Finalize lab
Review: Photoshop Clipping Masks Photoshop Gradients Photoshop Layers
Homework 01: Set of Web Banners

Wee Principles and k 04 Elements of Design.
Composition in
02/0 Graphic Design. 3 “Visual grammar:”
Design Principles.

Rule of Thirds – student presentation 2
 Definition of “The rule of thirds”
 Where to find the rule of thirds in action: (painting, photography, print, and web)?
 Are there any limitations?

Photoshop. Isolation techniques. Masks. Adjusting crop, rotation, and canvas. Edge refinements .
Lab 04: Isolations

Finalize lab
Review: Photoshop - Edge Refinement (isolation technique) Photoshop - hue / saturation adjustments Photoshop - Tools for selctions
Lab 05 preparation: watc h youtube videos by the SI faculty

 Can we break the rule?

(refer to today’s lab, CTools Dropbox). After that, find one abstract background and 3 “illustrative” backgrounds (large) that correspond with talk topics.

Wee Principles and k 05 Elements of Design.
Layout. Making and 02/1 Breaking the Grid. 0
Submit Homework 01

Grid systems and frameworks student presentation 3
 Pros & Cons; Context Matters
 Specific characteristic s of each system
 Keeping it cohesive
 The future of grid systems

Grids in Photoshop. Layout Compositio n
Lab 05: Web Slider layout (based on Lab 04)

Finalize the lab
Review: Photoshop Guides
Lab 06 preparation: Find 4 paintings by your favorite classic artist to correspond with the four seasons. Size - large, store images on you M+Box account

Homework 02: Conference Materials (SQ Club?)

Wee Principles and k 06 Elements of Design.
Gestalt Principles and 02/1 Graphic Design. The 7 power of an image.
Illustrations and Photography.
Submit Homework 02

Copyright law for visual materials – student presentation 4
 What is copyright (US copyright law)
 Basic Tenets

Introductio n to Illustrator workspace and tools. Masking techniques. Linking. Multiple artboards.

Finalize the lab
Review: Clipping Masks in Illustrator (illustrator_mask. mov) Illustrator Clipping Mask Illustrator Artboards

of Copyright Law  Copyright vs. Trademark vs. Patent  When do you need to ask permission?  How to protect your work?  Copyright infringement

Lab 06: Set of Bookmarks

Illustrator - Tools Panel Bleeds set up: bleeds_arrangeme nt Broadband.m4v

Wee Principles and k 07 Elements of Design.
Typography 02/2 Foundations.Understa 4 nding typographic
hierarchy.

Font resources overview – student presentation 5
 Licensing  Font
resources – where to buy / get  Web fonts (web-safe vs. font replacement)  Font formats and installation

Type in Photoshop and Illustrator. Outlines. Glyphs.
Lab 07: Typographi c specimen. Mauscript Grid layout

Review: Photoshop Filters (+ photoshop_filters. pdf) Illustrator Aligning and Distributing Illustrator Characters panel Review: Illustrator - Stroke Panel Play a kerning game!

Homework 03: Alphabet poster

03/03 - Spring Break. Watch Helvetica (a documentary about typography, graphic design, and global visual culture).

Wee Web and Graphic k 08 Design. “The good,
the bad, and the 03/1 ugly…” Design
0 Elements for Web
(logotypes,
background, buttons,
navigation, news

Contemporary trends in web design - student presentation 6
 General trends
 Design approaches

Illustrator. Pathfinder and pen tool. Working with styles. Path. Photoshop and

Review: Illustrator gradients panel and tool Illustrator paragraphs panel Illustrator - guides & grids

boxes, etc.)
Submit Homework 03

 Typography  Images and
Video  Navigation  Color  Layout  How to stay
current on the trends

Illustrator Libraries. Smart objects.
Lab 08: web layout

preferences
Homework 04: Website mockups

Wee Branding and k 09 Identity. Logo &
Logotype. What 03/1 makes a logo "great?" 7 Common Identity
Tools. Identity Style Guide.

History of logo design – student presentation 7
 General history of Logo Design
 Origin of the term
 Famous logo history: Case study

Vector graphics. Tracing vs. outlining.
Lab 09: Lo go. Vector Tracing

Review: Illustrator - Pen tool

Wee Branding for Web. k 10
03/2 4

Identity for digital environment – student presentation 8
 Graphic Identity, Identity Programs, and Brand Recognition
 Elements common to Print and DI (Image, Color, Type,

Custom Patterns. Rapport.

Finalize lab

Lab 10: UI pack

Layout)  Elements
exclusive to Digital Identity (Navigation, Input Fields, Buttons, Multimedia Players, etc.)

Wee Web and Graphic k 11 Design. “The
Templated Mind”
03/3
1 Submit Homework
04

Working with Commercial Web Templates - student presentation 9

Lab 11: Timelin e (15B ? 12A?)

Finalize lab
Homework 05: Infographic

 Resources that offer commercial web templates
 Options for buying templates
 Advantages vs. Disadvantage s
 Template or web designer?
 What makes a “good” template?
 Modifying templates

Wee Guest lecture or field k 12 trip - TBA
04/0 Submit Homework 7 05

Working with complex gradients and color

Review: Illustrator Tracing

libraries
Lab 12: Set of icons.

Wee Web and Graphic k 13 Design. Principles of
effective web design. 04/1 Mobile Applications 4 Design. Touch
Screens.

Challenges and advantages. Designing for mobile environment student presentation 10
 Limited screen space
 Mobile sites vs. Apps
 Responsive vs. Scalable Design and Layouts
 Skeuomorphi c vs. Flat
 Usability aspect

Illustrator and Photoshop. Multilayere d templates. Template files
Lab 13: Web app mockup

Sudy for the Quiz
Gather your homeworks - work on revisions.
Have all your homeworks and b labs ready for the lab 14: Save each work for web and devices (jpg or png). Keep all these files that you plan to include in portfolio gallery in one folder. Have this folder available and ready for the next class.

Wee Quiz

Portfolio Resources Adobe

Final project is

k 14

- student

Bridge

due in one week

Portfolio. Graphic

presentation 11

after the last day

04/2 Design: we are

Lab of this class

1 looking forward to it!

 Printed or

14: portfoli

digital

o website

Final Project

version

preparation

 Digital

portfolio

All Revisions are due

 Printed

portfolio

 Content

customizatio

n

