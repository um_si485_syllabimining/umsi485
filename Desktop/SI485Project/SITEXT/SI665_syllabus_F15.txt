1/14/2016

SI 665 Vital Information • Vital Information • Course Description • Learning Objectives • Weekly Outline •
• Finding Readings • Weekly Reading List • Grading • Expectations •

About this Course
Professor Karen Markey Class number: SI 665 Term: Fall semester 2015 Class name: Online Searching and Databases Credits: 3 Requirement: SI 665 is strongly recommended for SI students specializing in LIS Class meeting time: Thursdays, 8:40 am to 11:30 am Class meeting place: Room 2185, North Quad Purpose: Online Searching and Databases teaches you how to become an expert intermediary searcher who finds quality information online efficiently and effectively so that you can satisfy your own information needs and/or help library users satisfy theirs. Materials: Weekly handouts and other course materials are available on CTools. Consult Finding Readings to learn about finding the resources this class requires.
Testimonials
“This was the best, most practical class I took during my time at SI, for I have had the opportunity to apply what I learned to my real­world job. In addition, Dr. Markey not only made the class entertaining, but was an excellent teacher.”  (Marci Brandenburg, Informationist, Taubman Health Sciences Library, University of Michigan) “I use the concepts that I learned about in Professor Markey’s online searching class EVERY DAY in my job as an academic librarian. Understanding how databases are built and organized, and having the ability to search effectively, are skills that are more important than ever in today’s information­rich world (and certainly not just for librarians!).”  (Shevon Desai, Communication and Information Studies Librarian, University of Michigan Libraries) “Learning the ins and outs of how the databases work has been invaluable. Taking the class enabled me to hit the ground running when I got my first job. As an academic librarian, I'm expected to be able to find things the students and faculty cannot. Those skills came from SI 665. It’s what reference librarians DO all day.”  (Emily Thompson, Studio Librarian, University of Tennessee Chattanooga) “Everyone thinks that ‘information wants to be free.’ Well, guess what? It’s not! It’s hiding. Only when you understand how to search for it well, will you be able to find the information that you really want. And that’s what SI 665 is all about.” (Angie Oehrli, Learning Librarian, University of Michigan Libraries) What I learned in Online Searching has been critical to my success as a librarian. At Wooster, I often work with seniors who are all required to complete an undergraduate thesis project. I show them a variety of specialized research databases and work with them to divide their queries into facets, represent them in Venn diagrams, show them how to use Boolean, proximity operators, search sets, wildcards, just like we learned in 665. The students find these concepts CRITICAL to finding a wealth of scholarly resources for their thesis project and it validates their decision to consult a librarian. (Stephen X. Flynn, Emerging Technologies Librarian, The College of Wooster) 

http://ylime.people.si.umich.edu/si665/index.html

1/3

1/14/2016

SI 665 Vital Information

How to Contact Your Professor

Professor Markey’s office location: 3439 North Quad Her office hours: Thursdays, 11:35 am to 12:35 pm, or by appointment, or catch me after class on Fridays, same time and place Her office phone: 734­763­3581 Her home phone: 734­662­7576, leave message on answering machine SI main office phone: 734­764­9376, leave message with secretary Her email address: ylime AT umich.edu

About SI 665's Weekly Lab

Every week you will be assigned topics to search using retrieval systems and databases available through the MLibrary's Search Tools. These searches are mandatory, and we will take time in class to review search results one week after they are assigned. Four of the assigned searches will be graded.

About Optional Online Lab

In addition to weekly mandatory searches, I will assign you additional searches and/or search exercises from your Online Searching textbook to perform online in search systems and databases available through the MLibrary's Search Tools. These searches are optional but highly recommended. Suggested answers are in the text. If you bring your questions to class, we will do our best to discuss them.
Academic Integrity

At the University of Michigan and in professional settings generally, academic honesty and responsibility are fundamental to our scholarly and professional community. Students are responsible for maintaining high standards of conduct while engaged in course work, research, dissertation or thesis preparation, and other activities related to academics and their profession. Unless otherwise specified in an assignment, all submitted work must be your own original work.
Plagiarism is an extremely serious matter. All individual written submissions must be your own, original work, written entirely in your own words. You may incorporate excerpts from publications by other authors, but they must be clearly marked as quotations and properly attributed. You may obtain copy editing assistance, and you may discuss your ideas with others, but all substantive writing and ideas must be your own or else be explicitly attributed to another, using a citation sufficiently detailed for a reader to easily locate your source.
SI 665 will follow the same procedures for plagiarism that are in force in SI 500. All cases of plagiarism will be officially reported and dealt with according to the policies of the Rackham Graduate School. There will be no warnings, no second chances, no opportunity to rewrite or revise. All plagiarism cases will be immediately reported to SI's Dean of Academic Affairs. Consequences can range from failing the assignment (a grade of zero) or failing the course to expulsion from the University. For additional information about plagiarism, see the “Academic and Professional Integrity Policy Statement” on pages 8 to 11 of the SI Master’s Student Handbook, the Rackham pamphlet “Integrity in Scholarship” and “Understanding Plagiarism and Academic Integrity” published by the U­M Libraries. If you have any doubts about whether you are using the words or ideas of others appropriately, please discuss them with the professor.

http://ylime.people.si.umich.edu/si665/index.html

2/3

1/14/2016
Students With Disabilities

SI 665 Vital Information

If you think you need an accommodation for a disability, please let me know at your earliest convenience. Some aspects of this course, the assignments, the in­class activities, and the way we teach may be modified to facilitate your participation and progress. As soon as you make me aware of your needs, we can work with the Office of Services for Students with Disabilities (SSD) to help us determine appropriate accommodations. SSD (phone: 734­763­3000) typically recommends accommodations through a Verified Individualized Services and Accommodations (VISA) form. I will treat any information you provide as private and confidential.

In­class Use of Electronic Devices

You are encouraged to bring your personal computers to class and use them for taking notes, searching the University of Michigan Libraries’ Search Tools and its databases, and other activities connected to SI 665. You are discouraged from using them for doing activities that do not pertain to SI 665 ­­ this includes email, working on coursework for other courses, surfing the web, conversing via Google chat, doing Facebook and comparable social networking business, etc. Please use your judgment when distinguishing between coursework and non­ coursework.
You are discouraged from using cell phones during class. Students who are expecting an emergency call should inform the instructor before the start of the instructional period and sit in the back of the room to minimize disruptions. Please put your phone's call­receipt indicator in vibration mode or some other unobtrusive mode of indication. If you receive a call that you believe to be an emergency call, please relocate unobtrusively and quietly from the instructional area and take the call. If you must leave class, please notify the instructor as soon as reasonably possible with regard to the emergency situation. Again, please use your judgment regarding in­ class use of cell phones and all other electronic devices.
• Vital Information • Course Description • Learning Objectives • Weekly Outline • • Finding Readings • Weekly Reading List • Grading • Expectations •

http://ylime.people.si.umich.edu/si665/index.html

3/3

