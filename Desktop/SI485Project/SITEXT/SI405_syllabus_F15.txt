Course Syllabus

SI 405 001 FA 2015

SI 405 - Information Analysis Final Project Preparation Course

Fall 2015: Mondays 12-1pm NQ 2255

Instructor: Prof. Kevyn Collins-Thompson

Email:

kevynct@umich.edu

Office:

NQ 3344

Office hours: 4-5pm Tuesdays or by appointment

If you have questions about the course or your project, please feel free to come and talk with me during my office hours. You can also contact me via email: please put “405” in the subject line so I can be sure
to attend to it. (Please note that I may not be available on email over the weekend.)

Course Email: via Canvas
Note: Some syllabus details may be subject to change.
There is no midterm or final exam for this course. However, there are milestone assignments that must be completed satisfactorily to pass the course.

Course Description:
Information Analysis Final Project Preparation Course --- This preparation course lays the groundwork for the information Analytics final project course, and must be taken the semester immediately prior to that course. Students will learn about and engage in important preparation activities such as team formation, professional interaction with clients, assessing project needs, development of initial project ideas, and surveying related technology. On completion of the course, student teams will have created a coherent high-level project plan identified in collaboration with a client. This plan will serve as the basis for the actual final project development in the following semester.
Classroom policy:
Regular participation in this class is mandatory and required to pass the course. Students are asked to attend class on time and remain through the entire class.
Original Work:
Unless explicitly specified, all submitted work must be original work to you and/or your team, as appropriate It is a violation of the original work policy to copy code or other work from previous projects or

sources. In written project reports, any excerpts from the work of others must be clearly identified as a quotation, and a proper citation provided. Any violation of the School’s policy on Academic and Professional Integrity will result in severe penalties, which might range from failing an assignment, to failing a course, to being expelled from the program, at the discretion of the instructor and the Associate Dean for Academic Affairs. Accommodations for Students with Disabilities: If you think you need an accommodation for a disability, please let me know at your earliest convenience. Some aspects of this course, e.g. assignments, the in-class activities, and the way we teach may be modified to facilitate your participation and progress. As soon as you make me aware of your needs, we can work with the Office of Services for Students with Disabilities (SSD) to help us determine appropriate accommodations. SSD (734-763-3000; http://www.umich.edu/sswd/) typically recommends accommodations through a Verified Individualized Services and Accommodations (VISA) form. I will treat any information you provide as private and confidential.
There are no required texts for this course, but resources will be made available for specific topics as needed. Learning Objectives Students in this course will learn about and gain experience in:
 Forming effective teams to produce a coherent project plan in cooperation with a client.  Assessing project needs, applying effective project strategies and tools, and surveying related
technology.  Working and interacting professionally with project sponsors, faculty, and student peers.
Specific goals include: Competency:
 The ability to elicit and communicate client requirements.  Methods for proposing, vetting, and presenting an initial project plan.  The ability to clearly and professionally communicate with clients and peers.
Literacy:
 Strategies and tools for team projects: e.g. scheduling and tracking, collaboration tools.
Awareness:
 Project development methodologies

Class time focuses primarily on issues of team-building, project formation and planning and client interaction, but it may also include guest speakers.
Course Format
Assignments and Grading
The course will be graded as satisfactory/unsatisfactory. Obtaining a satisfactory grade is a prerequisite for continuing to the final project course, and regular attendance is essential. Students who miss 2 or more classes without a valid reason and permission from the instructor may obtain an ‘unsatisfactory’ grade.
Several milestone assignments will be assigned as part of developing a project plan. These assignments will be marked as satisfactory/unsatisfactory. In the case of an unsatisfactory grade on a milestone assignment, there will be an opportunity to revise the assignment and have it re-evaluated. To earn a satisfactory grade in the course, students must have satisfactory completion of all milestone assignments and regular attendance in class.
Example milestone assignments are shown in bold in the course content list below.
The assignments are team-based and typically will involve a short (1-2 page) progress report or talk (4-5 slides). Some milestones involve team presentations. All students in a team will receive the same grade, unless the instructor has reason to assess otherwise.
Course Content and Structure
The instructor will serve as guide and facilitator of the initial planning process and the interaction with clients. Our weekly meetings will cover specific topics some weeks, and most often will be an opportunity for teams to report and discuss progress toward project milestones. Any given class may use a combination of lecture, discussion, team presentations, or remote conferencing with clients depending on topics covered that week. Below is a tentative weekly schedule of class topics and activities, with example Fall 2015 dates marked.

Tentative schedule : some topics and due dates may be adjusted.

Date Topic

Due

Week 1 Mon 9/14

Overview of goals and schedule: course and final project.

Week 2 Mon 9/21

Basics of professional interaction with clients and peers. (Guest: Kelly Kowatch, UMSI

All students: upload current resume.

OCD)

Student-initiated

projects must submit

sponsor summary.

Week 3 Mon 9/28

Client meetings: preparing for the kick-off meeting, identifying requirements, team roles.

Week 4 Mon 10/5 Overview of client projects.

Students state project rankings and team preferences.

Teams and projects assigned. Week 5 Mon 10/12 Conducting a project technology review of
existing solutions/systems/resources.

Week 6 Mon 10/19 Study break

Week 7

Mon 10/26

Project/time planning (Guest: Kelly Kowatch, UMSI OCD)

Week 8

Mon 11/2

Team building: effective development of roles, teamwork, communication. (Guest: OSCR rep)

Week 9 Mon 11/9 Team reports: Discussion of team progress.

Presenting your project and talking with Week 10 Mon 11/16 potential employers (Joanna Kroll, UMSI
OCD)

Milestone 1: Initial team report on project specification based on client meeting.

Week 11 Mon 11/23 Team updates and discussion.

Milestone 2: Week 12 Mon 11/30 Software scheduling: strategies and methods. Technology/existing
resources review due

Week 13 Mon 12/7

Software rapid development and prototyping. Testing. Measuring and predicting engineering problems and software progress.

Week 14 Mon 12/14 Final project plan reports and presentations

Milestone 3: Final Team project summary plan, slide deck.

Prerequisites

To enroll in this project preparation course, students must have either completed, or be co-enrolled in with the expectation of completing, any prerequisites needed to start the Information Analysis final project the following semester.
Project teams
An early goal of the final project preparation course is to converge on project teams. The expected team size is 4 people, but depending on the project and class circumstances this be vary slightly. In general, project teams will be created to accommodate student preferences whenever possible. Teams will be assigned to client projects based on a matching mechanism that takes into account both client preferences for teams, and team preferences for clients (as well as adjustments as needed for other factors, like team size).

