Winter Semester 2015, Tuesday Evenings 6:00-8:30 PM Room 1255 North Quad

Evaluation Methods for Health Informatics – HMP 648/SI525

Instructor: Steven Schwartz, Ph.D

Phone:

313-300-1275

Email:

smschw@med.umich.edu or

smshcconsult@gmail.com

Office:

120 E. Liberty Street, Suite 250

Office Hours: Tuesday 12-2 pm, 4-5 pm.

Grader:

Andrew Miller, MPH amillerm@umich.edu

Course Materials:
Primary Text: Friedman & Wyatt. Evaluation Methods in Biomedical Informatics (2nd Ed.). NY: Springer-Verlag, 2006.
Must be the 2nd Edition!
Textbook available online for FREE to U of M students at:
http://mirlyn.lib.umich.edu/Record/005100203
Supplementary Text (not required):
Hayes, Barlow & Nelson-Gray. The Scientist Practitioner: Research and Accountability in the Age of Managed Care (2nd Ed.) NY: Allyn & Bacon. (1999).
McDavid & Hawthorn. Program Evaluation & Performance Measurement. Three Oaks, CA: Sage Press (2006).
CTools system will host the course website. Unless otherwise noted, all other course materials will be available on CTools. All assignments will be turned-in using either DropBox or the Assignments Tab in CTools
Changes in schedule or class administration will be posted on CTools and sent via email to all students.

Course Description:
This course examines health informatics as an empirical science with an emphasis on the diversity with which it is applied in dynamic real world settings. As such the focus of the course will be on the application of measurement, methods, management, technology and analysis of information to answer questions and solve problems applied to health care, population health, personal health, healthcare delivery and economics.
The application of critical thinking skills will be developed in the context of intended outcomes, how processes and resources can be improved, how one approach preforms relative to another, where processes and services are prone to error and other questions that are critical to the industry.
Course Objectives/Competencies:
After completing this course a competent student should be able to:
 Apply critical thinking skills to answer meaningful questions and help solve real problems in applied healthcare settings.
 Using critical thinking, the proficient student should be able to create a plan (including but not limited to measurement, metrics, methodology, data management/analysis plan, and project management plan for common scenarios in applied healthcare settings. o Identify appropriate sources and gather information effectively and efficiently o Appraise the literature and other source information and data critically o Understand and apply fundamentals of measurement, research design, surveillance and monitoring systems data, program evaluation. o Understand the analysis, interpretation and contextualization of data findings. o Listen/Learn/Interact. “Seek 1st to understand, then to be understood”, Stephen Covey. Development of good listening skills, know what key questions to ask, deftly interpreting information, making recommendations and formulating the defending argument through an empirical lens. o Organizational Awareness. This includes the formal institutional and regulatory structures that are relevant to the work but also goes beyond to include consideration of the other less formal cultural or contextual factors that can influence the work.
 The proficient student will begin to develop skills for interpretation and application of findings (recommendations) including the presentation of findings.
Course Format:

The course will be held in 2245 North Quad on Tuesday evening from 5:30-8:30 PM. We will start promptly at 5:40 PM and end promptly at 8:30 PM. Each student will create a name tent at the first session and will use these throughout the semester (as a kindness to an old man ).
The class will be divided into 3 roughly equal sections with 10 minute breaks in between with the following structure.
Starting in Week 2,
Section 1 - 25 minutes will be a review of the material in the weekly chapter. 25 minutes will set up an issue, question, and problem from the self-test or food for thought.
10 min break
Section 2 – Lecture on one or more skill based special topics relative to the issue presented in Section 1 (50 mins).
10 min break
Section 3 – Interactive discussion and lecture that around the relevant problem background and application of critical thinking and related methods to the issue, question or problem from the self-test or food for thought resulting in a set of class vetted action steps.
Readings:
Reading assignments for each session will include textbook chapters and papers from the published literature. The textbook chapter associated with each session is indicated on the course schedule.
TextBook Self-Tests & Food for Thought questions:
Both the Self-Tests and Food for Thought sections of the textbook are specifically designed to build your critical thinking skills which is the fundamental skill being developed in this class.
The Self-Tests associated with each chapter should be reviewed and/or completed as indicated in the class schedule. Some portions of these self-tests will be used as part of the discussion.
Food for Thought questions will also be used in the discussion and interactive portions of the class and the wise student will have at least read and considered the Food for Thought questions as well.
Papers assigned for each session will be posted in CTools no later than 1 week in advance and should be read before class. Students may volunteer to present one of the assigned papers as a “10 minute sweat” in which they will briefly describe the paper’s intent, findings (including validity), and implications and answer questions posed of them by the instructor or class at large. Presenting a 10 minute sweat can be done in exchange for omitting one of the

homework assignments 2-4 (assignments 1 and 5 cannot be exchanged). Depending on the topic at hand the class may also be broken into small discussion groups.
Assignments & Grading:
 There are 5 Homework Assignments. These assignments are a portion of the participation grade. Assignment and due dates are indicted in the course schedule and appendices in this syllabus. These assignments are graded pass/fail. Fail grades will require the student to meet with the Instructor to review the feedback and allow for 1 re-submission of the assignment but only if the assignment is turned in on the due date. Second submission failure will remain as fails. Missing assignments will be counted as fails and second submissions will not be considered.
 There will be two “design” exercises as part of the course. Each design exercise will require students to select a problem or question from one of several to be provided, define the problem (including a brief and highly selective review of the literature) and design for an empirical study. Each design exercise will be completed in small groups of no less than 2 and no more than 4. Each group will produce a design protocol of no more than 10 pages not including references or tables/figures and a PowerPoint presentation both of which layout the problem, the design and how the data and analysis plan will answer the question or inform a solution to the problem. Groups will share the grades their group PowerPoint presentations and papers receive.
 There is one (1) take home mastery test on objectivist methods available [date]. This is a one hour test completed on the honor system at a time and place of the student’s choosing. The mastery test must be complete on or before [date]. The test will be sent to the student upon request of the grader (Andrew Miller) and must be returned within 24 hours of receiving the test via email. The test will be graded A, A-, B+, B, no credit. Students receiving no credit on the first attempt may request a second test attempt up until [date]. The second test will cover the same material but using a different question set. The second mastery test shall receive grades no higher than a B+.
Final grades will be determined using the following weighting:
 Grade for Design Exercise I (30%)  Grade for Design Exercise II (30%)  Grade for Mastery Test (20%)  Participation and preparation including the Homework assignments (20%)
Note: 20% of your grade is participation and preparation because the work will require you to be able to assess and interpret information, formulate ideas and present and defend (or modify ideas in the face of strong valid arguments in public settings with other very smart people (and some not so smart). Developing this skill will advance your career and make the learning experience in this class far more enjoyable.

Participation and preparation will be based on level of completion and presentation in class discussion of ideas related to the self-tests, food for thought questions, or other prompts that are part of the interactive portions of the class. Students will be given 1 incomplete (forgiven) with regards to completion of these homework assignments and all assignments should be delivered to the grader (Andrew Miller) no later than the hour before class. Students with 2 or more incompletes will receive a grade no higher than a B.
Academic Integrity:
Unless otherwise specified in an assignment, all submitted work must be your own original work. Any other work cited, but does so using one of the several accepted formats (e.g. APA). Violations of the School’s policy on Academic and Professional Integrity (stated in the Master’s and Doctoral Student Handbooks) will result in severe penalties, which could range from failing an assignment, failure of the course, or removal from the program at the discretion of the instructor and the Associate Dean of Academic Affairs.
Your integrity is a precious thing…..protect it and nurture it wisely.
Accommodations for Students with Disabilities:
If you think you need or are already qualified for a disability, please let the instructor know at your earliest convenience. Depending on need, all aspects of the course can be considered for modification in order to accommodate the student’s needs and optimize the learning experience. Once we are aware we can work with the Office of Services for Students with Disabilities (SSD) to help use determine the needs and accommodations. SSD (734-763-3000; http://www.umich.edu/sswd/) typically recommends accommodations through a Verified Individualized Services and Accommodations (VISA) form. All information provided will be treated as private and confidential.
Email:
Email is a perfectly appropriate method of communication to and from all students in the course. Be sure to check that the email sent from the course CTools account is not inadvertently filtered into your junk mail or otherwise deleted. Students should check for course emails daily Monday thru Friday during the semester.
Your emails to me are important but keep in mind I have many email inputs during the course of the day. Unless otherwise communicated back to you via other means, your email will be acknowledged within 24 hours if received between the hours of 8am on Monday and noon Friday. If by chance 24 hours elapse and you have not received an acknowledgement from us, we apologize in advance and please ask again and/or approach me during a class break.

Semester Schedule Week #1

Date Jan 13th

#2 Jan 20th #3 Jan 27th #4 Feb 3rd

#5 Feb 10th #6 Feb 17th #7 Feb 24th
#8 Mar 3rd

Topic (Textbook Chapter)
Envisioning and Defining Empirical Studies (Have Read and Studied Chapter 1) The Panorama of Methods (Have Read and Studied Chapter 2) What Can Be Studied and Learned (Have Read and Studied Chapter 3) Introduction to Objectivist Studies (Have Read and Studied Chapter 4)
Basics of Measurement (Read Chapter 5) Cancelled Measurement Tools and Techniques (Read Chapter 6) Introduction to Clinical Trial Designs

Assignments/Due Dates
HW Task Assignment (HWTA) #1
HWTA #1 Due HWTA #2 (Answer the Self-Test or using one of the Self-Test scenarios discuss how and what you would balance with regards to the natural tension between internal and external validity) Design Exercise I distributed
HWTA #2 Due HWTA #3 (Measurement) Mastery Test Available

Off for Spring Break

#9

Mar 10th

Objectivist Study

HWTA #3 Due

Designs and Analysis I HTWA #4

(Read Chapter 7)

#10

Mar 17th

Objectivist Study

HTWA #4 Due

Designs and Analysis Design Exercise I Due

II (have Read and

HTWA #5 (Re-

Studied Chapter 8) evaluation of

Assignment #1)

#11

Mar 24th

Study Costs and

Design Exercise II

Benefits (have Read & Distributed

Studied Chapter 11)

#12

Mar 31st

Intro to Subjectivist

Approaches (have

Read and Studied

Chapter 9)

#13

April 7th

Subjectivist Data,

HTWA #5 Due

Collection & Analysis

(have Read and

Studied Chapter 10)

#14

April 14th

Studying

Organizations and

Culture/Subculture

(Assigned Reading on

Change Models)

#15

April 21st

Communications & Design Exercise II

Reporting (have Read Due

and Studied Chapter

12)

April 24th

Mastery Test Due

Homework Assignment #1
Due February 3rd
This assignment is based on the following scenario.
The Chief Medical Officer and Chief Nursing Officer have come to you as the head of Medical Informatics regarding a problem of falls in the hospital. As a critical HEDIS measure, Hospital leadership is very concerned about the fall rate. They are asking you to develop a prospective evaluation of factors that are contributing to the elevated fall rate. In no more than 5 pages create an evaluation plan that covers the following:
 Briefly describes HEDIS metric intent.  Briefly review the literature relevant to hospital falls (no more than 2 paragraphs)  Provide a plan and justification for what data elements (existing or created) would be
used to answer the question (based largely on your literature review).  Provide a plan for how you would analyze the data in order to inform the question.
Keep in mind that you cannot measure everything, budgets matter, and your resources are not infinite.
Suggested Outline for Evaluation Plan:
1) Specific Aims: List the primary objective(s) of the proposal and what the proposal is intended to accomplish. This
should minimally include the study hypothesis or research questions, any novel methods or technology, as well as a brief statement of clinical/scientific significance (1/2 page maximum)
2) Background and Significance: Briefly but critically sketch the relevant background literature that provides the
rationale for the study including gaps or methodological flaws that the proposal is intended to fill. Restate in greater detail the study hypotheses or research questions and the clinical/scientific significance in the Specific Aims (3 paragraphs max.).
3) Research Design and Methods: Describe the subject/patient/participant population to be studied, the
research design and procedures, and data management, analyses and interpretation plan as they relate to the Specific Aims 3 pages max.).
C1. Subjects/Patients/Participants: This section should include who will be recruited into the study in terms of
the total sample size (statistical power), basic populations, demographics (age, gender, etc.) and appropriate inclusion/exclusion criteria.
C2. Study Design: This section describes the basic type of study (e.g. case report, clinical trial, chart review) and
related study design (e.g. between groups, repeated measures, mixed model, etc.) to be used. Include the timeline for the study and the assessment points within that timeline (where appropriate). Note that the design must be appropriate to the research question(s) defined in the Specific Aims.
C3. Methods and Procedures: This section describes what is required or will be done to the participants and
should clearly describe a set of procedures that allow you to test your hypotheses. This should include a definition of all outcome measures, recruitment procedures, data collection techniques, etc.

C4. Data Management and Analyses: Discuss how data will be recorded and manipulated, including the
specific statistical procedures to be used. This section should directly be related to the outcome measures defined in the Methods and Procedures section.
4) Risk/Benefit Ratio: Discuss the reasonable potential risks that the study might impose on the participants and
any corrective action to be taken to minimize risks. Relate these risks to potential benefits that might accrue directly to the participant as a result of participation (1/2 page max.).
Homework Assignment #2
Due February 10th
Answer the Self-Test or using one of the Self-Test scenarios discuss how and what you would balance with regards to the natural tension between internal and external validity [no more than 3 pages].
Homework Assignment #3
Due February 24th
A new information resource “Diatel” has been developed to assist diabetic patients to find information to help them self-manage disease. The developers have created a measurement instrument in the form of a brief questionnaire, which they propose to administer to samples of patients who use the resource. The instrument is designed to ascertain the patients’ perceptions of the “value” of the information provided to them from Diatel. The instrument consists of 8 items each addressing the “value” of the information in different ways. Patients respond in a YES/NO format.
1) Provide 3 example questions that might be included in the instrument.
2) Since the instrument is new, appropriate studies would be needed to ascertain the psychometric properties of the instrument (reliability, validity, normative data, etc.). Briefly describe the study or studies needed in order to determine these properties. 1) Cover 1 test of reliability and justify your choice of reliability 2) Cover 1 test of validity and justify your choice of validity (face validity does not count).
3) Discuss how the instrument could be used to promote the product assuming strong psychometric properties.

Homework Assignment #4 Due March 10th
Evaluate the Following Questions:
“According to online physician weblogs what are practicing physicians’ reactions to the Federal Governments “Meaningful Use” incentive program?
Instructions:
1) Identify and select 3 different physician weblogs with blogs that express opinions on meaningful use.
2) Collect approximately 1 page of data from each blog.
Your blog data may include quotes, your own impressions, key concepts that you identify and wish to remember, the opinions of physicians paraphrased and rewritten in your own words. Consider this a naturalistic or observational study (akin to Goodall) and these will be considered the notes from your observations. These notes should have a semblance of organization but the format can be your own.
Homework Assignment #5 Due April 7thst
Re-evaluate the proposed evaluation plan you turned in as HTWA #1. Review your response relative to what you have learned since you completed that assignment and answer the following questions:
1) Would you change your approach to answering the question? If Yes, why? If No, why not?
2) What changes would you make and what information is the basis for that change? 3) Provide a revised protocol based on any changes you make (with track changes on). 4) Create a presentation ready PowerPoint of our final recommendations.

